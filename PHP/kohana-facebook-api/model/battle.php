<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Battle extends ORM {
	protected $_belongs_to = array(
		'user' => array('model' => 'user'), 
		'match' => array('model' => 'match')
	);
}
?>