<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Match extends ORM {
	protected $_has_many = array(
		'battles' => array('model' => 'battle')
	);
}
?>