<h1 class="logo"><img src="<?php echo url::base(). 'public/frontend/global/img/sfxt_logo_' . ( $chosen == 'us' ? 'r' : 'tm' ) . '.png' ?>" alt="Street Fighter X Tekken" /></h1>
<header class="top">
	<div class="social cta">
		<h4 class="title"><?php echo $values['copy_share'] ?></h4>
		<ul class="links">
			<li class="fb"><a href="//www.facebook.com/streetfighter" title="Facebook" target="_blank">Facebook</a></li>
			<li class="tw"><a href="//www.twitter.com/streetfighter" title="Twitter" target="_blank">Twitter</a></li>
			<li class="gplus"><a href="//plus.google.com/u/0/b/110276954776959626641/110276954776959626641/posts" title="Google+" target="_blank">Google+</a></li>
		</ul>
	</div>
</header>

<div id="boards">
	<ul id="tab" class="nav">
	<?php foreach($matches as $match): ?>
		<li class="<?php echo strtolower($match->title) . ($match->id < 2 ? ' active':'')?>"><a data-toggle="tab" href="#<?php echo strtolower($match->title) ?>"><?php echo $values['title_match_' . $match->id] ?></a></li>
	<?php endforeach; ?>
	</ul>
	<div class="tab-content">
	<?php foreach($matches as $match): ?>
		<div id="<?php echo strtolower($match->title) ?>" class="tab-pane fade<?php echo ($match->id < 2 ? ' active in':'') ?>">
			<ul class="positions">
			<?php if(count($leaderboards[strtolower($match->title)])>0): foreach($leaderboards[strtolower($match->title)] as $user): ?>
				<li class="position">
					<div class="rank"><?php echo $user['rank'] ?></div>
					<div class="user">
						<div class="img"><img src="https://graph.facebook.com/<?php echo $user['id'] ?>/picture" alt="" /></div>
					</div>
					<div class="name"><?php echo (isset($user['name']) && $user['name'] != '' ? $user['name'] : "<fb:name uid=\"". $user['id'] ."\" capitalize=\"true\" />") ?></div>
					<div class="points"><?php echo $user['pts'] ?> pts</div>
				</li>
			<?php endforeach; else: ?>
				<li class="nodata"><a href="<?php echo $base_url . $chosen . '/match' ?>">Be the first <?php echo $values['title_match_' . $match->id] ?>.</a></li>
			<?php endif; ?>
			</ul>
		</div>
	<?php endforeach; ?>
	</div>
</div>