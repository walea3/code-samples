<div id="scoresContainer">
	<div class="userInfo">
		<div class="user">
			<div class="img"><img src="https://graph.facebook.com/<?php echo $scores['id'] ?>/picture?type=large" alt="" /></div>
			<div class="pts"><?php echo $scores['points'] ?> pts</div>
		</div>
		<h3 class="userName"><?php echo (isset($scores['name']) && $scores['name'] != '' ? $scores['name'] : "<fb:name uid=\"". $scores['id'] ."\" capitalize=\"true\" useyou=\"false\" />") ?></h3>
	</div>
	<div class="social cta">
		<h5 class="title"><?php echo $values['copy_share'] ?></h5>
		<ul class="links">
			<li class="fb"><a href="//www.facebook.com/streetfighter" title="Facebook" target="_blank">Facebook</a></li>
			<li class="tw"><a href="//www.twitter.com/streetfighter" title="Twitter" target="_blank">Twitter</a></li>
			<li class="gplus"><a href="//plus.google.com/u/0/b/110276954776959626641/110276954776959626641/posts" title="Google+" target="_blank">Google+</a></li>
		</ul>
	</div>
	<div id="worldRank">
		<h1 class="heading"><?php echo $values['text_world'] ?> : <?php echo $scores['rank'] ?></h1>
		<div class="bestTitle">
			<h4 class="title"><?php echo $values['text_best'] ?></h4>
			<h3 class="match"><?php 
				echo ($scores['topMatch']['win']>0 ? $values['title_match_' . $scores['topMatch']['match_id']] : 'No Wins');
			?></h3>
		</div>
	</div>
	
	<div id="matchScores">
	<?php foreach($matches as $match): ?>
		<div id="<?php echo strtolower($match->title) ?>" class="matchContainer">
			<div class="containerWrapper">
			<div class="matchBox">
				<img src="<?php echo $base_url . 'public/frontend/global/img/score_' . strtolower($match->title) . '.jpg' ?>" alt="<?php echo $values['title_match_' . $match->id] ?>" />
			</div>
			<h2 class="matchTitle"><?php echo $values['title_match_' . $match->id] ?></h2>
			<div class="stats">
				<h4 class="wins"><?php echo $values['text_wins'] . ' : ' . (isset($scores['matches'][$match->id]) ? (int)$scores['matches'][$match->id]['win'] : 0) ?></h4>
				<h4 class="losses"><?php echo $values['text_losses'] . ' : ' . (isset($scores['matches'][$match->id]) ? (int)$scores['matches'][$match->id]['loss'] : 0) ?></h4>
			</div>
			</div>
		</div>
	<?php endforeach; ?>
	</div>
</div>
