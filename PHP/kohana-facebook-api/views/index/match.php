<header class="top">
	<h1 class="heading"></h1>
	<div id="progressBar">
		<ul id="steps">
			<li class="ring s1"><span class="inner">1</span></li>
			<li class="ring s2"><span class="inner">2</span></li>
			<li class="ring s3"><span class="inner">3</span></li>
		</ul>
		<div class="bar"><span class="cap"></span></div>
	</div>
</header>

<div class="boxContainer">
	<a href="#" class="modalContent btn help ring"><span class="inner">?</span></a>
	<a href="#step" data-step-dir="back" class="btn back ring"><span class="inner"> <img src="<?php echo $base_url . 'public/frontend/global/img/larr.png' ?>" alt="" /> </span></a>
	<div class="containerInner">
		<div id="TDFriendSelector" class="hide" data-step="s1">
			<div class="TDFriendSelector_dialog">
				<div class="TDFriendSelector_form">
					<div class="TDFriendSelector_content">
						<div class="TDFriendSelector_searchContainer TDFriendSelector_clearfix">
							<input type="text" placeholder="<?php echo $values['text_search'] ?>" id="TDFriendSelector_searchField" />
						</div>
						<div class="TDFriendSelector_friendsContainer"></div>
					</div>
					<div class="TDFriendSelector_footer TDFriendSelector_clearfix">
					<div class="flex-direction-nav">
						<a href="#" id="TDFriendSelector_pagePrev" class="TDFriendSelector_disabled flex-prev"><span><?php echo $values['text_prev'] ?></span></a>
						<a href="#" id="TDFriendSelector_pageNext" class="flex-next"><span><?php echo $values['text_next'] ?></span></a>
					</div>
						<div class="TDFriendSelector_pageNumberContainer">
							Page <span id="TDFriendSelector_pageNumber">1</span> / <span id="TDFriendSelector_pageNumberTotal">1</span>
						</div>
						<a href="#" id="TDFriendSelector_buttonOK" class="btn orange"><span class="btn-inner"><?php echo $values['text_next'] ?></span></a>
					</div>
				</div>
			</div>
		</div>
		<div id="matches" data-step="s2">
		<?php foreach($matches as $match): ?>
			<div class="matchBox">
				<a href="#fight" data-match-id="<?php echo $match->id ?>">
					<img src="<?php echo $base_url . 'public/frontend/global/img/match_' . strtolower($match->title) . '.jpg' ?>" alt="<?php echo $values['title_match_' . $match->id] ?>" />
					<span class="title">
						<span class="name"><?php echo $values['title_match_' . $match->id] ?></span>
						<span class="desc"><?php echo $values['desc_match_' . $match->id] ?></span>
					</span>
					<span class="completed">Match Completed</span>
				</a>
			</div>
		<?php endforeach; ?>
		</div>
		<div class="generating" data-step="s3">
			<?php echo $values['text_generating'] ?>
			<img src="<?php echo $base_url . 'public/frontend/global/img/gen.gif'; ?>" alt="" />
		</div>
	</div>
</div>

<div id="help" class="visuallyhidden">
	<h3><?php echo $values['text_information'] ?></h3>
	<div class="contentWrap">
		<div class="instr instr_1">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_1.jpg' ?>" alt="" /></div>
			<?php echo $values['copy_instr_1'] ?></div>
		<div class="instr instr_2">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_2.jpg' ?>" alt="" /></div><?php echo $values['copy_instr_2'] ?></div>
		<div class="instr instr_3">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_3.jpg' ?>" alt="" /></div><?php echo $values['copy_instr_3'] ?></div>
		<div class="instr instr_4">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_4.jpg' ?>" alt="" /></div><?php echo $values['copy_instr_4'] ?></div>
		<div class="instr instr_5">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_5.jpg' ?>" alt="" /></div><?php echo $values['copy_instr_5'] ?></div>
		<div class="instr instr_6">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_6.jpg' ?>" alt="" /></div><?php echo $values['copy_instr_6'] ?></div>
		<div class="instr instr_7">
			<div class="img"><img src="<?php echo url::base(). 'public/frontend/global/img/instr_7.jpg' ?>" alt="" /></div><?php echo $values['copy_instr_7'] ?></div>
	</div>
</div>
<?php if ($currStep && $selectedFriend): ?>
<script type="text/javascript">
	var currentStep = <?php echo $currStep ?>,
		selectedFreind =  <?php echo $selectedFriend ?>;
</script>
<?php endif; ?>