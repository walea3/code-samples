<header class="top">
	<h1 class="heading"><?php echo $values['copy_heading_3'] ?> <?php echo $values['title_match_' . $result->match_id] ?></h1>
	<div id="progressBar">
		<ul id="steps">
			<li class="ring active s1"><span class="inner">1</span></li>
			<li class="ring active s2"><span class="inner">2</span></li>
			<li class="ring active s3"><span class="inner">3</span></li>
		</ul>
		<div class="bar"><span class="cap"></span></div>
	</div>
</header>

<div id="lifeBar">
	<div class="barInner"></div>
		<div class="sf"><div class="bar" data-progress="<?php echo $result->user_score ?>"><span class="ov"></span></div><p class="perc"><?php echo $result->user_score ?>%</p>
		<p class="name"><?php echo "<fb:name uid=\"". $result->user_id ."\" capitalize=\"true\"></fb:name>" ?></p></div>
		<div class="tk"><div class="bar" data-progress="<?php echo $result->friend_score ?>"><span class="ov"></span></div><p class="perc"><?php echo $result->friend_score ?>%</p>
		<p class="name"><?php echo "<fb:name uid=\"". $result->friend_id ."\" capitalize=\"true\"></fb:name>" ?></p></div>
</div>
<div class="social cta">
	<h4 class="title"><?php echo $values['copy_share'] ?></h4>
	<ul class="links">
		<li class="fb"><a href="//www.facebook.com/streetfighter" title="Facebook" target="_blank">Facebook</a></li>
		<li class="tw"><a href="//www.twitter.com/streetfighter" title="Twitter" target="_blank">Twitter</a></li>
		<li class="gplus"><a href="//plus.google.com/u/0/b/110276954776959626641/110276954776959626641/posts" title="Google+" target="_blank">Google+</a></li>
	</ul>
</div>
<div id="versus">
	<div class="user <?php echo $result->winner == $result->user_id ? 'winner' : 'loser' ?> large">
		<div class="img"><img src="https://graph.facebook.com/<?php echo $result->user_id ?>/picture?type=large" alt="" /></div>
		<div class="pts"><?php echo $result->winner == $result->user_id ? $result->points : '0' ?> pts</div>
		<img src="<?php echo $base_url . 'public/frontend/global/img/x.png' ?>" alt="" class="cross" />
	</div>
	<div class="user <?php echo $result->winner == $result->friend_id ? 'winner' : 'loser' ?> large">
		<div class="img"><img src="https://graph.facebook.com/<?php echo $result->friend_id ?>/picture?type=large" alt="" /></div>
		<div class="pts"><?php echo $result->winner == $result->friend_id ? $result->points : '0' ?> pts</div>
		<img src="<?php echo $base_url . 'public/frontend/global/img/x.png' ?>" alt="" class="cross" />
	</div>
	<div class="winnerCaption">
		<?php if ($result->winner): ?>
			<?php echo "<fb:name uid=\"". $result->winner ."\" capitalize=\"true\" useyou=\"false\"></fb:name>" ?>&nbsp;<?php echo $values['winner_match_' . $result->match_id] ?>
		<?php else: ?>
			Draw!
		<?php endif; ?>
	</div>
</div>
<footer class="cta">
	<div class="line"></div>
	<a href="<?php echo $base_url . $chosen . '/match/2' ?>" class="btn orange rematch lrg"><span class="btn-inner"><img src="<?php echo $base_url . 'public/frontend/global/img/ico_reload.png'; ?>" alt="" /><br/><?php echo $values['text_rematch'] ?></span></a>
	<a href="<?php echo $base_url . $chosen . '/match' ?>" class="btn orange lrg"><span class="btn-inner"><?php echo $values['text_another'] ?></span></a>
</footer>
<div id="preloadImg" class="visuallyhidden hide">
	<?php foreach($matches as $match): ?><img src="<?php echo $base_url . 'public/frontend/global/img/match_' . strtolower($match->title) . '.jpg' ?>" alt="" />
	<?php endforeach; ?>
</div>
<script type="text/javascript">
	var matchResult = "<?php echo !$result->winner ? 'draw' : ($result->winner == $result->user_id ? 'win' : 'lose') ?>",
		matchTitle = "<?php echo strtolower($result->match_title) ?>",
		matchName = "<?php echo $values['title_match_' . $result->match_id] ?>";
</script>