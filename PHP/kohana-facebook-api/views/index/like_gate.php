<h1 class="logo"><img src="<?php echo url::base(). 'public/frontend/global/img/sfxt_logo_' . ( $chosen == 'us' ? 'r' : 'tm' ) . '.png' ?>" alt="Street Fighter X Tekken" /></h1>

<h1 class="heading"><?php echo $values['copy_like'] ?></h1>

<div class="like cta">
	<div class="btn fb"><div class="btn-inner">
		<fb:like id="fbLikeBtn" href="<?php echo Kohana::config('facebook.fanpage_url') ?>" data-layout="button_count" send="false" width="48" show_faces="false"></fb:like>
	</div></div>
</div>