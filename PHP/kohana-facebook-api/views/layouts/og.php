<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-GB"
      xmlns:fb="https://www.facebook.com/2008/fbml"> 
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# <?php echo Kohana::config('facebook.namespace') ?>: 
                  http://ogp.me/ns/fb/<?php echo Kohana::config('facebook.namespace') ?>#">
  <title>Street Fighter X Tekken - Friend Fighter</title>
  <meta property="fb:app_id" content="<?php echo Kohana::config('facebook.app_id') ?>" /> 
  <meta property="og:type" content="<?php echo Kohana::config('facebook.namespace') ?>:<?php echo $object ?>" /> 
  <meta property="og:url" content="<?php echo $ogUrl ?>" />
  <meta property="og:title" content="<?php echo $ogTitle ?>" /> 
  <meta property="og:description" content="<?php echo $ogDesc ?>" /> 
  <meta property="og:image" content="<?php echo $ogImage ?>" /> 
  <?php if ($type) { ?>
  	<meta property="og:determiner" content="a" /> 
  	<meta property="<?php echo Kohana::config('facebook.namespace') ?>:battle" content="<?php echo $type ?>" /> 
  <?php } ?>
</head>
<body>
<div id="fb-root"></div>
  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '<?php echo Kohana::config('facebook.app_id') ?>', // App ID
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
      });
    };

    // Load the SDK Asynchronously
    (function(d){
      var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
      js = d.createElement('script'); js.id = id; js.async = true;
      js.src = "//connect.facebook.net/en_GB/all.js";
      d.getElementsByTagName('head')[0].appendChild(js);
    }(document));
    
	window.location.href="<?php echo Kohana::config('facebook.fanpage_url') ?>app_<?php echo Kohana::config('facebook.app_id') ?>";
  </script>
<fb:activity actions="<?php echo Kohana::config('facebook.namespace') ?>:<?php echo $object ?>"></fb:activity>
</body>
</html>