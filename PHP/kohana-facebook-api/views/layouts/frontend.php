<?php 
	$base_url = url::base(); 
	$site_url = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https://" : "http://") . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']) . $base_url;
	View::set_global('base_url', $base_url);
	View::set_global('site_url', $site_url);
?>
<!DOCTYPE html>
<!--[if IEMobile 7]><html class="no-js iem7" xmlns="//www.w3.org/1999/xhtml"><![endif]-->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php echo $chosen; ?>" xmlns="//www.w3.org/1999/xhtml"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8 ie7" lang="<?php echo $chosen; ?>" xmlns="//www.w3.org/1999/xhtml"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9 ie8" lang="<?php echo $chosen; ?>" xmlns="//www.w3.org/1999/xhtml"><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie" lang="<?php echo $chosen; ?>" xmlns="//www.w3.org/1999/xhtml"><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--> <html class="no-js" lang="<?php echo $chosen; ?>" xmlns="//www.w3.org/1999/xhtml"><!--<![endif]-->
<head prefix="og=http://ogp.me/ns#">
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<meta property="fb:app_id" content="<?php echo Kohana::config('facebook.app_id') ?>"/>
	<meta property="og:title" content="<?php echo $values['app_title'] . ' - Street Fighter X Tekken' ?>"/>
	<meta property="og:type" content="website"/>
	<meta property="og:url" content="<?php echo Kohana::config('facebook.fanpage_url') ?>app_<?php echo Kohana::config('facebook.app_id') ?>"/>
	<meta property="og:image" content="<?php echo $site_url . 'public/frontend/global/img/thumb.png' ?>"/>
	<meta property="og:description" content="" />
	<title><?php echo $values['app_title'] . ' - Street Fighter X Tekken' ?></title>
	<meta name="description" content="<?php //echo $values['app_desc'] ?>" />
	<meta name="author" content="AA" />
	
	<meta name="HandheldFriendly" content="True" />
	<meta name="MobileOptimized" content="320" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<link type="text/css" href="https://fonts.googleapis.com/css?family=Francois+One" rel="stylesheet" media="all" />
	<link href="<?php echo $base_url ?>public/frontend/global/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
	<link href="<?php echo $base_url ?>public/frontend/global/css/style.css?v=<?php echo strtotime('now'); ?>" rel="stylesheet" type="text/css" media="all" />
	<!--[if IE 7]>
		<link rel="stylesheet" href="<?php echo $base_url ?>public/frontend/global/css/ie7.css?v=<?php echo strtotime('now'); ?>">
	<![endif]-->
    
	<!--[if (lt IE 9) & (!IEMobile)]>
	<script src="<?php echo $base_url ?>public/frontend/global/js/libs/selectivizr-min.js"></script>
	<![endif]-->
	<script src="<?php echo $base_url ?>public/frontend/global/js/libs/modernizr-2.5.3.min.js"></script>
	
	<link rel="apple-touch-icon-precomposed" href="<?php echo $base_url ?>public/frontend/global/img/thumb.jpg" />
	<link rel="shortcut icon" href="<?php echo $base_url ?>public/frontend/global/img/thumb.jpg" />
	
	<meta http-equiv="cleartype" content="on" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<!--[if (lt IE 7) & (!IEMobile)]><script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script><script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script><![endif]-->
	
	<link rel="canonical" href="<?php echo (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https://" : "http://") . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']) . url::site(request::current()->uri) ?>" />
</head>
<body<?php echo isset($body_id) ? ' id="' . $body_id . '" ' : ''?>class="<?php echo isset($body_class) ? $body_class : ''?><?php echo $user->loggedin ? ' loggedin' : ''?> clearfix">
	<div id="wrapper">
		<nav id="menu">
			<ul>
				<li<?php echo !isset($body_id) || !in_array($body_id, array('leaderboards','scores')) ? ' class="active"' : ''; ?>><a href="<?php echo $base_url . $chosen . '/' ?>"><?php echo $values['nav_home']; ?></a></li>
				<li<?php echo isset($body_id) && $body_id == 'leaderboards' ? ' class="active"' : ''; ?>><a href="<?php echo $base_url . $chosen . '/leaderboards' ?>"><?php echo $values['nav_leaderboards']; ?></a></li>
				<li<?php echo isset($body_id) && $body_id == 'scores' ? ' class="active"' : ''; ?>><a href="<?php echo $base_url . $chosen . '/scores' ?>"><?php echo $values['nav_myscore']; ?></a></li>
			</ul>
		</nav>
		<div id="preorder">
			<a href="//www.facebook.com/streetfighter/app_462678037088931" target="_top" title="Pre Order"><img src="<?php echo $base_url ?>public/frontend/global/img/preorder_<?php echo ($chosen == 'other' || $chosen == 'us' ? 'gl' : $chosen); ?>.gif" alt="" /></a>
			<img src="<?php echo $base_url ?>public/frontend/global/img/ps_vita.png" alt="" class="vita" />
			<span class="title">NOW AVAILABLE ON PS Vita</span>
		</div>
		<!-- Languages DIV -->
			<div class="langs btn-group">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
				<?php if ($chosen == "other") { ?>
					<i class="icon-globe icon-white"><?php echo ucwords(lang::translate($chosen)) ?></i><?php } else { ?>
					<img src="<?php echo $base_url ?>public/frontend/global/img/flags/<?php echo $chosen ?>.gif" alt="<?php echo ucwords(lang::translate($chosen)) ?>" /><?php } ?>
				<span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
				<?php foreach($all_languages as $language): ?>
					<?php if($language->language != 'other'): ?>
						<li <?php if($language->language == $chosen) echo 'class="active"'; ?>>
							<a href="<?php echo $base_url . $language->language . '/' . $action ?>" class="lang"><img src="<?php echo $base_url ?>public/frontend/global/img/flags/<?php echo $language->language ?>.gif" alt="<?php echo ucwords(lang::translate($language->language)) ?>" /></a>
						</li>
					<?php endif ?>
				<?php endforeach ?>
				<?php foreach($all_languages as $language): ?>
					<?php if($language->language === 'other'): ?>
						<li <?php if($language->language == $chosen) echo 'class="active"'; ?>>
							<a href="<?php echo $base_url . $language->language . '/' . $action ?>" class="lang"><i class="icon-globe"><?php echo ucwords(lang::translate($language->language)) ?></i></a>
						</li>
					<?php endif ?>
				<?php endforeach ?>
				</ul>
			</div>
		<!-- End -->
		<section id="container" class="container-fluid">
			<?php echo $body; ?>
			<div class="modal boxContainer hide" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel" aria-hidden="true">
				<div class="modal-body containerInner">
				</div>
			</div>
		</section> <!--! end of #container -->
		<footer role="base">
			<img src="<?php echo url::base() ?>public/frontend/global/img/capcom.jpg" alt="" class="capcom" />
			
			<div id="legalContainer">
				<p><a target="_blank" href="http://www.capcom.com/capcom/legal_privacy/privacy.html">Privacy Policy</a> | <a target="_blank" href="http://www.capcom.com/capcom/legal_privacy/legal.php">Legal</a><br/>
				&copy; CAPCOM U.S.A., INC. <?php echo date('Y'); ?> ALL RIGHTS RESERVED. <br/>&copy; NAMCO BANDAI GAMES INC.</p>
			</div>
	
			<div class="rating">
				<img src="<?php echo url::base() . (isset($values['img_rating']) && $values['img_rating'] != '' ? 'public/upload/original/' . $values['img_rating'] : 'public/frontend/global/img/'.($chosen == 'us' ? 'rating-us.jpg' : 'rating.png')); ?>" border="0">
			</div>
		</footer>
	</div> <!--! end of #wrapper -->
	
	<!-- JS -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script>!window.jQuery && document.write(unescape('%3Cscript src="<?php echo $base_url ?>public/frontend/global/js/libs/jquery-1.7.1.min.js"%3E%3C/script%3E'))</script>
	<!--[if (lt IE 8) & (!IEMobile)]>
	<script src="<?php echo $base_url ?>public/frontend/global/js/libs/json2.min.js" type="text/javascript"></script>
	<![endif]-->
	<script type="text/javascript" src="<?php echo $base_url ?>public/frontend/global/js/libs/plugins.js"></script>
	<script type="text/javascript">
		Modernizr.load({
			test: Modernizr.mq('(only all)'),
			nope: '<?php echo $site_url ?>public/frontend/global/js/libs/respond.min.js'
		});
		window.SF = window.SF || {};
		SF.config = SF.config || {
			baseUrl: window.location.protocol + '//' + window.location.host + '<?php echo $base_url ?>',
			assetUrl: window.location.protocol + '//' + window.location.host + '<?php echo $base_url ?>public/frontend/global/',
			apiUrl: window.location.protocol + '//' + window.location.host + '<?php echo $base_url ?>api/',
			chosen: '<?php echo $chosen ?>',
			FB: {
				id: '<?php echo Kohana::config('facebook.app_id') ?>',
				namespace: '<?php echo Kohana::config('facebook.namespace') ?>',
				likeStatus: <?php echo (int)$like_status ?>,
				postUrl: '<?php echo Kohana::config('facebook.fanpage_url') ?>app_<?php echo Kohana::config('facebook.app_id') ?>',
				Share: {
					method: 'feed',
					picture: window.location.protocol + '//' + window.location.host + '<?php echo $base_url ?>public/frontend/global/img/thumb.jpg',
					name: '<?php echo $values['app_title'] . ' - Street Fighter X Tekken' ?>',
					caption: '<?php echo $values['app_title'] ?>',
					link: '<?php echo Kohana::config('facebook.fanpage_url') ?>app_<?php echo Kohana::config('facebook.app_id') ?>',
					actions: { name: 'Fight Your Friends', link: '<?php echo Kohana::config('facebook.fanpage_url') ?>app_<?php echo Kohana::config('facebook.app_id') ?>' }
				}
			},
			TW: { 
				share: '',
				link: '<?php echo Kohana::config('facebook.fanpage_url') ?>app_<?php echo Kohana::config('facebook.app_id') ?>'
			},
			user: <?php echo json_encode($user); ?>,
			LANG: <?php 
			//remove unwanted values
			unset($values['swear_words']);
			foreach ($matches as $match) {	unset($values['winner_match_' . $match->id]); }
			echo json_encode($values); ?>,
			isLoading: false
		};
		
	  	/* Google Analytics */
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', '<?php echo Kohana::config('facebook.GA_tag') ?>']);
		_gaq.push(['_trackPageview']);
		
		(function(doc, script) {
		    var js, 
		        fjs = doc.getElementsByTagName(script)[0],
		        add = function(url, id) {
		            if (doc.getElementById(id)) {return;}
		            js = doc.createElement(script);
		            js.src = url;
		            id && (js.id = id);
		            fjs.parentNode.insertBefore(js, fjs);
		        };
		        
		    // Facebook
		    add('//connect.facebook.net/en_GB/all.js');
		    // Google Analytics
		    add(('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js', 'ga');
		    // Twitter SDK
		    add('//platform.twitter.com/widgets.js', 'twitter-wjs');
		}(document, 'script'));
	</script>
	<script src="<?php echo $base_url . 'public/frontend/global/js/app' . (Kohana::$environment === Kohana::PRODUCTION ? '.min' : '') .'.js?v=' . strtotime('now'); ?>" type="text/javascript"></script>
</body>
</html>