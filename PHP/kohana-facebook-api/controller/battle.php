<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller_Battle extends Controller {
	protected static $_instance;
	public $template = false;
	public $auto_render = false;
	public $filters = false;
	public $friends;
	
	public function __construct(){
		$this->template = false;
		$this->auto_render = false;
		$this->request = Request::instance()->current();
        $this->session = Session::instance();
        $_SESSION =& $this->session->as_array();
		$this->filters = $this->session->get('filters');
		$this->friends = $this->session->get('filterFriends');
		$this->match = ORM::factory('match');
		$this->matches = $this->match->find_all();
   }
   
   public static function instance()
	{
		if ( ! isset(self::$_instance))
			self::$_instance = new self();

		return self::$_instance;
	}
   
	public function action_fight( $id ) {
		if (!isset($_POST['match']) && !isset($_POST['friend'])) {
			Request::current()->redirect($this->request->param('lang') . '/');
		}
		$match = $this->match->find((int)$_POST['match']);
		$friendId = $_POST['friend'];
		
		if ( !$match->loaded() ) {
			if (request::$is_ajax) {
				header( "Content-Type: application/json" );
				echo false; exit;
			} else {
				return false;
			}
		}
		
		$this->session->set('match', $match->type);
		$this->session->set('friend', $friendId);
		
		$fbUser = (object)FB::instance()->account();
		$fb = FB::instance()->facebook();
		
		if (!isset($fbUser->id)) {
			Request::current()->redirect($this->request->param('lang') . '/');
		}
		
		$data = array();
		$data['user_id'] = $fbUser->id;
		$data['friend_id'] = $friendId;
		$userScore = NULL;
		$friendScore = NULL;
		
		//check if user has fought this match with friend
		$fought = $this->action_fought($fbUser->id, $friendId, $match->id, true);
		if ( $fought ) { 
			if (request::$is_ajax) {
				header( "Content-Type: application/json" );
				echo false; exit;
			} else {
				Request::current()->redirect($this->request->param('lang') . '/match'); 
			}
		}
			
		switch ($match->type) {
			case "popular":
			
				$fql = 'SELECT friend_count FROM user WHERE uid = %1$s';
				$userResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $fbUser->id) ));
				$friendResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $friendId) ));
				
				$userScore = $userResults[0]["friend_count"];
				$friendScore = $friendResults[0]["friend_count"];
				
				break;
			case "updates":
				$fql = 'SELECT message FROM status WHERE uid = %1$s';
				$userResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $fbUser->id) ));
				$friendResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $friendId) ));
				
				$values = $this->session->get('values');
				$swearWords  = str_getcsv($values['swear_words'], ',', "'", '\\');
				
				$userWords = implode(" ", array_map(function($item) { return $item['message']; }, $userResults));
				$friendWords = implode(" ", array_map(function($item) { return $item['message']; }, $friendResults));
				
				$userScore = $this->isStringBad( $userWords, $swearWords );
				$friendScore = $this->isStringBad( $friendWords, $swearWords );
				break;
			case "photos":
				$fql = 'SELECT pid FROM photo_tag WHERE subject = %1$s';
				$userResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $fbUser->id) ));
				$friendResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $friendId) ));
				
				$userScore = count($userResults);
				$friendScore = count($friendResults);
				break;
			case "checkins":
				$fql = 'SELECT page_id FROM checkin WHERE author_uid = %1$s';
				$userResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $fbUser->id) ));
				$friendResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $friendId) ));
				
				$userScore = count($userResults);
				$friendScore = count($friendResults);
				break;
			case "status":
				$fql = 'SELECT status_id FROM status WHERE uid = %1$s';
				$userResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $fbUser->id) ));
				$friendResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $friendId) ));
				
				$userScore = count($userResults);
				$friendScore = count($friendResults);
				break;
			case "events":
				$start_time = new DateTime();
				$start_time->modify('-5 years');
				$time = $start_time->format(DateTime::ISO8601);
				
				$fql = 'SELECT eid FROM event_member WHERE uid = %1$s AND rsvp_status = "attending" AND start_time > "%2$s"';
				$userResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $fbUser->id, $time) ));
				$friendResults = $fb->api(array( 'method' => 'fql.query', 'query' => sprintf($fql, $friendId, $time) ));
				
				$userScore = count($userResults);
				$friendScore = count($friendResults);
				break;
		}
		
		//calculate results
		$total = $userScore + $friendScore;
		if ($total>0) {
			$denominator = $total / 100;
			$userScore = $userScore / $denominator;
			$friendScore = $friendScore / $denominator;
		} else { //no data - TODO
			$userScore = 0;
			$friendScore = 0;
		}
		
		//store user scores
		$data['user_score'] = round($userScore);
		$data['friend_score'] = round($friendScore);
		
		//select winner & assign points
		if ($data['user_score'] > $data['friend_score']) {
			$data['points'] = $this->pointsForScore($data['user_score']);
			$data['winner'] = $fbUser->id;
			$data['loser'] = $friendId;
		} else if ($data['friend_score'] > $data['user_score']) {
			$data['points'] = $this->pointsForScore($data['friend_score']);
			$data['winner'] = $friendId;
			$data['loser'] = $fbUser->id;
		} else {
			$data['points'] = 0;
			$data['winner'] = NULL;
			$data['loser'] = NULL;
		}
		
		$data['match_id'] = $match->id;
		$data['match_title'] = strtolower($match->title);
		
		$this->storeBattle( $data, $match->id );
		$this->session->set('battleResult', $data);
		
		$matchResult = !$data['winner'] ? 'draw' : ($data['winner'] == $fbUser->id ? 'win' : 'lose');
		
		try {
			$namespace = Kohana::config('facebook.namespace');
			$url = URL::base(TRUE, TRUE);
			$matchTitle = $data['match_title'];
			$fbAction = $fb->api("/me/$namespace:$matchResult", 'POST', array(
				'fight' => "$url/og/fight/$matchTitle/"
			));
		} catch(FacebookApiException $e) {	
			//var_dump($e);
			$log = Kohana_Log::instance();
			$log->add('error', $e);
		}
		
		if (request::$is_ajax) {
			header( "Content-Type: application/json" );
			echo true; exit;
		} else {
			return false;
		}
	}

	// more than 50% = 10 points
	// more than 60% = 20 points
	// more than 70% = 30 points
	// more than 80% = 40 points
	// more than 90% = 50 points	
	public function pointsForScore($score) {
		...
	}
	
	public static function isStringBad($subjectString = '', $disallowedWords = array()) {
		$alphabetSubject = preg_replace('|[^a-zA-Z ]+|', '', $subjectString);
		$count = 0;
		foreach ($disallowedWords as $disallowedWord) {
			if (stripos($alphabetSubject, $disallowedWord) !== false) {
				$count++;
			}
		}
		return $count;
	}

	public function storeBattle( $item = array(), $matchId ) {
		if ( isset($item['user_id']) ) {
			$battle = ORM::factory('battle');
			
			$battle->match_id = $matchId;
			$battle->user_id = $item['user_id'];
			$battle->friend_id = $item['friend_id'];
			$battle->user_score = $item['user_score'];
			$battle->friend_score = $item['friend_score'];
			$battle->winner = $item['winner'];
			$battle->loser = $item['loser'];
			$battle->points = $item['points'];
			$battle->save();
			
		} else {
			return false;
		}
	}
	
	public function action_fought($userId, $friendId, $matchId = NULL, $return = false) {
		//which battles has the user fought friend
		$fought = DB::select('match_id')->from('battles')->where('user_id', '=', $userId)->and_where('friend_id', '=', $friendId);
			
		if(is_numeric($matchId)) {
			$result = $fought->and_where('match_id', '=', $matchId)->execute()->count() > 0 ? true : false;
		} else {
			$result = $fought->execute()->as_array();
		}
		
		if (request::$is_ajax && !$return) {
			header( "Content-Type: application/json" );
			echo json_encode($result); exit;
		} else {
			return $result;
		}
	}
	
	public function userScores($id) {
		$userScores = array(
			'id' => $id,
			'points' => 0,
			'rank' => 0,
			'matches' => 0,
			'topMatch' => 0
		);
		$sqlLoss = "SELECT match_id, count(loser) as loss
				FROM battles b
				WHERE loser = :user
				GROUP BY match_id
				ORDER BY match_id DESC";
		$sqlWin = "SELECT match_id, count(winner) as win
				FROM battles b
				WHERE winner = :user
				GROUP BY match_id
				ORDER BY match_id DESC";
		
		$sql = "SELECT l.match_id, w.win, l.loss FROM ($sqlWin) w
			RIGHT JOIN ($sqlLoss) l
			USING (match_id)
			UNION ALL
			SELECT w.match_id, w.win, l.loss FROM ($sqlWin) w
			LEFT OUTER JOIN ($sqlLoss) l
			USING (match_id)
			WHERE l.match_id is null OR w.match_id is null
			ORDER BY match_id";
		$query = DB::query(Database::SELECT, $sql);
		$query->parameters(array(
		    ':user' => $id
		));
		$matches = $query->execute()->as_array();
		
		if (count($matches)>0) {
			$matchInfo = array();
			foreach ($matches as $key => $row) {
				$win[$key]  = $row['win'];
				$loss[$key] = $row['loss'];
			}
			array_multisort($win, SORT_DESC, $loss, SORT_ASC, $matches);
			$matchInfo = array();
			foreach ($matches as $row) {
				$matchInfo[$row['match_id']] = $row;
			}
			//world rank + total points
			$sql = "SELECT x.* FROM (SELECT z.*, @rownum := @rownum + 1 AS rank FROM (SELECT winner as user, sum(points) AS pts FROM battles b WHERE points > 0 GROUP BY winner ORDER BY pts DESC) z, (SELECT @rownum := 0) r) x WHERE x.user = :user";
			$query = DB::query(Database::SELECT, $sql);
			$query->parameters(array(
			    ':user' => $id
			));
			
			$worldRank = $query->execute()->as_array();
			if (count($worldRank) > 0) {
				$userScores['points'] = $worldRank[0]['pts'];
				$userScores['rank'] = $worldRank[0]['rank'];
			}
			$userScores['matches'] = $matchInfo;
			$userScores['topMatch'] = $this->getTopMatch( $matchInfo );
		}
		
		if (request::$is_ajax) {
			header( "Content-Type: application/json" );
			echo json_encode($userScores); exit;
		} else {
			return $userScores;
		}
	}
	
	public function getTopMatch( $matches ) {
		$top = array_values($matches);
		return array_shift($top);
	}
	
	public function getLeaderboard( $match = NULL ) {
		if (is_numeric($match)) {
		
			$sql = "SELECT z.*, @rownum := @rownum + 1 AS rank FROM (SELECT winner as id, sum(points) AS pts FROM battles b WHERE points > 0 AND match_id = :match GROUP BY winner ORDER BY pts DESC) z, (SELECT @rownum := 0) r";
			$query = DB::query(Database::SELECT, $sql);
			$query->parameters(array(
			    ':match' => $match
			));
			$query = $query->execute();
			
		} else { //top scorers
		
			$sql = "SELECT z.*, @rownum := @rownum + 1 AS rank FROM (SELECT winner as id, sum(points) AS pts FROM battles b WHERE points > 0 GROUP BY winner ORDER BY pts DESC) z, (SELECT @rownum := 0) r WHERE z.pts >= 100 LIMIT 20";
			$query = DB::query(Database::SELECT, $sql)->execute();
			
		}
		
		if (request::$is_ajax) {
			header( "Content-Type: application/json" );
			echo json_encode($query->as_array()); exit;
		} else {
			return $query->as_array();
		}
	}
	
	public function action_user( $id ) {
		$user = ORM::factory('user');
		$existing = $user->where('fbid', '=', $id)->find();
		
		if ( !$existing->loaded() ) {
			$fbUser = (object)FB::instance()->account();
			$user->fbid = $id;
			$user->email = $fbUser->email;
			$user->fname = $fbUser->first_name;
			$user->lname = $fbUser->last_name;
			$user->country = $fbUser->locale;
			$user->save();
		}
		
		$existing = $user->where('fbid', '=', $id)->find();
		
		if (request::$is_ajax) {
			header( "Content-Type: application/json" );
			echo json_encode($existing->as_array()); exit;
		} else {
			return $existing->as_array();
		}
	}
}