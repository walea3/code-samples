<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller_Template {

    public $template = 'layouts/frontend';
    public $appMode = 'page';

	public function before()
	{        
		parent::before();
		
        $this->session = Session::instance();
        $_SESSION =& $this->session->as_array();

		$signed_request = isset($_REQUEST['signed_request']) ? $this->parse_signed_request($_REQUEST['signed_request'], Kohana::config('facebook.app_secret')) : ($this->session->get('signed_request') ? $this->parse_signed_request($this->session->get('signed_request'), Kohana::config('facebook.app_secret')) : null);
		
        $user = (object)FB::instance()->account();
		$user->loggedin = FB::instance()->logged_in();
		if ($user->loggedin) {
			$user->fbid = $user->id;
			
			$existing = ORM::factory('user')->where('fbid', '=', $user->fbid)->find();
			if ($existing->loaded()) {
				$user->id = $existing->id;
			}
		}
		
		if ($this->request->action !== "index" && $this->request->action !== "leaderboards" && $this->request->action !== "og" && (!$user->loggedin && !$signed_request)) {
			Request::current()->redirect($this->request->param('lang') . '/');
		}
		
		//geoip
		//$this->locator = new Locator();
		$this->template->locale = 'en_GB';
		$like_status = false;
    	//$user->location = $this->locator->locateIp(request::$client_ip);
    	
    	if ($signed_request) {
			$fb_country = $signed_request['user']['country'];
			$fb_locale = $signed_request['user']['locale'];
			$app_data = isset($signed_request['app_data']) ? $signed_request['app_data'] : null;
			$fb_locale_country = substr($fb_locale, 0,2);
			$fb_locale_2 = substr($fb_locale, 3,4);
			if ($fb_locale_country === "en") {
				$fb_locale_country = strtolower($fb_locale_2 == "GB" ? "uk" : $fb_locale_2);
			}
			
			if(isset($_REQUEST['signed_request'])) {
				$this->session->set('signed_request', $_REQUEST['signed_request']);
				if(in_array($app_data, array('result','match','leaderboards','scores'))) {
					Request::current()->redirect($this->request->param('lang') . '/' . $app_data);
				}
			}
			
			View::set_global('signed_request', $this->session->get('signed_request'));
			
			if( !isset($signed_request['page']) ) {
				$this->appMode = 'canvas';
			} else {
				$like_status = $signed_request['page']['liked'];
			}
    	} else {
    		if (Kohana::$environment === Kohana::PRODUCTION && $this->request->action !== "og") {
				Request::current()->redirect(Kohana::config('facebook.fanpage_url') . 'app_' .Kohana::config('facebook.app_id'));
    		}
    		$like_status = isset($_GET['like']) ? $_GET['like'] : ($user->loggedin ? true : false);
    	}

		$lang = $this->request->param('lang');
		$match = ORM::factory('match');
		View::set_global('matches', $match->find_all());
    	
    	...
    }
	
    public function action_index( $lang )
    {
		View::set_global('body_id', ($this->template->like_status || $this->appMode == 'canvas' ? 'landing' : 'like' ));
	    View::set_global('topScorers', Battle::instance()->getLeaderboard());
		$this->template->body = new View('index/' . ($this->template->like_status || $this->appMode == 'canvas' ? 'landing' : 'like_gate' ));
    }
    
    public function action_match( $lang, $step = 1 )
    {
    	View::set_global('body_class', 'step');
		View::set_global('body_id', 'match');
		$this->template->body = new View('index/match', array('currStep'=> $step, 'selectedFriend' => $this->session->get('friend')));
    }
    
    public function action_result( $lang )
    {
    	if ( $result = $this->session->get('battleResult') ) {
			View::set_global('body_id', 'result');
	    	View::set_global('body_class', 'step');
	    	View::set_global('result', (object)$result);
			$this->template->body = new View('index/result');
		} else {
			Request::current()->redirect($this->request->param('lang') . '/match');
		}
    }
    
    public function action_leaderboards( $lang )
    {
		View::set_global('body_id', 'leaderboards');
    	$leaderboards = array();
		foreach ($this->template->matches as $match) {
			$leaderboards[strtolower($match->title)] = Battle::instance()->getLeaderboard($match->id);
		}
		View::set_global('leaderboards', $leaderboards);
		$this->template->body = new View('index/leaderboards');
    }
    
    public function action_scores( $lang )
    {
		View::set_global('body_id', 'scores');
    	$userScores = array();
		View::set_global('scores', Battle::instance()->userScores($this->template->user->fbid));
		$this->template->body = new View('index/scores');
    }
    
    public function action_og( $object, $type = null )
    {
    	$this->auto_render = FALSE;
    	$site_url = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "https://" : "http://") . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']) . url::base();
    	
    	if ($object == 'fight') {
    		$match = ORM::factory('match')->where('title','LIKE',"%$type%")->find()->as_array();
    		if (count($match) > 0) {
				$view = View::factory("layouts/og", array(
					"object" => $object,
					"type" => ($type ? $match['title'] : $type),
					"ogUrl" =>  $site_url . "og/$object/$type",
					"ogTitle" => ($type ? $match['title'] . " Battle" : 'Street Fighter X Tekken - Friend Fighter'),
					"ogImage" => $site_url . ($type ? "public/frontend/global/img/match_$type.jpg" : "public/frontend/global/img/thumb.jpg"),
					"ogDesc" => ($type ? $this->template->values['desc_match_' . $match['id']] : 'Match up with a Friend via Facebook, in the Street Fighter X Tekken friend fighter.')
				));
				
		    	$this->request->response = $view;
	    	}
    	}
    }
    
	protected function parse_signed_request($signed_request, $secret) {
		list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

		// decode the data
		$sig = $this->base64_url_decode($encoded_sig);
		$data = json_decode($this->base64_url_decode($payload), true);

		if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
			error_log('Unknown algorithm. Expected HMAC-SHA256');
			return null;
		}

		// check sig
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
		if ($sig !== $expected_sig) {
			error_log('Bad Signed JSON signature!');
			return null;
		}

		return $data;
	}

	protected function base64_url_decode($input) {
		return base64_decode(strtr($input, '-_', '+/'));
	}
	
}