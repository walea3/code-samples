<?php
/**
 * The main template file.
 */

get_header(); ?>
	<article class="inview" data-coronis-view="start" data-coronis-ui="all" data-stellar-background-ratio="0.7">
		<h1 class="logo"><a title="Coronis Diary"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="Coronis Diary"></a></h1>

		<section class="landing">
			<h3 class="wrap"><?php _e( get_field('intro', 'options') ); ?></h3>

			<h2 class="pre-load loader">Loading ... Please wait</h2>
			<a href="#/start" class="btn-round post-load"><span><?php _e( get_field('start', 'options') ); ?></span></a>

			<div class="social" data-coronis-ui="mobile">
				<a href="//facebook.com/lostplanet" class="fb" title="Facebook" target="_blank">Facebook</a>
				<a href="//twitter.com/lostplanet" class="tw" title="Twitter" target="_blank">Twitter</a>
				<a href="//plus.google.com/lostplanet" class="gp" title="Google+" target="_blank">Google+</a>
			</div>
		</section>

		<section id="loadingContainer" class="loading">
			<div class="beam"></div>
			<div class="wrap">
				<div class="loader"><?php the_field('loading', 'option'); ?><i class="spinner" data-coronis-ui="mobile"></i><i class="spinner" data-coronis-ui="full"></i></div>
				<br><a href="#/continue" class="continue btn-hollow out"><span><?php _e( get_field('continue', 'options') ); ?></span></a>
				<div class="tip navigation" data-coronis-ui="full">
					<?php echo wpautop( __( get_field('navigation_info', 'option') ) ); ?>
					<i></i>
				</div>
				<div class="tip profile" data-coronis-ui="full">
					<?php echo wpautop( __( get_field('profile_info', 'option') ) ); ?>
					<i></i>
				</div>
				<div class="tip fb">
					<?php echo wpautop( '<i></i>' . __( get_field('connect_info', 'option') ) ); ?>
					<a href="#/connect" class="btn-hollow fb"><i></i><?php _e( get_field('connect', 'option') ); ?></a>
				</div>
			</div>
		</section>

		<footer class="landing">
			<a href="http://capcom.com/" class="capcom" title="Capcom Website" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/capcom.png" alt="Capcom"></a>
			<a href="http://lostplanetthegame.com/" class="lp3" title="Lost Planet 3 Official Site" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/lp3.png" alt="Lost Planet 3"></a>
		</footer>
		<div data-coronis-bg-layer="ice" data-coronis-ui="full"></div>
	</article>
<?php get_footer(); ?>