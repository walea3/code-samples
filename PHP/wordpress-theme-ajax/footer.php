<?php
/**
 * The template for displaying the footer.
 *
 */
?>
			<nav id="hud">
				<div id="profile" data-coronis-ui="full"></div>
				<ul id="controls">
					<li class="start" data-coronis-ui="mobile"><a href="#/coronis" title="Coronis Diary - <?php _e( get_field('start', 'option') ); ?>">Coronis Diary</a></li>
					<li class="profile" data-coronis-ui="full"><a href="#/profile" title="Profile">Profile</a></li>
					<li class="social">
						<a href="#/connect" class="fb" title="Sign in with Facebook" target="_blank">Facebook</a>
						<!-- <a href="//twitter.com/lostplanet" class="tw" title="Twitter" target="_blank">Twitter</a>
						<a href="//plus.google.com/lostplanet" class="gp" title="Google+" target="_blank">Google+</a> -->
					</li>
					<li class="more">
						<div class="lang"><?php if ( ! dynamic_sidebar( 'Language Selector' ) ) {} ?></div>
						<a href="#/connect" class="login">Login</a> 
						<a href="http://www.capcom.com/capcom/legal_privacy/privacy.html" target="_blank">Privacy Policy</a>
					</li>
				</ul>
				<div id="display" data-coronis-entry-type="normal" data-coronis-entry-state="" data-coronis-ui="full">
					<div class="decoder"><div class="wave"></div><h4><?php _e( get_field('decoding_in_progress', 'option') ); ?></h4><div class="progress"><span class="bar"></span><span class="perc">0%</span></div></div>
				</div>
			</nav>
			<div data-coronis-bg-layer="clouds" data-coronis-ui="full"></div>
			<canvas data-coronis-bg-layer="snow" data-coronis-ui="full"></canvas>
			<div data-coronis-bg-layer="char-steps" data-coronis-ui="full"><div class="jim"></div></div>
			<div data-coronis-layer="tint" data-coronis-ui="mobile"></div>
			<div data-coronis-bg-layer="ground" data-coronis-ui="full"></div>
		</div> <!--! end of #container -->
	</div> <!--! end of #wrapper -->
	<footer class="more">
		<div class="lang"><?php if ( ! dynamic_sidebar( 'Language Selector' ) ) {} ?></div>
		<a href="#/connect" class="login">Login</a> 
		<a href="http://www.capcom.com/capcom/legal_privacy/privacy.html" target="_blank">Privacy Policy</a>
		<div class="rating"><img src="<?php echo get_template_directory_uri() . '/img/' . ( qtrans_getLanguage() == 'us' ? 'esrbRP.png' : 'pegi16.jpg' ); ?>" alt=""></div>
	</footer>

<?php $ajax = true; ?>
	<script id="stageList" type="text/html">
	<?php include( locate_template( 'partials/stage-list.php' ) ); ?>
	</script>
	<script id="stageContent" type="text/html">
	<?php include( locate_template( 'partials/stage-content.php' ) ); ?>
	</script>
	<script id="transmissionContent" type="text/html">
	<?php include( locate_template( 'partials/transmission-content.php' ) ); ?>
	</script>
	<script id="transmissionItem" type="text/html">
	<?php include( locate_template( 'partials/transmission-item.php' ) ); ?>
	</script>
	<script id="profileContent" type="text/html">
	<?php include( locate_template( 'partials/profile.php' ) ); ?>
	</script>
<?php $ajax = null; ?>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="//code.jquery.com/jquery-migrate-1.0.0.js"></script>
	<script>window.jQuery.migrateWarnings || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/libs/jquery-migrate-1.0.0.js"><\/script>')</script>
	
	<script type="text/javascript">
		window.Coronis = window.Coronis || {};
		Coronis.storage = Coronis.storage || { 

			baseUrl : '<?php echo site_url() ?>',
			assetUrl : '<?php echo get_template_directory_uri(); ?>/',
			nevec : '<?php echo admin_url( 'admin-ajax.php?lang=' . qtrans_getLanguage() ) ?>',
			nevecID : '<?php echo wp_create_nonce( 'nevec-coronis-diary' ) ?>',
			FB : { 
				id : '473761472677462', 
				picture : '<?php echo get_template_directory_uri(); ?>/img/thumb.jpg',
				name: 'Coronis Diary',
				caption: 'The Coronis Diary is a transmission portal by Nevec to support diary logging for Coronis Base workers on E.D.N. III.'
			},
			langCode : '<?php echo ( $currentLang = qtrans_getLanguage() ); ?>',
			region : '<?php echo qtrans_localeForCurrentLanguage( qtrans_getLanguage() ); ?>',
			LANG : <?php echo ( $lang = getContent( 'lang' ) ) ? json_encode( $lang ) : "{}"; ?>,
			isConnecting : false,
			startView : <?php global $CoronisView; echo $CoronisView ? "'$CoronisView'" : 'false' ?>,
			viewKey : 'data-coronis-view',
			stageKey : 'data-coronis-stage',
			stages : <?php echo ( $stages = wp_cache_get( $currentLang . '_coronisStages' ) ) ? json_encode( $stages ) : "{}"; ?>,
			transmissions : <?php echo ( $transmissions = wp_cache_get( $currentLang . '_coronisTransmissions' ) ) ? json_encode( $transmissions ) : "{}"; ?>,
			user : <?php echo isset( $_SESSION[ 'coronisUser' ] ) ? json_encode( $_SESSION[ 'coronisUser' ] ) : "{}"; ?>

		};
		//Facebook Like/Send button
		// (function(d, s, id) {
		//   var js, fjs = d.getElementsByTagName(s)[0];
		//   if (d.getElementById(id)) return;
		//   js = d.createElement(s); js.id = id;
		//   js.src = "//connect.facebook.net/<?php echo qtrans_localeForCurrentLanguage( qtrans_getLanguage() ); ?>/all.js#xfbml=1&appId=473761472677462";
		//   fjs.parentNode.insertBefore(js, fjs);
		// }(document, 'script', 'facebook-jssdk'));
		
		//Tweet button
		!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
		
		//Google+
	  	// (function() {
	   //  	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	   //  	po.src = 'https://apis.google.com/js/plusone.js';
	   //  	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	  	// })();

	  	/* Google Analytics */
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-29286445-38']);
		_gaq.push(['_trackPageview']);
		
		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
	</script>

	<script src="<?php echo get_template_directory_uri(); ?>/js/plugins.2-0.js?v=<?php echo time() ?>" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/coronis.app.2-0.min.js?v=<?php echo time() ?>" type="text/javascript"></script>

	<?php wp_footer(); ?>
</body>
</html>