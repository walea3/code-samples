<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="container">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
global $like_status; $class = ' pre-load' . ( isFacebook() ? ' fb' . ($like_status ? ' liked' : '') : '' );
?><!DOCTYPE html>
<!--[if IEMobile 7]><html class="no-js iem7" <?php language_attributes(); ?>><![endif]-->
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8 ie7" <?php language_attributes(); ?>><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9 ie8" <?php language_attributes(); ?>><![endif]-->
<!--[if (IE 9)&!(IEMobile)]><html class="no-js ie" <?php language_attributes(); ?>><![endif]-->
<!--[if (gt IE 9)|(gt IEMobile 7)]><!--> <html class="no-js" <?php language_attributes(); ?>><!--<![endif]-->
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website# coronis_diary: http://ogp.me/ns/fb/coronis_diary#">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<!-- <meta name="google-site-verification" content="UHfL_ilIdwtcASeg0nfe-aOCVUH8qVQ2pikLD_GYrbQ"> -->
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="description" content="Coronis Diary - Lost Planet 3">
	<meta name="author" content="AA">
	
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/thumb.jpg">

	<link href="http://fonts.googleapis.com/css?family=Abel" rel="stylesheet" type="text/css" />
	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css?v=<?php echo time() ?>" rel="stylesheet" type="text/css" media="all">
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css?v=<?php echo time() ?>" rel="stylesheet" type="text/css" media="all">
	
	<link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/thumb.jpg">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png">
	<meta http-equiv="cleartype" content="on">
	<!--[if (lt IE 7) & (!IEMobile)]><script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.2/CFInstall.min.js"></script><script>window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})})</script><![endif]-->
	
	<link rel="canonical" href="<?php echo add_query_arg( $wp->query_string, '', home_url( $wp->request ) ); ?>/">

	<?php wp_head(); ?>
	<script src="<?php echo get_template_directory_uri(); ?>/js/libs/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body <?php body_class( $class ); ?> data-coronis-active="start">
	<!--[if lt IE 8]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
	<div id="wrapper">
		<noscript id="jsMsg">
			<h2>JavaScript must be enabled to use this site.</h2> 
			<h3>Please enable JavaScript in your browser and refresh the page.</h3>
		</noscript>
		<div id="supportedMsg" class="hide">
			<h2>This browser is not currently supported</h2> 
			<h3>We recommend viewing this site in Google Chrome, Apple Safari or Mozilla Firefox.</h3>
		</div>
		<div id="container">
