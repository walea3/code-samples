<?php if ( !$ajax && $stage ) { extract( $stage ); 
	$prev = $id === 1 ? false : $id - 1;
	$next = !$stages || count( $stages ) < ( $id + 1 ) ? $id + 1 : false;
} ?><article class="stage" data-coronis-view="stage-<?php echo $ajax ? "{{ id }}" : $id; ?>" data-coronis-ui="all">
		<header>
			<div class="userInfo">
				<span class="user<?php echo $ajax ? '{{^ user }} hide{{/ user }}' : ( !$user ? " hide" : null ); ?>"><?php echo __( get_field( 'hello', 'options') ) ?> <span class="name"><?php echo ( $ajax ? ' {{ user }} ' : " $user " ); ?></span> <a href="#/connect" class="login">Sign In</a><br></span>
				<?php printf( __( get_field( 'x_new_transmissions', 'options' ) ) , "<span class=\"num\">" . ($ajax ? "{{ newTransmissions }}" : $newTransmissions) . "</span>" ) ?>
			</div>
			<ul class="status">
			<?php if ( $ajax ): ?> 
				{{# status }}
					<li class="{{ type }}"><i>{{& type }}</i> {{ count }}</li>
				{{/ status }}
			<?php else : ?>
				<?php if ( $status ) { foreach( $k as $v ): ?>
					<li class="<?php echo $k ?>"><i><?php echo $k ?></i> <?php echo $v['count'] ?></li>
				<?php endforeach; } ?>
			<?php endif; ?> 
			</ul>
			<h3 class="title"><?php echo $ajax ? "{{& title }}" : $title; ?></h3>
			<nav>
				<?php echo $ajax ? '{{# prev }}<a href="#/stage-{{prev}}" class="prev">Prev</a>{{/ prev }}' : ( $prev ? "<a href=\"#/stage-$prev\" class=\"prev\">Prev</a>" : null ) ?> 
				<?php echo $ajax ? '{{# next }}<a href="#/stage-{{next}}" class="next">Next</a>{{/ next }}' : ( $next ? "<a href=\"#/stage-$next\" class=\"next\">Next</a>" : null ) ?> 
			</nav>
		</header>
		<section class="transmissionList">
			<ul class="transmissions">
			<?php if ($ajax): ?> 
				{{# transmissions }}
					{{> transmissionItem }}
				{{/ transmissions }}
			<?php else : ?> 
				<?php if ( $transmissions ){ foreach( $transmissions as $transmission ): ?>
					<?php get_template_part( 'partials/transmission', 'item' ) ?>
				<?php endforeach; } ?>
			<?php endif; ?> 
			</ul>
		</section> 
	</article> 
