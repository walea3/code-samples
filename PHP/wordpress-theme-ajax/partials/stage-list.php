<?php if ($ajax): ?>
	{{# stages }}<li class="{{ name }} stage" data-coronis-ui="full"><a href="#/{{ name }}" title="{{ title }}">{{ title }}</a></li>{{/ stages }}
<?php else: ?> 
	<?php $stages = getContent( 'stages' ); if( $stages ) { foreach( $stages as $stage ): extract( $stage ); ?>
		<li class="<?php echo $name ?> stage" data-coronis-ui="full"><a href="#/<?php echo $name ?>" title="<?php echo $title ?>"><?php echo $title ?></a></li>
	<?php endforeach; } ?>
<?php endif; ?> 
