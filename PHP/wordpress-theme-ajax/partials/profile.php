<?php if ( !$ajax && $user ) { extract( $user ); } ?><section id="profile" data-coronis-view="userProfile">
		<header>
			<div class="user"><img src="<?php echo $ajax ? '{{ picture }}' : $picture; ?>" alt="<?php echo $ajax ? '{{ name }}' : $name; ?>"> <span class="userName">Name <br><strong class="name"><?php echo $ajax ? '{{ name }}' : $name; ?></strong></span></div>
			<div class="status"><?php printf( __( get_field( 'read_x_of_x', 'options' ) ), "<span class=\"read\">" . ( $ajax ? "{{ profile.read }}" : $profile['read'] ) . "</span>", "<span class=\"total\">" . ( $ajax ? "{{ profile.total }}" : $profile['total'] ) . "</span>" ) ?></div>
		</header>
		<div class="transmissionList">
			<ul class="special">
			<?php if ($ajax): ?> 
				{{# profile.special }}
					{{> transmissionItem }}
				{{/ profile.special }}
			<?php else: ?> 
				<?php if($profile['special']){ foreach($profile['special'] as $transmission): ?>
					<?php include(locate_template('partials/transmission-item.php')); ?>
				<?php endforeach; } ?>
			<?php endif; ?> 
			</ul>
			<ul class="normal">
			<?php if ($ajax): ?> 
				{{# profile.normal }}
					{{> transmissionItem }}
				{{/ profile.normal }}
			<?php else: ?> 
				<?php if($profile['normal']){ foreach($profile['normal'] as $transmission): ?>
					<?php include(locate_template('partials/transmission-item.php')); ?>
				<?php endforeach; } ?>
			<?php endif; ?> 
			</ul>
		</div>
	</section>
