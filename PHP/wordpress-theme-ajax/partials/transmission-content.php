<?php if ( !$ajax && $transmission ) { extract( $transmission ); } ?><article class="transmission <?php echo $ajax ? "{{ type }}" : explode( ' ', $type ); ?>" data-coronis-view="transmission:<?php echo $ajax ? "{{ nevecKey }}" : $nevecKey; ?>" data-coronis-ui="" data-coronis-entry-id="<?php echo $ajax ? "{{ nevecID }}" : $nevecID; ?>" data-coronis-entry-type="<?php echo $ajax ? "{{ type }}" : explode( ' ', $type ); ?>" data-coronis-entry-state="<?php echo $ajax ? "{{ state }}" : $state; ?>" data-coronis-content="<?php echo $ajax ? "{{ messageType }}" : $messageType; ?>" data-coronis-stage="<?php echo $ajax ? "{{ stage }}" : $stage; ?>">
		<header>
			<h2 class="title"><?php echo $ajax ? "{{ title }}" : $title; ?> <span class="count"><?php echo $ajax ? "{{ transmissionCount }}" : $transmissionCount; ?></span></h2>
			<nav>
				<a href="#/back" class="back"><i></i><?php _e( get_field( 'go_back', 'options' ) ) ?></a>
			</nav>
		</header>
		<section class="entry">
			<i class="status"><?php echo $ajax ? "{{ statusMsg }}" : $statusMsg; ?></i>
			<?php if ($ajax): ?> 
				{{& message }}
			<?php else: ?> 
				<?php if ( $messageType === 'audio' ) { ?>
					<div class="audio" data-coronis-sound="<?php echo $audio ?>"></div>
				<?php } else if ( $messageType === 'video' ) { ?>
					<div class="video" data-coronis-video="<?php echo $video ?>"></div>
				<?php } else if ( $image ) { ?>
					<div class="image"><img src="<?php echo $image ?>" alt="<?php echo $headline ?>"></div>
				<?php } ?>
			<?php endif; ?> 
			<div class="text wrap">
				<h2 class="headline"><?php echo $ajax ? "{{& headline }}" : $headline; ?></h2>
				<?php echo $ajax ? "{{& text }}" : $text; ?>
				<div class="decode<?php echo $ajax ? '{{^ decode }} hide{{/ decode }}' : ( $state !== 'encoded'  ? " hide" : null ); ?>">
					<a href="#/decode/<?php echo $ajax ? "{{ nevecKey }}" : $nevecKey; ?>" class="btn"><?php _e( get_field( 'decode_transmission_button', 'options' ) ) ?></a>
				</div>
			</div>
		</section>
		<aside class="share">
			<ul>
				<li class="fb"><a href="#share" data-share-caption="<?php echo $ajax ? "{{& headline }}" : $headline; ?>" data-share-description="<?php echo $ajax ? "{{ text }}" : $text; ?>" data-share-link="<?php echo $ajax ? "{{ url }}" : $url; ?>" data-share-img="<?php echo $ajax ? "{{ image }}" : $image; ?>">Facebook</a></li>
				<li class="tw"><a href="//twitter.com/intent/tweet?url=<?php echo $ajax ? "{{ url }}" : urlencode( $url ) ; ?>&amp;via=lostplanet&amp;text=<?php echo $ajax ? "{{& headline }}" : $headline; ?>&amp;hashtags=LP3,CoronisDiary">Twitter</a></li>
				<li class="gp"><a href="//plus.google.com/share?url=<?php echo $ajax ? "{{ url }}" : urlencode( $url ) ; ?>" onclick="javascript:window.open(this.href,'','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">Google+</a></li>
			</ul>
		</aside>
	</article>
