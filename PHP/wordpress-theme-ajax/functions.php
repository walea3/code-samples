<?php

add_action('init', 'myStartSession', 1);
// add_action('wp_logout', 'myEndSession');
// add_action('wp_login', 'myEndSession');

function myStartSession() {
    if(!session_id()) {
        session_start();
    }

	unset( $_SESSION[ 'coronisUser' ] );
   $_SESSION[ 'transmissionQueue' ] = 1;
   $_SESSION[ 'transmissionLimit' ] = 5;

	if (!isFacebook()){}
}

function myEndSession() {
	removeGuest();
    session_destroy();
}

add_action( 'wp_ajax_nopriv_disconnect', 'myEndSession' );
add_action( 'wp_ajax_disconnect', 'myEndSession' );

function transmissions_only( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'post_type', 'transmission' );
    }
}
add_action( 'pre_get_posts', 'transmissions_only' );

function bootCoronis() {
	$nonce = $_POST['nevecID'];
	unset($_POST['nevecID']);

	if ( $nonce && wp_verify_nonce( $nonce, 'nevec-coronis-diary' ) && $_POST['start'] ) {
		//header( "Content-Type: application/json" );
		$transmissions = getContent( 'transmissions' );
		$stages = getContent( 'stages' );
		$user = getUser( true );

		echo json_encode( array( 'stages' => $stages, 'transmissions' => $transmissions, 'user' => $user ) );
		exit;
	}
}

add_action( 'wp_ajax_nopriv_boot_coronis', 'bootCoronis' );  
add_action( 'wp_ajax_boot_coronis', 'bootCoronis' );

function getContent ( $type = null, $info = null ) {
	global $wpdb; $content = array();
	$currentLang = isset( $_GET['lang'] ) ? $_GET['lang'] : qtrans_getLanguage();

	$type = $type ? $type : $_POST[ 'type' ];
	$nonce = isset( $_POST[ 'nevecID' ] ) ? $_POST[ 'nevecID' ] : false;
	unset( $_POST[ 'nevecID' ] );

	if ( $nonce && ! wp_verify_nonce( $nonce, 'nevec-coronis-diary' ) )
		die();

	switch ( $type ) {
		case 'stages':
			$cachedStages = wp_cache_get( $currentLang . '_coronisStages' );
			$cachedTransmissions = wp_cache_get( $currentLang . '_coronisTransmissions' );

			if ( $cachedStages && !isset( $info[ 'reload' ] ) ) {
				$content = $cachedStages;
				break;
			}

			// if ( isset($_SESSION[ $currentLang . '_coronisStages' ]) && !isset( $info[ 'reload' ] ) ) {
			// 	$content = $_SESSION[ $currentLang . '_coronisStages' ];
			// 	break;
			// }

			$cats = get_term_children( get_term_by( 'slug', 'stages', 'category' )->term_id, 'category' );
			$i = 1;
			foreach( $cats as $c ) {
				$cat = get_term( $c, 'category' );
				$status = array( 
					'new' => array( 'type'=> 'new', 'count' => 0 ),
					'read' => array( 'type'=> 'read', 'count' => 0 ),
					'locked' => array( 'type'=> 'locked', 'count' => 0 ),
					'special' => array( 'type'=> 'special', 'count' => 0 )
				);

				if ( $cachedTransmissions === false || ( $transmissionsList = $cachedTransmissions ) === false ) {
					$args = array(
						'post_type' => 'transmissions',
						'post_status' => array( 'publish', 'future' ),
						'cat' => $cat->term_id,
						'posts_per_page' => -1,
						'orderby' => 'date',
						'order' => 'ASC'
					);

					$transmissions = new WP_Query( $args );
					$transmissionsList = getTransmissionContent( $transmissions );
					wp_reset_postdata();
				}

				if ( is_array( $transmissionsList ) ) {
					foreach ($transmissionsList as $key => $transmission) {
						if ( $transmission['stage'] === $cat->slug ) {
							if ( ( is_array( $transmission[ 'type' ] ) && in_array( 'special' , $transmission[ 'type' ] ) ) || $transmission[ 'type' ] === 'special' ) {
								$status['special']['count'] += 1;
							} else if ( in_array( $transmission['state'], array( 'new' ) ) ) {
								$status['new']['count'] += 1;
							} else if ( in_array( $transmission['state'], array( 'read' ) ) ) {
								$status['read']['count'] += 1;
							}

							if ( in_array( $transmission['state'], array( 'locked', 'expired' ) ) ) {
								$status['locked']['count'] += 1;
							}
						} else {
							unset( $transmissionsList[ $key ] );
						}
					}
				}

				$content[ $i ] = array(
					'id' => $i,
					'name' => $cat->slug,
					'title' => __( $cat->name ),
					'status' => $status,
					'transmissions' => $transmissionsList
				);
				$i++;
			}
			wp_cache_set( $currentLang . '_coronisStages', $content );
			break;
		case 'lang':
			$fields = get_fields( 'options' );
				foreach( $fields as $field_name => $value ) {
					$field_key = get_option( '_' . $field_name );
					$field_name = str_replace( 'options_', '', $field_name );
					$field_meta = $wpdb->get_row($wpdb->prepare(
						"
						SELECT meta_value 
						FROM $wpdb->postmeta 
						WHERE meta_key = %s
						", 
						$field_key
					), ARRAY_A );
					$field_info = maybe_unserialize( $field_meta[ 'meta_value' ] );
					$field = get_field( $field_key, 'options' );
					$content[ $field_name ] = $field_info[ 'type' ] === 'textarea' ? wpautop( __( $field ) ) : __( $field );
				}
			break;
		case 'transmissions':
			$cachedTransmissions = wp_cache_get( $currentLang . '_coronisTransmissions' );

			if ( $cachedTransmissions && !isset( $info[ 'reload' ] ) ) {
				$content = $cachedTransmissions;
				break;
			}

			// if ( isset($_SESSION[ $currentLang . '_coronisTransmissions' ]) && !isset( $info[ 'reload' ] ) ) {
			// 	$content = $_SESSION[ $currentLang . '_coronisTransmissions' ];
			// 	break;
			// }

			$args = array(
				'post_type' => 'transmissions',
				'post_status' => array( 'publish', 'future' ),
				'posts_per_page' => -1,
				'orderby' => 'date',
				'order' => 'ASC'
			);
			$transmissions = new WP_Query( $args );
			$content = getTransmissionContent( $transmissions );
			wp_cache_set( $currentLang . '_coronisTransmissions', $content );
			wp_reset_postdata();
			break;
		case 'transmission':
			$key = isset( $info[ 'nevecKey' ] ) ? $info[ 'nevecKey' ] : $_POST[ 'nevecKey' ];
			$args = array(
			  'post_type' => 'transmissions',
			  'post_status' => array( 'publish', 'future' ),
			  'name' => $key,
			  'posts_per_page' => 1
			);

			$transmissions = new WP_Query( $args );
			$data = getTransmissionContent( $transmissions, false );
			wp_reset_postdata();

			if ( isset( $info[ 'nevecKey' ] ) ) {
				$results = '';
				if ( isset( $info['data'] ) ) {
					$content = $data;
				} else {
					ob_start();
						include( locate_template( 'partials/transmission-content.php' ) );
						$results .= ob_get_contents();
					ob_end_clean();
					$content = str_replace( array( "\r\n", "\r", "\n" ), "", $results );
				}
			} else if ( isset( $_POST[ 'nevecKey' ] ) ) {
				if ( $nonce ) {
					//header( "Content-Type: application/json" );
					echo json_encode( $data );
				}
				exit;
			}
			break;
	}

	return $content;
}

add_action( 'wp_ajax_nopriv_get_content', 'getContent' );  
add_action( 'wp_ajax_get_content', 'getContent' );

function transmissionState( $post ) {
	$state = 'available';
	$date = DateTime::createFromFormat( 'Y-m-d H:i:s', $post->post_date_gmt );
	$post_date = $date->getTimestamp();
	$date = new DateTime( '3 days ago' );
	$till = $date->getTimestamp();
	$date = new DateTime();
	$today = $date->getTimestamp();
	$user = isset( $_SESSION[ 'coronisUser' ] ) && count( $_SESSION[ 'coronisUser' ][ 'profile' ] ) > 0 ? $_SESSION[ 'coronisUser' ] : getUser();

	$normalTransmissions = isset( $user[ 'profile' ][ 'normal' ] ) ? $user[ 'profile' ][ 'normal' ] : array();
	$specialTransmissions = isset( $user[ 'profile' ][ 'special' ] ) ? $user[ 'profile' ][ 'special' ] : array();

	$userTransmissions = $normalTransmissions + $specialTransmissions;

	if ( $post->post_status == 'future' ) {
		$state = 'locked';
	} else if ( $post->post_status == 'publish' && has_category('expired', $post->ID) ) {
		$state = 'expired';
	} else if ( $post->post_status == 'publish' && $post_date >= $till && $post_date <= $today ) {
		$state = 'new';
	}
	if ( ( isset($user[ 'profile' ][ 'normal' ][ $post->post_name ]) && $user[ 'profile' ][ 'normal' ][ $post->post_name ]['state'] === 'read' ) || ( isset($user[ 'profile' ][ 'special' ][ $post->post_name ]) && $user[ 'profile' ][ 'special' ][ $post->post_name ]['state'] === 'read' ) ) {
		$state = 'read';
	}

	return $state;
}

function getTransmissionContent( $transmissions, $filter = true ) {
	$content = array();
	$user = isset( $_SESSION[ 'coronisUser' ] ) ? $_SESSION[ 'coronisUser' ] : getUser();

	while ( $transmissions->have_posts() ) : $transmissions->the_post();
		$cats = get_the_terms( get_the_ID(), 'category' );
		$type = array(); 
		$stage = '';
		$state = transmissionState( $transmissions->post );
		$msgType = get_field( 'type' );
		$totalCount = $transmissions->post_count;
		$read = ( !$filter || ( ( isset($user[ 'profile' ][ 'normal' ][ $transmissions->post->post_name ]) && $user[ 'profile' ][ 'normal' ][ $transmissions->post->post_name ]['state'] === 'read' ) || ( isset($user[ 'profile' ][ 'special' ][ $transmissions->post->post_name ]) && $user[ 'profile' ][ 'special' ][ $transmissions->post->post_name ]['state'] === 'read' ) ) ? true : false );
		
		foreach ( $cats as $cat ) {
			$parent = get_term( $cat->parent, 'category' );

			if ( ( $cat->slug === 'expired' && !$read ) || $parent->slug === 'transmissions' ) {
				$type[] = $cat->slug;
			} else if ( $parent->slug === 'stages' ) {
				$stage = $cat->slug;
			}
		}

		$content[ $transmissions->post->post_name ] = array(
			'nevecID' => $transmissions->post->ID,
			'nevecKey' => $transmissions->post->post_name,
			'position' => ( $transmissions->current_post + 1 ),
			'title' => __( get_field( 'transmission_received', 'option' ) ),
			'headline' => __( get_the_title() ),
			'messageType' => $msgType,
			'state' => $state,
			'statusMsg' => __( get_field( ( $state === 'new' ? 'new_entry' : 'available' ), 'option' ) ),
			'type' => $type,
			'stage' => $stage,
			'transmissionCount' => ( $transmissions->current_post + 1 ) . '/' . $totalCount,
			$msgType => ( in_array( $state, array( 'expired', 'locked' ) ) || in_array( 'encoded', $type ) ) && !$read ? null 
				: ( $msgType === "text" ? wpautop( __( get_field( $msgType . '_message' ) ) ) 
					: __( get_field( $msgType . '_message' ) ) ),
			'image' => ( in_array( $state, array( 'expired', 'locked' ) ) ||  in_array( 'encoded', $type ) ) && !$read ? null : get_field( 'image' ),
			'url' => get_permalink()
		);

		if ( $msgType !== "text" ) {
			$content[ $transmissions->post->post_name ][ 'text' ] = wpautop( __( get_field( 'text_message' ) ) );
		}

		if ( $state === 'expired' && !$read ) {
			$content[ $transmissions->post->post_name ][ 'title' ] = __( get_field( 'transmission_unavailable', 'options' ) );
			$content[ $transmissions->post->post_name ][ 'text' ] = wpautop( __( get_field( 'transmission_expired_info', 'options' ) ) );
			$content[ $transmissions->post->post_name ][ 'statusMsg' ] = '';
		} else if ( $state === 'locked' ) {
			$today = new DateTime();
			$date_available = DateTime::createFromFormat( 'Y-m-d H:i:s', $transmissions->post->post_date_gmt );
			$content[ $transmissions->post->post_name ][ 'title' ] = __( get_field( 'transmission', 'options' ) ) . ' ' . sprintf( __( get_field( 'available_in_x_days', 'options' ) ), $today->diff( $date_available )->format( "%a" ) );
			$content[ $transmissions->post->post_name ][ 'headline' ] = __( get_field( 'transmission_unavailable', 'options' ) );
			$content[ $transmissions->post->post_name ][ 'text' ] = sprintf( wpautop( __( get_field( in_array( 'special', $type ) ? 'special_transmission_available_on_x' : 'transmission_unavailable_info', 'options' ) ) ), $date_available->format('d-m-Y') );
			$content[ $transmissions->post->post_name ][ 'statusMsg' ] = '';
		} else if ( in_array( 'encoded', $type ) ) {
			if ( !$read && $filter ) {
				$content[ $transmissions->post->post_name ][ 'headline' ] = __( get_field( 'encoded_message_title', 'options' ) );
				$content[ $transmissions->post->post_name ][ 'text' ] = wpautop( __( get_field( 'encoded_message', 'options' ) ) );
			} else {
				$types = $content[ $transmissions->post->post_name ][ 'type' ];
				$content[ $transmissions->post->post_name ][ 'type' ] = array();
				foreach ($types as $value) {
					if ( $value !== 'encoded' )
						$content[ $transmissions->post->post_name ][ 'type' ][] = $value;
				}
			}
		}

	endwhile;

	return $content;
}

function checkTransmission() {
	if ( is_singular( 'transmissions' ) ) {
		global $wp_query, $post;
		transmissionOG( $wp_query, $post->post_name );
	}
}
add_action( 'wp_head', 'checkTransmission' );

function socialCount( $url ) {
	$type = $url ? $url : $_POST[ 'url' ];
	$count = array();
	$nonce = isset( $_POST[ 'nevecID' ] ) ? $_POST[ 'nevecID' ] : false;
	unset( $_POST[ 'nevecID' ] );

	if ( $nonce && ! wp_verify_nonce( $nonce, 'nevec-coronis-diary' ) )
		die();

	return $count;
}

add_action( 'wp_ajax_nopriv_social_count', 'socialCount' );  
add_action( 'wp_ajax_social_count', 'socialCount' );

function getUser ( $create = false ) {
	global $facebook_loader, $facebook, $wpdb;
	$guestMode = false;

	if ( !isset( $facebook ) && !( isset( $facebook_loader ) && $facebook_loader->load_php_sdk() ) )
		$guestMode = true;

	$nonce = isset( $_POST[ 'nevecID' ] ) ? $_POST[ 'nevecID' ] : false;
	unset( $_POST[ 'nevecID' ] );

	if ( $nonce && ! wp_verify_nonce( $nonce, 'nevec-coronis-diary' ) )
		die();

	if ( ! class_exists( 'Facebook_User' ) )
		require_once( $facebook_loader->plugin_directory . 'facebook-user.php' );

	if ( !isset( $_POST[ 'user' ] ) ) {
		if ( !class_exists( 'Facebook_User' ) || !( $fbUser = Facebook_User::get_current_user() ) ) {
			$guestMode = true;
			$fbUser = createGuest();
		}
	} else {
		$fbUser = $_POST[ 'user' ];
		$create = true;
	}

	if ( $fbUser && ( $user = $wpdb->get_results( $wpdb->prepare( 
			"
			SELECT post_id 
			FROM wp_postmeta
			WHERE meta_key = %s
			AND meta_value LIKE %s
			", 'facebook_id', $fbUser[ 'id' ]
		) ) )
	) {
		//get profile data
		$profile = get_fields( $user[ 0 ]->post_id );
		$fbUser[ 'nevecID' ] = $user[ 0 ]->post_id;
		$fbUser[ 'profile' ] = userProfile( $user[ 0 ]->post_id );

	} else if ( $fbUser && $create ) {
		//store user
		$post_id = -1; $author_id = 1; $slug = $fbUser[ 'username' ];
		$title = $fbUser[ 'name' ];
		if( null == get_page_by_title( $title ) ) {
			$post_id = wp_insert_post(
				array(
					'comment_status'	=>	'closed',
					'ping_status'		=>	'closed',
					'post_author'		=>	$author_id,
					'post_name'			=>	$slug,
					'post_title'		=>	$title,
					'post_status'		=>	'publish',
					'post_type'			=>	'user_profiles'
				)
			);
			$fbUser[ 'nevecID' ] = $post_id;
			$fbUser[ 'profile' ] = userProfile( $post_id );

			update_field( 'field_1', $fbUser[ 'id' ], $post_id );
			update_field( 'field_2', $fbUser[ 'first_name' ], $post_id );
			update_field( 'field_3', $fbUser[ 'last_name' ], $post_id );
			update_field( 'field_4', $fbUser[ 'email' ], $post_id );
			update_field( 'field_73', array(
				array( 
					"acf_fc_layout" => 'normal',
					'transmissions' => array()
				),
				array( 
					"acf_fc_layout" => 'special',
					'transmissions' => array()
				)
			), $post_id );
		}
	}

	$fbUser[ 'picture' ] = strtolower( $fbUser[ 'first_name' ] ) !== 'guest' ? 'https://graph.facebook.com/' . $fbUser[ 'username' ] . '/picture' : get_template_directory_uri() . '/img/thumb.jpg';

	$_SESSION[ 'coronisUser' ] = $fbUser;

	if ( $nonce ) {
		//header( "Content-Type: application/json" );
		echo json_encode( $fbUser );
		exit;
	}

	return $fbUser;
}
add_action( 'wp_ajax_nopriv_get_user', 'getUser' );
add_action( 'wp_ajax_get_user', 'getUser' );

function createGuest() {
	$today = new DateTime();
	$_SESSION['nevecGuest'] = isset( $_SESSION['nevecGuest'] ) ? $_SESSION['nevecGuest'] : array(
		'id' => session_id() . '_' . $today->getTimestamp(),
		'username' => 'nevecGuest'  . '_' . session_id(),
		'name' => 'Guest User',
		'first_name' => 'Guest',
		'last_name' => 'User',
		'email' => 'guest_' . session_id() . '@coronis.nevec'
	);
	return $_SESSION['nevecGuest'];
}

function removeGuest() {
	if ( !isset( $_SESSION['nevecGuest'] ) )
		return true;

	$guest = $_SESSION['nevecGuest'];
	unset( $_SESSION['nevecGuest'] );
}

function userProfile ( $id ) {
	$currentLang = isset( $_GET['lang'] ) ? $_GET['lang'] : qtrans_getLanguage();
	$cachedTransmissions = wp_cache_get( $currentLang . '_coronisTransmissions' );
	
	$transmissionsList = $cachedTransmissions ? $cachedTransmissions : getContent( 'transmissions' );
	$profile = array( 'read' => 0, 'total' => count( $transmissionsList ) );

	foreach ($transmissionsList as $key => $transmission) {
		$type = in_array( 'special', $transmission[ 'type' ] ) ? 'special' : 'normal';
		$profile[ $type ][ $key ] = $transmission;
	}
	
	if ( $id ) {
		while( has_sub_field( 'field_73', $id ) )
		{ 
			$transmissions = get_sub_field( 'transmissions', $id );

			if ( is_array( $transmissions ) ) {
				foreach( $transmissions as $transmission ) {
					$profile[ get_row_layout() ][ $transmission->post_name ]['state'] = 'read';
					$profile[ 'read' ] += 1;
				}
			}
		}
	}

	//var_dump( $profile );
	//$_SESSION[ $currentLang . '_coronisTransmissions'] = $profile['special'] + $profile['normal'];
	return $profile;
}

function userProgress ( $transmissions = false ) {
	$nonce = isset( $_POST[ 'nevecID' ] ) ? $_POST[ 'nevecID' ] : false;
	unset( $_POST[ 'nevecID' ] );

	$currUser = isset( $_SESSION[ 'coronisUser' ] ) ? $_SESSION[ 'coronisUser' ] : getUser();

	$fbUser = !( $fbUser = $currUser ) ? $_POST[ 'user' ] : $fbUser;
	$transmissions = $transmissions ? $transmissions : $_POST[ 'transmissions'];
	
	if ( $nonce && ! wp_verify_nonce( $nonce, 'nevec-coronis-diary' ) || !$fbUser )
		die();

	if ( $fbUser && $transmissions ) {
		$value = array();
		$unlocked = get_field( 'field_73', $fbUser[ 'nevecID' ] );

		if ( !$unlocked || count( $unlocked ) == 0 ) {
			update_field( 'field_73', array(
				array( 
					"acf_fc_layout" => 'normal',
					'transmissions' => array()
				),
				array( 
					"acf_fc_layout" => 'special',
					'transmissions' => array()
				)
			), $fbUser[ 'nevecID' ] );
			$unlocked = get_field( 'field_73', $fbUser[ 'nevecID' ] );
		}

		foreach ( $transmissions as $transmission ) {
			$index = ( is_array( $transmission[ 'type' ] ) && in_array( $unlocked[ 0 ][ 'acf_fc_layout' ], $transmission[ 'type' ] ) ) || $unlocked[ 0 ][ 'acf_fc_layout' ] === $transmission[ 'type' ] ? 0 : 1;

			$typeTransmissions = $unlocked[ $index ]['transmissions'];
			$isExists = false;

			if ( is_array( $typeTransmissions ) ) {
				foreach ($typeTransmissions as $t) {
					if ( $t->ID == $transmission[ 'nevecID' ] ) {
						$isExists = true; 
						break;
					}
				}
			}

			if ( !$isExists ) {
				$unlocked[ $index ]['transmissions'][] = $transmission[ 'nevecID' ];
			}
		}

		update_field( 'field_73', $unlocked, $fbUser[ 'nevecID' ] );
		$fbUser[ 'profile' ] = userProfile( $fbUser[ 'nevecID' ] );
	}

	if ( $nonce ) {
		//header( "Content-Type: application/json" );
		echo json_encode( $fbUser );
		exit;
	}

	return $fbUser;
}
add_action( 'wp_ajax_nopriv_user_progress', 'userProgress' );
add_action( 'wp_ajax_user_progress', 'userProgress' );


function transmissionOG( $post, $title = null ) {
	$transmission = getTransmissionContent( $post );
	if ( isset( $transmission[$title] ) && ( $t = $transmission[$title] ) ) {
		global $CoronisView;
		$CoronisView = "{$t['stage']}/{$t['nevecKey']}";
		$transmissionTitle = $post->queried_object->post_title;
		add_filter('fb_meta_tags', function( $metatags ) use ( $t, $transmissionTitle ) {
			$metatags['http://ogp.me/ns#title'] = $transmissionTitle;
			$metatags['http://ogp.me/ns#type'] = 'coronis_diary:transmission';
			$metatags['http://ogp.me/ns#url'] = $t['url'];
			$metatags['http://ogp.me/ns#image'] = isset( $t['image'] ) ? $t['image'] : null;
			return $metatags;
		});
	}
}

function fb_init_opts ( $args ) {
	$args[ 'frictionlessRequests' ] = true;
	return $args;
}

function fb_init_extras ( $args ) {
	return 'Coronis.FB.ready = true;';
}
add_filter( 'facebook_jssdk_init_options', 'fb_init_opts' );
add_filter( 'facebook_jssdk_init_extras', 'fb_init_extras' );

function isFacebook() {
	global $facebook_loader, $facebook, $like_status, $q_config, $isFB;
	$isFB = false;

	if ( ! isset( $facebook ) && ! ( isset( $facebook_loader ) && $facebook_loader->load_php_sdk() ) )
			return $isFB;

	$signed_request = ( isset($_REQUEST['signed_request'] ) ? parse_signed_request( $_REQUEST['signed_request'], $facebook_loader->credentials['app_secret'] ) : ( $_SESSION['signed_request'] ? parse_signed_request( $_SESSION['signed_request'], $facebook_loader->credentials['app_secret'] ) : null ) );
	
	if ( isset( $_GET['fb'] ) && $signed_request ) {
		if( isset( $_REQUEST['signed_request'] ) ) {
			$_SESSION['signed_request'] = $_REQUEST['signed_request'];
		}
		if( isset( $signed_request['page'] ) ) {
			$like_status = $signed_request['page']['liked'];
		}
		$fb_country = $signed_request['user']['country'];
		$fb_locale = $signed_request['user']['locale'];
		$app_data = isset( $signed_request['app_data'] ) ? $signed_request['app_data'] : null;
		$fb_locale_country = substr( $fb_locale, 0,2 );
		$fb_locale_2 = substr( $fb_locale, 3,4 );

		if ( $fb_locale_country === "en" ) {
			$fb_locale_country = strtolower( $fb_locale_2 == "GB" ? "en" : $fb_locale_2 );
		} else if ( $fb_locale_country === "pt" || $fb_locale_country === "fr" ) {
			$fb_locale_country = strtolower( $fb_locale_2 );
		}
		
		if( !isset( $_GET['lang'] ) && qtrans_isEnabled( $fb_locale_country ) ) { 
			$q_config['language'] = apply_filters( 'qtranslate_language', $fb_locale_country ); 
		}

		$isFB = true;
	}
	return $isFB;
}

function getData( $url ) {
	$ch = curl_init();
    $timeout = 5;
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
    curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
    $data = curl_exec($ch);

    //var_dump( curl_error( $ch ) ); var_dump( $ch ); var_dump( $data ); exit;
    curl_close($ch);
    return $data;
}

function parse_signed_request($signed_request, $secret) {
	list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

	// decode the data
	$sig = base64_url_decode($encoded_sig);
	$data = json_decode(base64_url_decode($payload), true);

	if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
		error_log('Unknown algorithm. Expected HMAC-SHA256');
		return null;
	}

	// check sig
	$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
	if ($sig !== $expected_sig) {
		error_log('Bad Signed JSON signature!');
		return null;
	}

	return $data;
}

function base64_url_decode($input) {
	return base64_decode(strtr($input, '-_', '+/'));
}

