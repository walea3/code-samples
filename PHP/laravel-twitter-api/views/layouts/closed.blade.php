<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>{{ Config::get('application.app_title') }}</title>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="cleartype" content="on">

	<meta property="og:title" content="{{ Config::get('application.app_title') }}" />
	<meta property="og:type" content="website">
	<meta property="og:url" content="{{ URL::base() }}">
	<meta property="og:image" content="{{ URL::to_asset('img/thumb.jpg') }}">
	<meta property="og:site_name" content="{{ Config::get('application.app_title') }}">
	<meta property="fb:admins" content="100003102645313">

	<link rel="shortcut icon" href="{{ URL::to_asset('img/favicon.jpg') }}">
	{{ HTML::style('css/bootstrap.css') }}
	{{ HTML::style('css/style.css') }}
	<!--[if (lt IE 9) & (!IEMobile)]>
	{{ HTML::script('js/lib/selectivizr-min.js') }}
	<![endif]-->
	{{ HTML::script('js/lib/modernizr-2.5.3-min.js') }}
</head>
<body>
	<div id="wrapper">
		<div id="container">
			<header role="banner">
				<h1><img src="{{ URL::to_asset('img/looper_title.png') }}" alt="Looper" /></h1>
				<h2>Enter the TK Challenge!</h2>
				<p class="info" data-info=" "></p>
			</header>
			<div id="main">
				@yield('content')
			</div>
		</div>
		<footer role="base">
			<p>&copy; {{ Date("Y") }} Looper Distribution, LLC. All Rights Reserved. <br />Packaging &copy; Entertainment One UK Limited 2013. <br class="mobileOnly"/> All Rights Reserved.</p>
			<a href="mailto:hi@crushldn.com?Subject={{ rawurlencode(Config::get('application.app_title')) }}" target="_blank">Help</a> | <a href="{{ URL::to_asset('terms_conditions.pdf') }}" target="_blank">Terms &amp; Conditions</a>
		</footer>
	</div>
	<div id="fb-root"></div>
	@section('scripts')
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><!--Load jQuery-->
		<script>window.jQuery || document.write(unescape('%3Cscript src="{{ URL::to_asset('js/lib/jquery-1.7.2.min.js') }}"%3E%3C/script%3E'))</script>
		<script type="text/javascript" src="//platform.twitter.com/anywhere.js?id={{ Config::get('twitter::twitter.key') }}&v1.1"></script>
		<script type="text/javascript">
			window.TKc = window.TKc || {};
			TKc.config = TKc.config || { 
				baseUrl: '{{ URL::base() }}',
				assetUrl: '{{ URL::to_asset('') }}',
				apiUrl: '{{ URL::to_route('api') }}',
				FB: { 
					id: '{{ Config::get('facebook.app_id') }}', 
					icon: '{{ URL::to_asset('img/thumb.jpg') }}'
				},
				TW: { tag: '{{ Config::get('twitter::twitter.tag') }}' },
				user: {{ json_encode($user) }},
				sessionEnd: new Date('{{ $sessionEnd }}'),
				compState: '{{ Config::get('application.state') }}'
			};
			TKc.config.user.friends = {{ isset($user['tw_handle']) && Cache::has($user['tw_handle'] . '_friends') ? Cache::get($user['tw_handle'] . '_friends') : '[]'}};
			
			//Tweet button
			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
			
			//Google+
		  	(function() {
		    	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    	po.src = 'https://apis.google.com/js/plusone.js';
		    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  	})();

		  	//Facebook
		  	(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId={{ Config::get('facebook.app_id') }}";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));

			//Google Analytics
			var _gaq=[["_setAccount","{{ Config::get('google.gacode') }}"],["_trackPageview"]];
			(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];g.async=1;
			g.src=("https:"==location.protocol?"//ssl":"//www")+".google-analytics.com/ga.js";
			s.parentNode.insertBefore(g,s)}(document,"script"));
		</script>
		{{ HTML::script('js/plugins.js') }}
		{{ HTML::script('js/tkc.js') }}
		{{ HTML::script('js/helper.js') }}
	@yield_section
</body>
</html>
