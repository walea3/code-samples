@layout('layouts/closed')

@section('content')
	<div id="tk"><img id="coin" src="{{ URL::to_asset('img/coin.png') }}" alt=""><img id="hand" src="{{ URL::to_asset('img/tk_hand.png') }}" alt=""><img id="progress" src="{{ URL::to_asset('img/indicator.png') }}" alt=""></div>

	<section class="view" data-view="start">
		<a class="btn btn-large closed">Competition closed</a>
	</section>
@endsection