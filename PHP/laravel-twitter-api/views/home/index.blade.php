@layout('layouts/main')

@section('content')
	<div id="countdown"></div>
	<div id="tk"><img id="coin" src="{{ URL::to_asset('img/coin.png') }}" alt=""><img id="hand" src="{{ URL::to_asset('img/tk_hand.png') }}" alt=""><img id="progress" src="{{ URL::to_asset('img/indicator.png') }}" alt=""></div>

	<section class="view" data-view="start">
		<a href="{{ URL::to('twitterAuth'); }}" class="btn tw">Login with Twitter <strong>to play</strong> <img src="{{ URL::to_asset('img/tw_btn.png') }}" alt=""> <span>(account required)</span></a>
		<ul class="share mobileHide">
			<li class="fb"><fb:like href="{{ URL::base() }}" data-layout="button_count" send="false" width="120" show_faces="false"></fb:like></li>
			<li class="tw"><a href="https://twitter.com/share" class="twitter-share-button" data-url="{{ URL::base() }}" data-text="..." data-hashtags="{{ Config::get('twitter::twitter.tag') }}" data-via="LooperUK">Tweet</a></li>
		</ul>
	</section>
	<section class="view" data-view="register" data-info="Please register your details below for the promotion.">
		<div class="social"><a href="//facebook.com/looperuk" class="fb" target="_blank">Facebook</a><a href="//twitter.com/looperuk" class="tw" target="_blank">Twitter</a></div>
		<div class="box">
			<label for="name">Full Name
				<input type="text" class="input-block-level" id="fullname" name="fullname" required>
			</label>
			<label for="name">E-Mail
				<input type="email" class="input-block-level" id="email" name="email" required>
			</label>
			<label for="name">Age
				<input type="text" class="input-block-level" id="age" name="age" required>
			</label>
			<label for="country">Country
				<select name="country" id="country" class="input-block-level" required>
	                <option value="">Select Country </option>
	                <option value="CA">Canada</option>
	                <option value="DE">Germany</option>
	                <option value="IT">Italy </option>
	                <option value="NL">Netherlands </option>
	                <option value="PT">Portugal </option>
	                <option value="ES">Spain </option>
	                <option value="GB">United Kingdom </option>
	                <option value="US">United States </option>
	                <option value="">----------------------</option>
	                <option value='AL'>Albania</option>
	                <option value='AR'>Argentina</option>
	                <option value='AM'>Armenia</option>
	                <option value='AU'>Australia</option>
	                <option value='AT'>Austria</option>
	                <option value='AZ'>Azerbaijan</option>
	                <option value='BS'>Bahamas</option>
	                <option value='BH'>Bahrain</option>
	                <option value='BY'>Belarus</option>
	                <option value='BE'>Belgium</option>
	                <option value='BL'>Belize</option>
	                <option value='BM'>Bermuda</option>
	                <option value='BO'>Bolivia</option>
	                <option value='BA'>Bosnia</option>
	                <option value='BR'>Brazil</option>
	                <option value='BG'>Bulgaria</option>
	                <option value='CM'>Cameroon</option>
	                <option value='AB'>Canada - Alberta</option>
	                <option value='BC'>Canada - British Columbia</option>
	                <option value='MB'>Canada - Manitoba</option>
	                <option value='NB'>Canada - New Brunswick</option>
	                <option value='ND'>Canada - Newfoundland</option>
	                <option value='NW'>Canada - Northwest Territories</option>
	                <option value='NS'>Canada - Nova Scotia</option>
	                <option value='NT'>Canada - Nunavut</option>
	                <option value='ON'>Canada - Ontario</option>
	                <option value='PE'>Canada - Prince Edward Island</option>
	                <option value='QC'>Canada - Quebec</option>
	                <option value='SK'>Canada - Saskatchewan</option>
	                <option value='YN'>Canada - Yukon</option>
	                <option value='CL'>Chile</option>
	                <option value='CN'>China</option>
	                <option value='CO'>Colombia</option>
	                <option value='CR'>Costa Rica</option>
	                <option value='HR'>Croatia</option>
	                <option value='CZ'>Czech Republic</option>
	                <option value='DK'>Denmark</option>
	                <option value='DR'>Dominican Republic</option>
	                <option value='EC'>Ecuador</option>
	                <option value='EG'>Egypt</option>
	                <option value='ER'>El Salvador</option>
	                <option value='EE'>Estonia</option>
	                <option value='FJ'>Fiji</option>
	                <option value='FI'>Finland</option>
	                <option value='GE'>Georgia</option>
	                <option value='DE'>Germany</option>
	                <option value='GR'>Greece</option>
	                <option value='GT'>Guatemala</option>
	                <option value='HN'>Honduras</option>
	                <option value='HK'>Hong Kong</option>
	                <option value='HU'>Hungary</option>
	                <option value='IS'>Iceland</option>
	                <option value='IN'>India</option>
	                <option value='ID'>Indonesia</option>
	                <option value='IE'>Ireland</option>
	                <option value='IL'>Israel</option>
	                <option value='IT'>Italy</option>
	                <option value='JM'>Jamaica</option>
	                <option value='JP'>Japan</option>
	                <option value='KZ'>Kazakhstan</option>
	                <option value='KE'>Kenya</option>
	                <option value='LV'>Latvia</option>
	                <option value='LB'>Lebanon</option>
	                <option value='LS'>Lesotho</option>
	                <option value='LI'>Liechtenstein</option>
	                <option value='LT'>Lithuania</option>
	                <option value='LU'>Luxembourg</option>
	                <option value='MK'>Macedonia</option>
	                <option value='MY'>Malaysia</option>
	                <option value='MT'>Malta</option>
	                <option value='MU'>Mauritius</option>
	                <option value='MX'>Mexico</option>
	                <option value='MD'>Moldova</option>
	                <option value='MO'>Mongolia</option>
	                <option value='ME'>Montenegro</option>
	                <option value='NP'>Nepal</option>
	                <option value='NL'>Netherlands</option>
	                <option value='NZ'>New Zealand</option>
	                <option value='NC'>Nicaragua</option>
	                <option value='NO'>Norway</option>
	                <option value='OT'>Other</option>
	                <option value='PW'>Palau</option>
	                <option value='PA'>Panama</option>
	                <option value='PG'>Papua New Guinea</option>
	                <option value='PY'>Paraguay</option>
	                <option value='PE'>Peru</option>
	                <option value='PH'>Philippines</option>
	                <option value='PL'>Poland</option>
	                <option value='PT'>Portugal</option>
	                <option value='PR'>Puerto Rico</option>
	                <option value='RO'>Romania</option>
	                <option value='RU'>Russia</option>
	                <option value='WS'>Samoa</option>
	                <option value='RS'>Serbia</option>
	                <option value='SG'>Singapore</option>
	                <option value='SI'>Slovenia</option>
	                <option value='SB'>Solomon Islands</option>
	                <option value='ZA'>South Africa</option>
	                <option value='KR'>South Korea</option>
	                <option value='ES'>Spain</option>
	                <option value='LK'>Sri Lanka</option>
	                <option value='SZ'>Swaziland</option>
	                <option value='SE'>Sweden</option>
	                <option value='CH'>Switzerland</option>
	                <option value='TW'>Taiwan</option>
	                <option value='TZ'>Tanzania</option>
	                <option value='TH'>Thailand</option>
	                <option value='TO'>Tonga</option>
	                <option value='TT'>Trinidad &amp; Tobago</option>
	                <option value='TR'>Turkey</option>
	                <option value='UA'>Ukraine</option>
	                <option value='GB'>United Kingdom</option>
	                <option value='US'>United States</option>
	                <option value='UR'>Uruguay</option>
	                <option value='UZ'>Uzbekistan</option>
	                <option value='VE'>Venezuela</option>
	                <option value='ZW'>Zimbabwe</option>
	            </select>
			</label>
			<label for="terms">
				<input type="checkbox" id="terms" name="terms" required>
				I accept the <a href="{{ URL::to_asset('terms_conditions.pdf') }}" target="_blank">Terms &amp; Conditions</a>
			</label>
			<a href="#reg" class="btn">Next</a>
		</div>
	</section>
	<section class="view" data-view="select" data-info="Select up to 3 followers to help keep your telekinesis going (optional)! Their Tweets with #{{ Config::get('twitter::twitter.tag') }} will help your coin stay afloat.">
		<div class="social"><a href="//facebook.com/looperuk" class="fb" target="_blank">Facebook</a><a href="//twitter.com/looperuk" class="tw" target="_blank">Twitter</a></div>
		<div class="box">
			<p>Type the username of up to three Twitter followers to get started.</p>
			<input class="input-block-level" id="friendHandle" name="friendHandle" type="text"  placeholder="Start typing ..." required>
			<a href="#skip" class="btn fl">Skip</a> <a href="#submit" class="btn">Submit</a>
		</div>
	</section>
	<section class="view" data-view="tweet" data-info="Tweet using <span class='tag'>#{{ Config::get('twitter::twitter.tag') }}</span> below at least once in each time interval, the clock resets at midnight. The more you tweet throughout until February 4th, the more your telekinesis will stay. Those who keep their coin afloat will be eligible for our sweepstakes and a chance to win.">
		<div class="social"><a href="//facebook.com/looperuk" class="fb" target="_blank">Facebook</a><a href="//twitter.com/looperuk" class="tw" target="_blank">Twitter</a></div>
		<div class="box">
			<textarea id="twitter-fld" class="input-block-level" maxlength="140" onblur="if (this.value == '') {this.value = 'Type tweet ...';}" onfocus="if (this.value == 'Type tweet ...') {this.value = '';}">Type tweet ...</textarea>
			<span class="meta">#{{ Config::get('twitter::twitter.tag') }} will be added to your tweet</span>
			<span class="twCount"></span>
			<a href="#tweet" class="btn tweet">Post</a>
		</div>
		<ul class="share">
			<li class="fb"><fb:like href="{{ URL::base() }}" data-layout="button_count" send="false" width="120" show_faces="false"></fb:like></li>
			<li class="tw"><a href="https://twitter.com/share" class="twitter-share-button" data-url="{{ URL::base() }}" data-text="..." data-hashtags="{{ Config::get('twitter::twitter.tag') }}" data-via="LooperUK">Tweet</a></li>
		</ul>
		<div id="twitter-list-box">
			<div id="tweets-list">
				<div class="twList"></div>
			</div>
		</div>
	</section>
@endsection