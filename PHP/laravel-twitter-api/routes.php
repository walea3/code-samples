<?php


Route::get('/', function()
{
	$view = Config::get('application.state') == 'closed' ? 'closed.index' : 'home.index';
	return View::make($view)
		->with('sessionEnd', date( "M j, Y H:i:s O", strtotime(Activity::periodEnd()) ))
		->with('user', Session::has('user') ? Session::get('user') : array('loggedin'=>false, 'friends'=>array()));
});

Route::get('/twitterAuth', function()
{
	$t = IoC::resolve('twitter');
	$u = $t->auth();
	return (isset($u['success']) && !$u['success'] ? Redirect::to($u['authUrl']) : Redirect::home());
});

Route::get('/t/friendsList', function()
{
	if (!Session::has('user')) { return Response::error('500'); }
	$t = IoC::resolve('twitter');
	$userHandle = Session::get('user.tw_handle');

	$friends = Cache::has($userHandle . '_friends') && count(Cache::get($userHandle . '_friends')) > 0 ? Cache::get($userHandle . '_friends') : $t->friendsList($userHandle);

	return Response::Json($friends);
});

Route::post('/t/addTeam', function()
{
	if (!Session::has('user')) { return Response::error('500'); }

	$u = Session::get('user');
	$friends = Input::get('friends');

	Team::addNew($u, $friends);
	$t = IoC::resolve('twitter');
	$t->post_tweet(Config::get('twitter::twitter.auto_msg') . ' #' . Config::get('twitter::twitter.tag') . ' at ' . URL::base());

	if ($friends) {
		$handles = '';
		foreach ($friends as $friend) {
			$handles .= '@' . $friend['tw_handle'] . ' ';
		}
		$t->post_tweet( $handles . 'I just selected you to take part with me in my TK Challenge! ' . URL::base() . ' #' . Config::get('twitter::twitter.tag'));
	}
	  
});

Route::post('/t/update', function()
{
	if (!Session::has('user')) { return Response::error('500'); }

	$msg = Input::get('msg') . ' #' . Config::get('twitter::twitter.tag') . ' at ' . URL::base() . ' via @' . Config::get('twitter::twitter.via');
	$t = IoC::resolve('twitter');
	return $t->post_tweet($msg);
});

Route::post('/t/registerUser', function()
{
	if (!Session::has('user')) { return Response::error('500'); }

	$u = Session::get('user');
	$regData = Input::all();

	$exists = User::find($u['id']);
	if (!$exists) {
		$newUser = User::create(array(
			'id' => $u['id'],
			'tw_handle' => $u['tw_handle'],
			'name'	=> $regData['name'],
			'email'	=> $regData['email'],
			'age'	=> $regData['age'],
			'country' => $regData['country'],
			'terms'	=> $regData['terms']
		));
	} else {
		$exists->name = $regData['name'];
		$exists->email = $regData['email'];
		$exists->age = $regData['age'];
		$exists->country = $regData['country'];
		$exists->terms = $regData['terms'];
		$exists->save();
	}

});
Route::get('/t/coinStatus',function(){
	if (!Session::has('user')) { return false; }
	$u = Session::get('user');
	return User::getActivityStatus($u['id']);
});
Route::any('/t', array('as' => 'api',function(){}));


Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

Route::filter('before', function()
{
	// Do stuff before every request to your application...
	if ( !Session::has('user') ) {
		$t = IoC::resolve('twitter');
		$u = $t->auth();

		if (!is_array($u)) return $u;

		if ($u['success']) {
			unset($u['user']['friends']);
			Session::put('user', $u['user']);
		}
	} else {
		$user = Session::get('user');
		$user['active'] = $active = isset($user['active']) && $user['active'] ? true : User::isActive($user['id']);

		if (!isset($user['reg']) || !$user['reg']) {
			$u = User::find($user['id']);
			$user['reg'] = $u && $u->email ? true : false;
		}

		if ($active){
			$coinStatus = User::getActivityStatus($user['id']);
			$user['coinStatus'] = $coinStatus;
		}

		Session::put('user', $user);
	}
});


Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});