<?php

class User extends Eloquent {
	
	public function activities() {
		return $this->has_many('Activity');
	}

	public static function isActive( $id ) {
		$active = Team::where('leader', '=', $id)->or_where('member', '=', $id)->get();
		return count($active) > 0 ? true : false;
	}

	public static function getActivityStatus( $id ) {
		//check user activity - coin percentange
		return Activity::getStatus($id);
	}

	public static function getTeam( $id ) {
		$team = false;
		$t = Team::where('leader', '=', $id)->get('member');

		if (!$t || (is_array($t) && count($t) < 1)) {
			$member = Team::where('member', '=', $id)->first();
			if ($member) {
				$team = array($member->leader);
				$t = Team::where('leader', '=', $member->leader)->get('member');
				foreach ($t as $tm) {
					$team[] = $tm->member;
				}
			}
		} else {
			$team = array($id);
			foreach ($t as $tm) {
				$team[] = $tm->member;
			}
		}
		return $team;
	}
}