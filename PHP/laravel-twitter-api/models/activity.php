<?php

class Activity extends Eloquent {
	
	public static $timestamps = false;

	public function user() {
		return $this->belongs_to('User');
	}

	public static function addNew( $id, $created, $tweet_id ) {
		$period = self::getPeriod( $created );
		$limit = (User::isActive( $id ) ? self::checkLimit( $id ) : true);

		$exists = self::where('tweet_id', '=', $tweet_id)->count();
		Log::activity('tweet_id = ' . $tweet_id . '; period = ' . $period->id . '; user = ' . $id . '; limit reached? ' . (int)$limit . '; tweet exists? ' . (string)$exists . '; can save? ' . (int)($limit == false && $exists == 0) . PHP_EOL);
		
		if ($limit == false && $exists == 0) {
			$new = new self;
			$new->user_id = $id;
			$new->period_id = $period->id;
			$new->tweet_id = $tweet_id;
			$new->save();
		}
	}

	public static function checkLimit( $id ) {
		$period = self::getPeriod();
		$periodLimit = Config::get('application.period_limit');
		$team = User::getTeam( $id );
		$periodTotal = ($team ? self::where_in('user_id', $team)->where('period_id', '=', $period->id)->count() : $periodLimit);

		return $periodTotal < $periodLimit ? false : true;
	}

	public static function getStatus( $user ) {
		$team = User::getTeam($user);
		if ($team) {
			$total = DB::table('periods')->count() * Config::get('application.period_limit');
			$activities = 0;

			$activePeriods = DB::table('periods')->where('end_at', '<', DB::raw('NOW()'))->or_where(function($query)
		    {
		        $query->where('start_at', '<=', DB::raw('NOW()'));
		        $query->where('end_at', '>', DB::raw('NOW()'));
		    })->get('id');

			if (count($activePeriods) > 0) {
				$periods = array();
			    foreach ($activePeriods as $period) {
			    	$periods[] = $period->id;
			    }
				$activities = self::where_in('user_id', $team)->where_in('period_id', $periods)->count();
			}

			return $activities / ($total / 100);
		} else {
			return 0;
		}
	}

	public static function getPeriod( $at = false ) {
		$at = !$at ? DB::raw('NOW()') : date("Y-m-d H:i:s", strtotime($at));
		$period = DB::table('periods')->where(function($query) use ($at)
	    {
	        $query->where('start_at', '<=', $at);
	        $query->where('end_at', '>', $at);
	    })->first();

		return $period;
	}

	public static function periodStart( $at = false ) {
		$period = self::getPeriod($at);
		return $period->start_at;
	}

	public static function periodEnd( $at = false ) {
		$period = self::getPeriod($at);
		return $period->end_at;
	}
}