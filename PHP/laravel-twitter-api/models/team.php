<?php

class Team extends Eloquent {
	
	public function users() {
		return $this->has_many('User', 'id');
	}

	public static function addNew ( $leader, $members = array() ) {
		//add main user if
		$exists = User::find($leader['id']);
		if (!$exists) {
			$newUser = User::create(array(
				'id' => $leader['id'],
				'tw_handle' => $leader['tw_handle'],
				'country' => $leader['country']
			));
		}

		foreach ($members as $k => $v) {
			$exists = User::find($v['id']);
			if (!$exists) {
				$newUser = User::create(array(
					'id' => $v['id'],
					'tw_handle' => $v['tw_handle'],
					'country' => $v['country']
				));
			}
			if (DB::table('teams')->where('leader', '=', $leader['id'])->where('member', '=', $v['id'])->count() < 1) {
				DB::table('teams')->insert(array('leader'=> $leader['id'],'member' => $v['id']));
			}
		}
	}
}