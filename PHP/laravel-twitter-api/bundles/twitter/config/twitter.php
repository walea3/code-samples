<?php
return array(
	'key' => '',
	'secret' => '',
	'dev_token' => '',
	'dev_secret' => '',
	'application' => 'TK Challenge',
	'format' => 'json',
	'lang' => 'en',
	'rpp' => 100,
	'page' => 1,
	'since_id' => NULL,
	'show_user' => FALSE,
	'username' => '',
	'password' => '',
	'tag' => 'TKchallenge',
	'via' => '',
	'auto_msg' => 'Automate app tweet'
);
