<?php

class Twitter {
	
	private $t;

	// Twitter method urls
	const STATUS		= '1.1/statuses';
	const USER			= '1.1/users';
	const MESSAGE		= '1.1/direct_messages';
	const FRIENDSHIP	= '1.1/friendships';
	const FRIENDS		= '1.1/friends';
	const FOLLOWERS		= '1.1/followers';
	const ACCOUNT		= '1.1/account';
	const FAVORITE		= '1.1/favorites';
	const NOTIFICATION	= '1.1/notifications';
	const BLOCK			= '1.1/blocks';
	const HELP			= '1.1/help';
	const SEARCH		= '1.1/search/tweets';
	const TREND			= '1.1/trends';
	const LOOKUP_SIZE	= 100;
	
	/**
	 * Twitter class constructor
	 *
	 * @return  object
	 */
	public function __construct()
	{
		$this->t = new tmhOAuth(array(
		  'consumer_key'    => Config::get('twitter::twitter.key'),
		  'consumer_secret' => Config::get('twitter::twitter.secret')
		));
		
		if ( Session::has('access_token') ) {
			$this->t->config['user_token']  = Session::get('access_token.oauth_token');
			$this->t->config['user_secret'] = Session::get('access_token.oauth_token_secret');
		}
		
		return $this->t;
	}
	
	public function auth() {
		$here = tmhUtilities::php_self();

		if ( Session::has('access_token') ) {
		  $this->t->config['user_token']  = Session::get('access_token.oauth_token');
		  $this->t->config['user_secret'] = Session::get('access_token.oauth_token_secret');

		  $code = tmhUtilities::auto_fix_time_request($this->t, 'GET', $this->t->url('1.1/account/verify_credentials'));
		  if ($this->t->response['code'] == 200) {
		    $resp = json_decode($this->t->response['response']);

			return array(
				'success' => true,
				'user' => array(
					'tw_handle' => $resp->screen_name,
					'id' => $resp->id_str,
					'country' => $resp->geo_enabled ? $resp->location : '',
					'friends' => !User::isActive($resp->id_str) ? self::friendsList($resp->screen_name) : array()
				)
			);
		  } else {
		    Log::error($this->t->response['response'] . ' auth fail ' . PHP_EOL);
		    Session::flush();
			return Redirect::home();
		  }
		// we're being called back by Twitter
		} else if (Request::get('oauth_verifier')) {
			$this->t->config['user_token']  = Session::get('oauth.oauth_token');
		  	$this->t->config['user_secret'] = Session::get('oauth.oauth_token_secret');

			$code = tmhUtilities::auto_fix_time_request($this->t, 'POST', $this->t->url('oauth/access_token', ''), array(
				'oauth_verifier' => Request::get('oauth_verifier')
			));

			if ($this->t->response['code'] == 200) {
				Session::put('access_token', $this->t->extract_params($this->t->response['response']));
				Session::forget('oauth');
				return self::auth();
			} else {
				Log::error($this->t->response['response'] . ' verfication fail ' . PHP_EOL);
				Session::flush();
				return Redirect::home();
			}
		// start the OAuth dance
		} else {
			$callback = Request::get('oob') ? 'oob' : $here;

			$params = array(
				'oauth_callback' => $callback
			);

			if (Request::get('force_write')) :
				$params['x_auth_access_type'] = 'write';
			elseif (Request::get('force_read')) :
				$params['x_auth_access_type'] = 'read';
			endif;

			tmhUtilities::auto_fix_time_request($this->t, 'POST', $this->t->url('oauth/request_token', ''), $params);

			if ($this->t->response['code'] == 200) {
				Session::put('oauth', $this->t->extract_params($this->t->response['response']));
				$method = Request::get('authenticate') ? 'authenticate' : 'authorize';
				$force  = Request::get('force') ? '&force_login=1' : '';
				$authurl = $this->t->url("oauth/authenticate", '') .  "?oauth_token=" . Session::get('oauth.oauth_token') .$force;
				return array('success' => false, 'authUrl' => $authurl);
			} else {
				Log::error($this->t->response['response'] . ' oauth fail ' . PHP_EOL);
				//return Redirect::home();
			}
		}
	}

	public function friendsList( $user ){
		$parent = $this;
		$t = $this->t;
		$friends = Cache::sear($user . '_friends', function() use ($parent, $t) {
			$friends = array();
			$ids = $parent->cursorLoop('GET', $t->url($parent::FRIENDS . '/ids'), array('user_id' => Session::get('user.id')), 'ids');
			if (count($ids) < 1) {
				$ids = $parent->cursorLoop('GET', $t->url($parent::FOLLOWERS . '/ids'), array('user_id' => Session::get('user.id')), 'ids');
			}

			$paging = ceil(count($ids) / $parent::LOOKUP_SIZE);
			$users = array();
			for ($i=0; $i < $paging ; $i++) {
				$set = array_slice($ids, $i*$parent::LOOKUP_SIZE, $parent::LOOKUP_SIZE);

				$t->request('GET', $t->url($parent::USER . '/lookup'), array(
					'user_id' => implode(',', $set)
				));

				// check the rate limit
				$parent->check_rate_limit($t->response);

				if ($t->response['code'] == 200) {
					$users = json_decode($t->response['response'], true);
					$friends = array_merge($friends, $users);
				} else {
					Log::error($t->response['response'] . ' friend lookup fail ' . PHP_EOL);
					break;
				}

			}

			foreach ($friends as $friend) {
				$fr[] = array(
					'tw_handle' => $friend['screen_name'],
					'id' => $friend['id_str'],
					'country' => $friend['geo_enabled'] ? $friend['location'] : ''
				);
			}

			return $fr ? json_encode($fr) : null;
		});
		
		return json_decode($friends);
	}

	public function cursorLoop($type, $method, $params, $return) {
		$result = array();
		$params['cursor'] = -1;

		do {
		    $code = tmhUtilities::auto_fix_time_request($this->t, $type, $method, $params); 
		    $this->check_rate_limit($this->t->response); 
		    if ($this->t->response['code'] == 200) {
		    	$resp = json_decode($this->t->response['response'], true);
		    	$result = array_merge($result, $resp[$return]);
		    	$params['cursor'] = $resp['next_cursor_str'];
		    } else {
		    	Log::error($this->t->response['response'] . ' request fail ' . PHP_EOL);
		    	$params['cursor'] = 0;
		    	break;
		    }
		}
		while ( $params['cursor'] != 0 );

		return $result;
	}

	function check_rate_limit($response) {
		$headers = $response['headers'];
		if ($headers['X-Rate-Limit-Remaining'] == 0) :
			$reset = $headers['X-Rate-Limit-Reset'];
			$sleep = time() - $reset;
			Log::info('rate limited. reset time is ' . $reset . PHP_EOL);
			Log::info('sleeping for ' . $sleep . ' seconds');
			sleep($sleep);
		endif;
	}

	public static function activityCallback($data, $length, $metrics) {
		$data = json_decode($data, true);

		$newActivity = Activity::addNew($data['user']['id_str'], $data['created_at'], $data['id_str']);

		return file_exists(dirname(__FILE__) . '/STOP');
	}

	public function post_tweet( $msg ) {
		$this->t->config['user_token']  = Session::get('access_token.oauth_token');
		$this->t->config['user_secret'] = Session::get('access_token.oauth_token_secret');

		$code = tmhUtilities::auto_fix_time_request($this->t, 'POST', $this->t->url(self::STATUS . '/update'), array(
			'status' => $msg,
		));

		if ($this->t->response['code'] == 200) {
			$data = json_decode($this->t->response['response']);
			$newActivity = Activity::addNew(
				$data->user->id_str, 
				$data->created_at, 
				$data->id_str);
			return true;
		} else {
			Log::error($this->t->response['response'] . ' tweet fail ' . PHP_EOL);
			return false;
		}
	}
}