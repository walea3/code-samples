<?php

Autoloader::map(array(
	'Twitter' => Bundle::path('twitter').'twitter.php',
    'tmhOAuth' => Bundle::path('twitter').'libraries/tmhOauth/tmhOAuth.php',
	'tmhUtilities' => Bundle::path('twitter').'libraries/tmhOauth/tmhUtilities.php',
));

IoC::singleton('twitter', function()
{
    return new Twitter();
});