<?php 

class Twitter_Activity_Task {
 	public $count = 0;
	public $first_id = 0;

    public function run()
    {
		$t = new tmhOAuth(array(
		  'consumer_key'    => Config::get('twitter::twitter.key'),
		  'consumer_secret' => Config::get('twitter::twitter.secret'),
		  'user_token' => Config::get('twitter::twitter.dev_token'),
		  'user_secret'	=> Config::get('twitter::twitter.dev_secret')
		));
		
		$params = array(
			'track'     => '#' . Config::get('twitter::twitter.tag'),
			'delimited'	=> 'length'
		);
		$t->streaming_request('POST', 'https://stream.twitter.com/1.1/statuses/filter.json', $params, 'Twitter::activityCallback');
	}

	public function scan()
    {
		$t = new tmhOAuth(array(
		  'consumer_key'    => Config::get('twitter::twitter.key'),
		  'consumer_secret' => Config::get('twitter::twitter.secret'),
		  'user_token' => Config::get('twitter::twitter.dev_token'),
		  'user_secret'	=> Config::get('twitter::twitter.dev_secret')
		));

	  	$result = array();
		$params = array(
			'q'     => urlencode('#' . Config::get('twitter::twitter.tag')),
			'count'	=> 100,
			'since' => date("Y-m-d", strtotime(DB::table('periods')->find(1)->start_at))
		);
		$finished = false;

		do {
		    $code = $t->request('GET', 'https://api.twitter.com/1.1/search/tweets.json', $params);  
		    if ($code == 200) {
		    	$resp = json_decode($t->response['response'], true);
		    	if ( count($resp['statuses']) > 0) {
					foreach ($resp['statuses'] as $activity) {
						$newActivity = Activity::addNew($activity['user']['id_str'], $activity['created_at'], $activity['id_str']);
					}
			    	$params['since_id'] = $resp['search_metadata']['max_id'];
		    		sleep(10);
			    } else {
			    	$finished = true;
			    }
		    } else {
		    	$finished = true;
		    	Log::error($t->response['response'] . ' search fail ' . PHP_EOL);
		    	sleep(60*20);
		    	break;
		    }
		}
		while ( !$finished );
	}
 
}