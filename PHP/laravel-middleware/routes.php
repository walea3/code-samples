<?php

/*
 * use a controller if it exists (story, category)
 * examples:
 * story/path-name - accepts a redirect from get/(:any)
 * category/tv - show the tv category view
*/

Route::controller(array('content', 'preview'));

/*
 * root mapping redirects to the lead story
 */

Route::get('/virg.co', function(){
	return Response::Json( Content::getShortUrl( Input::get('url'), Input::get('slug'), Input::get('title') ) );
});

Route::get('/publish/(:any)/(:any)' , array('uses'=>'content@publish'));
Route::get('/issue/(:any)', array('uses'=>'content@issue'));
Route::get('/(:any)/(:any?)', array('uses'=>'content@category'));
Route::get('/(:any)/(:any)/(:any)', array('uses'=>'content@story'));
Route::get(array('/', '/(:any)'), array('uses'=>'content@index'));


Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function($exception)
{
	return Response::error('500');
});


Route::filter('before', function()
{
	Common::globalXssClean();

	$isPreview = Input::get( 'live_preview' ) && Input::get( 'key' ) ? true : false;
	Config::set( 'application.isPreview', $isPreview);

	// prep issues - from cache / or server
	$customer = new Customer();

	$package = URI::segment(1, $isPreview && Input::get( 'package' ) ? Input::get( 'package' ) : null );

	$issue = new Content( ( $isPreview && Input::get( 'issue' ) ? Input::get( 'issue' ) : 'all' ) );

	if ( in_array( strtoupper( $package ), Config::get( 'application.packages' )) ) {
		Config::set( "application.package", $package );
	}

	$currentIssue = Config::get( 'application.issue' );

	if ( isset( $currentIssue['error'] ) ) {
		$issue = Content::get( 'issue', Config::get( 'application.default_issue' ) );
	}

});


Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});
