<?php

class Content_Controller extends Base_Controller {

	public $url_parameters = '';
    public $title = '';
	public $encrypted = null;
	public $first_name = null;
	public $issue_date = ''; // TODO: replace with JSON data!
    public $max_stories = 24;

    public function __construct() {

    	$this->package = Config::get( "application.package" );
        $this->title = Config::get( 'application.site_name' );
        // this is set by the package

        Asset::add('cookie', 'scripts/jquery.cookie.js');
        parent::construct();
    }
    
    public function action_index( $packageKey = null, $categoryKey = 'all' ) {
        $issue = Config::get( 'application.issue' );

        //check issue array provides required fields otherwise load blank issue
        if ( isset( $issue[ 'error' ] ) || !isset( $issue[ 'mapping' ] ) )
            $issue = Config::get( 'application.blank_issue' );

        // get stories for the selected category
        $stories = Content::get( 'category', $categoryKey );

        //store the customer or default content package
        $customerPackage = strtolower( Config::get( "application.package" ) );

        //store the content mapping for the current package
        $packageStories = Content::get( 'mapping', $categoryKey );

        //r
        return View::make('home.index')
            ->with( 'title', $this->title )
            ->with( 'issue', $issue )
            ->nest( 'tiles', 'issue.tiles', array( 'package' => $packageStories, 'stories' => $stories ) )
            ->nest( 'stories', 'issue.stories', array( 'package' => $packageStories, 'stories' => $stories ) );
    }

    public function action_category( $categoryPackage, $key = 'all' ) {
        $noPackage = Config::get( 'application.categories.' . strtolower( $categoryPackage ) );
        $category = Config::get( 'application.categories.' . strtolower( $key ) );

        if ( $noPackage && $key !== 'all' )
            return self::action_story( '', $categoryPackage, $key );

        if ( $category )
            $content = Content::get( 'category', $key );

        $actualCategory = $noPackage ? $categoryPackage : ( $category ? $key : 'all' );

        Config::set('application.request.category', strtolower( $actualCategory ));

        if ( $actualCategory !== 'all' )
            $this->title .= ' | ' . ( $noPackage ? $noPackage : $category );

        return self::action_index( !$noPackage ? $categoryPackage : null, $key );
    }

    public function action_story( $package = '', $categoryKey, $key ) {
        $category = Config::get( 'application.categories.' . strtolower( $categoryKey ) );

        /* get all the stories for the category */
        $content = Content::get( 'story', $key );
        $og = array(
            'title' => $content['heading'] . ' | ' . $content['programme_information'],
            'description' => $content['excerpt'],
            'image' => isset( $content['images'][ 11 ] ) ? $content['images'][ 11 ][ 'desktop' ] : URL::to_asset('images/vmp-logo.png'),
            'url' => URL::to_action( 'content@story', array($this->package, $categoryKey, $content['slug'] ) ),
        );

        Config::set('application.request.category', strtolower( $categoryKey ));
        Config::set('application.request.story', strtolower( $key ));

        Config::set( 'application.og', $og );

        $this->title .= ' | ' . $content['heading'];
        return self::action_index( null, 'all' );
    }

    public function action_issue( $key )
    {
        Content::get( $key, 'issue' );
        return self::action_index( null, 'all' );
    }

    public function action_publish( $hash, $issue )
    {
        if ( $hash !== Config::get( 'application.md5hash' ) )
            return Response::error('404');

        Content::get( 'issue', $issue );
        return self::action_index( null, 'all' );
    }

}
