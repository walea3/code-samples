@if ( !isset( $blank ) )
<?php 
    $categories = Config::get( 'application.categories' );
    $category = array_search( $story['primary_category'], $categories );

    $type = ( $story[ 'story_type' ] == 'Video' || $story[ 'video_id' ] !== '' ? 'Video' : $story[ 'story_type' ] );

    $tileClass = "tile $category is{$type}";
    $mediaBodyClass = "media-body";
    $imageSize = null;
    $imgSrcs = " data-img-dimension=\"$imageSize\"";
    $animInt = '';

    //layout definitions - class asignment
    foreach ( Config::get( "application.grid_templates.$tpl.tile_classes" ) as $class => $tiles ) {
        $tileClass .= in_array( $key, $tiles ) ? " $class" : "";
    }
    foreach ( Config::get( "application.grid_templates.$tpl.media-body_classes" ) as $class => $tiles ) {
        $mediaBodyClass .= in_array( $key, $tiles ) ? " $class" : "";
    }

    //animation interval assignment
    foreach ( Config::get( "application.grid_templates.$tpl.tile_animation_intervals" ) as $int => $tiles ) {
        if ( in_array( $key, $tiles ) ) {
            $animInt = " data-anim-int=\"$int\"";
            break;
        }
    }

    //tile image side assignment
    foreach ( Config::get( "application.grid_templates.$tpl.image_dimensions" ) as $dimension => $tiles ) {
        if ( in_array( $key, $tiles ) ) {
            $imageSize = $dimension;
            break;
        }
    }
    //tile image assignment
    $images = isset( $story['images'] ) && isset( $story['images'] ) ? $story['images'] : null;
    if ( $images ) {
        foreach ( $images as $dimension => $imageSet ) {
            foreach ( $imageSet as $size => $image ) {
                if ( $image )
                    $imgSrcs .= " data-src-{$dimension}-{$size} =\" $image \"";
            }
        }
    }
?>
<li id="tile-{{ $key }}" class="{{ $tileClass }}"{{$animInt}}>
    <a href="#story-{{ $key }}" data-id="{{ $story[ 'story_id' ] }}" data-slug="{{ $story[ 'slug' ] }}" title="Link to full story">
        <article id="tile-story-{{ $story['story_id'] }}" class="media">
            <img class="image" src="{{ isset( $images[ $imageSize ][ 'mobile' ] ) ? $images[ $imageSize ][ 'mobile' ] : '' }}" alt="{{ $story[ 'heading' ] }}" {{ $imgSrcs }} />
            <div class="{{ $mediaBodyClass }}">
            @if( $type === 'Video' )
                <div class="panel panel-play">
                    <i class="icon icon-play"></i><span class="is-visuallyhidden">Play</span>
                </div>
            @endif
                <span class="panel panel-category">{{ $story[ 'primary_category' ] }}</span>
                <h1 class="title">{{ $story[ 'heading' ] }}</h1>
                <h2 class="programme_name">{{ $story['programme_information'] }}</h2>
                <h3 class="programme_data">{{ $story[ 'channel_information' ] }}</h3>
                <div class="excerpt">{{ $story[ 'excerpt' ] }}</div>
            </div>
        </article>
        <div class="tile-active"></div>
    </a>
</li>
@elseif ( Config::get( 'application.isPreview' ) )
<li id="tile-{{ $key }}" class="tile empty"></li>
@endif