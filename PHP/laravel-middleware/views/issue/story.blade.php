<?php 
    $categories = Config::get( 'application.categories' );
    $category = array_search( $story['primary_category'], $categories );

    $type = ( $story[ 'story_type' ] == 'Video' || $story[ 'video_id' ] !== '' ? 'Video' : $story[ 'story_type' ] );
    $images = isset( $story['images'] ) && isset( $story['images'][ 169 ] ) ? $story['images'][ 169 ] : null;
    $imgSrcs = " data-img-dimension=\"169\"";

    $storyUrl = URL::to_action( 'content@story', array( Config::get( 'application.package' ), $category, $story[ 'slug' ] ) );
    $shareCopy = Config::get( 'application.site_name' ) . ': ' . $story['heading'] . ' | ' . $story['primary_category'];

    if ( $images ) {
        foreach ( $images as $size => $image ) {
            if ( $image )
                $imgSrcs .= ' data-src-169-' . $size . ' = "' . $image . '"';
        }
    }
?>
<div id="story-{{ $key }}" data-id="{{ $story[ 'story_id' ] }}" data-slug="{{ $story[ 'slug' ] }}" class="story">
    <div class="showpic">
        <span class="panel panel-category">{{ $story['primary_category'] }}</span>
        <a href="#" class="panel panel-close"><i class="icon icon-close"></i><span class="is-visuallyhidden">Close story</span></a>
        @if ($type == 'Video')
            <div class="video_wrapper">
            <img class="ratio" src="{{ URL::to_asset( 'images/169_placeholder.gif' ) }}"/>
            <iframe id="video_player_{{ $story[ 'slug' ] }}" class="video_embed video_type-{{ $story['video_type'] }}" src="{{ $story['video_type'] === 'vimeo' ? '//player.vimeo.com/video/' . $story[ 'video_id' ] . '?api=1&amp;player_id=video_player_' . $story[ 'slug' ] .'&amp;origin=' . URL::base() : '//www.youtube.com/v/' . $story[ 'video_id' ] . '?enablejsapi=1&amp;version=3&amp;playerapiid=video_player_' . $story[ 'slug' ] .'&amp;origin=' . URL::base() }}" width="100%" height="456" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
            </div>
        @else
         <img src="{{ $images[ 'mobile' ] }}" alt="{{ $story['heading'] }}" {{ $imgSrcs }} />
        @endif
    </div>
    <div class="copy">
        <header>
            <h1 class="title">{{ $story['heading'] }}</h1>
            <h2 class="programme_name">{{ $story['programme_information'] }}</h2>
            <h3 class="programme_data_full">{{ $story[ 'channel_information' ] }}</h3>
        </header>
        <div class="body">
            {{ HTML::p( $story[ 'post_content' ] ) }}
        </div>
        <div class="watch">
        @if ( !in_array( $story[ 'watch_live_url' ], array('', 'http://') ) || !in_array( $story[ 'record_programme_url' ], array('', 'http://') ) )
            <h4 class="heading-discreet">Other ways to watch</h4>
            <ul class="menu l-listless l-inline">
            @if ( !in_array( $story[ 'record_programme_url' ], array('', 'http://') ) )
                <li class="watchbox">
                    <a href="{{ $story[ 'record_programme_url' ] }}" target="_blank" class="media">
                        <div class="image">
                            <i class="icon icon-record"></i>
                        </div>
                        <div class="media-body">Record on your TiVo box now</div>
                    </a>
                </li>
            @endif
            @if ( !in_array( $story[ 'watch_live_url' ], array('', 'http://') ) )
                <li class="watchbox">
                    <a href="{{ $story[ 'watch_live_url' ] }}" target="_blank" class="media">
                        <div class="image">
                            <i class="icon icon-anywhere"></i>
                        </div>
                        <div class="media-body">Watch live on Virgin TV Anywhere</div>
                    </a>
                </li>
            @endif
                <!-- <li class="watchbox">
                    <a href="#" class="media">
                        <div class="image">
                            <i class="icon icon-catchup"></i>
                        </div>
                        <div class="media-body">Catch Up on your TV in ITV On Demand</div>
                    </a>
                </li> -->
            </ul>
        @endif
        @if ( count( $story['cta'] ) > 0 )
            <ul class="menu l-listless l-inline">
            @foreach( $story[ 'cta' ] as $cta )
            @if ( $cta['link_destination'] !== '')
                <li class="watchbox cta">
                    <a href="{{ $cta['link_destination'] }}" class="media" target="_blank">
                        <div class="media-body">{{ $cta['button_text'] }}</div>
                    </a>
                </li>
            @endif
            @endforeach
            </ul>
        @endif
        </div><!-- End .watch -->
    </div><!-- End .copy -->
    <footer class="story-footer">
        <div class="social" data-url="{{ urlencode( $storyUrl ) }}" data-slug="{{ $story['slug'] }}" data-title="{{{ urlencode( $shareCopy ) }}}">
            <h4 class="heading-discreet">Share with friends</h4>
            <ol class="l-inline l-listless">
                <li class="email"><a href="mailto:&nbsp;?subject={{{ 'A ' . Config::get( 'application.site_name' ) . ' story' . ( Config::get( 'application.customer.name' ) !== 'Guest' ? ' from ' . Config::get( 'application.customer.name' ) : '' ) }}}&amp;body={{{ rawurlencode( $story['heading'] . ' | ' . $story['programme_information'] . ' - ' ) }}}[storyUrl]" title="Email story"><i class="icon icon-email"></i><span class="is-visuallyhidden">Email story</span></a></li>
                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode( $storyUrl ) }}" target="_blank" title="Share on Facebook"><i class="icon icon-facebook"></i><span class="is-visuallyhidden">Share on Facebook</span></a></li>
                <li><a href="https://twitter.com/share?url=[storyUrl]" target="_blank" title="Share on Twitter"><i class="icon icon-twitter"></i><span class="is-visuallyhidden">Share on Twitter</span></a></li>
                <li><a href="https://plus.google.com/share?url={{ urlencode( $storyUrl ) }}" target="_blank" title="Share on Google+"><i class="icon icon-google-plus"></i><span class="is-visuallyhidden">Share on Google+</span></a></li>
            </ol>
        </div>
        @if ( count( $story[ 'lttt' ] ) > 0)
        <div class="explore">
            <h2 class="heading">More to explore</h2>
            <ol class="explore-items l-inline l-listless">
            @foreach( $story[ 'lttt' ] as $explore )
                <li>
                    <article>
                        <a{{ !in_array( $explore[ 'lttt_destination_link' ], array('', 'http://') ) ? ' href="' . $explore[ 'lttt_destination_link' ] . '" target="_blank"' : '' }} class="img"><img class="showpic" src="{{ $explore[ 'lttt_image' ] }}" alt="{{ $explore[ 'lttt_title' ] }}"></a>
                        <h1 class="programme_name">
                            <a{{ !in_array( $explore[ 'lttt_destination_link' ], array('', 'http://') ) ? ' href="' . $explore[ 'lttt_destination_link' ] . '" target="_blank"' : '' }}>{{ $explore[ 'lttt_title' ] }}</a>
                        </h1>
                        <h2 class="programme_channel">
                            <a{{ !in_array( $explore[ 'lttt_destination_link' ], array('', 'http://') ) ? ' href="' . $explore[ 'lttt_destination_link' ] . '" target="_blank"' : '' }}>{{ $explore[ 'lttt_programme_info' ] }}</a>
                        </h2>
                        <div class="excerpt">{{ $explore[ 'lttt_text' ] }}</div>
                    </article>
                </li>
            @endforeach
            </ol>
        </div><!-- End .explore -->
        @endif
        <p>{{ $story['additional_disclaimers'] }}</p>
        <br>
    </footer>
</div><!-- End #story-{{ $key }} -->