  <div class="tiles l-layout_client">
    <ol class="category is-behind l-listless"></ol>
    <ol id="primaryGrid" class="l-listless">
<?php 
	$adCount = 1; 
	$template = Config::get( 'application.issue.metadata.issue.template' );
?>
@for ($i = 1; $i <= Config::get( 'application.grid_templates.' . $template . '.story_tiles' ); $i++)
	<?php 
		$isAd = in_array( $i, Config::get( 'application.grid_templates.' . $template . '.ad_tiles' ) ) ? true : false;
		$adKey = str_pad( $adCount, 2, 0, STR_PAD_LEFT ); 
		$key = str_pad( ( $i ), 2, 0, STR_PAD_LEFT ); 
	?>

	@if ( $isAd && $package && isset( $package[ 'ads' ][ $adCount ] ) && $stories[ $package[ 'ads' ][ $adCount ] ][ 'advert' ] && ( $storyKey = $package[ 'ads' ][ $adCount ] ) >= 0 )
		@render( 'issue/ad-tile', array( 'tpl' => $stories[ $storyKey ][ 'template_identifier' ], 'key' => $adKey, 'story' => $stories[ $storyKey ] ) )
	@elseif ( $isAd )
		@render( 'issue/ad-tile', array( 'key' => $adKey, 'blank' => '' ) )
	@endif

	@if ( $package && isset( $package[ 'stories' ][ $i ] ) && ( $storyKey = $package[ 'stories' ][ $i ] ) >= 0 )
		@render( 'issue/story-tile', array( 'tpl' => $stories[ $storyKey ][ 'template_identifier' ], 'key' => $key, 'story' => $stories[ $storyKey ] ) )
	@else
		@render( 'issue/story-tile', array( 'key' => $key, 'blank' => '' ) )
	@endif
	
	<?php if( $isAd ) $adCount++; ?>
@endfor
    </ol><!-- End #primaryGrid -->
</div><!-- End .tiles -->