@if ( !isset( $blank ) )
<li id="ad-{{ $key }}" class="ad tile {{ array_search( $story['primary_category'], Config::get( 'application.categories' ) ) }}">
    <a href="{{ $story[ 'advert_link' ] }}" title="Link to ad" target="_blank">
        <img src="{{ isset( $story['images'][169]['retina'] ) ? $story['images'][169]['retina'] : null }}" alt="{{ $story[ 'heading' ] }}">

        <div class="media-body">
            <span class="panel panel-category panel-category-ad">{{ $story[ 'primary_category' ] }}</span>
        </div>

        <div class="tile-active"></div>
    </a>
</li>
@elseif ( Config::get( 'application.isPreview' ) )
<li id="ad-{{ $key }}" class="ad tile empty"></li>
@endif