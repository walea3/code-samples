<div class="stories">
@if ( $package && isset( $package[ 'stories' ] ) )
	@foreach( $package[ 'stories' ] as $id => $storyKey )
	<?php if ( !isset( $stories[ $storyKey ] ) )
        	break;
        $key = str_pad( $id, 2, 0, STR_PAD_LEFT ); 
    ?>
	    @include( 'issue/story', array( 'key' => $key, 'story' => $stories[ $storyKey ] ) )
	@endforeach
@endif
</div><!-- End .stories -->