<header class="main-header" role="banner">
    <div class="branding cf">
        <a href="#main" class="is-hidden">Skip navigation</a>
        <a href="#menu" id="menu-toggle-button"><i class="icon icon-menu"></i><span class="is-visuallyhidden">Menu</span></a>
        <div class="vmp-logo">
            <img src="{{ URL::to_asset('images/vmp-logo.png') }}" alt="Virgin Media logo" />
        </div>
        <p class="date">{{ Config::get( 'application.issue_date' ) }}</p>
    </div>
    <nav id="main-nav" class="menu-toggle" role="navigation">
        <ul id="menu" class="menu">
        @foreach( Config::get( 'application.categories' ) as $k => $v )
            <li id="{{ $k }}"><span class="glossy"><a href="{{ URL::to_action( 'content@category', $k ) }}" title="{{ $v }}">{{ $v }} <span class="symbol-go"></span></a></span></li>
        @endforeach
        </ul>
    </nav>
</header><!-- End .main-header -->
