<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js "> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ $title }}</title>
    <meta property="description" content="{{ strip_tags( Config::get( 'application.og.description', Config::get( 'application.site_name' ) ) ); }}" /> 
    <meta property="fb:admins" content="{{ Config::get( 'application.fb_admins' ); }}" /> 
    <meta property="og:type" content="article" /> 
    <meta property="og:url" content="{{ Config::get( 'application.og.url', URL::current() ); }}" /> 
    <meta property="og:title" content="{{ Config::get( 'application.og.title', $title ); }}" /> 
    <meta property="og:description" content="{{ strip_tags( Config::get( 'application.og.description', Config::get( 'application.site_name' ) ) ); }}" /> 
    <meta property="og:image" content="{{ Config::get( 'application.og.image', URL::to_asset('images/vmp-logo.png') ); }}" /> 
    <meta property="og:site_name" content="{{ Config::get( 'application.site_name' ) }}" /> 

    <link type="text/plain" rel="author" href="{{ URL::to_asset('humans.txt') }}" />
    <link rel="stylesheet" href="{{ URL::to_asset('static/vmp-styles.css') }}?{{ time('U') }}" />
    <link rel="stylesheet" href="{{ URL::to_asset('static/styles/font.css') }}" /> <!-- TODO: CSS not working when imported into SCSS -->
    <link type="text/plain" rel="help" href="{{ URL::to_asset('style.txt') }}" />
    <link rel="shortcut icon" href="{{ URL::to_asset('favicon.png') }}" />
    <!-- TODO: temp turn off CDN javascript, as delay on refresh. what the heck is that about? -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">// CDN with local fallback</script> -->
    <!-- <script type="text/javascript">
    if (typeof jQuery == 'undefined') {
        document.write(unescape("%3Cscript src='{{ URL::to_asset('components/jquery/jquery.min.js') }}' type='text/javascript'%3E%3C/script%3E"));
    }
    </script> -->
    <script src="{{ URL::to_asset('components/modernizr-latest.js') }}"></script>
    <script src="{{ URL::to_asset('components/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ URL::to_asset('components/picturefill.js') }}"></script>

    <script src="{{ URL::to_asset('scripts/transit.min.js') }}"></script>

    <script type="text/javascript">
        window.VMP = window.VMP || {};
        VMP.config = {
            baseURL : '{{ URL::base() }}',
            cat : '{{ Config::get('application.request.category') }}',
            story : '{{ Config::get('application.request.story') }}',
            gridWidthPx : {{ Config::get('application.grid_templates.' . Config::get( 'application.issue.metadata.issue.template' ) . '.width_px', 0) }}
        };
    </script>

    <script>
        if( $(window).width() >= 640 ){
            document.write('<script src="{{ URL::to_asset('scripts/vmp-common.js') }}?{{ time('U') }}"><\/script>');
            document.write('<script src="{{ URL::to_asset('scripts/vmp.js') }}?{{ time('U') }}"><\/script>');
        } else {
            document.write('<script src="{{ URL::to_asset('static/scripts/mobile.js') }}?{{ time('U') }}"><\/script>');
        }
    </script>
    <link rel="canonical" href="{{ Config::get( 'application.og.url', URL::current() ); }}">
</head>
<body class="{{ Config::get( 'application.isPreview' ) ? 'preview' : ( Session::get( 'customerHasVisited' ) !== 1 && !Config::get( 'application.isPreview' ) ? 'first' : '' ) }}">

<div class="arrow left"> &lt;  </div>    
<div class="arrow right"> &gt; </div>

@include('layouts/splash')
<div class="tlw"><!--Top Level Wrapper, used only in mobile-->
    @include('layouts/top')

    <div class="wrapper is-animate-reveal">
