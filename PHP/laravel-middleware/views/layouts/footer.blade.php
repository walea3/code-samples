        <footer class="main-footer">
            <nav class="footer-nav">
                <ol class="menu menu-footer l-inline">
                    <li class="copyright">&copy; 2013 Virgin Media</li>
                    @foreach( Config::get( 'application.info_content' ) as $key => $content )
                        <li class="{{ $key }}"><a href="#{{ $key }}" title="{{ $content[ 'title' ] }}">{{ $content[ 'title' ] }}</a>
                        </li>
                    @endforeach
                </ol>
            </nav>
            <nav class="button-footer" role="navigation">
                <a href="#top">Back to top</a>
            </nav>
        </footer><!-- End .main-footer -->

        


    </div><!-- End .wrapper -->

        <div class="off-canvas">
            <div class="off-canvas-content"></div>
        </div>

</div><!--Top Level Wrapper, used only in mobile-->

</div>

</body>
</html>
