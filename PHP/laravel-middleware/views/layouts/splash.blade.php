@if ( Session::get( 'customerHasVisited' ) !== 1 && !Config::get( 'application.isPreview' ) )
<div class="splash is-animate-fade">
    <div class="splash-content">
        <img class="vmp-logo" src="{{ URL::to_asset('images/vmp-logo.png') }}" alt="Virgin Media logo" />
        <h3 class="greeting">Hello {{ Config::get( 'application.customer.name' ) }},
        <span>Your unmissable week starts here</span></h3>
        <img class="loading" src="{{ URL::to_asset('images/icons/loading.gif') }}" alt="" />
    </div>
</div>
@endif