@layout('layouts.default')

@section('content')
	<div id="main" class="">
		{{ $tiles }}
		{{ $stories }}
	</div><!-- End #main -->
@foreach( Config::get( 'application.info_content' ) as $key => $content )
	@include( 'infobox', array( 'key' => $key, 'content' => $content ) )
@endforeach
@endsection