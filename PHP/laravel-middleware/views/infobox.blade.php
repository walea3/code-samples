	<div class="infoBox {{ $key }}">
		<a href="#" class="infoClose panel panel-close"><i class="icon icon-close"></i><span class="is-visuallyhidden"> &times; </span></a>
		<h3>{{ $content[ 'title' ] }}</h3>
		{{ HTML::p( $content[ 'body' ] ) }}
	</div>