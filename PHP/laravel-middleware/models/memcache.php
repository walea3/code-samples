<?php

class Cache {

    public function __construct() {
        $this->memcache = new Memcache() or die('install memcache!');
        $memcache->connect('localhost', 11211) or die ("Could not connect to memcache");

        $this->version = $memcache->getVersion();
    }

    public function flush() {
        $memcache->flush();
    }


    public function put($key, $value) {

        $tmp_object = new stdClass;
        $tmp_object->int_id = $value->id;
        $tmp_object->object = $value;

        $memcache->set($key, $tmp_object, false, 10) or die ("Failed to save data at the server");
        echo "Store data in the cache (data will expire in 10 seconds)<br/>\n";
    }

    public function get($key) {
        $get_result = $memcache->get($key);

        //echo "Data from the cache:<br/>\n";
        //var_dump($get_result);

        return $get_result->object;
    }

}