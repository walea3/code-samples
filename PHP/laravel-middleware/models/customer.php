<?php

class Customer {

    public static $key = "hUh783ghjek8jhwh";
    public static $separator = '|';
    public $encrypted = '';

    public function __construct() {
        ini_set('auto_detect_line_endings', TRUE);

        $url = URL::full();
        
        // tests for decryption
        // Aristotle George
        //$url = 'http://virginmediapresents.com/1the-fall/?pa=XL&sk=&utm_campaign=&utm_source=vmpresents&utm_medium=email&utm_content=main-feature&utm_term=&cust=m2wQrwq6gsAo3o0EDQkMmSzP2ZaM-rbTLNDEqN6YcFE,';
        // Asenio Hall
        // $url = 'http://virginmediapresents.com/1the-fall/?pa=L&sk=&utm_campaign=&utm_source=vmpresents&utm_medium=email&utm_content=main-feature&utm_term=&cust=uQ7j3gO5DO4MaysdUqIwgVGGYx6dnTuI8zN4wUOR-rY,';
        
        $parts = explode('?', rawurldecode($url));
        
        if (isset($parts) && count($parts)>1) {
            
            // the URL's parameters are used to populate every link on the site
            $url_parameters = $parts[1];

            // Encrypted string has 43 encrypted word chars _ and -, delimited by a comma
            preg_match('/cust\=([\w-_]+)\,/', $parts[1], $matched);
        
            if (isset($matched) && count($matched)>1) {
                $this->set_encrypted( $matched[1] );
            }

            preg_match('/pa\=([A-Z]+)/', $parts[1], $package);

            if ( isset($package[1]) && count($package) > 1 && in_array( strtoupper( $package[1] ), Config::get( 'application.packages' ) ) ) {
                Config::set( 'application.package', strtolower( $package[1] ) );
            }
        }

        if ( $this->encrypted ) {
            $custInfo = $this->getInfo();
            Config::set( 'application.customer', $custInfo );

            if ( Session::has('customer.name') && Session::get('customer.name') == $custInfo[ 'name' ] ) {
                Session::put( 'customerHasVisited', 1 );
            }

            Session::put( 'customer', $custInfo );
        }
    }

    private function set_encrypted($string) {
        $this->encrypted = $string;
    }

    /*
     * Encryption function that generates a URL-friendly string
     */
    public static function encrypt_data($value) {
    	$key = self::$key;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $value, MCRYPT_MODE_ECB, $iv);
        return strtr(base64_encode($crypttext), '+/=', '-_,');
    }

    /*
     * Decryption function (for Redwood)
     */
    private static function decrypt_data($value) {
    	$key = self::$key;
        $crypttext = base64_decode(strtr($value, '-_,', '+/='));
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    /*
     * Helper function to retrieve the user information (for Redwood)
     */
    public function getInfo() {
        if ( $this->encrypted ) {
            $plaintext = self::decrypt_data($this->encrypted);
            $customer_information = explode(self::$separator, $plaintext);
            return array(
                'id' => $customer_information[0],
                'name' => $customer_information[1],
            );
        } else {
            return false;
        }
    }
}
