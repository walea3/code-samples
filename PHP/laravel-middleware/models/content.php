<?php

class Content
{
	public function __construct( $issue = 'all' )
	{
		if ( Config::get( 'application.isPreview' ) ) {
			self::get( 'issue', $issue );
		} else {
			self::prepIssue( $issue );
		}
		self::issuePeriod();
	}

	// Prep content from WordPress
	public static function prepIssue( $issue ) {
		//get issue number to requrest from cms
		$issue = ( $issue === 'all' ? Config::get( 'application.default_issue' ) : $issue );

		//load issue data - remeber in cache if doesn't already exist
		$issueData = Cache::remember( "issue_{$issue}" , function() use ( $issue ) {
			$data = trim( Content::getData( Config::get( 'application.cms_url' ) . $issue . '/' . ( Config::get( 'application.isPreview' ) && Input::get( 'key' ) ? Input::get( 'key' ) : '' ) ) );
			$dataArray = json_decode( $data, true );

			// check error code is 0 and return data
			return isset( $dataArray[ 'metadata' ] ) && in_array( $dataArray[ 'metadata' ][ 'error_code' ], array( 0, 8 )) ? $data : json_encode( array( 'error' => $data ) );
		}, 10080); //remember for 1 week

		//store issue data in app config to be use globally
		$issueArray = json_decode( $issueData, true );
		Config::set( "application.issue", $issueArray );

		return $issueArray;
	}

	public static function get( $type = 'issue', $key ) {
		$content = 0;
		$issue = Config::get( "application.issue" );
		$package = Config::get( "application.package" );

		switch ( $type ) {
			// get all stories in current category and package
			case 'category':
				$content = array();
				$categories = Config::get( 'application.categories' );
				if ( isset( $categories[ $key ] ) ) {
					$category = $categories[ $key ];

					if ( isset( $issue[ 'mapping' ] ) && isset( $issue[ 'stories' ] ) ) {
						foreach ( $issue[ 'stories' ] as $i => $issueStory ) {
							$isFound = ( $key === 'all' || $issueStory[ 'primary_category' ] === $category );
							$isPackage = !$package || ( $isFound && ( in_array( $i , $issue[ 'mapping' ][ strtolower( $package ) ][ 'stories' ] ) || in_array( $i , $issue[ 'mapping' ][ strtolower( $package ) ][ 'ads' ] ) ) );
							if ( $isPackage )
								$content[ $i ] = $issueStory;
						}
					}
				}

				break;
			// get story and check availability in package
			case 'story':
				$keyField = is_numeric($key) ? 'story_id' : 'slug';
				foreach ( $issue[ 'stories' ] as $issueStory ) {
					$isFound = ( $issueStory[ $keyField ] == $key );

					$isPackage = !$package || ( $isFound && $issueStory[ 'package' ] == $package );
					// 1 = not available with current package
					// 0 = story not found
					$content = ( $isPackage ? $issueStory : ( !$isFound ? 0 : 1 ) );
					if ( $isFound )
						break;
				}

				break;
			// get story / ads mapping for selected category in package
			case 'mapping':
				$content = array();
				if ( !isset($issue[ 'mapping' ]) )
					return null;

				$map = $issue[ 'mapping' ][ strtolower( $package ) ];

				if ( $key && $key !== 'all' ) {
					foreach ( $issue[ 'stories' ] as $storyIndex => $issueStory ) {
						$isFound = in_array( $storyIndex, $map[ 'stories' ] ) || in_array( $storyIndex, $map[ 'stories' ] );

						$isPackage = !$package || ( $isFound &&  $issueStory[ 'primary_category' ] == Config::get( 'application.categories.' . $key ) );

						$storyIn = array_search($storyIndex, $map[ 'stories' ] );
						$adIn = array_search($storyIndex, $map[ 'ads' ] );

						if ( $isPackage && $storyIn ) {
							$content[ 'stories' ][ $storyIn ] = $storyIndex;
						} else if ( $isPackage && $adIn ) {
							$content[ 'ads' ][ $adIn ] = $storyIndex;
						}
					}
				} else { $content = $map; }

				break;
			// cleat issue cache and get content
			default:
				Cache::forget( "issue_{$key}" );
				$content = self::prepIssue( $key );
				break;
		}

		return $content;
	}

	//Format and retun the issue period (week)
	public static function issuePeriod() {
		if ( !Config::get('application.issue.metadata.issue.start_date') )
			return false;
		$start = DateTime::createFromFormat( 'Ymd', Config::get('application.issue.metadata.issue.start_date') );
		$end = DateTime::createFromFormat( 'Ymd', Config::get('application.issue.metadata.issue.end_date') );
		Config::set( 'application.issue_date', $start->format('d - ') . $end->format('d F') );
	}

	public static function getShortUrl ($url, $slug, $title = '', $format = 'simple') {
        $login = Config::get('application.virgco_login');
        $pass = Config::get('application.virgco_pass');

        if ( $login && $pass ) {
            $connectURL = "http://virg.co/yourls-api.php?action=shorturl&username=$login&password=$pass&url=" . urlencode($url) . "&title=$title&format=$format";
            $shortUrl = Cache::remember('virgco_' . $slug, function() use ( $connectURL, $url ) {
                $shortUrl = Content::getData( $connectURL );
                return $shortUrl && $shortUrl != 'RATE_LIMIT_EXCEEDED' ? $shortUrl : $url;
            }, 262974); //remember for 6 months
            return $shortUrl == 'RATE_LIMIT_EXCEEDED' ? $url : $shortUrl;
        } else { return $url; }
    }

	//Grab content via curl
	public static function getData( $url ) {
		$ch = curl_init();
        $timeout = 5;
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        $data = curl_exec($ch);

        //var_dump( curl_error( $ch ) ); var_dump( $ch ); var_dump( $data ); exit;
        curl_close($ch);
        return $data;
	}

}