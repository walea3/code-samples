<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Application URL
	|--------------------------------------------------------------------------
	|
	| The URL used to access your application without a trailing slash. The URL
	| does not have to be set. If it isn't, we'll try our best to guess the URL
	| of your application.
	|
	*/

	'url' => '',

	/*
	|--------------------------------------------------------------------------
	| Asset URL
	|--------------------------------------------------------------------------
	|
	| The base URL used for your application's asset files. This is useful if
	| you are serving your assets through a different server or a CDN. If it
	| is not set, we'll default to the application URL above.
	|
	*/

	'asset_url' => '',

	/*
	|--------------------------------------------------------------------------
	| Application Index
	|--------------------------------------------------------------------------
	|
	| If you are including the "index.php" in your URLs, you can ignore this.
	| However, if you are using mod_rewrite to get cleaner URLs, just set
	| this option to an empty string and we'll take care of the rest.
	|
	*/

	'index' => '',

	/*
	|--------------------------------------------------------------------------
	| Application Kej
	|--------------------------------------------------------------------------
	|
	| This key is used by the encryption and cookie classes to generate secure
	| encrypted strings and hashes. It is extremely important that this key
	| remains secret and it should not be shared with anyone. Make it about 32
	| characters of random gibberish.
	|
	*/

	'key' => 'kejdhsjshjshwberudkshwgshwjshdjs',

	/*
	|--------------------------------------------------------------------------
	| Profiler Toolbar
	|--------------------------------------------------------------------------
	|
	| Laravel includes a beautiful profiler toolbar that gives you a heads
	| up display of the queries and logs performed by your application.
	| This is wonderful for development, but, of course, you should
	| disable the toolbar for production applications.
	|
	*/

	'profiler' => false,

	/*
	|--------------------------------------------------------------------------
	| Application Character Encoding
	|--------------------------------------------------------------------------
	|
	| The default character encoding used by your application. This encoding
	| will be used by the Str, Text, Form, and any other classes that need
	| to know what type of encoding to use for your awesome application.
	|
	*/

	'encoding' => 'UTF-8',

	/*
	|--------------------------------------------------------------------------
	| Default Application Language
	|--------------------------------------------------------------------------
	|
	| The default language of your application. This language will be used by
	| Lang library as the default language when doing string localization.
	|
	*/

	'language' => 'en',

	/*
	|--------------------------------------------------------------------------
	| Supported Languages
	|--------------------------------------------------------------------------
	|
	| These languages may also be supported by your application. If a request
	| enters your application with a URI beginning with one of these values
	| the default language will automatically be set to that language.
	|
	*/

	'languages' => array(),

	/*
	|--------------------------------------------------------------------------
	| SSL Link Generation
	|--------------------------------------------------------------------------
	|
	| Many sites use SSL to protect their users' data. However, you may not be
	| able to use SSL on your development machine, meaning all HTTPS will be
	| broken during development.
	|
	| For this reason, you may wish to disable the generation of HTTPS links
	| throughout your application. This option does just that. All attempts
	| to generate HTTPS links will generate regular HTTP links instead.
	|
	*/

	'ssl' => true,

	/*
	|--------------------------------------------------------------------------
	| Application Timezone
	|--------------------------------------------------------------------------
	|
	| The default timezone of your application. The timezone will be used when
	| Laravel needs a date, such as when writing to a log file or travelling
	| to a distant star at warp speed.
	|
	*/

	'timezone' => 'UTC',

	/*
	|--------------------------------------------------------------------------
	| Class Aliases
	|--------------------------------------------------------------------------
	|
	| Here, you can specify any class aliases that you would like registered
	| when Laravel loads. Aliases are lazy-loaded, so feel free to add!
	|
	| Aliases make it more convenient to use namespaced classes. Instead of
	| referring to the class using its full namespace, you may simply use
	| the alias defined here.
	|
	*/

	'aliases' => array(
		'Auth'       	=> 'Laravel\\Auth',
		'Authenticator' => 'Laravel\\Auth\\Drivers\\Driver',
		'Asset'      	=> 'Laravel\\Asset',
		'Autoloader' 	=> 'Laravel\\Autoloader',
		'Blade'      	=> 'Laravel\\Blade',
		'Bundle'     	=> 'Laravel\\Bundle',
		'Cache'      	=> 'Laravel\\Cache',
		'Command'    	=> 'Laravel\\CLI\\Command',
		'Config'     	=> 'Laravel\\Config',
		'Controller' 	=> 'Laravel\\Routing\\Controller',
		'Cookie'     	=> 'Laravel\\Cookie',
		'Crypter'    	=> 'Laravel\\Crypter',
		'DB'         	=> 'Laravel\\Database',
		'Eloquent'   	=> 'Laravel\\Database\\Eloquent\\Model',
		'Event'      	=> 'Laravel\\Event',
		'File'       	=> 'Laravel\\File',
		'Filter'     	=> 'Laravel\\Routing\\Filter',
		'Form'       	=> 'Laravel\\Form',
		'Hash'       	=> 'Laravel\\Hash',
		'HTML'       	=> 'Laravel\\HTML',
		'Input'      	=> 'Laravel\\Input',
		'IoC'        	=> 'Laravel\\IoC',
		'Lang'       	=> 'Laravel\\Lang',
		'Log'        	=> 'Laravel\\Log',
		'Memcached'  	=> 'Laravel\\Memcached',
		'Paginator'  	=> 'Laravel\\Paginator',
		'Profiler'  	=> 'Laravel\\Profiling\\Profiler',
		'URL'        	=> 'Laravel\\URL',
		'Redirect'   	=> 'Laravel\\Redirect',
		'Redis'      	=> 'Laravel\\Redis',
		'Request'    	=> 'Laravel\\Request',
		'Response'   	=> 'Laravel\\Response',
		'Route'      	=> 'Laravel\\Routing\\Route',
		'Router'     	=> 'Laravel\\Routing\\Router',
		'Schema'     	=> 'Laravel\\Database\\Schema',
		'Section'    	=> 'Laravel\\Section',
		'Session'    	=> 'Laravel\\Session',
		'Str'        	=> 'Laravel\\Str',
		'Task'       	=> 'Laravel\\CLI\\Tasks\\Task',
		'URI'        	=> 'Laravel\\URI',
		'Validator'  	=> 'Laravel\\Validator',
		'View'       	=> 'Laravel\\View',
	),
	'site_name' => 'Virgin Media Unmissable',
	'fb_admins' => '571337657',
	'virgco_login' => 'vmedia',
	'virgco_pass' => 'abc123',
	'md5hash' => 'f45a0d3c05837693c61a0c5fe7db43e2',
	'cms_url' => 'https://cms.virginmediapresents.com/',
	'default_issue' => '0000_00',
	'issue_date' => '',
	'package' => 'xls',
	'packages' => array( 'M', 'MS', 'L', 'LS', 'XL', 'XLS' ),
	'categories' => array( 
		'all' => 'All',
		'tv' => 'TV',
		'movies' => 'Movies',
		'sport' => 'Sport',
		'music' => 'Music',
		'games' => 'Games',
		'exclusives' => 'Your Exclusives',
		'tips' => 'Tips & Tricks'
	),
	'request' => array( 'category' => '', 'story' => '' ),
	'customer' => array( 'name' => 'Guest' ),
	'grid_templates' => array(
		'tpl_one' => array(
			'width_px' => 3990,
			'total_tiles' => 23,
			'story_tiles' => 20,
			'ad_tiles' => array( 7, 13, 17 ),
			'tile_classes' => array(
				'hero ' => array( '01' ),
				'big' => array( '01', '04', '07', '08', '13', '17' ),
				'tile-offer' => array( '04', '17' ),
				'is-animate' => array( '02', '06', '09', '10', '11', '14', '18', '19', ),
				'is-animate-short' => array( '01' ),
			),
			'tile_animation_intervals' => array(
				'1' => array( '01', '08', '19' ),
				'3' => array( '06', '09', '14' ),
				'5' => array( '02', '10', '17' ),
				'7' => array( '07', '11', '18' ),
			),
			'image_dimensions' => array(
				'11' => array( '01', '02', '06', '07', '08', '09', '10', '11', '13', '14', '17', '18', '19' ),
				'1921' => array( '03', '04', '05', '12', '17', '15', '16', '20' )
			),
			'media-body_classes' => array(
				// 'flipIn flipOutX animated' => array( '06' ),
				// 'fadeInD fadeOutDown animated' => array( '09' ),
				// 'fadeInR fadeOutRightBig animated' => array( '11' ),
				// 'flipInWhy flipOutY animated' => array( '18' )
			)
		)
	),
	'info_content' => array(
		'about' => array(
			'title' => 'About Virgin Media',
			'body' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
		),
		'privacy' => array(
			'title' => 'Privacy & Cookies',
			'body' => "We use cookies on virginmedia.com to help us give you the best experience on our website. Our cookies have now been set in your browser, and we'll assume you’re happy with this if you keep using our website. <br> <a href=\"http://store.virginmedia.com/our-cookies.html\" target=\"_blank\" class=\"btn cta\"><span class=\"glossy\"><span class=\"tx\">Read More</span></span></a>"
		),
		'legal' => array(
			'title' => 'Legal Stuff',
			'body' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit, Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. "
		),
		'feedback' => array(
			'title' => 'Feedback',
			'body' => "We'd love to hear your comments about Virgin Media Presents. You can email us and send press releases, reports of broken links or any other queries here: virginmedia.presents@redwoodgroup.net

				For help with any of your Virgin Media products, please click the Virgin Media Help link below.>

				Thank you."
		)
	)
);
