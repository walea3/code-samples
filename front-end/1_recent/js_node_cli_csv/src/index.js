const keys = require('lodash/keys');
const reduce = require('lodash/reduce');

const statsHandlers = {};

exports.add = (key, handler) => {
  const statHandler = handler || require(`./stats/${key}`);
  if (statHandler) statsHandlers[key] = statHandler;
  return this;
};

exports.utils = require('./utils');

exports.parse = data => reduce(keys(statsHandlers), (stats, key) => {
  stats[key] = statsHandlers[key](data);
  return stats;
}, {});
