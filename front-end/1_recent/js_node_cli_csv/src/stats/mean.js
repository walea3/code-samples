const mean = require('lodash/mean');
const utils = require('../utils');

module.exports = data => mean(utils.allIntegers(data)).toFixed(3);
