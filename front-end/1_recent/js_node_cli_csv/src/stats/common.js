const _ = require('lodash');
const last = require('lodash/last');
const utils = require('../utils');

module.exports = data => _(utils.allIntegers(data))
  .countBy()
  .toPairs()
  .maxBy(last)
  .shift();
