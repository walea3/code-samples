const _ = require('lodash');
const sum = require('lodash/sum');
const utils = require('../utils');

module.exports = data => _(data)
  .map(item => sum(utils.parseRecord(item)))
  .max();
