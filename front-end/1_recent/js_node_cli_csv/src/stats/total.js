const sum = require('lodash/sum');
const utils = require('../utils');

module.exports = data => sum(utils.allIntegers(data));
