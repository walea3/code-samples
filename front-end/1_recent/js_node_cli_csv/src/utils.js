const map = require('lodash/map');
const compact = require('lodash/compact');
const flattenDeep = require('lodash/flattenDeep');
const toInteger = require('lodash/toInteger');

const parseRecord = record => compact(map(record, toInteger));
const allIntegers = data => flattenDeep(map(data, parseRecord));

module.exports = {
  parseRecord,
  allIntegers
};
