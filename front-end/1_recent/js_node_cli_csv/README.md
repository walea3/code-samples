# CSV Stats analysis
> Adewale Adeyemi

## Requirements
- [Node 5.0+](https://nodejs.org/en/)
  - To better manage installed versions use [NVM](https://github.com/creationix/nvm#install-script)

## Getting started
- Open a terminal with this as the current directory.
- Install dependencies
  ```bash
  npm install
  ```
- Running program with cli
  ```bash
  npm run stats -- path/to/file.csv
  ```
