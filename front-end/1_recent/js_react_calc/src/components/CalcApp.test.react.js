/** @jsx React.DOM */
'use strict';

jest.dontMock('./CalcApp.react.js');

var
  React = require('react/addons'),
  CalcApp = require('./CalcApp.react');


describe('CalcApp.react', function() {
  it('should expect calc prop and set calc.curr to initial state', function() {
    
  });

  it('should own CalcDisplay within div.calc__display', function() {
    
  });

  it('should own CalcDisplay with display prop', function() {
    
  });

  it('should own CalcControls', function() {
    
  });
});