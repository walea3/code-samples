/**
 * Calculator controls component
 * @jsx React.DOM
 * @module: CalcControls
 */
'use strict';

var
  React = require('react'),

  INPUT_LIST = ['9','8','7','6','5','4','3','2','1','0','.','='],
  OPERATOR_LIST = ['\u002b','\u2212','\u00d7','\u00f7'],

  CalcKey = React.createClass({
    render: function () {
      var
        className = 'calc__key',
        modifier = ['.','='].indexOf( this.props.val ) >= 0 || !this.props.type ? ' ' + className + '--dark' : '';
      return (
        <button className={ className + ( this.props.type ? '--' + this.props.type : '' ) + modifier }
        onClick={ this.props.onInput.bind(null, this.props.val) }>{ this.props.val }</button>
      );
    }
  }),

  CalcControls = React.createClass({
    statics: {
      VALID_INPUTS: INPUT_LIST.concat(OPERATOR_LIST).join('').replace(/\,/g,'')
    },
    render: function () {
      return (
        <section className="calc__controls">
          <div className="calc__keys">
            <div className="calc__inputs">
              {INPUT_LIST.map( this._addKey.bind(this, 'input') )}
            </div>
            <div className="calc__operators">
              {OPERATOR_LIST.map( this._addKey.bind(this, 'operator') )}
            </div>
          </div>
          <div className="calc__keys">
            <div className="calc__clear">
              {this._addKey( null, 'C', 'clr' )}
            </div>
            <div className="calc__del">
              {this._addKey( null, '<', 'del' )}
            </div>
          </div>
        </section>
      );
    },
    _addKey: function ( type, val, index ) {
      return this.transferPropsTo(<CalcKey key={ index } type={ type } val={ val } />);
    }
  });

module.exports = CalcControls;