/** @jsx React.DOM */
'use strict';

jest.dontMock('./CalcControls.react.js');

var
  React = require('react/addons'),
  Controls = require('./CalcControls.react'),
  CalcControls = Controls.reactClass;


describe('CalcControls.react', function() {
  it('should own CalcKey for input key within div.calc__inputs', function() {
    
  });

  it('should own CalcKey for operator keys within div.calc__operators', function() {
    
  });
});