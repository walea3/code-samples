/**
 * Calculator app component
 * @jsx React.DOM
 * @module: CalcApp
 */
'use strict';

var
  React = require('react'),
  CalcControls = require('./CalcControls.react'),

  SUBMIT_KEYS = [13,187],
  DEL_KEYS = [8,46],
  CLEAR_KEYS = [99,27,35],

  CalcDisplay = React.createClass({
    render: function () {
      return <div className="calc__display">{ this.props.display }</div>;
    }
  }),

  CalcApp = React.createClass({
    componentDidMount: function () {
      React.initializeTouchEvents(true);
      document.addEventListener('keypress', this.onKeyPress);
    },
    componentDidUnmount: function () {
      React.initializeTouchEvents(false);
      document.removeEventListener('keypress', this.onKeyPress);
    },
    render: function () {
      return (
        <section className="calc">
          <CalcDisplay display={ this.props.calc.curr.op.join('') } />
          <CalcControls onInput={ this.onInput } />
        </section>
      );
    },
    onInput: function ( val, e ) {
      // console.log( val, typeof val );
      switch( val ) {
        case '=':
          this.props.calc.calculate();       
          break;
        case '<':
          this.props.calc.del();
          break;
        case 'C':
          this.props.calc.clr();
          break;
        default:
          this.props.calc.key( val );
      }
      this.forceUpdate();
      e && e.target.blur();
    },

    onKeyPress: function ( e ) {
      var code = e.which || e.keyCode, key = String.fromCharCode( code );
      // console.log( key, code, CalcControls.VALID_INPUTS.indexOf( key ) >= 0, SUBMIT_KEYS.indexOf( code ) >= 0, DEL_KEYS.indexOf( code ) >= 0 );
      if ( CalcControls.VALID_INPUTS.indexOf( key ) >= 0 ) {
        return this.onInput( key );
      }

      if ( e.target === document.body ) {
        SUBMIT_KEYS.indexOf( code ) >= 0 && this.onInput( '=' );
        DEL_KEYS.indexOf( code ) >= 0 && this.onInput( '<' );
        CLEAR_KEYS.indexOf( code ) >= 0 && this.onInput( 'C' );
      }
    }
  });

module.exports = CalcApp;