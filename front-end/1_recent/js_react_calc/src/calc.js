/*!
 * Simple Calculator
 * @module: Calc
 * @author: Wale Adeyemi
 */
 
/*jslint white:true */
'use strict';

/**
 * @class Calc
 * @constructor
 * @alias module:calc
 */
function Calc() {
  this.hist = [];
  this.clr();
}

/** 
 * @const
 */
Calc.ADD_VAL = '\u002b'; // +
Calc.SUBTRACT_VAL = '\u2212'; // −
Calc.MULTIPLY_VAL = '\u00d7'; // ×
Calc.DIVIDE_VAL = '\u00f7'; // ÷
Calc.IS_INPUT = /^\d*[.]?\d+$/;
Calc.IS_DECIMAL = /^\.$/;
Calc.IS_OPERATOR = new RegExp('^[' + Calc.ADD_VAL + Calc.SUBTRACT_VAL + Calc.MULTIPLY_VAL + Calc.DIVIDE_VAL + ']$');

/**
 * Clear current operation
 * @function Calc#clr
 * @api public
 * @returns {instance}
 */
Calc.prototype.clr = function () {
  this.curr = { op: [], total: 0 };
  return this;
};

/**
 * Delete last item from current operation
 * @function Calc#del
 * @api public
 * @returns {instance}
 */
Calc.prototype.del = function () {
  var last = this.curr.op[ this.curr.op.length - 1 ];

  if ( last ) {
    last.length > 1 ? (this.curr.op[ this.curr.op.length - 1 ] = last.substr(0, last.length - 1)) : this.curr.op.pop();
  }
  return this;
};

/**
 * Add an input to current operation
 * @function Calc#key
 * @api public
 * @params {number / string} val - Number, decimal or operator + - * /
 * @returns {instance}
 */
Calc.prototype.key = function ( val ) {
  val = val.toString();
  var
    prev = this.curr.op[ this.curr.op.length - 1 ],
    isInput = Calc.IS_INPUT.test( val ),
    isDecimal = Calc.IS_DECIMAL.test( val ),
    isOperator = Calc.IS_OPERATOR.test( val );

  // console.log( val, prev, isInput, isDecimal, isOperator );

  // first item cannot be decimal or operator
  // & only single decimal
  if (  !( isInput || isDecimal || isOperator ) 
    || ( !prev && (isOperator || isDecimal) )
    || ( prev && prev.indexOf('.') >= 0 && isDecimal )
  ) {
    throw new Error('invalid_input');
  }

  if ( isOperator && Calc.IS_OPERATOR.test( prev ) ) {
    // replace previous operator
    this.curr.op[ this.curr.op.length - 1 ] = val;
  } else if ( prev && ( Calc.IS_INPUT.test( prev ) || Calc.IS_DECIMAL.test( prev.substr(-1) ) ) && ( isInput || isDecimal ) ) {
    // update previous input
    this.curr.op[ this.curr.op.length - 1 ] += val;
  } else {
    // add new input
    this.curr.op.push( val );
  }
  return this;
};

/**
 * Calculates current operation
 * @function Calc#calculate
 * @api public
 * @returns {instance}
 */
Calc.prototype.calculate = function () {
  var op = this.curr.op, len = op.length, currTotal, val, i;

  for (i = 0; i < len; i++) {
    if ( Calc.IS_OPERATOR.test( op[i] ) ) { 
      continue; 
    }

    val = Math.abs( op[i] );
    // console.log( op[i], val );

    if ( i === 0 ) {
      this.curr.total = val;
      continue;
    }
    // console.log( this.curr.total, op[i-1], val );

    switch( op[i-1] ) {
      case Calc.ADD_VAL:
        this.curr.total += val;
        break;
      case Calc.SUBTRACT_VAL:
        this.curr.total -= val;
        break;
      case Calc.MULTIPLY_VAL:
        this.curr.total *= val;
        break;
      case Calc.DIVIDE_VAL:
        this.curr.total /= val;
        break;
      default:
        this.curr.total = val;
    }
  }

  currTotal = this.curr.total;
  this.hist.push( this.curr );
  this.clr();
  // set current total as the first input
  this.curr.op.push( currTotal.toString() );
  return this;
};

window.Calc = module.exports = Calc;