/**
 * ReactCalcApp bootstrap.
 * @jsx React.DOM
 * @author: Wale Adeyemi
 */
'use strict';

var
  React = require('react'),
  Calc = require('./Calc'),
  CalcApp = require('./components/CalcApp.react');

React.renderComponent( <CalcApp calc={ new Calc() } />, document.getElementById('container') );