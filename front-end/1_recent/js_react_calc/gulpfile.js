'use strict';

var 
  gulp = require('gulp'),
  jsxhint = require('jsxhint'),
  browserify = require('browserify'),
  reactify = require('reactify'),
  streamify = require('gulp-streamify'),
  uglify = require('gulp-uglify'),
  source = require('vinyl-source-stream');


gulp.task('lint', function () {
  gulp.src(['./src/**/*.js', './gulpfile.js'])
    .pipe(jsxhint('.jshintrc'));
});

gulp.task('build:libs', function() {
  browserify({
    insertGlobals: true
  })
  .require('react/dist/react.min', { expose: 'react' })
  .bundle()
  .pipe(source('libs.js'))
  .pipe(streamify(uglify({ preserveComments: 'some' })))
  .pipe(gulp.dest('./'));
});

gulp.task('build:app', function() {
    browserify('./src/index.js')
    .transform(reactify)
    .external('react')
    .bundle()
    .pipe(source('app.js'))
    .pipe(streamify(uglify({ preserveComments: 'some' })))
    .pipe(gulp.dest('./'))
});

gulp.task('connect', function () {
  var app = require('connect')();

  app.use(require('serve-static')('./'));

  require('http')
    .createServer( app )
    .listen(8800)
    .on('listening', function () {
      console.log('Started connect web server on http://0.0.0.0:8800');
    });
});

gulp.task('watch', function() {
  gulp.watch(['src/**/*', '!src/**/*.test*'], ['build:app']);
});

gulp.task('default', ['build:libs', 'build:app', 'connect', 'watch']);