# Hybrid mobile application platform
### Codename: Viper Moth

## Overview
Viper Moth aims to serve as a foundation for cross-platform mobile application development - the core of which is made up of web technologies, primarily; JavaScript, HTML, CSS & HTTP.

This 'web' core will be packaged up in a the native code (Objective-C for iOS, Java for Android, etc) using the supported platforms SDKs, creating an installable app than can be distributed in each platforms app store. This is acomplished by locally hosting the web application in a native WebView via Cordova/PhoneGap.

Cordova's core plugins provide the mobile device's main functionality to the WebView by creating a JavaScript facade/API to interface with the native code, bridging the gap between the native technologies of the mobile platforms and the web app core.


## Architecture

### Technology Stack
This platform is built up from the following open-source technologies/tools:

* [AngularJS]( http://angularjs.org/ ):
	Front-end application development framework follows MVC style principals and extends HTML with directives
* [Cordova]( http://cordova.apache.org/ ):
  Provides JavaScript libraries with APIs to access native device functions and event listeners for device events
* [ionic]( http://ionicframework.com/ ):
  The base Front-end UI framework

Additional technologies for development & workflow:

* [Node]( http://nodejs.org )
* [Bower]( http://bower.io )
* [Grunt]( http://gruntjs.com/getting-started )
* [SASS]( http://sass-lang.com ) & [Compass]( http://compass-style.org/install ), Compiled with [Ruby]( https://www.ruby-lang.org/en/downloads )
* Testing suite: [Jasmine](http://jasmine.github.io/edge/introduction.html), [Karma](http://karma-runner.github.io/) & [Protractor](https://github.com/angular/protractor)

* for development deps - see package.json
* for application deps - see bower.json

### Application structure
The platform consists of modular core and service components and adopts a package by feature approach, which should promote good planning, organisation and navigation of code.

As development is based on AngularJS which is very opinionated, the platform code is divided up into various blocks within dependancy injectable modules i.e, controllers, services & directives amongst others.


## Core components
These components are designed to be both essential and optional requirements for most applications, creating a foundation to begin planning and development. Most should be project/application agnostic by providing useful features such as; navigation, storage, auth/account and settings.

NB. There may be inter-dependancies between other core components.

### Current components:
* **Shell:**
  A General purpose module for all common features and functionality for the application container, controllers & views

* **Navigation:**
  Controls navigation requirements, app side menus, allow modules to register menu items and groups

* **Account:**
  Manages anything relating to user account, authentication and authorisation through out the app

* **Storage:**
  For any data that needs to be stored locally or sent/synchronised to an external service / API by allowing modules to add resources (endpoints) and create storage factories.

  It will cater for any offline capabilities with a sync queue for data syncing tasks.

* **Events:**
  Application-wide events manager (pub/sub & listeners)

* **Settings:**
  Provides a settings list and allow other modules to register setting options, it will also manage the about view

* **Dashboard:**
  Acts as center piece of the application allowing other modules / services to register their cards (widgets) for display

* **Platform:**
  Provides a bridge to the mobile device features and functionality, i.e. device info, network, notification

* **Pages:**
  Manages content simple pages

* **Helpers**

* **Demo mode**


## Service components
These are dependant on the project/app and will generally vary. They will have core component dependance in most cases, i.e. a services that needs a link in the app's menu will require the navigation component and register it's menu items.

Some projects may require other service components and the dependacy injection AngularJS provides makes this a simple process.

### Example component: Weather
A weather feed and outside temp. based on defined location via an external service such as Yahoo! or Open Weather.

The module will consist of the following:

  * **Weather service**
    - to abscract interaction with the external weather service API. It will provide functionality to: request current temp based on location and request forcast for X amount of days
    - this service will require the core storage component to store data locally
  * **Dashboard card w/ view template**
    - to display a forecast feed on the dashboard screen. This depends on the dashboard component and will be injected into the module to allow creation of the card
  * **Current weather directive w/ template**
    - to display the current outside temp. This can be displayed any where in the app
    - it can also be available as a dashboard card


## Connectivity & Data
As with any mobile / web application, connection to a backend service is required, unless it's purpose is for offline use only.

The primary means of server-side communication will be over HTTP/S ideally with a REST API/service and request and response data will mostly be in JSON format.

A WebSocket or long polling connection may be useful for certain tasks/events for much quicker responses.


## Cordova Plugins
The main cordova plugins included and utilised by the platform are:

* [Device](http://plugins.cordova.io/#/package/org.apache.cordova.device)
* [Dialogs](http://plugins.cordova.io/#/package/org.apache.cordova.dialogs) & [Vibration](http://plugins.cordova.io/#/package/org.apache.cordova.vibration)
* [InAppBrowser](http://plugins.cordova.io/#/package/org.apache.cordova.inappbrowser)
* [Network Information](http://plugins.cordova.io/#/package/org.apache.cordova.network-information)

### Getting started
-------------------
Run 'npm install';
 - This will install the node and bower dependancies