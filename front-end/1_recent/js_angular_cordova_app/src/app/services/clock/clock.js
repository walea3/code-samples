/*global define */
define([
  'angular',
], function ( ng ) {
  'use strict';
  // Clock service module
  // ------------------------

  //  services/clock/clock.js
  return ng.module('service.clock', [])
    .directive('controlTime', [function () {
      return {
        restrict: 'AE',
        replace: true,
        scope: {
          time: '=for',
          action: '@'
        },
        templateUrl: 'services/clock/clock.control.html',
        link: function (scope) {
          scope.changeTime = false;
          scope.updateTime = function () {

          };
          scope.updateClock = function () {
            var end = new Date( scope.time ),
            second = end.getSeconds() * 6,
            minute = end.getMinutes() * 6 + second / 60,
            hour = ((end.getHours() % 12) / 12) * 360 + 90 + minute / 12;

            ng.element( document.querySelector('.clock__hour') ).css({
              'transform': 'rotate(' + hour + 'deg)',
              '-webkit-transform': 'rotate(' + hour + 'deg)'
            });
            ng.element( document.querySelector('.clock__minute') ).css({
              'transform': 'rotate(' + minute + 'deg)',
              '-webkit-transform': 'rotate(' + minute + 'deg)'
            });
            ng.element( document.querySelector('.clock__second') ).css({
              'transform': 'rotate(' + second + 'deg)',
              '-webkit-transform': 'rotate(' + second + 'deg)'
            });
          };
          scope.updateClock();
          scope.$watch('time', scope.updateClock);
        }
      };
    }])

    .factory('Clock', [function () {
      return {
      };
    }]);
});