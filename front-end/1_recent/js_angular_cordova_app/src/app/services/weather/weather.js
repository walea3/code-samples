/*global define */
define([
  'angular',
  'core/storage/storage',
  'core/dashboard/dashboard'
], function ( ng ) {
  'use strict';
  // Weather service module
  // ------------------------

  //  services/weather/weather.js
  return ng.module('service.weather', [
    'core.storage', 'core.dashboard'
  ])

  .config(['DashboardProvider', function ( DashboardProvider ) {
    var mainDash = DashboardProvider.board('main');

    mainDash.card('weather', {
      templateUrl: 'services/weather/weather.card.html',
      controller: 'WeatherCardCtrl',
      position: 1
    });

    mainDash.card('forecast', {
      template: '<forecast days="2" bindonce bo-text="placeholder"></forecast>',
      position: 2
    });
  }])

  .directive('forecast', ['Weather', function ( Weather ) {
    return {
      restrict: 'E',
      link: function (scope, el, attr) {
        scope.placeholder = attr.days + '-DAY FORECAST';
        scope.forecast = Weather.forecast( attr.days );
      }
    };
  }])

  .controller('WeatherCardCtrl', ['$scope', 'Weather', 'Dashboard', function ($scope, Weather, Dashboard) {
    $scope.card = Dashboard.board('main').card('weather');
    $scope.weather = Weather.now();
  }])

  .factory('Weather', [function () {
    var providers = {
      'openweathermap' : {
        key: '...',
        query: function ( location, forecast ) {
          var
            api = 'http://api.openweathermap.org/data/2.5/' + (forecast ? 'forecast' : 'weather') + '?type=accurate',
            loc = (function () {
              if ( location.lat && location.lon ) {
                return '&lat=' + location.lat + '&lon=' + location.lon;
              }
              return '&q=' + location;
            })(),
            count = '&cnt=' + forecast || '1',
            units = '&units=metric';
          return api + loc + count + units;
        },
        now: function () {
          var weatherData = this.query( 'Bristol, GB' );
          return weatherData;
        },
        forecast: function ( days ) {
          var forecastData = this.query( 'Bristol, GB', days );
          return forecastData;
        }
      }
    },
    currentProvider = 'openweathermap',
    provider = providers[ currentProvider ];

    return {
      now: provider.now.bind( provider ),
      forecast: provider.forecast.bind( provider )
    };
  }]);
});