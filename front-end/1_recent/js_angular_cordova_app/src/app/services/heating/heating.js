/*global define */
define([
  'angular',
  'core/app.conf',
  'core/storage/storage',
  'core/navigation/navigation',
  'core/dashboard/dashboard.service',
  'core/settings/settings'
], function ( ng ) {
  'use strict';
  // Heating service module
  // ------------------------

  //  services/heating/heating.js
  return ng.module('service.heating', [
    'core.config',
    'core.storage', 'core.navigation',
    'core.dashboard', 'core.settings'
  ])

  .factory('Heating', ['Schedule', 'Settings', 'Events', '$interval', function ( Schedule, Settings, Events, $interval ) {
    var
      inTemp = 18,
      outTemp = 12,
      targetTemp = Schedule.now().setpoint,
      tempCheck,
      checkTemp = function () {
        $interval.cancel(tempCheck);
        tempCheck = $interval(function() {
          inTemp += inTemp < targetTemp ? 0.35 : ( inTemp >= ( outTemp + 2 ) && inTemp > ( targetTemp + 0.35 ) ? -0.12 : 0.01 );

          Events.publish('heating:temp', {
            now: Number(parseFloat( inTemp ).toFixed(1)),
            target: targetTemp
          });
        }, inTemp <= targetTemp ? 5000 : 10000);
        return Number(parseFloat( inTemp ).toFixed(1));
      },
      checkHeating = function () {
        return targetTemp > inTemp;
      };

    Events.subscribe('heating:setpoint', function (ev, to) {
      targetTemp = to.args;
      checkTemp();
    });

    return {
      status: checkHeating,
      now: checkTemp,
      out: outTemp,
      target: targetTemp,
      schedule: Schedule.for.bind( null, 'heating' ),
      settings: function ( all ) {
        var heatingSettings = Settings.for( 'heating' );
        return all ? heatingSettings : heatingSettings.settings;
      }
    };
  }])

  .filter( 'tempUnit', ['Heating', function ( Heating ) {
    var
      settings = Heating.settings(),
      tpl = ' &#12442;<span class="temp__unit">' + settings.tempUnit.value + '</span>';
    return function ( val ) {
      return val + tpl;
    };
  }])

  .directive('heatingStatus', ['Heating', 'Events', function ( Heating, Events ) {
    return {
      restrict: 'AE',
      replace: true,
      scope: true,
      templateUrl: 'services/heating/heating.status.html',
      link: function (scope) {
        function updateStatus () {
          scope.heating = Heating.status();
        }
        var
          setpointWatch = Events.subscribe('heating:setpoint', updateStatus.bind(null)),
          tempWatch = Events.subscribe('heating:temp', updateStatus.bind(null));

        updateStatus();
        scope.$on('$destroy', function () {
          setpointWatch();
          tempWatch();
        });
      }
    };
  }])

  .directive('controlSetpoint', ['$timeout', 'Events', 'Heating', function ($timeout, Events, Heating) {
    return {
      restrict: 'AE',
      replace: true,
      templateUrl: 'services/heating/setpoint.control.html',
      link: function ( scope, $el ) {
        var updating, heatLevel = 0;

        function update( make ) {
          var inc = function (lower) {
            var indClass;
          heatLevel += lower ? -1 : 1;
          indClass = 'ind' + (heatLevel < 0 ? '-minus' : '-plus-');// + (heatLevel + (lower && heatLevel > 0 ? 1 : 0));
          scope.tempStatus = '';

          if ( heatLevel === 0 ) {
            $el[0].className = $el[0].className.replace(/\bind-(.*)\s?\b/g, '');
          } else {
            if ( (heatLevel > 0 && !lower) || (heatLevel < 0 && lower) ) {
              indClass += heatLevel;
              $el.addClass( indClass );
            } else {
              indClass += heatLevel + ( heatLevel < 0 ? -1 : 1 );
              $el.removeClass( indClass );
            }
          }
            // $el.addClass('ind-' + status);
            return lower ? scope.targetTemp-- : scope.targetTemp++;
          };

          if ( (make === 'warmer' && heatLevel === 3 ) || (make === 'cooler' && heatLevel === -3) ) {
          // if ( (make === 'warmer' && scope.targetTemp >= 32) || (make === 'cooler' && scope.targetTemp <= 5) ) {
            return;
          }

          inc( make !== 'warmer' );
          $timeout.cancel( updating );
          $timeout.cancel( scope.$parent.controlDisplay );

          // Simulate updating
          updating = $timeout(function () {
            scope.tempStatus = '<i class="icon ion-refreshing"></i> Updating ...';
            updating = $timeout(function () {
              Events.publish('heating:setpoint', scope.targetTemp);
              scope.tempStatus = '<i class="icon ion-checkmark-circled"></i> Saved';
              updating = $timeout(function () {
                scope.tempStatus = '';
                scope.$parent.state.showControls = false;
              }, 500);
            }, 1000);
          }, 3000);
        }

        scope.targetTemp = Heating.target;
        scope.tempNow = Heating.now();
        scope.warmer = update.bind(null, 'warmer');
        scope.cooler = update.bind(null, 'cooler');
        Events.publish('heating:setpoint', scope.targetTemp);
        Events.subscribe('heating:setpoint', function ( ev, to ) {
          scope.targetTemp = to.args;
        });
      }
    };
  }])

  .controller('HeatingCardCtrl', ['$scope', 'Heating', 'Dashboard', 'Events', function ($scope, Heating, Dashboard, Events) {
    $scope.card = Dashboard.board('main').card('heating');
    $scope.temp = Heating.now();
    $scope.outside = Heating.out;
    Events.subscribe('heating:temp', function ( ev, to ) {
      $scope.temp = to.args.now;
    });
  }])

  .controller('HeatingControlCtrl', ['$scope', 'Heating', function ($scope, Heating) {
    $scope.setTitle('HEATING CONTROL');
    $scope.temp = Heating.now();
  }])

  .controller('HeatingScheduleCtrl', ['$scope', 'Heating', function ($scope, Heating) {
    $scope.setTitle('HEAT SCHEDULE');
    $scope.schedule = Heating.schedule();
  }])

  .controller('HeatingSettingsCtrl', ['$scope', 'Heating', function ($scope, Heating) {
    $scope.setTitle('HEAT SETTINGS');
    $scope.settings = Heating.settings();
  }])

  .config(['$stateProvider', '$httpProvider', 'NavProvider', 'StorageProvider', 'DashboardProvider', 'SettingsProvider',
    function ( $stateProvider, $httpProvider, NavProvider, StorageProvider, DashboardProvider, SettingsProvider ) {
      $stateProvider
        .state('heating', {
          abstract: true
        })

        .state('heating.control', {
          parent: 'core.main',
          url: '/heating',
          templateUrl: 'services/heating/control.html',
          controller: 'HeatingControlCtrl',
          accessLevel: accessLevels.user
        })

        .state('heating.schedule', {
          parent: 'core.main',
          url: '/heating/schedule',
          templateUrl: 'services/heating/schedule.html',
          controller: 'HeatingScheduleCtrl',
          accessLevel: accessLevels.user
        })

        .state('heating.settings', {
          parent: 'settings',
          url: '/heating',
          accessLevel: accessLevels.user,
          views: {
            'settings' : {
              templateUrl: 'services/heating/settings.html',
              controller: 'HeatingSettingsCtrl'
            }
          }
        });

      NavProvider.menu( 'main', [{
        key: 'heatingControl',
        title: 'Heating',
        items: [{
          title: 'Control Heating',
          state: 'heating.control'
        },
        {
          title: 'Manage Heating',
          state: 'heating.schedule'
        }],
        position: 1
      }]);

      var mainDash = DashboardProvider.board('main');
      mainDash.card('boost', {
        template: '<div class="boiler" ng-init="boosting=false" ng-click="boosting=!boosting" ng-class="{\'boosting\': boosting}"><h4 class="ion-flame" ng-bind="boosting?\'Boosting\':\'Boost\'"></h4></div>',
        position: 1
      });
      // mainDash.card('heating-status', {
      //   template: '<heating-status></heating-status>',
      //   position: 1
      // });

      SettingsProvider.group('heating', {
        title: 'Heating', state: 'heating.settings'
      })
      .setting('tempUnit', {
        title: 'Set temp. unit',
        type: 'switch',
        options: {
          'metric' : 'C',
          'imperial' : 'F'
        },
        value: 'C'
      });

      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
  ]);
});