angular.module('core.templates', ['core/account/login/login.html', 'core/account/register/account.html', 'core/account/views/changeemail.html', 'core/account/views/changepassword.html', 'core/account/views/endpoints.html', 'core/account/views/resetpassword.html', 'core/dashboard/dashboard.html', 'core/demo/tour.html', 'core/navigation/menu-list.html', 'core/settings/about.html', 'core/settings/settings.html', 'core/shell/layouts/app.html', 'core/shell/layouts/content.html', 'core/shell/layouts/main.html', 'core/shell/partials/action-bar.html', 'core/shell/partials/brand-nav-button.html', 'core/shell/partials/branding-full.html', 'core/shell/partials/loading.html', 'core/shell/partials/modal.html', 'core/shell/views/error.html', 'core/shell/views/landing.html', 'core/shell/views/page.html', 'services/clock/clock.control.html', 'services/heating/control.html', 'services/heating/heating.card.html', 'services/heating/heating.status.html', 'services/heating/schedule.html', 'services/heating/setpoint.control.html', 'services/heating/settings.html', 'services/occupancy/footer.card.html', 'services/occupancy/holiday.card.html', 'services/occupancy/occupancy.control.html', 'services/occupancy/occupancy.switch.html', 'services/occupancy/schedule.card.html', 'services/occupancy/schedule.html', 'services/occupancy/settings.html', 'services/weather/weather.card.html']);

angular.module("core/account/login/login.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/account/login/login.html",
    "<ion-view title=\"Sign in\" hide-nav-bar=\"true\">\n" +
    "  <ion-content has-footer=\"true\" padding=\"true\">\n" +
    "    <!-- <div core-branding=\"{ style: 'white' }\"></div> -->\n" +
    "\n" +
    "    <form class=\"page-form\" name=\"loginForm\" role=\"login\" novalidate\n" +
    "    ng-class=\"{ 'is-working': working }\"\n" +
    "    ng-submit=\"submit(loginForm)\">\n" +
    "      <label class=\"item item-input\">\n" +
    "        <span class=\"input-label\">Email</span>\n" +
    "        <input required type=\"email\" class=\"form-control\" ng-model=\"login.email\" name=\"email\" autocapitalize=\"off\"  placeholder=\"\">\n" +
    "      </label>\n" +
    "      <label class=\"item item-input\">\n" +
    "        <span class=\"input-label\">Password</span>\n" +
    "        <input required type=\"password\" class=\"form-control\" ng-model=\"login.password\" name=\"password\" placeholder=\"\">\n" +
    "      </label>\n" +
    "\n" +
    "      <div class=\"cta\">\n" +
    "        <button class=\"btn-lg bg-orange full\" type=\"submit\"><span>Sign in</span></button>\n" +
    "        <label class=\"form-group checkbox\">\n" +
    "          <input type=\"checkbox\" ng-model=\"login.autoLogin\"> Stay logged in\n" +
    "        </label>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </ion-content>\n" +
    "\n" +
    "  <ion-footer-bar class=\"padding\">\n" +
    "    <a class=\"button icon-left ion-arrow-left-b button-clear button-light\" ui-sref=\"core.landing\">Back</a>\n" +
    "    <a href=\"#/reset-password\">Forgot your password?</a>\n" +
    "  </ion-footer-bar>\n" +
    "\n" +
    "  <modal title=\"Login error\" active=\"login.wrong\" action=\"retryLogin()\" cta=\"Try again\" cancelable=\"true\">\n" +
    "    Please enter a valid e-mail address and password\n" +
    "  </modal>\n" +
    "</ion-view>");
}]);

angular.module("core/account/register/account.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/account/register/account.html",
    "<ion-view title=\"Create your account\" hide-nav-bar=\"true\">\n" +
    "  <ion-content padding=\"true\">\n" +
    "    <div core-branding></div>\n" +
    "\n" +
    "    <form class=\"page-form\" name=\"registerForm\" role=\"registration\" novalidate\n" +
    "    ng-class=\"{ 'is-working': working }\"\n" +
    "    ng-submit=\"submit(registerForm)\">\n" +
    "      <!-- <h3>Create your account</h3> -->\n" +
    "      <p>Your supplier account has been confirmed, you can now create your account.</p>\n" +
    "      <div class=\"form-group float-input\">\n" +
    "        <input type=\"email\" class=\"form-control\" placeholder=\"Email address\" name=\"email\"\n" +
    "        required floating-label\n" +
    "        ng-model=\"regUser.email\">\n" +
    "        <label for=\"email\">Email address</label>\n" +
    "        <ul class=\"errors\"\n" +
    "        ng-class=\"{ active: registerForm.email.$invalid && registerForm.email.$dirty }\">\n" +
    "          <li class=\"error ng-hide\"\n" +
    "          ng-show=\"registerForm.email.$error.email\">E-Mail seems invalid.</li>\n" +
    "          <li class=\"error ng-hide\"\n" +
    "          ng-show=\"registerForm.email.$error.used\">E-Mail is already taken.</li>\n" +
    "        </ul>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group float-input\">\n" +
    "        <input type=\"password\" class=\"form-control\" placeholder=\"Create password\" name=\"password\"\n" +
    "        required floating-label\n" +
    "        ng-model=\"regUser.password\">\n" +
    "        <label for=\"password\">Create password</label>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"cta\">\n" +
    "        <button class=\"btn-lg\" type=\"submit\"><span>Sign up</span></button>\n" +
    "      </div>\n" +
    "\n" +
    "      <p class=\"small-print\">\n" +
    "        <span>By clicking <strong>Sign up</strong>, I agree to WaleA3's<br><a href=\"#/terms\">Terms of Service</a> and <a href=\"#/privacy\">Privacy Policy</a></span>\n" +
    "      </p>\n" +
    "    </form>\n" +
    "  </ion-content>\n" +
    "  <ion-footer-bar class=\"hide-on-keyboard-open padding\">\n" +
    "    <a class=\"button icon-left ion-arrow-left-b button-clear button-light\" ui-sref=\"core.landing\">Back</a>\n" +
    "  </ion-footer-bar>\n" +
    "\n" +
    "  <form class=\"page-form\" name=\"verifyForm\" role=\"validation\" novalidate\n" +
    "    ng-submit=\"confirmAccount(verifyForm)\">\n" +
    "    <modal title=\"Complete sign up\" active=\"verifyEmail\" action=\"confirmAccount(verifyForm)\" cta=\"Finish\" cancelable=\"true\">\n" +
    "      <p>A confirmation email has been sent to you.</p>\n" +
    "      <p>Please click the <strong>confirm link in the email</strong> or <strong>enter the code</strong> from the email.</p>\n" +
    "        <div class=\"form-group\">\n" +
    "          <input type=\"text\" class=\"form-control dark-input lg-input all-caps\" placeholder=\"Enter verification code\" name=\"verify_code\"\n" +
    "          required no-spaces\n" +
    "          ng-model=\"$parent.verifyCode\">\n" +
    "        </div>\n" +
    "    </modal>\n" +
    "  </form>\n" +
    "\n" +
    "  <modal title=\"Registration failed\" active=\"hasError\" action=\"clearError()\" cta=\"Try again\" cancelable=\"true\">\n" +
    "    {{ errorMsg }}\n" +
    "  </modal>\n" +
    "</ion-view>");
}]);

angular.module("core/account/views/changeemail.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/account/views/changeemail.html",
    "<core-view title=\"Change email\" left-buttons=\"leftButtons\" right-buttons=\"rightButtons\">\n" +
    "  <core-content has-header=\"true\" scroll=\"true\" padding=\"true\">\n" +
    "    <form class=\"page-form\" name=\"changeEmailForm\" role=\"update\"\n" +
    "    ng-class=\"{ 'is-working': working }\"\n" +
    "    ng-submit=\"submit(changeEmailForm)\" novalidate>\n" +
    "      <h2 class=\"heading\">Update email</h2>\n" +
    "      <div ng-if=\"!changeEmail.sent\">\n" +
    "        <div class=\"form-group float-input\">\n" +
    "          <input required floating-label type=\"email\" class=\"form-control\" ng-model=\"changeEmail.email\" name=\"email\" autocapitalize=\"off\" placeholder=\"New email address\"><label for=\"email\">New email address</label>\n" +
    "        </div>\n" +
    "        <div class=\"form-group float-input\">\n" +
    "          <input required floating-label type=\"email\" class=\"form-control\" ng-model=\"changeEmail.confirm_email\" name=\"confirm_email\" autocapitalize=\"off\"  placeholder=\"Repeat new email address\"><label for=\"confirm_email\">Repeat new email address</label>\n" +
    "        </div>\n" +
    "        <div class=\"cta\">\n" +
    "          <button class=\"btn-lg\" type=\"submit\"><span>Update email</span></button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "\n" +
    "      <div ng-if=\"changeEmail.sent\">\n" +
    "        <p>Your registered email has been successfully updated.</p>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </core-content>\n" +
    "</core-view>\n" +
    "");
}]);

angular.module("core/account/views/changepassword.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/account/views/changepassword.html",
    "<core-view title=\"Change password\" left-buttons=\"leftButtons\" right-buttons=\"rightButtons\">\n" +
    "  <core-content has-header=\"true\" scroll=\"true\" padding=\"true\">\n" +
    "    <form class=\"page-form\" name=\"changePassForm\" role=\"update\"\n" +
    "    ng-class=\"{ 'is-working': working }\"\n" +
    "    ng-submit=\"submit(changePassForm)\" novalidate>\n" +
    "      <h2 class=\"heading\">Change password</h2>\n" +
    "      <div ng-if=\"!changePass.sent\">\n" +
    "        <div class=\"form-group float-input\">\n" +
    "          <input required floating-label type=\"password\" class=\"form-control\" ng-model=\"changePass.current_password\" name=\"current_password\" placeholder=\"Current password\">\n" +
    "          <label for=\"current_password\">Current password</label>\n" +
    "        </div>\n" +
    "        <div class=\"form-group float-input\">\n" +
    "          <input required floating-label type=\"password\" class=\"form-control\" ng-model=\"changePass.new_password\" name=\"new_password\" placeholder=\"New password\">\n" +
    "          <label for=\"new_password\">New password</label>\n" +
    "        </div>\n" +
    "        <div class=\"form-group float-input\">\n" +
    "          <input required floating-label type=\"password\" class=\"form-control\" ng-model=\"changePass.confirm_new_password\" name=\"confirm_new_password\" placeholder=\"Confirm new password\">\n" +
    "          <label for=\"confirm_new_password\">Confirm new password</label>\n" +
    "        </div>\n" +
    "        <label class=\"form-group checkbox\">\n" +
    "          <input type=\"checkbox\" ng-model=\"changePass.show\">\n" +
    "          Show password\n" +
    "        </label>\n" +
    "        <div class=\"cta\">\n" +
    "          <button class=\"btn-lg\" type=\"submit\"><span>Change password</span></button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "\n" +
    "      <div ng-if=\"changePass.sent\">\n" +
    "        <p>Your password has been successfully updated.</p>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </core-content>\n" +
    "</core-view>");
}]);

angular.module("core/account/views/endpoints.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/account/views/endpoints.html",
    "<ion-view title=\"Reset\" hide-nav-bar=\"true\">\n" +
    "  <ion-content padding=\"true\">\n" +
    "    <div core-branding></div>\n" +
    "    <form class=\"page-form\" name=\"resetServer\" role=\"update\" ng-submit=\"submit(resetServer)\" novalidate>\n" +
    "      <div class=\"form-group\">\n" +
    "        <select name=\"selectedEnv\" ng-model=\"servers\" ng-options=\"key for ( key, value ) in environments\">\n" +
    "          <option value=\"\">Select Environment</option>\n" +
    "        </select>\n" +
    "      </div>\n" +
    "      <div class=\"form-group float-input\">\n" +
    "        <input required floating-label type=\"text\" class=\"form-control\" ng-model=\"servers.fit\" name=\"fit\" autocapitalize=\"off\" placeholder=\"Hub\"><label for=\"fit\">Hub</label>\n" +
    "      </div>\n" +
    "      <!-- <div class=\"form-group float-input\">\n" +
    "        <input required floating-label type=\"text\" class=\"form-control\" ng-model=\"servers.sso\" name=\"sso\" autocapitalize=\"off\" placeholder=\"SSO\"><label for=\"sso\">SSO</label>\n" +
    "      </div> -->\n" +
    "      <div class=\"cta\">\n" +
    "        <button class=\"btn-lg\" type=\"submit\"><span>Update</span></button>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </ion-content>\n" +
    "  <ion-footer-bar class=\"hide-on-keyboard-open padding\">\n" +
    "    <a class=\"button icon-left ion-arrow-left-b button-clear button-dark\" ui-sref=\"app.login()\">Back</a>\n" +
    "  </ion-footer-bar>\n" +
    "</ion-view>\n" +
    "");
}]);

angular.module("core/account/views/resetpassword.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/account/views/resetpassword.html",
    "<ion-view title=\"Reset password\" hide-nav-bar=\"true\">\n" +
    "  <ion-content has-footer=\"true\" padding=\"true\">\n" +
    "    <div core-branding></div>\n" +
    "    <form class=\"page-form\" name=\"resetPassForm\" role=\"update\"\n" +
    "    ng-class=\"{ 'is-working': working }\"\n" +
    "    ng-submit=\"submit(resetPassForm)\" novalidate>\n" +
    "      <h2 class=\"heading\">Reset password</h2>\n" +
    "      <div ng-if=\"!resetPass.sent\">\n" +
    "        <p>Please enter your email address to reset your password.</p>\n" +
    "        <div class=\"form-group float-input\">\n" +
    "          <input required floating-label type=\"email\" class=\"form-control\" ng-model=\"resetPass.email\" name=\"email\" autocapitalize=\"off\" placeholder=\"Email address\"><label for=\"email\">Email address</label>\n" +
    "        </div>\n" +
    "        <div class=\"cta\">\n" +
    "          <button class=\"btn-lg\" type=\"submit\"><span>Reset password</span></button>\n" +
    "        </div>\n" +
    "      </div>\n" +
    "      <div ng-if=\"resetPass.sent\">\n" +
    "        <p>We've sent you an email with instructions on resetting your password.</p>\n" +
    "        <p>If you haven't seen anything in a few minutes - please check your spam box</p>\n" +
    "      </div>\n" +
    "    </form>\n" +
    "  </ion-content>\n" +
    "\n" +
    "  <ion-footer-bar class=\"padding\">\n" +
    "    <a class=\"button icon-left ion-arrow-left-b button-clear\" ui-sref=\"core.landing\">Back</a>\n" +
    "  </ion-footer-bar>\n" +
    "</ion-view>\n" +
    "");
}]);

angular.module("core/dashboard/dashboard.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/dashboard/dashboard.html",
    "<ion-content class=\"dashboard__cards\">\n" +
    "  <div class=\"dashboard__card card\" dash-card=\"card.name\" bindonce\n" +
    "    ng-repeat=\"card in dashCards\"\n" +
    "    bo-class=\"'card--' + card.name\"></div>\n" +
    "</ion-content>");
}]);

angular.module("core/demo/tour.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/demo/tour.html",
    "<core-slide-box>\n" +
    "  <core-slide>\n" +
    "    <div class=\"box blue\"><h1>BLUE</h1></div>\n" +
    "  </core-slide>\n" +
    "  <core-slide>\n" +
    "    <div class=\"box yellow\"><h1>YELLOW</h1></div>\n" +
    "  </core-slide>\n" +
    "  <core-slide>\n" +
    "    <div class=\"box pink\"><h1>PINK</h1></div>\n" +
    "  </core-slide>\n" +
    "</core-slide-box>");
}]);

angular.module("core/navigation/menu-list.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/navigation/menu-list.html",
    "<!-- Main menu -->\n" +
    "<!-- <div class=\"user-info\">\n" +
    "  <a title=\"Your details\" class=\"user-info__name\"\n" +
    "  ng-click=\"navigateTo('app.fit.details')\" bo-bind=\"auth.user.fullName\"></a>\n" +
    "</div> -->\n" +
    "\n" +
    "<ion-content>\n" +
    "  <ul class=\"menu-list\">\n" +
    "    <li class=\"menu-item\" bindonce\n" +
    "    ng-repeat=\"item in menuItems\">\n" +
    "      <button class=\"item button-full\"\n" +
    "      bo-if=\"item.state\"\n" +
    "      bo-bind=\"item.title\"\n" +
    "      bo-class=\"item.class\"\n" +
    "      ng-class=\"{ 'current' : item.active }\"\n" +
    "      ng-click=\"navigateTo(item.state)\"></button>\n" +
    "      <div class=\"item-group\"\n" +
    "      bo-if=\"item.items\">\n" +
    "        <h3 class=\"item-divider\"\n" +
    "        bo-bind=\"item.title\"></h3>\n" +
    "        <button class=\"item button-full\"\n" +
    "        ng-repeat=\"subItem in item.items\"\n" +
    "        bo-bind=\"subItem.title\"\n" +
    "        bo-class=\"subItem.class\"\n" +
    "        ng-class=\"{ 'current' : subItem.active }\"\n" +
    "        ng-click=\"navigateTo(subItem.state)\"></button>\n" +
    "      </div>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "</ion-content>\n" +
    "\n" +
    "<ion-footer-bar class=\"bottom\" bindonce>\n" +
    " <button class=\"btn icon--lg bg-blue\"\n" +
    "  ng-click=\"navigateTo('settings')\" title=\"Settings\"><i class=\"fa fa-cog\"></i></button>\n" +
    " <button class=\"btn icon--lg bg-orange\"\n" +
    "  ng-click=\"$parent.logout()\" title=\"Log out\"><i class=\"fa fa-power-off\"></i></button>\n" +
    "</ion-footer-bar>");
}]);

angular.module("core/settings/about.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/settings/about.html",
    "<core-content>\n" +
    "  <header>\n" +
    "    <h2>About</h2>\n" +
    "  </header>\n" +
    "  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, sequi, dolorum perferendis eveniet commodi molestiae reprehenderit accusantium similique id doloremque harum quod facilis aliquid reiciendis ducimus deserunt minus repellat praesentium?</p>\n" +
    "</core-content>");
}]);

angular.module("core/settings/settings.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/settings/settings.html",
    "<core-content>\n" +
    "  <div ui-view=\"list\" ng-class=\"listClass\">\n" +
    "    <article class=\"item\" bindonce ng-repeat=\"settingsGroup in settingsList track by $index\">\n" +
    "      <header class=\"h4\"\n" +
    "      bo-if=\"!settingsGroup.conf.state && settingsGroup.settings && settingsGroup.conf.title\" bo-bind=\"settingsGroup.conf.title\"></header><a\n" +
    "      bo-if=\"settingsGroup.conf.state\" ui-sref=\"{{settingsGroup.conf.state}}\" bo-bind=\"settingsGroup.conf.title\"></a>\n" +
    "      <ul\n" +
    "      bo-if=\"!settingsGroup.conf.state && settingsGroup.settings\" class=\"settings-list\" ng-init=\"settingsOrder = settingsGroup.settingsOrder()\">\n" +
    "        <li class=\"setting-list__setting item\" ng-repeat=\"setting in settingsOrder track by $index\">\n" +
    "          <div ng-if=\"setting.type!=='state'\">\n" +
    "            <strong>{{ setting.title }}:</strong> {{ setting.value }}\n" +
    "          </div>\n" +
    "          <a ng-if=\"setting.type=='state'\" ui-sref=\"{{setting.value}}\" bo-bind=\"setting.title\"></a>\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </article>\n" +
    "  </div>\n" +
    "  <div ui-view=\"settings\" ng-class=\"settingsClass\"></div>\n" +
    "</core-content>");
}]);

angular.module("core/shell/layouts/app.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/layouts/app.html",
    "<ion-nav-bar delegate-handle=\"action-bar\"></ion-nav-bar>\n" +
    "<ion-nav-buttons side=\"right\"><span class=\"button loading-icon\"></span></ion-nav-buttons>\n" +
    "<ion-side-menus delegate-handle=\"main-menu\">\n" +
    "  <ion-side-menu-content drag-content=\"false\" edge-drag-threshold=\"100\">\n" +
    "    <ion-view ui-view class=\"shell app-view\"></ion-view>\n" +
    "  </ion-side-menu-content>\n" +
    "  <ion-side-menu class=\"menu-inactive\" side=\"left\" menu-list=\"main\"></ion-side-menu>\n" +
    "</ion-side-menus>");
}]);

angular.module("core/shell/layouts/content.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/layouts/content.html",
    "<ion-nav-view class=\"shell content-view\"></ion-nav-view>\n" +
    "<ion-nav-buttons side=\"left\">\n" +
    "  <button class=\"button {{ leftButton().type }}\" ng-click=\"leftButton().tap()\"></button>\n" +
    "</ion-nav-buttons>");
}]);

angular.module("core/shell/layouts/main.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/layouts/main.html",
    "<ion-nav-view animation=\"crossFade\" class=\"shell main-view\"></ion-nav-view>");
}]);

angular.module("core/shell/partials/action-bar.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/partials/action-bar.html",
    "<header class=\"bar bar-header nav-bar{{navBarClass()}}\">\n" +
    "  <div class=\"left-zone\">\n" +
    "  <!-- <action-box class=\"hide\" ng-class=\"{ hide: !actionBoxEnabled }\"></action-box> -->\n" +
    "  </div>\n" +
    "  <h1 ng-bind-html=\"title\" class=\"title\">Title placeholder text</h1>\n" +
    "\n" +
    "  <div class=\"right-zone\">\n" +
    "    <button ng-click=\"button.tap($event)\" ng-repeat=\"button in rightButtons\" class=\"button no-animation {{button.type}}\" ng-bind-html=\"button.content\">\n" +
    "    </button>\n" +
    "  </div>\n" +
    "</header>");
}]);

angular.module("core/shell/partials/brand-nav-button.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/partials/brand-nav-button.html",
    "<i class=\"icon ion-navicon-round\"></i> <i class=\"action-icon\"></i> <span class=\"brand-title\">Codename: Viper Moth</span>");
}]);

angular.module("core/shell/partials/branding-full.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/partials/branding-full.html",
    "<i class=\"vpr-brand\" bindonce bo-class=\"brand.style + ' ' + brand.type\">Viper Moth</i>\n" +
    "<h1 class=\"brand-title h3\">Viper Moth<small class=\"app-version\" bindonce bo-bind=\"'v' + appVersion\"></small></h1>\n" +
    "<div ng-transclude></div>");
}]);

angular.module("core/shell/partials/loading.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/partials/loading.html",
    "<i class=\"ion-loading-c\"></i>");
}]);

angular.module("core/shell/partials/modal.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/partials/modal.html",
    "<div class=\"modal-wrap\"\n" +
    "ng-class=\"state\" bindonce>\n" +
    "  <div class=\"modal-inner\">\n" +
    "    <div class=\"modal-box\">\n" +
    "      <div class=\"modal-title\">\n" +
    "        <strong bo-bind=\"title\"></strong>\n" +
    "        <a class=\"modal-cancel icon ion-close\"\n" +
    "        ng-show=\"cancelable\"></a>\n" +
    "      </div>\n" +
    "      <div class=\"modal-content\"\n" +
    "      ng-transclude></div>\n" +
    "      <button ng-if=\"hasAction\" class=\"modal-button\"\n" +
    "      ng-click=\"action()\" bo-bind=\"cta\"></button>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "</div>");
}]);

angular.module("core/shell/views/error.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/views/error.html",
    "<core-view class=\"error\" title=\"Error\" left-buttons=\"leftButtons\">\n" +
    "  <core-content has-header=\"true\" padding=\"true\">\n" +
    "    <div ng-switch=\"$stateParams.error\">\n" +
    "      <p class=\"text-danger\"\n" +
    "      ng-switch-when=\"unauthorized\">You are not authorized</p>\n" +
    "      <p class=\"text-danger\"\n" +
    "      ng-switch-when=\"401\">You are not authorized</p>\n" +
    "      <p class=\"text-danger\"\n" +
    "      ng-switch-default>Some error has occurred</p>\n" +
    "    </div ng-switch>\n" +
    "  </core-content>\n" +
    "</core-view>\n" +
    "");
}]);

angular.module("core/shell/views/landing.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/views/landing.html",
    "<div id=\"landing\" class=\"pad\" core-branding=\"{ style: 'rounded center' }\"></div>\n" +
    "<footer class=\"bottom pad\">\n" +
    "  <button class=\"col btn-lg\" ui-sref=\"core.login\">Log In</button>\n" +
    "  <button class=\"col btn-lg\" ui-sref=\"core.preview\">Preview</button>\n" +
    "</footer>");
}]);

angular.module("core/shell/views/page.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("core/shell/views/page.html",
    "<ion-view title=\"{{ content.title }}\">\n" +
    "  <ion-content class=\"page\" has-header=\"true\" padding=\"true\" bindonce>\n" +
    "    <div bo-html=\"content.body\"></div>\n" +
    "  </ion-content>\n" +
    "</ion-view>");
}]);

angular.module("services/clock/clock.control.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/clock/clock.control.html",
    "<div class=\"clock\" ng-click=\"changeTime=true\">\n" +
    "  <div class=\"clock__container\">\n" +
    "    <div class=\"clock__face\">\n" +
    "      <div class=\"clock__hour\"></div>\n" +
    "      <div class=\"clock__minute\"></div>\n" +
    "      <div class=\"clock__second\"></div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "  <time class=\"clock__time\">{{ time | date:'h:mma' }}</time>\n" +
    "\n" +
    "  <!-- <modal class=\"center\" title=\"Change time\" active=\"changeTime\" cancelable=\"true\">\n" +
    "  	<table><tbody><tr><td><a href=\"#\" data-action=\"incrementHour\"><i class=\"icon-chevron-up\"></i></a></td><td class=\"separator\">&nbsp;</td><td><a href=\"#\" data-action=\"incrementMinute\"><i class=\"icon-chevron-up\"></i></a></td><td class=\"separator\">&nbsp;</td><td class=\"meridian-column\"><a href=\"#\" data-action=\"toggleMeridian\"><i class=\"icon-chevron-up\"></i></a></td></tr><tr><td><input type=\"text\" class=\"bootstrap-timepicker-hour\" maxlength=\"2\"></td> <td class=\"separator\">:</td><td><input type=\"text\" class=\"bootstrap-timepicker-minute\" maxlength=\"2\"></td> <td class=\"separator\">&nbsp;</td><td><input type=\"text\" class=\"bootstrap-timepicker-meridian\" maxlength=\"2\"></td></tr><tr><td><a href=\"#\" data-action=\"decrementHour\"><i class=\"icon-chevron-down\"></i></a></td><td class=\"separator\"></td><td><a href=\"#\" data-action=\"decrementMinute\"><i class=\"icon-chevron-down\"></i></a></td><td class=\"separator\">&nbsp;</td><td><a href=\"#\" data-action=\"toggleMeridian\"><i class=\"icon-chevron-down\"></i></a></td></tr></tbody></table>\n" +
    "  </modal> -->\n" +
    "</div>");
}]);

angular.module("services/heating/control.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/heating/control.html",
    "<ion-content padding=\"true\">\n" +
    "  <h4>Current home temp = <strong ng-bind-html=\"temp | tempUnit\"></strong></h4>\n" +
    "  <a ui-sref=\"heating.settings\">Settings</a>\n" +
    "</ion-content>");
}]);

angular.module("services/heating/heating.card.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/heating/heating.card.html",
    "<div class=\"heating-card col-50\">\n" +
    "  <header class=\"h3\">HEATING: <heating-status></heating-status></header>\n" +
    "  <article class=\"list card\">\n" +
    "	<div class=\"item item-icon-left temp temp--current\">\n" +
    "		<i class=\"icon ion-thermometer\"></i>\n" +
    "	  <strong class=\"temp__label\">Current:</strong>\n" +
    "	  <span class=\"temp__val\" ng-bind-html=\"temp | tempUnit\" ng-class=\"{ 'assertive' : temp > 15, 'positive': temp <= 15 }\"></span>\n" +
    "	</div>\n" +
    "\n" +
    "	<!-- <div class=\"item item-icon-left temp temp--outside\">\n" +
    "		<i class=\"icon ion-cloud\"></i>\n" +
    "	  <strong class=\"temp__label\">Outside:</strong>\n" +
    "	  <span class=\"temp__val\" ng-bind-html=\"outside | tempUnit\" ng-class=\"{ 'assertive' : outside > 15, 'positive': outside <= 15 }\"></span>\n" +
    "	</div> -->\n" +
    "  </article>\n" +
    "  <control-setpoint class=\"card\"></control-setpoint>\n" +
    "</div>");
}]);

angular.module("services/heating/heating.status.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/heating/heating.status.html",
    "<strong class=\"heating-status ion-flame\" ng-class=\"{ 'assertive' : heating, 'positive' : !heating }\"> {{ heating ? 'ON' : 'OFF' }} </strong>");
}]);

angular.module("services/heating/schedule.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/heating/schedule.html",
    "<ion-content></ion-content>");
}]);

angular.module("services/heating/setpoint.control.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/heating/setpoint.control.html",
    "<div class=\"setpoint-control\">\n" +
    "  <button class=\"btn--arrow-up assertive temp__warmer ion-plus\" ng-click=\"warmer()\">\n" +
    "    <i class=\"control__ind--plus\"></i>\n" +
    "  </button>\n" +
    "  <div class=\"temp temp--target control__display\">\n" +
    "    <strong class=\"temp__label\">Now:</strong>\n" +
    "    <span class=\"temp__val\"\n" +
    "    ng-bind-html=\"tempNow | tempUnit\"\n" +
    "    ng-class=\"{ 'assertive' : tempNow > 15, 'positive': tempNow <= 15 }\"></span>\n" +
    "  </div>\n" +
    "  <div class=\"temp temp--target control__display ng-hide\">\n" +
    "    <strong class=\"temp__label\"> Target:</strong>\n" +
    "    <span class=\"temp__val\"\n" +
    "    ng-bind-html=\"targetTemp | tempUnit\"\n" +
    "    ng-class=\"{ 'assertive' : targetTemp > 15, 'positive': targetTemp <= 15 }\"></span>\n" +
    "  </div>\n" +
    "  <button class=\"btn--arrow-down positive temp__cooler ion-minus\" ng-click=\"cooler()\">\n" +
    "    <i class=\"control__ind--minus\"></i>\n" +
    "  </button>\n" +
    "  <center class=\"temp__status balanced\" ng-if=\"!!tempStatus\" ng-bind-html=\"tempStatus\"></center>\n" +
    "</div>");
}]);

angular.module("services/heating/settings.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/heating/settings.html",
    "<ion-content>\n" +
    "  <ul class=\"settings-list\">\n" +
    "    <li class=\"setting-list__setting item\" ng-repeat=\"setting in settings track by $index\">\n" +
    "      <div ng-if=\"setting.type!=='state'\">\n" +
    "        <strong>{{ setting.title }}:</strong> {{ setting.value }}\n" +
    "      </div>\n" +
    "      <a ng-if=\"setting.type=='state'\" ui-sref=\"{{setting.value}}\" bo-bind=\"setting.title\"></a>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "</ion-content>");
}]);

angular.module("services/occupancy/footer.card.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/footer.card.html",
    "<ion-footer-bar>\n" +
    "  <occupancy-status class=\"button button-clear button-dark\"></occupancy-status>\n" +
    "  <a class=\"assertive icon-left ion-refresh pull-right\">Reset</a>\n" +
    "  <a class=\"balanced icon-left ion-help pull-right\">Help</a>\n" +
    "</ion-footer-bar>");
}]);

angular.module("services/occupancy/holiday.card.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/holiday.card.html",
    "<div class=\"holiday-card\">\n" +
    "  <heading>Going away?</heading>\n" +
    "  <section><button ui-sref=\"occupancy.holiday\">Add a holiday</button></section>\n" +
    "</div>");
}]);

angular.module("services/occupancy/occupancy.control.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/occupancy.control.html",
    "<ul class=\"occupancy-controls\">\n" +
    "  <li class=\"occupancy-state\"\n" +
    "  ng-class=\"stateClass( state, true )\"\n" +
    "  ng-repeat=\"state in occupancyStates track by $index\">\n" +
    "    <a class=\"occupancy-state__btn\" ng-click=\"selectState( state )\">\n" +
    "      <img class=\"occupancy-state__img\"\n" +
    "      ng-src=\"assets/images/f_occupancy_states/{{ state.ref }}.png\" />\n" +
    "      <span class=\"occupancy-state__title\">{{ state.name }}</span>\n" +
    "    </a>\n" +
    "    <div class=\"state-control control\" control-setpoint for=\"state\"\n" +
    "    bo-if=\"state.controlSetPoint\"\n" +
    "    ng-show=\"state.active\"></div>\n" +
    "    <div class=\"state-control control\" control-duration=\"state.duration\" for=\"state\"\n" +
    "    bo-if=\"state.controlTime\"\n" +
    "    ng-show=\"state.active\"></div>\n" +
    "  </li>\n" +
    "</ul>");
}]);

angular.module("services/occupancy/occupancy.switch.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/occupancy.switch.html",
    "<div class=\"schedule-control\">\n" +
    "  <a class=\"occupancy-state\" ng-class=\"stateClass( currentSchedule.occupancy )\" ng-click=\"changeOccupancy=true\">\n" +
    "    <img class=\"occupancy__img\" ng-src=\"assets/images/f_occupancy_states/{{ currentSchedule.occupancy.ref }}.png\" />\n" +
    "    <span class=\"occupancy__title\">{{ currentSchedule.occupancy.name }}</span>\n" +
    "  </a>\n" +
    "\n" +
    "  <modal class=\"center\" title=\"Change occupancy status\" active=\"changeOccupancy\" cancelable=\"true\">\n" +
    "    <a class=\"occupancy-state\" ng-class=\"stateClass( state, true )\" ng-repeat=\"state in occupancyStates track by $index\" ng-click=\"selectState( state )\">\n" +
    "      <img class=\"occupancy__img\" ng-src=\"assets/images/f_occupancy_states/{{ state.ref }}.png\" />\n" +
    "      <span class=\"occupancy__title\">{{ state.name }}</span>\n" +
    "    </a>\n" +
    "  </modal>\n" +
    "</div>");
}]);

angular.module("services/occupancy/schedule.card.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/schedule.card.html",
    "<div class=\"occupancy-card\">\n" +
    "  <h3 class=\"occupancy-for\">NOW</h3>\n" +
    "  <div class=\"center\">\n" +
    "    <control-occupancy for=\"currentSchedule\"></control-occupancy>\n" +
    "    <label class=\"occupancy-label--until\">UNTIL</label>\n" +
    "    <control-time for=\"currentSchedule.time\"></control-time>\n" +
    "    <label class=\"occupancy-label--then\">THEN</label>\n" +
    "    <control-occupancy for=\"nextSchedule\"></control-occupancy>\n" +
    "  </div>\n" +
    "</div>");
}]);

angular.module("services/occupancy/schedule.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/schedule.html",
    "<ion-content></ion-content>");
}]);

angular.module("services/occupancy/settings.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/occupancy/settings.html",
    "<ion-content></ion-content>");
}]);

angular.module("services/weather/weather.card.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("services/weather/weather.card.html",
    "<div class=\"weather-card\">\n" +
    "  <weather for=\"now\" size=\"l\"></weather>\n" +
    "  <forecast days=\"3\" bindonce bo-text=\"placeholder\"></forecast>\n" +
    "</div>");
}]);
