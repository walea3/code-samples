define([
  'angular',
  'angular-mocks',
  'core/app.conf',
  'core/storage/storage'
], function ( ng ) {
/* jshint -W084 */
  'use strict';

  return ng.module('mock.server', [
    'ngMockE2E',
    'core.config', 'core.storage'
  ])
  .factory('delayHTTP', function ($q, $timeout) {
    return {
      request: function (request) {
        var delayedResponse = $q.defer();
        $timeout(function () {
          delayedResponse.resolve(request);
        }, 70);
        return delayedResponse.promise;
      },
      response: function (response) {
        var deferResponse = $q.defer();

        if (response.config.timeout && response.config.timeout.then) {
          response.config.timeout.then(function () {
            deferResponse.reject();
          });
        } else {
          deferResponse.resolve(response);
        }

        return $timeout(function () {
          deferResponse.resolve(response);
          return deferResponse.promise;
        });
      }
    };
  })
  .factory('mockServer', ['$httpBackend', '$log', 'coreConfig', 'Storage', 'User',
    function ($httpBackend, $log, coreConfig, coreStorage, coreUser) {
      return {
        isRunning: false,
        run: function () {
          if ( this.isRunning ) {
            return;
          }
          this.isRunning = true;

          var
            backendStore = coreStorage('factory', 'Backend', {
              Tokens: {},
              Users: {}
            }),
            tokens = backendStore.get('Tokens') || {},
            users = backendStore.get( 'Users' ),
            coreApp = coreStorage('get', 'coreApp');

          // Check and corrects old localStorage values, backward-compatibility!
          if (!coreApp || coreApp.version !== coreConfig.version) {
            tokens = {};
            coreStorage('set', 'coreApp', coreConfig);
          }

          if ( ng.equals(users, {}) ) {
            // Init default users
            backendStore.set('Users', {
              'john.dott@myemail.com': ng.extend(coreUser.schema(), {
                firstName: 'John',
                lastName: 'Dott',
                password: 'hello',
                email: 'john.dott@myemail.com',
                acc: '190093827',
                meter: '2893289323',
                pcode: 'RG1 1AA',
                userRole: userRoles.user
              }),
              'bitter.s@provider.com': ng.extend(coreUser.schema(), {
                firstName: 'Sandra',
                lastName: 'Bitter',
                password: 'world',
                email: 'bitter.s@provider.com',
                acc: '190093828',
                meter: '39589724720',
                pcode: 'NW12',
                userRole: userRoles.admin
              })
            });
          }

          // fakeLogin
          $httpBackend.when('POST', coreConfig.getApi('/login', 'sso')).respond(function (method, url, data) {
            var
              postData = ng.fromJson(data),
              user = users[postData.email],
              newToken;

            $log.info(method, '->', url);

            if ( !ng.isDefined(user) || user.password !== postData.password ) {
              return [401, 'wrong combination email/password', {}];
            }

            newToken = vprMth.Helpers.randomUUID();

            user.tokens.push(newToken);
            backendStore.set( 'Users', users );

            tokens[newToken] = postData.email;
            backendStore.set('Tokens', tokens);

            return [200, {
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName,
              pcode: user.pcode,
              supplier: user.supplier,
              acc: user.acc,
              meter: user.meter,
              token: newToken,
              userRole: user.userRole
            }, {}];
          });

          // fakeLogout
          $httpBackend.when('GET', coreConfig.getApi('/logout', 'sso')).respond(function (method, url, data, headers) {
            var queryToken, userTokens;
            $log.info(method, '->', url);

            if ( !(queryToken = headers['X-Token']) ) {
              return [401, 'auth token invalid or expired', {}];
            }
            if ( !ng.isDefined(tokens[queryToken]) ) {
              return [401, 'auth token invalid or expired', {}];
            }

            userTokens = users[tokens[queryToken]].tokens;
            // Update userStore AND tokens
            userTokens.splice(userTokens.indexOf(queryToken));
            delete tokens[queryToken];
            backendStore.set('Tokens', tokens);
            return [200, {}, {}];
          });

          // fakeRegister
          $httpBackend.when('POST', coreConfig.getApi('/account', 'sso')).respond(function (method, url, data) {
            var
              postData = ng.fromJson(data),
              errors = [],
              newUser, creationDate;
            $log.info(method, '->', url);

            if (ng.isDefined(users[postData.email])) {
              errors.push({ field: 'email', name: 'used' });
            }

            if (errors.length) {
              return [409, { valid: false, errors: errors }, {}];
            }

            creationDate = new Date().toISOString();
            newUser = ng.extend(postData, { userRole: userRoles[postData.role], tokens: [], created: creationDate });
            delete newUser.role;

            users[newUser.email] = newUser;
            backendStore.set( 'Users', users );
            return [201, { valid: true, creationDate: creationDate }, {}];
          });

          // fakeUser
          $httpBackend.when('GET', coreConfig.getApi('/account', 'sso')).respond(function (method, url, data, headers) {
            var queryToken, user;
            $log.info(method, '->', url);

            if ( !(queryToken = headers['X-Token']) ) {
              return [401, 'auth token invalid or expired', {}];
            }
            if ( !ng.isDefined(tokens[queryToken]) ) {
              return [401, 'auth token invalid or expired', {}];
            }

            // if is present in a registered users array.
            user = users[tokens[queryToken]];

            return [200, {
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName,
              supplier: user.supplier,
              acc: user.acc,
              meter: user.meter,
              token: queryToken,
              userRole: user.userRole
            }, {}];
          });

          // $httpBackend.when('GET', vprMth.Helpers.regexpStr('^(?:(?!'+ coreConfig.getApi('/oauth/token', 'sso', 'local') + ').)*$')).passThrough();
          // $httpBackend.when('GET', vprMth.Helpers.regexpStr('^(?:(?!'+ coreConfig.getApi('/login', 'sso', 'local') + ').)*$')).passThrough();
          // $httpBackend.when('POST', vprMth.Helpers.regexpStr('^(?:(?!'+ coreConfig.getApi('/login', 'sso', 'local') + ').)*$')).passThrough();
          // $httpBackend.when('GET', vprMth.Helpers.regexpStr('^(?:(?!'+ coreConfig.getApi('/logout', 'sso', 'local') + ').)*$')).passThrough();
          // $httpBackend.when('GET', vprMth.Helpers.regexpStr('^(?:(?!'+ coreConfig.getApi('/account', 'sso', 'local') + ').)*$')).passThrough();
          // $httpBackend.when('POST', vprMth.Helpers.regexpStr('^(?:(?!'+ coreConfig.getApi('/account', 'sso', 'local') + ').)*$')).passThrough();
        }
      };
    }
  ])
  // delay HTTP
  .config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('delayHTTP');
  }])
  .run(['Events', 'mockServer', function (Events, mockServer) {
    Events.subscribe('runMockServer', mockServer.run.bind(mockServer));
    mockServer.run();
  }]);
});