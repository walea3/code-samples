require.config({
  paths: {
    'ionic': 'assets/bower_components/ionic/release/js/ionic',
    'angular': 'assets/bower_components/angular/angular',
    'ionic-angular': 'assets/bower_components/ionic/release/js/ionic-angular',
    'angular-route': 'assets/bower_components/angular-route/angular-route',
    'angular-animate': 'assets/bower_components/angular-animate/angular-animate',
    'angular-sanitize': 'assets/bower_components/angular-sanitize/angular-sanitize',
    'angular-touch': 'assets/bower_components/angular-touch/angular-touch',
    'angular-resource': 'assets/bower_components/angular-resource/angular-resource',
    'angular-translate': 'assets/bower_components/angular-translate/angular-translate',
    'angular-bindonce': 'assets/bower_components/angular-bindonce/bindonce',
    'angular-ui-router': 'assets/bower_components/angular-ui-router/release/angular-ui-router',
    'angular-mocks': 'assets/bower_components/angular-mocks/angular-mocks',
    'text': 'assets/bower_components/requirejs-text/text',
    'store': 'assets/bower_components/store.js/store',
    'templates': 'templates'
  },
  shim: {
    'ionic': {'exports':'ionic'},
    'angular' : {'exports' : 'angular'},
    'ionic-angular': ['angular', 'ionic'],
    'angular-route': ['angular'],
    'angular-animate': ['angular'],
    'angular-sanitize': ['angular'],
    'angular-touch': ['angular'],
    'angular-resource': ['angular'],
    'angular-translate': ['angular'],
    'angular-bindonce': ['angular'],
    'angular-ui-router': ['angular'],
    'angular-mocks': {
      deps:['angular'],
      'exports':'angular.mock'
    },
    'templates': ['angular']
  },
  priority: [
    'angular',
    'ionic'
  ]
});

//http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
window.name = 'NG_DEFER_BOOTSTRAP!';
window.vprMth = {};

require([
  'angular',
  'ionic',
  'angular-touch',
  'angular-animate',
  'angular-sanitize',
  'angular-resource',
  'angular-bindonce',
  'angular-ui-router',
  'angular-translate',
  'ionic-angular',
  'templates',
  'core/app.conf',
  'core/app',
  'core/helpers',
  'core/pages',
  'mocks/hub'
], function ( ng ) {
  'use strict';
  // use app here
  ng.element(document).ready(function() {
    ng.resumeBootstrap();
  });
});