define([
  'angular',
  'core/events/events',
  'core/platform/platform'
], function ( ng ) {
  'use strict';
  /*jshint -W030 */
  /*
   * # Navigation Module
   *
   * Should manage everything related to the side menu and navigating around the app.
   */

  // Menu service handles the items in the side menu.
  function NavService () {
    var
      nav = this,
      menus = {},
      menuInterface = {
        items: [],
        groupIndex: {},
        stateIndex: {},
        updateActiveStates: function ( to, from ) {
          // Update the active state of all menu items to indicate current
          if ( from ) {
            if ( from.menuParent && this.stateIndex[ from.menuParent ] ) {
              this.stateIndex[ from.menuParent ].active = false;
            } else if ( from.name && this.stateIndex[ from.name ] ) {
              this.stateIndex[ from.name ].active = false;
            }
          }

          if ( this.stateIndex[ to.menuParent ] ) {
            this.stateIndex[ to.menuParent ].active = true;
          } else if ( this.stateIndex[ to.name ] ) {
            this.stateIndex[ to.name ].active = true;
          }
        }
      };
      // itemInterface = {
      //   title: null,
      //   class: null,
      //   state: null
      // },
      // groupInterface = {
      //   key: null,
      //   title: null,
      //   class: null,
      //   items: []
      // };


    this.menu = function ( name, items ) {
      var menu, i, i2;

      if ( !name ) {
        return;
      }

      menu = menus[ name ] || ( menus[ name ] = ng.extend( {}, menuInterface ) );

      if ( items && items.length ) {
        // Loop through menu items to be added and insert into menu.items array
        for (i = items.length - 1; i >= 0; i--) {
          var
            item = items[i],
            subItems = items[i].items,
            // determin position item will be inserted in
            position =  item.position >= 0 && item.position <= menu.items.length ?
              item.position :
              menu.items.length;

          // Adding a new menu group
          if ( item.key && !menu.groupIndex[ item.key ] ) {
            // Add menu group to object and store menu.items location
            menu.groupIndex[ item.key ] = position;
          }
          // Adding menu items to existing group
          if ( item.toGroup && menu.groupIndex[ item.toGroup ] >= 0 ) {
            for (i2 = subItems.length - 1; i2 >= 0; i2--) {
              menu.items[ menu.groupIndex[ item.toGroup ] ]
                .items.splice( position, 0, subItems[i2] );
              if ( subItems[i2].state ) {
                menu.stateIndex[ subItems[i2].state ] = subItems[i2];
              }
            }
          } else {
            // Add item to menu.items array
            menu.items.splice( position, 0, item );
            if ( subItems ) {
              for (i2 = subItems.length - 1; i2 >= 0; i2--) {
                menu.stateIndex[ subItems[i2].state ] = subItems[i2];
                subItems[i2].active = false;
              }
            }
            if ( item.state ) {
              menu.stateIndex[ item.state ] = item;
              item.active = false;
            }
          }
        }
      }

      return menu;
    };

    this.$get = ['$state', '$timeout', '$ionicNavBarDelegate', '$ionicSideMenuDelegate', 'Platform', 'Events',
      function ($state, $timeout, $ionicNavBarDelegate, $ionicSideMenuDelegate, corePlatform, coreEvents) {
        var
          sideMenu = $ionicSideMenuDelegate.$getByHandle('main-menu'),
          actionBar = $ionicNavBarDelegate.$getByHandle('action-bar'),
          navService = {
            menu: nav.menu,
            init: function ( menuName, navScope ) {
              var
                me = this,
                menu = menus[ menuName ];

              // Make menu items available
              $timeout(function() {
                sideMenu.canDragContent( true );
                actionBar.showBar( false );
                ng.element( document.querySelector('.menu') ).removeClass('menu-inactive');
              }, 10);
              // me.items = menu.items;
              coreEvents.publish( menuName + ' menuReady' );

              // make the current state active in menu
              menu.updateActiveStates( $state.current );
              // Listen for state changes and update active states
              coreEvents.subscribe('$stateChangeSuccess', function (ev, to, toParams, from) {
                menu.updateActiveStates( to, from );
              });

              corePlatform.onMenuButton( me.toggle );
              corePlatform.onSearchButton( me.navigateTo.bind(null, 'core.main.home') );

              navScope.$parent.$parent.navigateTo = this.navigateTo;
              navScope.$parent.$parent.setTitle = ng.noop;//actionBar.setTitle.bind(actionBar);
              navScope.$parent.$parent.showBar = actionBar.showBar.bind(actionBar);
              navScope.$parent.$parent.back = actionBar.back.bind(actionBar);

              navScope.$on('destroy', function () {
                me.end();
              });
              coreEvents.subscribe('app.logout', me.end.bind(me));
            },
            navigateTo: function ( state ) {
              navService.close();
              $state.go( state );
            },
            toggle: function () {
              sideMenu.toggleLeft();
            },
            close: function () {
              if ( sideMenu.isOpen() ) {
                $timeout(function () {
                  sideMenu.isOpenLeft() && sideMenu.toggleLeft();
                  sideMenu.isOpenRight() && sideMenu.toggleRight();
                }, 350);
              }
              return (sideMenu.close && sideMenu.close());
            },
            end: function () {
              $timeout(sideMenu.canDragContent.bind( sideMenu, false ), 10);
              navService.close();
              actionBar.showBar( false );
              ng.element( document.querySelector('.menu') ).addClass('menu-inactive');
            }
          };
        return navService;
      }
    ];
  }

  return ng.module('core.navigation', [
    'core.events', 'core.platform'
  ])

  .directive('menuList', ['Nav', function ( coreNav ) {
    return {
      restrict: 'A',
      transclude: true,
      templateUrl: 'core/navigation/menu-list.html',
      link: function (scope, el, attrs) {
        scope.menuItems = coreNav.menu( attrs.menuList ).items;
      }
    };
  }])

  .provider('Nav', NavService);
});