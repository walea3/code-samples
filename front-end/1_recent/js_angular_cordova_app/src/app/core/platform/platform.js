define([
  'angular',
  'core/events/events'
], function ( ng ) {
  'use strict';

  // Platform module
  // ----------------

  //     platform.js
  //     This is a module that contains services to help work with
  //     different delivery platforms - for example different
  //     browsers or different devices (iOS, Android etc).
  //     Note that most platform specific code should live in the
  //     platform merges file of the phonegap application, the
  //     exception to this are changes that can only be determined
  //     at runtime.

  var platformIntent = { 'state': null, 'params': null };
  function handleURL ( url ) {
    // window.alert('received url: ' + url );
    var urlData = url.split('://')[1].replace(/\//g, '.').split('?');
    platformIntent = { 'state': urlData[0], 'params': vprMth.Helpers.qsData( urlData[1] ) };
  }

  window.handleOpenURL = handleURL;

  return ng.module('core.platform', ['core.events'])

    .factory('deviceReady', ['Events', function ( coreEvents ) {
      return {
        toPerform: function ( action ) {
          return coreEvents.listener.addTo(document, 'deviceready', action, false);
        },
        toReturn: function ( action ) {
          var queue = [];

          var impl = function () {
            queue.push(Array.prototype.slice.call(arguments));
          };

          coreEvents.listener.addTo(document, 'deviceready', function () {
            queue.forEach(function (args) {
              action.apply(this, args);
            });
            impl = action;
          }, false);

          return function () {
            return impl.apply(this, arguments);
          };
        }
      };
    }])

    .directive('appContainer', ['$window', 'deviceReady', function ( $window, deviceReady ) {
      return {
        restrict: 'C',
        link: function ( scope, el ) {
          deviceReady.toPerform(function () {
            var
              // device = corePlatform.getDevice(),
              appHeight = $window.innerHeight;
            // el[0].style.minHeight = appHeight + 'px';
            el[0].style.backgroundSize = 'auto ' + appHeight + 'px';
          });
        }
      };
    }])

    .directive('externalUrl', [ '$window', function ( $window )  {
      return {
        restrict: 'A',
        compile: function () {
          return function postLink ( scope, el, attrs ) {
            scope.launch = function () {
              $window.open(encodeURI( attrs.externalUrl ), '_system');
              return false;
            };
            el.on('tap click', scope.launch);
            scope.$on('$destroy', function () {
              el.off('tap click', scope.launch);
            });
          };
        }
      };
    }])

    .factory('Platform', ['$state', '$timeout', 'deviceReady', 'Events', 'coreConfig',
      function ($state, $timeout, deviceReady, coreEvents, coreConfig) {

        return {
          init: deviceReady.toReturn(function () {
            window.handleOpenURL = function ( url ) {
              handleURL( url );
              handleIntent();
            };

            var handleIntent = function () {
              if ( platformIntent.state ) {
                $timeout(function() {
                  if ( $state.get( platformIntent.state ) ) {
                    $state.go( platformIntent.state, platformIntent.params );
                  }
                  coreEvents.publish('urlEntry', platformIntent);
                }, 0);
              }
            };
            handleIntent();

            if ( window.Keyboard ) {
              window.Keyboard.shrinkView(true);
              window.Keyboard.hideFormAccessoryBar(true);
              window.Keyboard.disableScrollingInShrinkView(true);
            }

            $timeout(function () {
              return navigator.splashscreen && navigator.splashscreen.hide();
            }, 500);
          }),

          getDevice: deviceReady.toReturn(function () {
            return window.device;
          }),

          getIntent: function ( uri ) {
            var
              device = this.getDevice(),
              intentUrl = coreConfig.scheme + ':' + uri;

            if ( device && device.platform === 'Android' ) {
              intentUrl = 'intent:' + uri + '#Intent;package=' + coreConfig.package + ';scheme=' + coreConfig.scheme + ';end;';
            }

            return intentUrl;
          },

          isOnline: (function () {
            var check = deviceReady.toReturn(function () {
              // console.log('phonegap ready:', navigator.connection);
              return navigator.connection.type !== Connection.NONE;
            });
            // For post Cordova if random offline
            if ( coreConfig.env !== 'production' ) {
              setTimeout(function(){
                if ( typeof Cordova === 'undefined' ) {
                  // console.log('cordova check:', typeof Cordova);
                  check = function() {
                    return !!Math.floor(Math.random() * 2);
                  };
                }
              }, 0);
            }

            return check;
          }()),

          onOnline: function ( action ) {
            // For post Cordova if random offline take up to 5 seconds to get back online
            if ( coreConfig.env !== 'production' ) {
              setTimeout(function(){
                if ( typeof Cordova === 'undefined' && typeof action === 'function' ) {
                  // console.log('cordova check onOnline:', typeof Cordova === 'undefined' && typeof action === 'function');
                  action();
                }
              }, Math.floor(Math.random() * 5000));
            }
            return coreEvents.listener.addTo(document, 'online', action, false);
          },

          onMenuButton: function ( action ) {
            return coreEvents.listener.addTo(document, 'menubutton', action, false);
          },

          onSearchButton: function ( action ) {
            return coreEvents.listener.addTo(document, 'searchbutton', action, false);
          },

          vibrate: function ( time ) {
            if ( navigator.notification ) {
              navigator.notification.vibrate(time);
            }
          }
        };
      }
    ]);
});