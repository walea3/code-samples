define([
  'angular'
], function ( ng ) {
  'use strict';
  // Events module
  // ----------------

  //    events.js
  //    Used to emit and listen for global events and allow the ability to
  //    add async listeners to the callbacks

  function eventService ( $q, $rootScope ) {
    var listener = (function(){
      var
        i = 1,
        listeners = {},
        listenerService = {
          addTo: function(element, event, handler, capture) {
            element.addEventListener(event, handler, capture);
            listeners[i] = {
              element: element,
              event: event,
              handler: handler,
              capture: capture
            };
            return listenerService.removeFrom.bind(null, i++);
          },
          removeFrom: function(id) {
            if(id in listeners) {
              var h = listeners[id];
              h.element.removeEventListener(h.event, h.handler, h.capture);
            }
          }
        };
      return listenerService;
    }());

    return {
      listener: listener,
      /** raise an event with a given name, returns an array of promises for each listener */
      publish: function (name, args) {

        //there are no listeners
        if (!$rootScope.$$listeners[name]) {
          return [];
        }

        //setup a deferred promise for each listener
        var deferred = [];
        for (var i = 0; i < $rootScope.$$listeners[name].length; i++) {
          deferred.push($q.defer());
        }
        //create a new event args object to pass to the
        // $emit containing methods that will allow listeners
        // to return data in an async if required
        var eventArgs = {
          args: args,
          reject: function (a) {
            deferred.pop().reject(a);
          },
          resolve: function (a) {
            deferred.pop().resolve(a);
          }
        };

        //send the event
        $rootScope.$emit(name, eventArgs);

        //return an array of promises
        var promises = deferred.map(function(p) {
          return p.promise;
        });
        return promises;
      },

      /** subscribe to a method, or use scope.$on = same thing */
      subscribe: function(name, callback) {
        return $rootScope.$on(name, callback);
      },

        /** pass in the result of subscribe to this method, or just call the method returned from subscribe to unsubscribe */
      unsubscribe: function(handle) {
        if (angular.isFunction(handle)) {
          handle();
        }
      }

    };
  }

  return ng.module('core.events', [])
    .factory('Events', ['$q', '$rootScope', eventService]);
});