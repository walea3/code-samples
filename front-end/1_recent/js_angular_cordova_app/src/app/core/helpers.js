define(['angular'], function ( ng ) {
  'use strict';
  /* jshint newcap:false */
  // Polyfill Function.prototype.bind
  // @see https://gist.github.com/dsingleton/1312328
  Function.prototype.bind=Function.prototype.bind||function(b){if(typeof this!=='function'){throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');}var a=Array.prototype.slice,f=a.call(arguments,1),e=this,c=function(){},d=function(){return e.apply(this instanceof c?this:b||window,f.concat(a.call(arguments)));};c.prototype=this.prototype;d.prototype=new c();return d;};

  vprMth.Helpers = {
    qsData: function ( query ) {
      var
        match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, ' ')); },
        urlParams = {};
      while ( (match = search.exec(query)) ) {
        urlParams[decode(match[1])] = decode(match[2]);
      }

      return urlParams;
    },

    /**
     * Generates random Token
     */
    randomUUID: function () {
      var
        charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
        randomToken = '',
        randomPoz, i;
      for (i = 0; i < 36; i++) {
        if (i === 8 || i === 13 || i === 18 || i === 23) {
          randomToken += '';
          continue;
        }
        randomPoz = Math.floor(Math.random() * charSet.length);
        randomToken += charSet.substring(randomPoz, randomPoz + 1);
      }
      return randomToken;
    },

    regexpStr: function (regexp) {
      regexp = new RegExp( regexp );
      return {
        test: function (str) {
          this.matches = str.match(regexp);
          return this.matches && this.matches.length > 0;
        }
      };
    },

    /*getGUID: function () {
      // internal function to generate a random number guid generation
      var
        S4 = function() {
          return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        },
        // generates a guid for adding items to array
        guid = function () {
          return (S4() + S4() + '-' + S4() + '-4' + S4().substr(0,3) + '-' + S4() + '-' + S4() + S4() + S4()).toLowerCase();
        };
      return guid();
    },*//*

    formatNumber: function() {
      return function ( format ) {
        /**
        * Formats the number according to the 'format' string;
        * adherses to the american number standard where a comma
        * is inserted after every 3 digits.
        *  note: there should be only 1 contiguous number in the format,
        * where a number consists of digits, period, and commas
        *        any other characters can be wrapped around this number, including '$', '%', or text
        *        examples (123456.789):
        *          '0′ - (123456) show only digits, no precision
        *          '0.00′ - (123456.78) show only digits, 2 precision
        *          '0.0000′ - (123456.7890) show only digits, 4 precision
        *          '0,000′ - (123,456) show comma and digits, no precision
        *          '0,000.00′ - (123,456.78) show comma and digits, 2 precision
        *          '0,0.00′ - (123,456.78) shortcut method, show comma and digits, 2 precision
        *
        * @method format
        * @param format {string} the way you would like to format this text
        * @return {string} the formatted number
        * @public
        *

        if ( typeof format !== 'string' ) {
          return '';
        } // sanity check

        var
          hasComma = -1 < format.indexOf(','),
          psplit = format.replace(/[^\d.]/g, '').split('.'),
          that = this, fnum;

        // compute precision
        if (1 < psplit.length) {
          // fix number precision
          that = that.toFixed(psplit[1].length);
        }
        // error: too many periods
        else if (2 < psplit.length) {
          throw('NumberFormatException: invalid format, formats should have no more than 1 period: ' + format);
        }
        // remove precision
        else {
          that = that.toFixed(0);
        }

        // get the string now that precision is correct
        fnum = that.toString();

        // format has comma, then compute commas
        if (hasComma) {
          // remove precision for computation
          psplit = fnum.split('.');

          var
            cnum = psplit[0],
            parr = [],
            j = cnum.length,
            m = Math.floor(j / 3),
            n = cnum.length % 3 || 3; // n cannot be ZERO or causes infinite loop

          // break the number into chunks of 3 digits; first chunk may be less than 3
          for (var i = 0; i < j; i += n) {
            if (i != 0) {n = 3;}
            parr[parr.length] = cnum.substr(i, n);
            m -= 1;
          }

          // put chunks back together, separated by comma
          fnum = parr.join(',');

          // add the precision back in
          if (psplit[1]) {fnum += '.' + psplit[1];}
        }

        // replace the number portion of the format with fnum
        return format.replace(/[\d,?\.?]+/, fnum);
      };
    },*/

    layoutEngine: (function() {
    /*!
      * Layout Engine v0.8.0
      *
      * Adds the rendering engine and browser names as a class on the html tag and returns a JavaScript object containing the vendor, version and browser name (where appropriate)
      *
      * Possible vendors: '.vendor-' + 'ie', 'khtml', 'mozilla', 'opera', 'webkit'
      * '.vendor-ie' also adds the version: 'vendor-' + 'ie-11', 'ie-10', 'ie-9', 'ie-8', 'ie-7'
      * '.vendor-opera-mini' is also detected
      *
      * Possible browsers: '.browser-' + 'android', 'chrome', 'wiiu'
      *
      * Copyright (c) 2013 Matt Stow
      *
      * http://mattstow.com
      *
      * Licensed under the MIT license
      */
      var layoutEngine = (function() {
        var
          html = document.documentElement,
          style = html.style,
          vendor = ' vendor-',
          ie = 'ie',
          khtml = 'khtml',
          mozilla = 'mozilla',
          opera = 'opera',
          webkit = 'webkit',
          browser = ' browser-',
          android = 'android',
          chrome = 'chrome',
          wiiu = 'wiiu',
          cssClass = vendor;

        // WebKit
        if ('WebkitAppearance' in style) {
          cssClass += webkit;
          var ua = navigator.userAgent;

          if (ua.indexOf('Android') >= 0 && ua.indexOf('Chrome') === -1) {
            html.className += cssClass + browser + android;
            return {
              vendor: webkit,
              browser: android
            };
          }
          else if (!!window.chrome || ua.indexOf('OPR') >= 0) {
            html.className += cssClass + browser + chrome;
            return {
              vendor: webkit,
              browser: chrome
            };
          }
          else if (!!window.wiiu) {
            html.className += cssClass + browser + wiiu;
            return {
              vendor: webkit,
              browser: wiiu
            };
          }
          else {
            html.className += cssClass;
            return {
              vendor: webkit
            };
          }
        } else if ('MozAppearance' in style) {
          html.className += cssClass + mozilla;
          return {
            vendor: mozilla
          };
        } else if ('-ms-scroll-limit' in style || 'behavior' in style) {
          cssClass += ie + vendor + ie;
          if ('-ms-ime-align' in style) {
            html.className += cssClass + '-11';
            return {
              vendor: ie,
              version: 11
            };
          }
          else if ('-ms-user-select' in style) {
            html.className += cssClass + '-10';
            return {
              vendor: ie,
              version: 10
            };
          }
          else if ('fill' in style) {
            html.className += cssClass + '-9';
            return {
              vendor: ie,
              version: 9
            };
          }
          else if ('widows' in style) {
            html.className += cssClass + '-8';
            return {
              vendor: ie,
              version: 8
            };
          }
          else {
            html.className += cssClass + '-7';
            return {
              vendor: ie,
              version: 7
            };
          }
        } else if ('OLink' in style || !!window.opera) {
          cssClass += opera;
          if ('OMiniFold' in style) {
            html.className += cssClass + opera + '-mini';
            return {
              vendor: opera,
              version: 'mini'
            };
          } else {
            html.className += cssClass;
            return {
              vendor: opera
            };
          }
        } else if ('KhtmlUserInput' in style) {
          html.className += cssClass + khtml;
          return {
            vendor: khtml
          };
        } else {
          return false;
        }
      })();
      return layoutEngine;
    }()),

    regroup: function () {
      this.$get = function() {
        return function ( original, groupedBy, makeGroup ) {
          if ( arguments.length < 3 ) {
            return;
          }
          var
            regrouped = [],
            groups = {},
            group;

          ng.forEach(original, function( current ) {
            group = ( typeof makeGroup === 'function' ) ?
              makeGroup( current[ groupedBy ] ) :
              current[ groupedBy ];

            if ( !( group in groups ) ) {
              groups[ group ] = {
                groupedBy: group,
                items: []
              };
              regrouped.push( groups[ group ] );
            }
            groups[ group ].items.push( current );
          });
          return regrouped;
        };
      };
    },

    validations: function () {
      var defaultOpts = {
          EMAIL_REGEXP: /@/,
          POSTCODE_REGEXP: /^(([gG][iI][rR] 0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) [0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$/
        };

      this.addOpt = function( validation ) {
        defaultOpts.push( validation );
      };

      this.$get = [function() {
        return defaultOpts;
      }];
    },

    Validator: function ( ) {
      var
        conditions, toValidate = {},
        validatedData = {
          hasFailed: false,
          val: ''
        },
        addConditions = function( condition ) {
          conditions.push( condition );
          return this;
        },
        setInitialValue = function( val ){
          toValidate.initial = val;
          return this;
        },
        setInputtedValue = function( val ){
          toValidate.inputted = val;
          return this;
        };

      this.set = function( obj, val ) {
        switch( obj ) {
          case 'initial' || 'initialData' :
            setInitialValue( val );
            break;
          case 'inputted' || 'inputtedData' :
            setInputtedValue( val );
            break;
          case 'condition' :
            addConditions( val );
            break;
          case 'conditions' :
            conditions = val;
            break;
        }
        return this;
      };

      this.validateEntry = function() {
        validatedData.hasFailed = false;

        for (var i = conditions.length - 1; i >= 0; i--) {
          if ( conditions[i] ) {
            validatedData.hasFailed = true;
            break;
          }
        }
        if ( !validatedData.hasFailed ) {
          validatedData.val = ( (toValidate.initial + toValidate.inputted) || '' ).trim();
        }

        return validatedData;
      };
    }
  };

  return ng.module('core.helpers', [])
    .provider('regroup', [vprMth.Helpers.regroup])
    .provider('validations', [vprMth.Helpers.validations])
    // .service('formatNumber', [vprMth.Helpers.formatNumber])
    .service('Validator', ['validations', vprMth.Helpers.Validator]);
});