define([
  'angular'
], function ( ng ) {
  'use strict';

  return ng.module('core.shell.ui', [])
    .directive('coreBranding', ['coreConfig', function ( coreConfig ) {
      return {
        transclude: true,
        scope: {
          brand: '=coreBranding'
        },
        templateUrl: 'core/shell/partials/branding-full.html',
        compile: function () {
          return {
            pre: function (scope) {
              scope.appVersion = coreConfig.version;
            }
          };
        }
      };
    }])

    .directive('isLoading', ['Events', function (coreEvents) {
      return {
        restrict: 'AC',
        compile: function () {
          return function (scope, el) {
            var finishLoading;
            coreEvents.subscribe('doingResolve', function () {
              el.addClass('is-loading');
            });
            coreEvents.subscribe('resolveDone', function () {
              finishLoading = setTimeout(function(){ el.removeClass('is-loading'); }, 500);
            });
          };
        }
      };
    }])

    .directive('noSpaces', [function () {
      return {
        restrict: 'A',
        link: function (scope, el) {
          el.on('keydown', function(e) {
            return e.which !== 32;
          })
          .on('change', function() {
            el[0].value = el[0].value.replace(/\s/g, '');
          });
        }
      };
    }])

    .directive('modal', ['$timeout', function ( $timeout ) {
      return {
        restrict: 'AE',
        replace: true,
        transclude: true,
        scope: {
          title: '@title',
          cta: '@cta',
          cancelable: '@cancelable',
          active: '=active',
          action: '&action'
        },
        templateUrl: 'core/shell/partials/modal.html',
        link: function ( scope, el, attr ) {
          var
            states = {
              VISIBLE: 'visible',
              HIDDEN: ''
            },
            toggle = function ( show ) {
              $timeout(function () {
                scope.state = show ? states.VISIBLE : states.HIDDEN;
                if ( show ) {
                  el[0].focus();
                }
              }, 0);
            },
            close = function ( ev ) {
              if ( !scope.cancelable ) {
                return;
              }
              var
                el = ng.element( ev.target ),
                allowed = [
                  el.hasClass('modal-wrap'),
                  el.hasClass('modal-inner'),
                  el.hasClass('modal-box'),
                  el.hasClass('modal-cancel')
                  // el.hasClass('modal-button') && scope.state === states.VISIBLE
                ];

              if ( allowed.indexOf(true) >= 0 ) {
                $timeout(function () {
                  scope.active = false;
                }, 0);
              }
            },
            watchActive = scope.$watch( 'active', toggle );

          scope.state = states.HIDDEN;
          scope.visible = false;
          scope.hasAction = !!attr.action;
          scope.close = close;

          el.on('tap click', close);
          el.children().children().children()[1].style.maxHeight = (window.innerHeight - 115) + 'px';

          scope.$on('$destroy', function () {
            el.off('tap click', close);
            watchActive();
          });
        }
      };
    }]);

  //   setTranslateX: ionic.animationFrameThrottle(function(amount) {
  //     $element[0].style[ionic.CSS.TRANSFORM] = 'translate3d(' + amount + 'px, 0, -' + amount/1.2 + 'px) rotateY(-' + amount/12.5 + 'deg)';
  //     if ( sideMenuCtrl.left.el ) {
  //       sideMenuCtrl.left.el.style[ionic.CSS.TRANSFORM] = 'translate3d(' + ((amount*2)-amount) + 'px, 0, 0)';
  //     }
  //     $timeout(function() {
  //       $scope.sideMenuContentTranslateX = amount;
  //     });
  //   }),
});