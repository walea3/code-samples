define([
  'angular',
  'core/shell/shell.ui'
], function ( ng ) {
  'use strict';
  // Shell
  // ----------------

  //     core/shell/shell.js
  //     This contains controllers

  return ng.module('core.shell', [ 'core.shell.ui' ])
    .controller('MainCtrl', ['$scope', 'Nav', 'Events',
      function ( $scope, coreNav, coreEvents ) {
        coreEvents.publish('app.loggedin');
        coreNav.init( 'main', $scope );
      }
    ])

    // Shell page controllers
    .controller('ContentCtrl', [ '$scope', '$rootScope', '$state', 'Pages',
      function ( $scope, $rootScope, $state, corePages ) {
        var
          previousView = $rootScope.$viewHistory ? $rootScope.$viewHistory.backView || $rootScope.$viewHistory.forwardView : null,
          hasUser = !!$rootScope.auth.isLogged,
          hasPrevious = !!previousView;

          $scope.leftButton = function() {
            var
              goBack = 'icon ion-arrow-left-c',
              goHome = 'icon ion-home';
            return {
              type : hasPrevious ? goBack : goHome,
              action : hasPrevious ? previousView.stateId : ( hasUser ? 'core.main.home' : 'core.landing' )
            };
          };
        $scope.content = corePages.get( $state.current.name.split('.').pop() );
      }
    ]);
});