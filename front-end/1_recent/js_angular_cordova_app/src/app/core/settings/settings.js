/*global define */
define([
  'angular',
  'core/storage/storage',
  'core/navigation/navigation'
], function ( ng ) {
  'use strict';
  // Settings service module
  // ------------------------

  //  core/settings/settings.js
  return ng.module('core.settings', [
    'core.storage', 'core.navigation'
  ])

  .controller('SettingsCtrl', ['$rootScope', '$state', '$scope', 'Settings', function ($rootScope, $state, $scope, Settings) {
    $scope.setTitle('SETTINGS');
    $scope.settingsList = Settings.all( true );
    $scope.listClass = $state.current.views ? 'hide' : 'list-visible';

    $rootScope.$on('$stateChangeStart', function ( e, to ) {
      if ( to.parent !== 'settings' && to.name !== 'settings' ) {
        return;
      }
      $scope.listClass = to.views ? 'hide' : 'list-visible';
    });
  }])

  .controller('AboutCtrl', ['$scope', function ($scope) {
    $scope.setTitle('ABOUT');
  }])

  .provider('Settings', ['StorageProvider', function ( StorageProvider ) {
    var
      groups = {},
      groupsIndex = [],
      settingTypes = {
        'boolean': {
          // 2 values
          requires: ['trueVal', 'falseVal'],
          template: 'core/settings/types/boolean.html'
        },
        'switch': {
          // 2 - 4 values
          requires: ['options']
        },
        'select': {
          // 4+ values
          requires: ['options', 'multiple']
        },
        'list': {
          optional: ['default']
        },
        'range': {
          requires: ['min', 'max'],
          optional: ['default', 'unit']
        },
        'state': {
          requires: ['value']
        },
        'custom': {
          requires: ['template']
        },
         // defaults to simple text
        'basic': {
          requires: [],
          optional: ['readOnly', 'onUpdate', 'onSave']
        }
      },
      store = StorageProvider.prep('factory', 'Settings', {});

    function positionIn ( indexArray, name, position ) {
      indexArray.splice( position, 0, name );
    }

    var Setting = function ( name, conf ) {
      var self = this;
      if ( !conf.type || !settingTypes[ conf.type ] ) {
        throw new Error('A valid setting type is required.' );
      }
      this.name = name;
      this.type = conf.type;
      this.title = conf.title;
      this.value = conf.value || null;
      this.listed = (typeof conf.listed !== 'undefined' ? conf.listed : true);

      var
        assign = function ( name ) {
          if ( conf[ name ] ) {
            self[ name ] = conf[ name ];
            return true;
          }
        },
        required = settingTypes.basic.requires.concat( settingTypes[ this.type ].requires || [] ),
        optional = settingTypes.basic.optional.concat( settingTypes[ this.type ].optional || [] ),
        i;

      for (i = 0; i < required.length; i++) {
        if ( !assign( required[i] ) ) {
          throw new Error('Missing settings requirement: ' + required[i] + ', for ' + this.type + ' type.' );
        }
      }

      for (i = 0; i < optional.length; i++) {
        assign( optional[i] );
      }
    };
    Setting.prototype.update = function () {
      if ( typeof this.onUpdate === 'function' ) {
        this.onUpdate();
      }
      return this;
    };
    Setting.prototype.save = function () {
      if ( typeof this.onSave === 'function' ) {
        this.onSave();
      }
      this.parent().save();
      return this;
    };
    Setting.prototype.assemble = function () {};

    var SettingsGroup = function ( name, conf ) {
      this.settings = {};
      this.conf = {
        name: '',
        title: '',
        listed: true,
        settingsIndex: []
      };
      ng.extend( this.conf, conf );
      this.conf.name = name;
    };
    SettingsGroup.prototype.save = function () {
      store.set( this.conf.name, this.settings );
      return this;
    };
    SettingsGroup.prototype.setting = function ( name, settingConf ) {
      if ( !name ) {
        return this;
      }

      var
        group = this,
        settings = this.settings;

      if ( !settingConf ) {
        return settings[ name ] || null;
      }

      settings[ name ] = new Setting( name, settingConf );
      settings[ name ].parent = function() {
        return group;
      };

      if ( settings[ name ].listed ) {
        positionIn( this.conf.settingsIndex, name, settingConf.position );
      }

      return this.save();
    };
    SettingsGroup.prototype.settingsOrder = function ( onlyListed ) {
      var
        settingsInOrder = [],
        settingsIndex = this.conf.settingsIndex;
      for (var i = 0; i < settingsIndex.length; i++) {
        if ( onlyListed && !this.settings[ settingsIndex[i] ].listed ) {
          return;
        }
        settingsInOrder.push(this.settings[ settingsIndex[i] ]);
      }

      return settingsInOrder;
    };

    this.group = function( name, conf ) {
      if ( !name ) {
        return this;
      }

      if ( conf && !groups[ name ] ) {
        positionIn( groupsIndex, name, conf.position );
      }
      return groups[ name ] ? groups[ name ] : ( groups[ name ] = new SettingsGroup( name, conf ) );
    };

    this.registerSettingType = function () {
      return;
    };

    this.$get = [function () {
      return {
        all: function ( onlyListed ) {
          var groupsInOrder = [];

          ng.forEach(groupsIndex, function ( group ) {
            group = groups[ group ];
            if ( ( onlyListed && group.conf.listed ) || !onlyListed ) {
              groupsInOrder.push( group );
            }
          });
          return groupsInOrder;
        },
        for: function ( group ) {
          return groups[ group ] || null;
        }
      };
    }];
  }])

  .config(['$stateProvider', 'NavProvider', 'SettingsProvider',
    function ( $stateProvider, NavProvider, SettingsProvider ) {
      $stateProvider
        .state('settings', {
          parent: 'core.main',
          url: '/settings',
          templateUrl: 'core/settings/settings.html',
          controller: 'SettingsCtrl',
          accessLevel: accessLevels.user
        })
        .state('settings.about', {
          parent: 'core.main',
          url: '/about',
          templateUrl: 'core/settings/about.html',
          controller: 'AboutCtrl',
          accessLevel: accessLevels.user
        });

      NavProvider.menu( 'main', [{
        title: 'Settings',
        state: 'settings'
      }]);

      SettingsProvider.group('main', {}).setting('about', {
        title: 'About',
        type: 'state',
        value: 'settings.about'
      });
    }
  ]);
});