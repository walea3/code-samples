define([
  'angular',
  'core/account/account',
  'core/account/auth.service'
], function ( ng, coreAccount ) {
  'use strict';

  return coreAccount.controller('LoginCtrl', ['$scope', '$state', 'coreConfig', 'Auth',
    function ($scope, $state, coreConfig, coreAuth) {
      $scope.working = false;
      $scope.resetPass = coreConfig.resetPass;

      $scope.submit = function ( formInstance ) {
        // setup promise, and 'working' flag
        if ( !formInstance.$valid || $scope.working ) {
          $scope.login.wrong = true;
          return false;
        }

        $scope.working = true;
        $scope.login.wrong = false;

        if ( coreConfig.env === 'local' ) {
          coreAuth.demoUser( $scope.login );
          $scope.working = false;
        } else {
          coreAuth.loginUser( $scope.login )
            .then(function () {
              $state.go('core.main.home');
            })
            .catch(function () {
              $scope.login.wrong = true;
            })
            .finally(function () {
              $scope.working = false;
            });
        }
      };

      $scope.retryLogin = function() {
        $scope.login.wrong = false;
      };
    }
  ]);
});