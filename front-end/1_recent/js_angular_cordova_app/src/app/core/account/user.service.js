define([
  'angular',
  'core/account/auth.service',
  'core/storage/storage',
  'core/events/events'
], function ( ng, coreAccount ) {
  'use strict';

  return coreAccount.provider('User', function () {
    var
      userSchema = {
        salutation: '',
        first_name: '',
        last_name: '',
        email: '',
        postcode: '',
        account_number: '',
        home_id: '',
        confirmed: '',
        // created: '',
        userRole: {}
        // tokens: []
      };

    this.$get = ['$rootScope', 'Storage', 'Events', 'Settings',
      function ( $rootScope, coreStorage, coreEvents, coreSettings ) {
        var
          getFactory = function () {
            return coreStorage('factory', 'Users', {
              currentUser: null,
              device: {}
            });
          },

          service = {
            store: getFactory(),

            schema: function () {
              return ng.copy( userSchema );
            },

            isActive: function () {
              var
                user = service.load( service.store.get( 'currentUser' ) ),
                token = coreStorage('get', 'userToken');
              return user !== null && user.access_token === token;
            },

            me: function () {
              var user = service.load( service.store.get( 'currentUser' ) );
              if ( service.isActive() && !user.fullName ) {
                user.fullName = ( user.first_name || '' ) + ' ' + ( user.last_name || '' );
              }

              return user;
            },

            load: function ( email ) {
              var
                all = service.store.get( 'device' ) || [],
                single = all[ email ] || null;

              return ng.isDefined( email ) ? single : all;
            },

            update: function ( user ) {
              var all = service.load();

              if ( ng.isDefined(user) ) {
                if ( user !== null && user.email ) {
                  user = ng.extend(all[ user.email ] || {}, user);
                  // Add user to device users
                  all[ user.email ] = user;
                  service.store.set( 'device', all );
                }
                // update the user in storage
                service.store.set( 'currentUser', user ? user.email : null );
                if ( $rootScope.auth ) {
                  $rootScope.auth.user = user || {};
                }
              }
              return service;
            },

            end: function () {
              service.update(null);
              return service;
            }
          };

        coreEvents.subscribe('app.loggedin', function ( ev, res ) {
          service.update( res.args );

          var userSettings = coreSettings.for( 'user' );
          userSettings.setting('fullName').value = service.me().fullName;
          userSettings.save();
        });
        coreEvents.subscribe('app.logout', service.end);

        return service;
      }
    ];
  });
});