define([
  'angular',
  'core/events/events',
  'core/storage/storage',
  'core/settings/settings'
], function ( ng ) {
  'use strict';

  return ng.module('core.account', [
    'core.config', 'core.events', 'core.storage', 'core.settings'
  ])

  .config(['$stateProvider', 'SettingsProvider',
    function($stateProvider, SettingsProvider) {

      $stateProvider
        .state('core.login', {
          url: '/login',
          templateUrl: 'core/account/login/login.html',
          controller:'LoginCtrl',
          accessLevel: accessLevels.anon
        })

        // .state('core.register', {
        //   url: '/register',
        //   templateUrl: 'core/account/register/account.html',
        //   controller:'RegisterCtrl',
        //   accessLevel: accessLevels.anon
        // })

        // .state('core.register.verify', {
        //   parent: 'app',
        //   params: ['v'],
        //   templateUrl: 'core/account/register/account.html',
        //   controller:'RegisterCtrl',
        //   accessLevel: accessLevels.anon
        // })

        .state('core.endpoints', {
          templateUrl: 'core/account/views/endpoints.html',
          controller: 'UpdateAccountCtrl',
          accessLevel: accessLevels.anon
        })

        .state('core.resetpassword', {
          url: '/reset-password',
          templateUrl: 'core/account/views/resetpassword.html',
          controller: 'UpdateAccountCtrl',
          accessLevel: accessLevels.anon
        })

        .state('core.main.changeemail', {
          url: '/account/change-email',
          templateUrl: 'core/account/views/changeemail.html',
          controller: 'UpdateAccountCtrl',
          menuParent: 'core.main.account.details',
          accessLevel: accessLevels.user
        })

        .state('core.main.changepassword', {
          url: '/account/change-password',
          templateUrl: 'core/account/views/changepassword.html',
          controller: 'UpdateAccountCtrl',
          menuParent: 'core.main.account.details',
          accessLevel: accessLevels.user
        });

      // StorageProvider.addResource('Account', {
      //   'user': {}
      // });

      SettingsProvider.group('user', { title: 'Account Information' })
        .setting('fullName', {
          title: 'Full name',
          type: 'basic',
          readOnly: true
        })
        .setting('email', {
          title: 'Email',
          type: 'basic',
          readOnly: true,
          value: 'jane.doe@email.com'
        })
        .setting('accNo', {
          title: 'Account No.',
          type: 'basic',
          readOnly: true,
          value: '21071614546'
        });
    }
  ])

  .run(['$rootScope', '$state', 'Auth', 'User', 'Events',
    function ($rootScope, $state, coreAuth, coreUser, coreEvents ) {
      // Auth service exposed and a new Object containing login user/pwd
      $rootScope.auth = coreAuth;
      $rootScope.login = {
        autoLogin: true,
        working: false,
        wrong: false
      };
      $rootScope.logout = function () {
        coreEvents.publish('app.logout');
      };

      /* Auto login */
      $rootScope.$on('$stateChangeStart', function (e, to) {
        if ( to.name !== 'core.error' && coreUser.isActive() && coreUser.me().autoLogin && ( !to.parent && to.accessLevel && to.accessLevel.title === 'public' && to.name !== 'core.register' ) ) {
          e.preventDefault();
          $state.go('core.home', {notify: false});
        }
      });
    }
  ])

  .controller('UpdateAccountCtrl', ['$scope', '$state', '$http', '$timeout', 'coreConfig', 'Storage', 'Events',
    function ($scope, $state, $http, $timeout, coreConfig, coreStorage, coreEvents) {
      var forms = {
        'changeEmailForm': {
          model: 'changeEmail',
          endpoint: '/account/change-email',
          onSuccess: ng.noop
        },
        'changePassForm': {
          model: 'changePass',
          endpoint: '/account/change-pass',
          onSuccess: ng.noop
        },
        'resetPassForm': {
          model: 'resetPass',
          endpoint: '/reset-pass',
          onSuccess: ng.noop
        }
      };

      $scope.working = false;
      if ( coreConfig.env !== 'production' && $state.current.name === 'core.endpoints' ) {
        $scope.environments = coreConfig.servers;
        $scope.servers = $scope.environments[ coreConfig.env ];
      }

      $scope.submit = function ( formInstance ) {
        if ( coreConfig.env !== 'production' && $state.current.name === 'core.endpoints' ) {
          var envOpts = document.forms[0].selectedEnv;
          coreConfig.env = envOpts.options[envOpts.selectedIndex].text;

          coreStorage('reloadResource', 'Hub');
          coreStorage('set', 'coreApp', coreConfig);
          coreEvents.publish('runMockHub');
          return;
        }

        var
          form = forms[ formInstance.$name ],
          formModel = form.model ? $scope[ form.model ] : {},
          submitPromise;

        // setup promise, and 'working' flag
        if ( !formInstance.$valid || $scope.working ) {
          return false;
        }

        $scope.working = true;
        $scope.wrong = false;
        $scope.sent = false;

        submitPromise = $http.post( coreConfig.getApi( form.endpoint ), formModel )
          .error(function () {
            $scope.wrong = true;
          })
          .success(function( data ) {
            $scope.sent = true;
            form.onSuccess( data, formModel );
            if ( form.finalState) {
              $state.go( form.finalState );
            }
          })
          .finally(function () {
            $timeout(function(){ $scope.working = false; }, 1000);
          });
      };
    }
  ]);
});