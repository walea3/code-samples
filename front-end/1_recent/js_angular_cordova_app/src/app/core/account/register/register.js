define([
  'angular',
  'core/account/account',
  'core/account/auth.service'
], function ( ng, coreAccount ) {
  'use strict';

  return coreAccount.controller('RegisterCtrl', ['$scope', '$state', '$timeout', 'Auth',
    function ($scope, $state, $timeout, coreAuth) {
      var errs = {
        'invalid': 'There was a problem creating your account, please check your details and try again.',
        'already_registered' : 'These account details and/or e-mail address have already been registered and cannot be used again.',
        'verification_failed': 'The verification code is invalid, please check and try again or re-send the email.',
        'autologin_failed': 'Your account was created but there was a problem. Please close the app and login.',
        'unknown': 'Your account couldn\'t be created, please check your connection and try again.'
      };

      $scope.working = $scope.confirming = $scope.hasError = $scope.verifyEmail = false;
      $scope.regUser = coreAuth.user;
      $scope.regUser.role = 'user';

      $scope.submit = function (formInstance) {
        if ( !formInstance.$valid || $scope.working ) {
          $scope.hasError = true;
          $scope.errorMsg = errs.invalid;
          return;
        }
        // is working
        $scope.working = true;
        $scope.hasError = false;
        // VERIFY supplier account
        if ( $state.current.name === 'core.register' ) {
          coreAuth.verifyAccount( $scope.regUser )
            .success(function ( user ) {
              $state.go( user.confirmed ? 'core.main.home' : 'core.register.account' );
            })
            .error(function () {
              $scope.hasError = true;
              $scope.working = false;
            });
        }

        // CREATE user account
        if ( $state.current.name === 'core.register.account' ) {
          coreAuth.createAccount( $scope.regUser )
            .success(function () {
              $scope.verifyEmail = true;
            })
            .error(function (data) {
              data.errors.forEach(function (error) {
                formInstance[error.field].$error[error.name] = true;
              });

              $scope.hasError = true;
              $scope.errorMsg = errs.already_registered;
            })
            .finally(function () {
              $scope.working = false;
            });
         }
      };

      $scope.confirmAccount = function (formInstance) {
        if ( (formInstance && !formInstance.$valid) || $scope.confirming ) {
          return;
        }
        // is confirming
        $scope.confirming = true;

        coreAuth.confirmAccount( $scope.verifyCode )
          .then(function () {
            $state.go('core.main.home');
          }, function (error) {
            $scope.hasError = true;
            $scope.confirming = false;
            $scope.errorMsg = errs[ error ] || errs.unknown;
          });
      };

      $scope.clearError = function() {
        $scope.hasError = false;
      };

      if ( $state.current.name === 'core.register.verify' ) {
        $scope.verifyEmail = true;
        $scope.verifyCode = $state.params.v;
        // $scope.regUser.account_number = $state.params.accNo;
        // $scope.regUser.email = $state.params.email;
        $scope.confirmAccount();
      }
    }
  ]);
});