define([
  'angular',
  'core/account/account',
  'core/account/auth.conf'
], function ( ng, coreAccount ) {
  'use strict';

  // Note the login services use bitwise logic, and hence we need to turn off js hint checking
  // of bitwise functions for this file.
  // This code is based on https://github.com/mrgamer/angular-login-example
  /* jshint bitwise: false */

  function AuthService () {
    var
      userTokenKey = 'userToken',
      authStates = {
        error: 'error',
        loggedin: 'core.home',
        loggedout: 'core.landing'
      };

    this.setAuthStates = function ( authState, state ) {
      if ( authStates[ authState ] ) {
        authStates[ authState ] = state;
      }
      return this;
    };

    this.$get = ['$http', '$q', '$state', 'coreConfig', 'Storage', 'Events', 'Platform',
      function ($http, $q, $state, coreConfig, coreStorage, coreEvents, corePlatform) {
        var userToken = coreStorage('get', userTokenKey);
        /**
         * Low-level, private functions.
         */
        var setHeaders = function (token) {
          if (!token) {
            delete $http.defaults.headers.common.Authorization;
            return;
          }
          $http.defaults.headers.common.Authorization = 'Bearer ' + token.toString();
        };

        var setToken = function (token) {
          if (!token) {
            coreStorage('remove', userTokenKey);
          } else {
            coreStorage('set', userTokenKey, token);
          }
          setHeaders(token);
        };

        var getLoginData = function () {
          var
            userStore = coreStorage('get', 'Users'),
            currentUser = userStore ? userStore.currentUser : null;
          if ( userToken && currentUser ) {
            setHeaders(userToken);
            wrappedService.loginHandler( userStore.device[ currentUser ] );
          } else {
            wrappedService.userRole = userRoles.public;
            wrappedService.isLogged = false;
            wrappedService.doneLoading = true;
          }
        };

        var managePermissions = function () {
          // Register routing function.
          coreEvents.subscribe('$stateChangeStart', function (event, to, toParams) {
            /**
             * $stateChangeStart is a synchronous check to the accessLevels property
             * if it's not set, it will setup a pendingStateChange and will let
             * the grandfather resolve do his job.
             *
             * In short:
             * If accessLevels is still undefined, it let the user change the state.
             * Grandfather.resolve will either let the user in or reject the promise later!
             */
            if (wrappedService.userRole === null) {
              wrappedService.doneLoading = false;
              wrappedService.pendingStateChange = {
                to: to,
                toParams: toParams
              };
              return;
            }

            // if the state has undefined accessLevel, anyone can access it.
            // NOTE: if `wrappedService.userRole === undefined` means the service still doesn't know the user role,
            // we need to rely on grandfather resolve, so we let the stateChange success, for now.
            if (to.accessLevel === undefined || (to.accessLevel.bitMask & wrappedService.userRole.bitMask)) {
              ng.noop(); // requested state can be transitioned to.
            } else {
              event.preventDefault();
              coreEvents.publish('$statePermissionError');
              $state.go(authStates.error, { error: 'unauthorized' }, { location: false, inherit: false });
            }
          });

          /**
           * Gets triggered when a resolve isn't fulfilled
           * NOTE: when the user doesn't have required permissions for a state, this event
           *       it's not triggered.
           *
           * In order to redirect to the desired state, the $http status code gets parsed.
           * If it's an HTTP code (ex: 403), could be prefixed with a string (ex: resolvename403),
           * to handle same status codes for different resolve(s).
           * This is defined inside $state.redirectMap.
           */
          coreEvents.subscribe('$stateChangeError', function (event, to, toParams, from, fromParams, error) {
            /**
             * This is a very clever way to implement failure redirection.
             * You can use the value of redirectMap, based on the value of the rejection
             * So you can setup DIFFERENT redirections based on different promise errors.
             */
            var redirectObj;
            // in case the promise given to resolve function is an $http request
            // the error is an object containing the error and additional informations
            error = (typeof error === 'object') ? error.toString() : error;
            // in case of a random 4xx/5xx status code from server, user gets loggedout
            // otherwise it *might* forever loop (look call diagram)
            if (/^[45]\d{2}$/.test(error)) {
              wrappedService.logoutUser();
            }
            /**
             * Generic redirect handling.
             * If a state transition has been prevented and it's not one of the 2 above errors, means it's a
             * custom error in your application.
             *
             * redirectMap should be defined in the $state(s) that can generate transition errors.
             */
            if (ng.isDefined(to.redirectMap) && ng.isDefined(to.redirectMap[error])) {
              if (typeof to.redirectMap[error] === 'string') {
                return $state.go(to.redirectMap[error], { error: error }, { location: false, inherit: false });
              } else if (typeof to.redirectMap[error] === 'object') {
                redirectObj = to.redirectMap[error];
                return $state.go(redirectObj.state, { error: redirectObj.prefix + error }, { location: false, inherit: false });
              }
            }
            return $state.go(authStates.error, { error: error }, { location: false, inherit: false });
          });
        };

        /**
         * High level, public methods
         */
        var wrappedService = {
          loginHandler: function (user) {
            /**
             * Custom logic to manually set userRole goes here
             *
             * Commented example shows an userObj coming with a 'completed'
             * property defining if the user has completed his registration process,
             * validating his/her email or not.
             *
             * EXAMPLE:
             * if (user.hasValidatedEmail) {
             *   wrappedService.userRole = userRoles.registered;
             * } else {
             *   wrappedService.userRole = userRoles.invalidEmail;
             *   $state.go('core.nagscreen');
             * }
             */
            // update user
            ng.extend(wrappedService.user, user);
            // setup token
            setToken(wrappedService.user.access_token || wrappedService.user.token);
            // flag true on isLogged
            wrappedService.isLogged = true;
            // update userRole
            wrappedService.userRole = user.userRole;
            return wrappedService.user;
          },

          verifyAccount: function ( userData ) {
            var httpPromise = $http.get( encodeURI(coreConfig.getApi( '/verify-account' )), {
              params: {
                // 'meter_id' : userData.meter_id,
                'postcode' : userData.postcode.toUpperCase(),
                'email': userData.email
              }
            });

            return httpPromise.success(function ( user ) {
              if ( user.access_token ) {
                setToken( user.access_token );
                // update user
                // user = ng.extend(wrappedService.user, user);
              }
              // Create temp store for sign up data
              coreStorage('set', 'userSignUp', user);
              return user;
            });
          },


          createAccount: function ( userData ) {
            var
              userSignUp = coreStorage('factory', 'userSignUp'),
              // verifyURL = corePlatform.getIntent('//app/register/verify?accNo=${account_number}&email=${email}&v=${email_verification_token}'),
              verifyURL = corePlatform.getIntent('//app/register/verify?v=${email_verification_token}'),
              httpPromise = $http.post( coreConfig.getApi('/account'), {
                'email': userData.email,
                'password': userData.password,
                'sso_token': userSignUp.get('sso_token'),
                'email_verification_appurl': verifyURL
              });

            userSignUp.set('email_verification_appurl', verifyURL);
            userSignUp.set('userRole', userRoles.user);
            userSignUp.set('email', userData.email);
            userSignUp.set('_', userData.password);

            return httpPromise;
          },

          confirmAccount: function ( verifyCode ) {
            var
              me = this,
              confirm = $q.defer(),
              userSignUp = coreStorage('factory', 'userSignUp'),
              creds = {
                'email': userSignUp.get('email'),
                'password': userSignUp.get('_'),
                // 'account_number': userSignUp.get('account_number'),
                'autoLogin': true
              };

            $http.post( coreConfig.getApi('/confirm-account'), ng.extend({
              'email_verification_token': verifyCode.toUpperCase(),
              'password': userSignUp.get('_')
            }, userSignUp._data ) )
            .success(function ( regInfo ) {
              regInfo.confirmed = true;

              userSignUp
                .remove('_')
                .remove('password')
                .remove('email_verification_token')
                .remove('email_verification_appurl');

              wrappedService.user = userSignUp._data;
              ng.extend(wrappedService.user, regInfo);

              me.loginUser( creds ).then(function () {
                coreStorage('remove', 'userSignUp');
                confirm.resolve();
              })
              .catch(confirm.reject.bind( null, 'autologin_failed' ));
            })
            .error(confirm.reject.bind( null, 'verification_failed' ));

            return confirm.promise;
          },

          loginUser: function ( loginData ) {
            var
              me = this,
              login = $q.defer();

            $http.get( coreConfig.getApi('/oauth/token'), {
              params: {
                username: loginData.email,
                password: loginData.password,
                grant_type: 'password'
              },
              headers:  {
                'Authorization': 'Basic ' + btoa(loginData.email + ':' + loginData.password),
                'Content-Type': 'application/x-www-form-urlencoded'
              }
            })
            .success(function ( loginInfo ) {
              var user = coreStorage('get', 'Users').device[ loginData.email ] || {};

              loginInfo.userRole = userRoles.user;
              user = me.loginHandler( ng.extend( user, loginInfo ) );
              user.autoLogin = loginData.autoLogin;

              coreEvents.publish('app.loggedin', user);
              login.resolve(user);
            })
            .error(login.reject.bind(null));

            return login.promise;
          },
          demoUser: function ( userData ) {
            var
              demoUser = ng.extend(userData, {
                // email: 'john.doe@email.com',
                firstName: 'John',
                lastName: 'Doe',
                acc: '9704109642',
                token: 'e4c611d2-cee4-11e3-9ada-1a514932ac01',
                userRole: userRoles.user
              }),
              user = this.loginHandler( demoUser );

            user.autoLogin = demoUser.autoLogin;
            $state.go(authStates.loggedin);
            coreEvents.publish('app.loggedin', user);
          },
          logoutUser: function (httpPromise) {
            /**
             * De-registers the userToken remotely
             * then clears the loginService as it was on startup
             */
            setToken(null);
            this.userRole = userRoles.public;
            this.user = {};
            this.isLogged = false;
            $state.go(authStates.loggedout);
            coreEvents.publish('app.loggedout');
            return httpPromise;
          },
          checkAuth: function () {
            var
              roleDefined = $q.defer(),
              self = this;
            /**
             * In case there is a pendingStateChange means the user requested a $state,
             * but we don't know yet user's userRole.
             *
             * Calling resolvePendingState makes the login service retrieve his userRole remotely.
             */
            if (self.pendingStateChange) {
              return self.resolvePendingState($http.get( coreConfig.getApi('/account') ));
            }
            roleDefined.resolve();
            return roleDefined.promise;
          },
          resolvePendingState: function (httpPromise) {
            var
              checkUser = $q.defer(),
              self = this,
              pendingState = self.pendingStateChange;

            // When the $http is done, we register the http result into loginHandler, `data` parameter goes into loginService.loginHandler
            httpPromise.success(self.loginHandler);

            httpPromise.then(
              function success() {
                self.doneLoading = true;
                // duplicated logic from $stateChangeStart, slightly different, now we surely have the userRole informations.
                if (pendingState.to.accessLevel === undefined || pendingState.to.accessLevel.bitMask & self.userRole.bitMask) {
                  checkUser.resolve();
                } else {
                  checkUser.reject('unauthorized');
                }
              },
              function reject(httpObj) {
                checkUser.reject(httpObj.status.toString());
              }
            );
            /**
             * I set up the state change inside the promises success/error,
             * so i can safely assign pendingStateChange back to null.
             */
            self.pendingStateChange = null;
            return checkUser.promise;
          },
          /**
           * Public properties
           */
          userRole: null,
          user: coreStorage('get', 'userSignUp') || {},
          isLogged: null,
          pendingStateChange: null,
          doneLoading: null
        };

        getLoginData();

        managePermissions();

        coreEvents.subscribe('app.logout', wrappedService.logoutUser.bind(wrappedService));

        return wrappedService;
      }
    ];
  }

  return coreAccount.provider('Auth', AuthService);
});