define([
  'angular',
  'core/dashboard/dashboard'
], function ( ng, coreDash ) {
  'use strict';

  function DashboardService () {
    var
      dashboard = this,
      boards = {};

    /*
     * Board object
    */
    var Board = function ( name, conf ) {
      this.name = name;
      this.conf = {};
      this.cards = {};
      this.stick = {};
      this.cardIndex = [];

      ng.extend( this.conf, conf );
      return this;
    };

    Board.prototype.card = function ( name, conf, events ) {
      var board = this, dest, card, position = 0;
      if ( !name ) {
        return;
      }

      dest = conf && conf.sticky ? board.stick : board.cards;
      card = dest[ name ];

      if ( conf ) {
        conf.board = function () {
          return board;
        };

        position = conf.position >= 0 && conf.position <= board.cardIndex.length ?
          conf.position :
          board.cardIndex.length;

        if ( !conf.stick ) {
          board.cardIndex.splice( position, 0, name );
        }
      }

      return card || ( dest[ name ] = new Card( name, conf, events ) );
    };

    Board.prototype.sticky = function ( name ) {
      var board = this, card;
      card = board.sticky[ name ];
      return card;
    };

    Board.prototype.allCards = function () {
      var cardsInOrder = [];
      for (var i = 0; i < this.cardIndex.length; i++) {
        cardsInOrder.push(this.cards[ this.cardIndex[i] ]);
      }
      return cardsInOrder;
    };

    /*
     * Card object
    */
    var Card = function ( name, conf, events ) {
      this.name = name;
      this.conf = {
        board: false,
        sticky: false,
        template: null,
        templateUrl: null
      };
      this.$el = null;

      function done ( deferred ) {
        deferred.resolve();
      }

      this.on = {
        init: ng.noop,
        display: done,
        remove: done
      };

      ng.extend( this.conf, conf );
      ng.extend( this.on, events );
      return this;
    };

    Card.prototype.init = function () {
      this.on.init();
      return this;
    };

    Card.prototype.board = function ( board ) {
      return this.conf.board || ( this.conf.board = board );
    };

    Card.prototype.removeBoard = function () {
      this.conf.board = false;
    };

    // Create main board
    boards.main = new Board('main');

    this.board = function ( name, conf ) {
      var board = boards[ name ] || boards.main;
      return board || ( boards[ name ] = new Board( name, conf ) );
    };

    this.removeBoard = function ( name ) {
      var board = boards[ name ];
      if ( !board ) {
        return;
      }

      board.onRemove().then(function () {
        delete boards[ name ];
      });
      return;
    };

    this.$get = ['$q', '$timeout', '$templateCache', function ( $q, $timeout, $templateCache ) {
      Card.prototype.storeEl = function ( $el ) {
        this.$el = $el;
        return this;
      };

      Card.prototype.getTemplate = function () {
        var conf = this.conf;
        return conf.template ? conf.template : ( conf.templateUrl ? $templateCache.get( conf.templateUrl ) : '' );
      };

      Card.prototype.onDisplay = function () {
        var
          me = this,
          deferred = $q.defer();

        $timeout(function () {
          me.on.display.call( me, deferred );
        }, 100);
        return deferred.promise;
      };

      Card.prototype.onRemove = function () {
        var
          me = this,
          board = this.board(),
          deferred = $q.defer();

        $timeout(function () {
          me.on.remove.call( me, deferred );
          if ( board ) {
            board.removeCard( me.name );
            me.removeBoard();
          }
        }, 100);
        return deferred.promise;
      };

      var dashService = {
        board: dashboard.board
      };
      return dashService;
    }];
  }

  return coreDash.provider('Dashboard', DashboardService);
});