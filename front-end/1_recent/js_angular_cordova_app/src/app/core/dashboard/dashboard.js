define([
  'angular',
  'angular-ui-router',
  'core/navigation/navigation'
], function ( ng ) {
  'use strict';

  return ng.module('core.dashboard', ['ui.router', 'core.navigation'])

    .controller('DashboardCtrl', ['$scope', function ($scope) {
      $scope.setTitle('');
    }])

    .directive('dash', ['Dashboard', function ( coreDash ) {
      return {
        templateUrl: 'core/dashboard/dashboard.html',
        restrict: 'EA',
        controller: function($scope, $element, $attrs) {
          $scope.board = coreDash.board( $attrs.board );
          $scope.dashCards = $scope.board.allCards();
          this.card = $scope.board.card.bind( $scope.board );
        }
      };
    }])

    .directive('dashCard', ['$compile', function ( $compile ) {
      return {
        require: '^dash',
        restrict: 'EA',
        scope: true,
        link: function (scope, el) {
          var
            card = scope.$parent.card,
            tpl = ng.element( card.getTemplate() );
            // cardCtrl = card.conf.controller ? $controller( card.conf.controller, { '$scope': scope.$parent }) : null;

          tpl = el.append( tpl.addClass('card__inner') );
          card.init().storeEl( tpl );

          card.onDisplay().then(function () {
            $compile(tpl.contents())( scope.$parent );
            tpl.addClass('card--active');
          });
        }
      };
    }])

    .config(['$stateProvider', 'NavProvider', function ($stateProvider, NavProvider) {
      $stateProvider
        .state('core.home', {
          parent: 'core.main',
          url: '/home',
          controller: 'DashboardCtrl',
          template: '<dash board="main" class="pane dashboard"></dash>',
          accessLevel: accessLevels.user
        });

        NavProvider.menu( 'main', [{
          key: 'home',
          title: 'My Home',
          items: [{
            title: 'Home stream',
            state: 'core.home'
          }],
          position: 0
        }]);
    }]);
});