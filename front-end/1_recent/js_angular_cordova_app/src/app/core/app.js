/*global define */
define([
  'angular',
  'core/account/user.service',
  'core/account/login/login',
  'core/shell/shell',
  'core/platform/platform',
  'core/dashboard/dashboard.service',
  'services/heating/heating',
  'services/weather/weather'
], function ( ng ) {
  'use strict';
  // Main application module
  // ----------------

  //  core/app.js
  //  This is the main application module. It contains
  //  configuration and initial one off set up.
  return ng.module('core', [
    // Vendor modules
    'ionic', 'ngTouch', 'ui.router', 'ngSanitize', 'ngResource',
    'pascalprecht.translate', 'pasvaz.bindonce',
    // Service Modules
    'service.heating', 'service.weather',
    // Core modules
    'core.config', 'core.platform',
    'core.account','core.dashboard',
    'core.shell',
    // Dev modules
    'mock.hub'
  ])

  .config(['$urlRouterProvider', '$provide',
    function ( $urlRouterProvider, $provide) {
      $provide.decorator('$rootScope', ['$delegate', function($delegate){
        Object.defineProperty($delegate.constructor.prototype, '$onRootScope', {
          value: function(name, listener){
            var unsubscribe = $delegate.$on(name, listener);
            this.$on('$destroy', unsubscribe);
          },
          enumerable: false
        });
        return $delegate;
      }]);

      $urlRouterProvider.otherwise('/');
    }
  ])

  // Run happens at application startup, it is only called once.
  .run(['$rootScope', '$log', '$state', 'Platform',
    function ($rootScope, $log, $state, corePlatform) {
      // Initialise all runtime platform specific functionality.
      corePlatform.init();

      /**
       * doingResolve & resolveDone events are useful to display a spinner on changing states.
       * Some states may require remote data so it will take awhile to load.
       */

      function resolveDone () {
        $rootScope.$emit('resolveDone');
      }
      $rootScope.$on('$stateChangeError', resolveDone);
      $rootScope.$on('$statePermissionError', resolveDone);
      $rootScope.$on('$locationChangeSuccess', resolveDone);
      $rootScope.$on('$stateChangeSuccess', resolveDone);
      $rootScope.$on('$stateChangeStart', function (e, to) {
        $rootScope.$emit('doingResolve');

        if (!to.data || !ng.isFunction(to.data.condition)) {
          return;
        }
        var result = to.data.condition();

        if (result.fail && result.to) {
          e.preventDefault();
          $state.go(result.to, result.params, {notify: false});
        }
      });
    }
  ]);
});