define([
  'angular',
  'core/app'
], function ( ng, appCore ) {
  'use strict';
  // Content service
  // ----------------

  //     core/content.public.js
  //     This contains publically accessable content

  function PageService () {
    var pages = {
      blank: {
        title: '',
        body: ''
      },
      terms: {
        title: 'Terms & Conditions',
        body: [
          '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <strong>Itaque ab his ordiamur.</strong> Illud enim rectum est quod katortwma dicebas contingitque sapienti soli, hoc autem inchoati cuiusdam officii est, non perfecti, quod cadere in non nullos insipientes potest.</p>',

          '<p>Si in ipso corpore multa voluptati praeponenda sunt, ut vires, valitudo, velocitas, pulchritudo, quid tandem in animis censes? Quid interest, nisi quod ego res notas notis verbis appello, illi nomina nova quaerunt, quibus idem dicant? <u>Murenam te accusante defenderem.</u> Si enim idem dicit, quod Hieronymus, qui censet summum bonum esse sine ulla molestia vivere, cur mavult dicere voluptatem quam vacuitatem doloris, ut ille facit, qui quid dicat intellegit?</p>',

          '<ol>',
          ' <li>Neque solum ea communia, verum etiam paria esse dixerunt.</li>',
          ' <li>Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.</li>',
          ' <li>Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat.</li>',
          ' <li>Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?</li>',
          ' <li>Sed quia studebat laudi et dignitati, multum in virtute processerat.</li>',
          ' <li>Indicant pueri, in quibus ut in speculis natura cernitur.</li>',
          ' <li>Dicam, inquam, et quidem discendi causa magis, quam quo te aut Epicurum reprehensum velim.</li>',
          '</ol>',

          '<h2>Quippe: habes enim a rhetoribus;</h2>',

          '<p><em>Negat enim summo bono afferre incrementum diem.</em> Etsi dedit talem mentem, quae omnem virtutem accipere posset, ingenuitque sine doctrina notitias parvas rerum maximarum et quasi instituit docere et induxit in ea, quae inerant, tamquam elementa virtutis. Quid enim interest, divitias, opes, valitudinem bona dicas anne praeposita, cum ille, qui ista bona dicit, nihilo plus iis tribuat quam tu, qui eadem illa praeposita nominas? Habes, inquam, Cato, formam eorum, de quibus loquor, philosophorum. Nihil enim desiderabile concupiscunt, plusque in ipsa iniuria detrimenti est quam in iis rebus emolumenti, quae pariuntur iniuria. <strong>Pugnant Stoici cum Peripateticis.</strong> <a href="#" target="_blank">Maximus dolor, inquit, brevis est.</a> Ita similis erit ei finis boni, atque antea fuerat, neque idem tamen; Minime id quidem, inquam, alienum, multumque ad ea, quae quaerimus, explicatio tua ista profecerit. Quibus natura iure responderit non esse verum aliunde finem beate vivendi, a se principia rei gerendae peti; In quibus hoc primum est in quo admirer, cur in gravissimis rebus non delectet eos sermo patrius, cum idem fabellas Latinas ad verbum e Graecis expressas non inviti legant. <em>Nihil sane.</em> </p>',

          '<ol>',
          ' <li>Neque solum ea communia, verum etiam paria esse dixerunt.</li>',
          ' <li>Idem etiam dolorem saepe perpetiuntur, ne, si id non faciant, incidant in maiorem.</li>',
          ' <li>Varietates autem iniurasque fortunae facile veteres philosophorum praeceptis instituta vita superabat.</li>',
          ' <li>Ut nemo dubitet, eorum omnia officia quo spectare, quid sequi, quid fugere debeant?</li>',
          ' <li>Sed quia studebat laudi et dignitati, multum in virtute processerat.</li>',
          ' <li>Indicant pueri, in quibus ut in speculis natura cernitur.</li>',
          ' <li>Dicam, inquam, et quidem discendi causa magis, quam quo te aut Epicurum reprehensum velim.</li>',
          '</ol>',

          '<p>Si enim, ut mihi quidem videtur, non explet bona naturae voluptas, iure praetermissa est; Aliis esse maiora, illud dubium, ad id, quod summum bonum dicitis, ecquaenam possit fieri accessio. <em>Quid de Pythagora?</em> Equidem etiam curiam nostram-Hostiliam dico, non hanc novam, quae minor mihi esse videtur, posteaquam est maior-solebam intuens Scipionem, Catonem, Laelium, nostrum vero in primis avum cogitare; <a href="#" target="_blank">Certe non potest.</a> <em>Tria genera bonorum;</em> Aut pertinacissimus fueris, si in eo perstiteris ad corpus ea, quae dixi, referri, aut deserueris totam Epicuri voluptatem, si negaveris. <strong>Nunc haec primum fortasse audientis servire debemus.</strong> <u>Si longus, levis dictata sunt.</u> Id autem eius modi est, ut additum ad virtutem auctoritatem videatur habiturum et expleturum cumulate vitam beatam, de quo omnis haec quaestio est. </p>',

          '<p>Ego autem: Ne tu, inquam, Cato, ista exposuisti, ut tam multa memoriter, ut tam obscura, dilucide, itaque aut omittamus contra omnino velle aliquid aut spatium sumamus ad cogitandum; Quin etiam ferae, inquit Pacuvius, quíbus abest, ad praécavendum intéllegendi astútia, iniecto terrore mortis horrescunt. <em>Quae sequuntur igitur?</em> Sed isti ipsi, qui voluptate et dolore omnia metiuntur, nonne clamant sapienti plus semper adesse quod velit quam quod nolit? Nisi mihi Phaedrum, inquam, tu mentitum aut Zenonem putas, quorum utrumque audivi, cum mihi nihil sane praeter sedulitatem probarent, omnes mihi Epicuri sententiae satis notae sunt. Nec enim, dum metuit, iustus est, et certe, si metuere destiterit, non erit; <u>Non est igitur voluptas bonum.</u> Honestum igitur id intellegimus, quod tale est, ut detracta omni utilitate sine ullis praemiis fructibusve per se ipsum possit iure laudari. Nam si dicent ab illis has res esse tractatas, ne ipsos quidem Graecos est cur tam multos legant, quam legendi sunt. Levatio igitur vitiorum magna fit in iis, qui habent ad virtutem progressionis aliquantum. </p>',

          '<p>Quoniam, inquiunt, omne peccatum inbecillitatis et inconstantiae est, haec autem vitia in omnibus stultis aeque magna sunt, necesse est paria esse peccata. <a href="#" target="_blank">Hunc vos beatum;</a> <em>Itaque fecimus.</em> <strong>Hic nihil fuit, quod quaereremus.</strong> Praeclare, inquit, facis, cum et eorum memoriam tenes, quorum uterque tibi testamento liberos suos commendavit, et puerum diligis. <a href="#" target="_blank">A mene tu?</a> <a href="#" target="_blank">Negare non possum.</a> <u>Sed residamus, inquit, si placet.</u> <a href="#" target="_blank">Nihilo magis.</a> E quo efficitur, non ut nos non intellegamus quae vis sit istius verbi, sed ut ille suo more loquatur, nostrum neglegat. Nam quod ait sensibus ipsis iudicari voluptatem bonum esse, dolorem malum, plus tribuit sensibus, quam nobis leges permittunt, cum privatarum litium iudices sumus. Ipse negat, ut ante dixi, luxuriosorum vitam reprehendendam, nisi plane fatui sint, id est nisi aut cupiant aut metuant. </p>',

          '<ul>',
          ' <li>Nam his libris eum malo quam reliquo ornatu villae delectari.</li>',
          ' <li>Ut proverbia non nulla veriora sint quam vestra dogmata.</li>',
          ' <li>Hic nihil fuit, quod quaereremus.</li>',
          '</ul>'
        ].join('\n')
      },
      privacy: {
        title: 'Privacy Policy',
        body: [
          '<p>Lorem ipsum dolor sit amet, eu sed mollis discere epicurei, labore labitur dolores an eam, ne eam atqui iracundia eloquentiam. Duo te duis reque sadipscing, tempor meliore omittantur ad pri. In inani dictas scaevola pri. Vel etiam nulla dissentiet at, ad ius diam nibh. Te errem postea has, cum utamur torquatos instructior eu, nec ut facer oratio feugait.</p>',
          '<p>Veritus pertinacia nec ex. Nec verear suavitate in, nostrum honestatis at est. Ubique accumsan comprehensam et eam, vel eros brute an. Veri nostrum vim in.</p>',
          '<p>Mea natum indoctum philosophia id. Laboramus adversarium contentiones mea an, eum id magna nostrum sadipscing. Te brute accusam mel. Eam cu principes prodesset, malorum mnesarchum ne eam.</p>',
          '<p>Vim cu quando facilis, ut justo clita eos. An mea prima delicatissimi. An sit feugait concludaturque. Nemore impetus et mea, vel ei cibo elitr eripuit. Affert docendi consetetur his id, habemus evertitur ne est. Mollis equidem at nam. Ad error munere placerat vim, affert oporteat corrumpit ius ut, nonumes mentitum atomorum vim te.</p>',
          '<p>Sint velit vim ut. At his fuisset praesent gloriatur. No sea solum postea noluisse. Ne alia fabulas nonumes ius, natum iisque ad quo, quo in tale delicata. Ut eos postea utamur deleniti, pro cu quem magna. Cu eum rebum alienum.</p>',
          '<p>Populo luptatum explicari mel ea, veri facilis te nec, ex facilis epicuri appellantur vis. Solum constituam vituperatoribus mei te. Exerci aliquip inimicus ut quo, doctus graecis definitiones in per. Summo impedit scaevola ne per, wisi prodesset usu id. Nam ea falli mediocritatem, sumo erat assum vim ad, fierent petentium expetendis eos ea.</p>',
          '<p>Eam oratio volumus euripidis te, mei ne homero honestatis, duo principes persequeris complectitur in. An pro amet nominavi imperdiet, eum ad homero inimicus interesset. Ceteros iudicabit assentior qui no, mei ad consul omnesque. Vim posse populo salutatus at, eam ponderum mandamus ut, aeterno sententiae adipiscing his ex.</p>',
          '<p>Mis causae at eos. Sit omnis repudiare mediocritatem at, cu pri tantas oportere democritum. In qui inermis postulant, an virtute ornatus moderatius sea. Postea nostro recusabo cu sea. Vim ex nonumy perpetua, pro et sint offendit sententiae, pro brute libris patrioque at. Oratio decore commune et pro, at graeci pericula usu, sea cu percipitur comprehensam. An facilisi argumentum duo, veniam aperiam reprehendunt has id, te paulo tation eloquentiam mel.</p>',
          '<p>Ei cum molestie maluisset. Alia copiosae necessitatibus in est, cibo nemore volumus quo at. Nec cu utinam voluptua neglegentur, has adhuc appetere intellegat ea. No sumo nulla civibus eum. An legere vulputate his, quas consul et cum. Cum in summo sapientem.</p>',
          '<p>An cibo vulputate deseruisse est, et inermis contentiones quo. Wisi eirmod ne eam, sit natum solum regione ad. Cu mandamus iudicabit mei, cu vis sale iisque ocurreret. Ad eam quidam menandri, ea utinam sensibus sed. Quo ea simul urbanitas. Eu sit lucilius electram.</p>'
        ].join('\n')
      }
    };

    this.get = function( key ) {
      var page = pages[ key ] ? pages[ key ] : pages.blank;
      return page;
    };
  }

  return appCore
    .service('Pages', PageService);
});
