define([
  'store',
  'angular',
  'core/events/events',
  'core/platform/platform'
], function ( store, ng ) {
  'use strict';
  // Storage module
  // ----------------

  //     storage.js

  function StorageService () {
    var
      storage = this,
      InternalStore = function ( provider ) {
        this._data = {};
        this._provider = provider;
      };

    InternalStore.prototype.has = function ( key ) {
      if ( this._provider && typeof this._provider.get === 'function' ) {
        return !!this._provider.get( key );
      }

      return !!this._data[ key ];
    };
    InternalStore.prototype.get = function ( key, item ) {
      var
        data = this._provider ?
          this._provider.get( key ) :
          key ? this._data[ key ] : this._data;

      // get a specific item
      if ( item && data[item] ) {
        data = data[item];
      }

      return data;
    };
    InternalStore.prototype.set = function ( key, val ) {
      this._data[ key ] = val;

      if ( this._provider && typeof this._provider.set === 'function' ) {
        return this._provider.set( key, val );
      }
      return this._data[ key ];
    };
    InternalStore.prototype.remove = function ( key ) {
      delete this._data[ key ];
      // this._data.splice( this._data.indexOf( key ), 1);

      if ( this._provider && typeof this._provider.remove === 'function' ) {
        this._provider.remove( key );
      }

      return this;
    };

    var
      opts = {
        local: new InternalStore( store ),
        memory: new InternalStore(),
        server: {
          resources: {}
        }
      },

      Sync, createResource,

      getOpt = function ( action, resourceKey ) {
        var
          actionInfo = action.split( ':', 2 ),
          toSync = !!actionInfo[1] && actionInfo[1] === 'sync';

        action = actionInfo[0];

        // console.log( 'Storage:', action, '->', resourceKey, ', sync with server? ', toSync );

        if ( Sync && toSync && !!opts.server[resourceKey] ) {
          return new Sync( resourceKey );
        }
        return opts.local._provider && opts.local._provider.enabled ? opts.local : opts.memory;
      },

      storageFacade = function ( action, key, val ) {
        var
          storeOpt = getOpt( action, key ),
          service = {
            add: function ( key, val ) {
              storage.addResource( key, val );
              createResource( val, key );
              return this;
            },
            reloadResource: function ( key ) {
              createResource( storage.getResource( key ), key );
            },
            factory: function ( key, schema ) {
              var
                factory = this,
                alreadyExists = this.has( key ),
                data = alreadyExists ? this.get( key ) : this.set( key, schema );

              if ( alreadyExists ) {
                // update storage schema
                ng.forEach(schema, function ( v, k ) {
                  if ( !ng.isDefined( data[k] ) ) {
                    data[k] = v;
                  }
                });
                this.set( key, data );
              }

              function Storage(){
                this.set = function ( k, v ) {
                  this.parent.set.call( this, k, v );
                  return factory.set( key, this._data );
                };
                this.remove = function ( k ) {
                  this.parent.remove.call( this, k );
                  factory.remove( key );
                  return this;
                };
              }

              Storage.prototype = new InternalStore();
              Storage.prototype.constructor = Storage;
              Storage.prototype.parent = InternalStore.prototype;
              Storage.prototype._data = data;

              return new Storage();
            },
            has: function ( key ) {
              return !!this.get( key );
            },
            get: function ( key, val ) {
              return storeOpt.get( key, val );
            },
            set: function ( key, val ) {
              return storeOpt.set( key, val );
            },
            remove: function ( key ) {
              return storeOpt.remove( key );
            }
          };
        ng.extend( service, storeOpt );

        action = action.split(':', 1).toString();

        return typeof service[ action ] === 'function' ?
          service[ action ]( key, val ) : ng.noop();
      };

    this.prep = storageFacade.bind(null);

    this.addResource = function ( key, resource ) {
      if ( !key ) {
        return this;
      }
      opts.server.resources[ key ] = resource;
      return this;
    };

    this.getResource = function ( key ) {
      var resource = opts.server.resources[ key ];
      return resource || ng.noop;
    };

    this.$get = ['$q', '$resource', 'Platform', 'Events', function ( $q, $resource, corePlatform, coreEvents ) {
      createResource = function ( loadResource, key ) {
        var resource = loadResource();
        if ( resource && ( resource.url || resource.actions ) ) {
          opts.server[ key ] = $resource(resource.url, null, resource.actions);
        }
      };

      Sync = (function () {
        var
          syncQueue = storageFacade('factory', 'syncQueue', {
            tasks: {}
          }),
          syncTasks = syncQueue.get( 'tasks' ),
          syncListeners = {},

          whenOnline = function ( perform ) {
            var
              deferredTask = $q.defer(),

              addToQueue = function ( task ) {
                task.created = new Date().toISOString();

                coreEvents.publish('syncQueue:add(' + task.action + ')', task);

                syncTasks[ task.action ] = task;
                syncQueue.set( 'tasks', syncTasks );
              },

              runTask = function ( task ) {
                var
                  resource = storage.getResource( task.resource )(),
                  server = opts.server[ task.resource ],
                  actionKey = task.action.split(':', 1).toString(),
                  resourceAction = resource.actions[ actionKey ],
                  taskParams = task.params || null, reqParams = [], urlParams = {},
                  pass, fail, reqLen;

                if ( typeof server[ actionKey ] !== 'function' ) {
                  deferredTask.reject('Resource:', actionKey, 'for', server, 'not found.');
                  return;
                }

                pass = function (res) {
                  coreEvents.publish('syncQueue:pass(' + task.action + ')', { details: task, results: res });
                  deferredTask.resolve( res );
                };

                fail = function (res) {
                  coreEvents.publish('syncQueue:fail(' + task.action + ')', task);
                  deferredTask.reject( res );
                };

                reqParams = resource ? ( resourceAction || resource ).reqParams : [];

                // prep params
                for (reqLen = reqParams.length - 1; reqLen >= 0; reqLen--) {
                  if (!taskParams) {
                    break;
                  }
                  urlParams[ reqParams[reqLen] ] = taskParams[ reqParams[reqLen] ];
                }

                deferredTask.notify('running');

                // Task running, remove from syncQueue
                delete syncTasks[ task.action ];
                // syncTasks.splice( syncTasks.indexOf( task.action ), 1);
                syncQueue.set( 'tasks', syncTasks );

                if ( ['POST', 'PUT'].indexOf( ( resourceAction || resource ).method ) >= 0 ) {
                  server[ actionKey ]( urlParams, taskParams, pass, fail);
                  return;
                }
                server[ actionKey ]( urlParams, pass, fail);
              };

            setTimeout(function (){
              if ( !perform ) {
                deferredTask.reject('No task to perform');
              } else {
                // save for sync then
                addToQueue( perform );

                if ( corePlatform.isOnline() ) {
                  runTask( perform );
                } else {
                  deferredTask.notify('offline');

                  // wait till online then send
                  syncListeners[ perform.action ] = corePlatform.onOnline(function () {
                    runTask( perform );
                    // Remove onOnline event listener
                    if ( typeof syncListeners[ perform.action ] === 'function' ) {
                      syncListeners[ perform.action ]();
                      delete syncListeners[ perform.action ];
                      // syncListeners.splice( syncListeners.indexOf( perform.action ), 1);
                    }
                  });
                }
              }
            }, 10);

            return deferredTask.promise;
          };

        // run existing sync tasks when user logged in
        coreEvents.subscribe('app.loggedin', function () {
          ng.forEach( syncTasks, whenOnline );
        });
        coreEvents.subscribe('app.logout', function () {
          storageFacade('remove', 'syncQueue');
        });

        return function ( resourceKey ) {
          var
            syncFacade = this,
            basicActions = [ 'set', 'get', 'remove' ], 
            resourceActions = Object.keys( storage.getResource( resourceKey )().actions ), 
            i = 0;

          function addActionMethods ( action ) {
            syncFacade[ action ] = function ( key, params ) {
              var performTask = {
                action: action + ':' + key,
                params: params,
                resource: resourceKey
              };

              if ( action === 'remove' ) {
                delete performTask.params;
              }

              return whenOnline( performTask );
            };
          }
          for (; i < basicActions.length; i++) {
            addActionMethods( basicActions[i] );
          }
          for (i = 0; i < resourceActions.length; i++) {
            addActionMethods( resourceActions[i] );
          }
        };
      })();

      // add resource object to defined resources
      ng.forEach( opts.server.resources, createResource );

      return storageFacade;
    }];
  }

  return ng.module('core.storage', ['ngResource', 'core.events', 'core.platform'])
    .provider('Storage', StorageService);
});