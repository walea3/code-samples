// Karma configuration
// http://karma-runner.github.io/0.10/config/configuration-file.html
'use strict';
module.exports = function(config) {
  config.set({
  // base path, that will be used to resolve files and exclude
  basePath: '../',

  // testing framework to use (jasmine/mocha/qunit/...)
  frameworks: ['jasmine','ng-scenario'],

  // list of files / patterns to load in the browser
  files: [
    'app/assets/bower_components/angular/angular.js',
      'app/assets/bower_components/angular-route/angular-route.js',
      'app/assets/bower_components/angular-touch/angular-touch.js',
      'app/assets/bower_components/angular-animate/angular-animate.js',
      'app/assets/bower_components/angular-sanitize/angular-sanitize.js',
      'app/assets/bower_components/angular-resource/angular-resource.js',
      'app/assets/bower_components/angular-ui-router/release/angular-ui-router.js',

      'app/assets/bower_components/angular-bindonce/bindonce.js',
      'app/assets/bower_components/angular-mocks/angular-mocks.js',
      'app/assets/bower_components/store.js/store.js',

    {pattern: 'app/**/*.js', included: true},
    {pattern: 'test/**/*.spec.js', included: true}
  ],

  // list of files / patterns to exclude
  exclude: [],

  // web server port
  port: 9990,

  // level of logging
  // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
  logLevel: config.LOG_INFO,


  // enable / disable watching file and executing tests whenever any file changes
  autoWatch: true,


  // Start these browsers, currently available:
  // - Chrome
  // - ChromeCanary
  // - Firefox
  // - Opera
  // - Safari (only Mac)
  // - PhantomJS
  // - IE (only Windows)
  browsers: ['PhantomJS'],


  // Continuous Integration mode
  // if true, it capture browsers, run tests and exit
  singleRun: false

  // Uncomment the following lines if you are using grunt's server to run the tests
  // proxies: {
  //   '/': 'http://localhost:9000/'
  // },
  // URL root prevent conflicts with the site root
  // urlRoot: '_karma_'
  });
};
