'use strict';

describe( 'App shell controllers:', function() {
  beforeEach(function() {
    module('vpr.core');
    // module('vpr.mock');
  });

  describe('Main Controller', function() {
    var scope, ctrl, pvEvents, pvMenu;

    beforeEach(function() {
      inject(function($rootScope, $controller, _Events_, _mkMenu_) {
        scope = $rootScope.$new();

        pvEvents = _Events_;
        spyOn(pvEvents, 'publish');
        pvMenu = _mkMenu_;
        spyOn(pvMenu, 'init');
        ctrl = $controller('MainCtrl', {
          '$scope': scope,
          'Events' : pvEvents,
          'Menu' : pvMenu
        });
      });
    });

    it('should be defined', function() {
      expect(ctrl).toBeDefined();
    });

    it('should publish a login event', function() {
      expect(pvEvents.publish).toHaveBeenCalledWith('app.login');
    });

    it('should initialize the Menu service', function() {
      expect(pvMenu.init).toHaveBeenCalledWith( scope );
    });

    it('should set the navigateTo method from Menu service', function() {
      expect(scope.navigateTo).toBe(pvMenu.navigateTo);
    });

    it('should set the menu items from Menu service', function() {
      expect(scope.menuItems).toBe(pvMenu.menuItems);
    });

    it('should set the menu toggle button for the actionbar', inject(function($templateCache) {
      var leftButtons = [{
        type: 'nav',
        content: $templateCache.get('shell/partials/brand-nav-button.html'),
        tap: pvMenu.toggle
      }];

      expect(scope.leftButtons).toEqual(leftButtons);
    }));

    it('should set the loading icon for the actionbar', function() {
      var rightButtons = [{
        type: 'loading-icon'
      }];

      expect(scope.rightButtons).toEqual(rightButtons);
    });

    it('should have a logout method to publish logout event', function() {
      scope.logout();
      expect(pvEvents.publish).toHaveBeenCalledWith('app.logout');
    });
  });

  describe('Content Controller', function() {
    var scope, ctrl, state, pvContent, view = {
      'back': 'b',
      'forward': 'f'
    };

    beforeEach(function() {
      inject(function($rootScope, $controller, $state, _ContentPublic_) {
        scope = $rootScope.$new();
        state = $state;

        $rootScope.$viewHistory.backView = view.back;
        $rootScope.$viewHistory.forwardView = view.forward;

        pvContent = _ContentPublic_;
        spyOn(pvContent, 'get');

        ctrl = $controller('ContentCtrl', {
          '$scope': scope,
          '$rootScope': $rootScope,
          '$state': state,
          'Content' : pvContent
        });

        // controller local vars
        ctrl.previousView = $rootScope.$viewHistory.backView || $rootScope.$viewHistory.forwardView,
        ctrl.hasUser = !!$rootScope.auth.isLogged,
        ctrl.hasPrevious = !!ctrl.previousView,
        ctrl.leftButton = function() {
          var
            goBack = 'icon ion-arrow-left-c',
            goHome = 'icon ion-home';
          return {
            type : ctrl.hasPrevious ? goBack : goHome,
            action : ctrl.hasPrevious ? ctrl.previousView.stateId : ( ctrl.hasUser ? 'app.fit.send' : 'app.landing' )
          };
        }
      });
    });

    it('should be defined', function() {
      expect(ctrl).toBeDefined();
    });

    it('should assign the content for the current page', function() {
      expect(pvContent.get).toHaveBeenCalledWith( state.current.name );
      expect(scope.content).toEqual( pvContent.get( state.current.name ) );
    });

    it('should set the back/home button for the actionbar', function() {
      expect(scope.leftButtons[0].type).toEqual(ctrl.leftButton().type);
      expect(angular.isFunction(scope.leftButtons[0].tap)).toBe(true);
    });

    it('should set the loading icon for the actionbar', function() {
      var rightButtons = [{
        type: 'loading-icon'
      }];

      expect(scope.rightButtons).toEqual(rightButtons);
    });
  });
});