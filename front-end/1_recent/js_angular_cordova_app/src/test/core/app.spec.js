'use strict';

describe( 'App core', function() {
  var $rootScope, pvPlatform;

  beforeEach(function() {
    module('passiv.core');
    inject(function( _Platform_, _$rootScope_ ) {
      pvPlatform = _Platform_;
      spyOn(pvPlatform, 'init');
      $rootScope = _$rootScope_.$new();
    });
  });

  it("should initialise the Platform service", function() {
    // TODO: actual inin in .run() being called before this
    pvPlatform.init();
    expect(pvPlatform.init).toHaveBeenCalled();
  });

  it("should assign the auth service", inject(function( Auth ) {
    expect($rootScope.auth).toBe( Auth );
  }));

  it("should assign a default login object", function() {
    var defaultLogin = {
      autoLogin: true,
      working: false,
      wrong: false
    };
    expect($rootScope.login).toEqual( defaultLogin );
  });
});