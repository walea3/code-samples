'use strict';

describe('Platform module:', function () {
  var $window, mockEvent, pvEvents, deviceReady, mockDeviceReady;

  // load the service's module
  beforeEach(function () {
    module('vpr.platform');
    module('vpr.mock');

    inject(function( _$window_, _Events_, _deviceReady_, _mockEvent_ ) {
      $window = _$window_;
      $window.Keyboard = {
        shrinkView : angular.noop,
        hideFormAccessoryBar : angular.noop,
        disableScrollingInShrinkView : angular.noop
      };
      mockEvent = _mockEvent_;
      mockDeviceReady = mockEvent( 'deviceready');

      pvEvents = _Events_;
      spyOn(pvEvents.listener, 'addTo').andCallThrough();
      // .andCallFake(function(el, event, handler, capture) {
      //   return handler;
      // });

      deviceReady = _deviceReady_;
      spyOn(deviceReady, 'toPerform').andCallThrough();
      // .andCallFake(function( action ) {
      //   pvEvents.listener.addTo(document, 'deviceready', action, false);
      //   return action;
      // });
    });
  });

  describe('Platform factory', function () {
    var $timeout, pvPlatform;

    beforeEach(inject(function ( _$timeout_, _Platform_ ) {
      $timeout = _$timeout_;
      pvPlatform = _Platform_;
      spyOn(pvPlatform, 'init').andCallThrough();
      pvPlatform.init();
    }));

    it("init() should assign global handle open url method when device ready", function() {
      expect(pvPlatform.init).toHaveBeenCalled();

      expect($window.handleOpenURL).not.toBeDefined();
      mockDeviceReady();
      expect($window.handleOpenURL).toBeDefined();

      spyOn($window, 'handleOpenURL').andCallThrough();
      spyOn($window, 'alert');
      var url = 'http://google.com';
      $window.handleOpenURL( url );
      $timeout.flush(0);
      expect($window.alert).toHaveBeenCalledWith( 'received url: ' + url.split('://')[1] );
    });

    it("init() should hide splashscreen 2 seconds after device ready", function() {
      $window.navigator.splashscreen = {
        hide: angular.noop
      };

      expect(pvPlatform.init).toHaveBeenCalled();

      spyOn($window.navigator.splashscreen, 'hide');

      expect($window.navigator.splashscreen.hide).not.toHaveBeenCalled();

      mockDeviceReady();
      $timeout.flush(1000);
      expect($window.navigator.splashscreen.hide).not.toHaveBeenCalled();
      $timeout.flush(1000);
      expect($window.navigator.splashscreen.hide).toHaveBeenCalled();
    });

    it("getDevice() should return the device object when device ready", function() {
      $window.device = {
        cordova: '3.4.0',
        model: 'Hammerhead', /* Google Nexus 5 */
        platform: 'Android',
        uuid: '550e8400-e29b-41d4-a716-446655440000',
        version: '4.4.1'
      };
      expect( pvPlatform.getDevice() ).not.toBe( $window.device );

      mockDeviceReady();
      expect( pvPlatform.getDevice() ).toBe( $window.device );
    });

    it("isOnline() should return true only when device ready and is online", function() {
      $window.Connection = {
        NONE: 'none'
      };
      $window.navigator.connection = {
        type: null
      };
      expect( pvPlatform.isOnline() ).not.toBe(true);

      mockDeviceReady();
      // is online = true
      $window.navigator.connection.type = 'online';
      expect( pvPlatform.isOnline() ).toBe(true);
      // is offline = false
      $window.navigator.connection.type = 'none';
      expect( pvPlatform.isOnline() ).not.toBe(true);
    });

    it("onOnline() should execute callback only when online event is dispatched", function() {
      var
        mockDeviceOnline = mockEvent('online'),
        nowOnline = false,
        callback = function () {
          nowOnline = true;
        };

      pvPlatform.onOnline(callback);
      expect(pvEvents.listener.addTo).toHaveBeenCalledWith( document, 'online', callback, false );
      expect(nowOnline).toBe(false);

      mockDeviceOnline();
      expect(nowOnline).toBe(true);
    });

    it("onMenuButton() should execute callback only when menubutton event is dispatched", function() {
      var
        mockMenuButton = mockEvent('menubutton'),
        menuButtonClicked = 0,
        callback = function () {
          menuButtonClicked++;
        };

      pvPlatform.onMenuButton(callback);
      expect(pvEvents.listener.addTo).toHaveBeenCalledWith( document, 'menubutton', callback, false );
      expect(menuButtonClicked).toBe(0);

      mockMenuButton();
      expect(menuButtonClicked).toBe(1);
      mockMenuButton();
      expect(menuButtonClicked).toBe(2);
    });

    it("onSearchButton() should execute callback only when searchbutton event is dispatched", function() {
      var
        mockSearchButton = mockEvent('searchbutton'),
        searchButtonClicked = 0,
        callback = function () {
          searchButtonClicked++;
        };

      pvPlatform.onSearchButton(callback);
      expect(pvEvents.listener.addTo).toHaveBeenCalledWith( document, 'searchbutton', callback, false );
      expect(searchButtonClicked).toBe(0);

      mockSearchButton();
      expect(searchButtonClicked).toBe(1);
      mockSearchButton();
      expect(searchButtonClicked).toBe(2);
    });

    it("vibrate() should be vibrate for time assigned", function() {
      var vibrationTime = 3000;
      $window.navigator.notification = {
        vibrate : angular.noop
      };
      spyOn($window.navigator.notification, 'vibrate');
      pvPlatform.vibrate( vibrationTime );
      expect( $window.navigator.notification.vibrate ).toHaveBeenCalledWith( vibrationTime );
    });
  });

  describe('Device ready factory', function () {
    var deviceReadyCallback;

    beforeEach(function () {
      deviceReadyCallback = deviceReady.toPerform(angular.noop);
    });

    it('should add an event listener for deviceready', function () {
      expect(deviceReady.toPerform).toHaveBeenCalledWith( angular.noop );
      expect(pvEvents.listener.addTo).toHaveBeenCalledWith( document, 'deviceready', angular.noop, false );
    });

    it('should return a method to remove the listener', function () {
      expect( angular.isFunction( deviceReadyCallback ) ).toBe(true);
    });
  });

  describe('App container directive', function () {
    var el, $scope;

    beforeEach(inject(function (_$compile_, _$rootScope_) {
      el = angular.element( '<div class="app-container"></div>' );
      $scope = _$rootScope_.$new();
      _$compile_( el )( $scope );
      $scope.$digest();
    }));

    it('should add an event listener for deviceready', function() {
      expect(deviceReady.toPerform).toHaveBeenCalled();
    });

    it('should update the background size of the element when device is ready', function() {
      mockDeviceReady();
      expect( parseInt(el[0].style.backgroundSize, 10) ).toEqual( parseInt($window.innerHeight ,10) );
    });
  });

  describe('External URL directive', function () {
    var
      $scope, el,
      url = 'http://www.walea3.com',
      html = '<a external-url="' + url + '">Go to WaleA3</a>';

    beforeEach(inject(function (_$compile_, _$rootScope_) {
      $scope = _$rootScope_.$new();

      el = angular.element(html);
      // spyOn( el, 'on' ).andCallFake(function(){ console.log( arguments ); });
      // spyOn( el, 'off' );
      _$compile_( el )( $scope );

      // $scope = el.scope();
      $scope.$digest();
    }));

    // it('should register a on tap event listener', function() {
    //   expect(el.on).toHaveBeenCalledWith( 'tap', $scope.launch );
    // });

    it('should launch a new window when element is tapped', function() {
      spyOn( $window, 'open' );
      el.triggerHandler('tap');
      expect($window.open).toHaveBeenCalledWith( encodeURI( url ), '_system' );
    });

    // it('should clean up the on tap listener when destroyed', function() {
    //   $scope.$destroy();
    //   expect(el.off).toHaveBeenCalledWith( 'tap', $scope.launch );
    // });
  });
});