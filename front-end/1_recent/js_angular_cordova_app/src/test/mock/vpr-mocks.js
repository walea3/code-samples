(function ( global, ng, vpr, undefined ) {
  'use strict';
  vpr.mock = {};

  vpr.mock.mockedEvent = function( eventName ) {
    return function () {
      var e = document.createEvent('Events');
      e.initEvent( eventName );
      document.dispatchEvent(e);
    };
  };

  vpr.mock.Menu = function () {
    this.$get = function () {
      return {
        init: function ( serviceScope ) {
          this.menuItems = ['home','about','contact'];
          return serviceScope;
        },
        navigateTo: function ( location ) {
          return 'go to ' + location;
        },
        toggle: ng.noop,
        close: ng.noop
      }
    };
  };

  vpr.mock.Events = function () {
    var i = 1;
    return {
      listener: {
        addTo: function () { return this.removeFrom.bind(null, i++); },
        removeFrom: ng.noop
      },
      publish: ng.noop,
      subscribe: ng.noop,
      unsubscribe: ng.noop
    };
  };

  vpr.mock.Storage = function () {
    var
      store = {},
      resources = {};

    this.addResource = function ( key, resource ) {
      if ( !key ) {
        return this;
      }
      resources[ key ] = resource;
      return this;
    };

    this.$get = function () {
      var serviceFacade = function ( action, key, val ) {
        function Storage () {
          this.has = function ( key ) {
            return !!store[ key ];
          },
          this.get = function ( key, item ) {
            var data = key ? store[ key ] : store;
            if ( item && data[item] ) {
              data = data[item];
            }
            return data;
          },
          this.set = function ( key, val ) {
            store[ key ] = val;
            return store[ key ];
          },
          this.remove = function ( key ) {
            delete store[ key ];
            return this;
          }
        }
        var service = {
          factory: function() {
            return new Storage();
          },
          has: Storage.has,
          get: Storage.get,
          set: Storage.set,
          remove: Storage.remove
        };

        action = action.split(':', 1).toString();
        return typeof service[ action ] === 'function' ?
          service[ action ]( key, val ) :
          ng.noop();
      };
      return serviceFacade;
    };
  };
  vpr.mock.Platform = function () {
    return {
      init: ng.noop,
      getDevice: ng.noop,
      isOnline: function () { return !!Math.floor(Math.random() * 2) },
      onOnline: ng.noop,
      onMenuButton: ng.noop,
      onSearchButton: ng.noop,
      vibrate: ng.noop
    };
  };

  vpr.mock.EmailComposerProvider = function () {
    this.$get = function () {
      return new vpr.mock.EmailComposer();
    };
  };

  vpr.mock.EmailComposer = function () {
    var self = this;

    self.composeEmail = function (onSuccess, subject, body, toRecipients, ccRecipients, bccRecipients, isHtml, attachments, attachmentsData) {
      self.successFunction = onSuccess;
      self.subject = subject;
      self.body = body;
      self.toRecipients = toRecipients;
      self.ccRecipients = ccRecipients;
      self.bccRecipients = bccRecipients;
      self.isHtml = isHtml;
      self.attachments = attachments;
      self.attachmentsData = attachmentsData;
    };

    self.reset = function () {
      self.successFunction = undefined;
      self.subject = undefined;
      self.body = undefined;
      self.toRecipients = undefined;
      self.ccRecipients = undefined;
      self.bccRecipients = undefined;
      self.isHtml = undefined;
      self.attachments = undefined;
      self.attachmentsData = undefined;
    };
  };
  ng.module('vpr.mock')
    .provider({
      mkMenu: vpr.mock.Menu,
      mkStorage: vpr.mock.Storage,
      emailcomposer: vpr.mock.EmailComposerProvider
    })
    .factory({
      mkEvents: vpr.mock.Events,
      mkPlatform: vpr.mock.Platform
    })
    .value({
      mockEvent: vpr.mock.mockedEvent
    });
})( window, angular, window.viperMoth || ( window.viperMoth = {} ) );