'use strict';
var LIVERELOAD_PORT = 35730;
var lrSnippet = require('connect-livereload')({port: LIVERELOAD_PORT});
var mountFolder = function (connect, dir) {
  return connect.static(require('path').resolve(dir));
};

module.exports = function (grunt) {
  require('time-grunt')(grunt);
  require('load-grunt-tasks')(grunt);

  var
    pkg = grunt.file.readJSON('package.json'),
    mainCfg = grunt.file.readJSON('src/config.json'),
    appCfg = mainCfg.appConfig,
    mobileCfg = mainCfg.mobileConfig;

  var device = {
    platform: grunt.option('platform') || 'all',
    family: grunt.option('family') || 'default',
    target: grunt.option('target') || 'emulator'
  };

  try {
    appCfg.app = require('bower.json').appPath || appCfg.app;
  } catch (e) {}

  process.chdir( process.cwd() + '/src');

  grunt.initConfig({
    pkg: pkg,
    mainCfg: mainCfg,
    app: appCfg,
    mobile: mobileCfg,
    banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '* <%= pkg.author.url %>/\n' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> ' +
      '<%= pkg.author.name %>; Licensed  */',
    // TODO: Make this conditional
    watchfiles: {
      all: [
        '<%= app.dev %>/index.html',
        '<%= app.dev %>/main.js',
        '<%= app.dev %>/<%= app.core =>/**/*.js',
        '<%= app.dev %>/<%= app.services =>/**/*.js',
        '<%= app.dev %>/<%= app.scripts =>/{,*/}*.js',
        '.tmp/<%= app.styles %>/{,*/}*.css',
        '<%= app.dev %>/<%= app.images %>/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
      ]
    },
    watch: {
      templates: {
        files: [
          '<%= app.dev %>/<%= app.core %>/**/*.html',
          '<%= app.dev %>/<%= app.services %>/**/*.html'
        ],
        tasks: ['copy:templates', 'html2js:server']
      },
      compass: {
        files: ['<%= app.dev %>/<%= app.styles %>/{,*/}*.{scss,sass}'],
        tasks: ['compass:server', 'autoprefixer']
      },
      styles: {
        files: ['<%= app.dev %>/<%= app.styles %>/{,*/}*.css'],
        tasks: ['copy:styles', 'autoprefixer']
      },
      karma: {
        files: ['<%= app.dev %>/<%= app.core %>/**/*.js', '<%= app.dev %>/<%= app.services %>/**/*.js', '<%= app.test %>/**/*.spec.js'],
        tasks: ['karma:unit:run']
      },
      livereload: {
        options: {
          livereload: LIVERELOAD_PORT
        },
        files: ['<%= watchfiles.all %>']
      }
    },
    autoprefixer: {
      options: ['last 1 version'],
      prod: {
        files: [{
          expand: true,
          cwd: '.tmp/<%= app.styles %>/',
          src: '{,*/}*.css',
          dest: '.tmp/<%= app.styles %>/'
        }]
      }
    },
    autoshot: {
      prod: {
        options: {
          path: '/screenshots/',
          remote : {
            files: [
              { src: 'http://localhost:<%= connect.options.port %>', dest: 'app.jpg'}
            ]
          },
          viewport: ['320x480','480x320','384x640','640x384','602x963','963x602','600x960','960x600','800x1280','1280x800','768x1024','1024x768']
        }
      }
    },
    responsive_images: {
      dev: {
        options: {
          sizes: [
            { width: 320 },
            { width: 640 },
            { width: 1024 }
          ]
        },
        files: [{
          expand: true,
          cwd: '<%= app.dev %>/<%= app.images %>',
          src: '{,*/}*.{png,jpg,jpeg}',
          dest: '<%= app.build %>/<%= app.images %>'
        }]
      }
    },
    connect: {
      options: {
        port: 9990,
        // change this to '0.0.0.0' to access the server from outside
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              lrSnippet,
              mountFolder(connect, '.tmp'),
              mountFolder(connect, appCfg.dev)
            ];
          }
        }
      },
      test: {
        options: {
          middleware: function (connect) {
            return [
              mountFolder(connect, '.tmp'),
              mountFolder(connect, appCfg.test)
            ];
          }
        }
      },
      prod: {
        options: {
          middleware: function (connect) {
            return [
              mountFolder(connect, appCfg.build)
            ];
          }
        }
      }
    },
    open: {
      server: {
        path: 'http://localhost:<%= connect.options.port %>'
      }

    },
    clean: {
      prod: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '.sass-cache',
            '<%= app.build %>/*',
            '!<%= app.build %>/.git*'
          ]
        }]
      },
      server: '.tmp'
    },
    browser_sync: {
      dev: {
        bsFiles: {
          src : '<%= app.dev %>/<%= app.styles %>/main.css'
        },
        options: {
          watchTask: false,
          debugInfo: true,
          // Change to 0.0.0.0 to access externally
          host: 'http://localhost:<%= connect.options.port %>',
          server: {
            baseDir: '<%= app.dev %>'
          },
          ghostMode: {
            clicks: true,
            scroll: true,
            links: true,
            forms: true
          }
        }
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish'),
        ignores: [
          '<%= app.dev %>/{,*/}templates.js',
          '<%= app.dev %>/<%= app.vendors %>/**/*.js'
        ]
      },
      build: [
        '<%= app.dev %>/**/*.js'
      ],
      test: {
        options: {
          jshintrc: '<%= app.test %>/.jshintrc'
        },
        src: [
          '<%= app.test %>/spec/**/*.js'
        ]
      }
    },
    compass: {
      options: {
        sassDir: '<%= app.dev %>/<%= app.styles %>',
        cssDir: '.tmp/<%= app.styles %>',
        imagesDir: '<%= app.images %>',
        fontsDir: '<%= app.fonts %>',
        javascriptsDir: '<%= app.dev %>/<%= app.scripts %>',
        generatedImagesDir: '.tmp/<%= app.images %>/generated',
        importPath: '<%= app.dev %>/assets/bower_components',
        httpImagesPath: '../images',
        httpGeneratedImagesPath: '../images/generated',
        httpFontsPath: '../fonts',
        relativeAssets: false,
        raw: 'Sass::Script::Number.precision = 15\n' // Use `raw` since it's not directly available.
      },
      prod: {
        options: {
          outputStyle: 'compressed',
          environment: 'production',
          imagesPath: '<%= app.build %>/<%= app.images %>',
          fontsPath: '<%= app.build %>/<%= app.fonts %>',
          generatedImagesDir: '<%= app.build %>/<%= app.images %>/generated'
        }
      },
      server: {
        options: {
          imagesPath: '<%= app.dev %>/<%= app.images %>',
          fontsPath: '<%= app.dev %>/<%= app.fonts %>',
          outputStyle: 'compressed',
          debugInfo: true
        }
      }
    },
    cmq: {
      options: {
        log: false
      },
      dev: {
        files: {
          '<%= app.dev %>/<%= app.styles %>/' : ['.tmp/<%= app.styles %>/{,*/}*.css']
        }
      }
    },
    html2js: {
      options: {
        module: 'core.templates'
      },
      server: {
        options : {
          base: '.tmp/templates/'
        },
        src: ['.tmp/templates/**/*.html'],
        dest: '<%= app.dev %>/templates.js'
      },
      prod: {
        options : {
          base: '<%= app.build %>',
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          }
        },
        src: [
          '<%= app.build %>/<%= app.core %>/**/*.html',
          '<%= app.build %>/<%= app.services %>/**/*.html'
        ],
        dest: '<%= app.build %>/templates.js'
      }
    },
    // not used since Uglify task does concat,
    // but still available if needed
    /*concat: {
      prod: {}
    },*/
    requirejs: {
      main: {
        options: {
          name: 'main',
          mainConfigFile: '<%= app.dev %>/main.js',
          optimize: 'none',
          almond: true,
          removeCombined: true,

          /*singlefile*/
          // out: '<%= app.build %>/main.js',

          /*else*/
          dir: '<%= app.build %>',
          appDir: '<%= app.dev %>',
          baseUrl: './',
          module: [{ name: 'main' }],
          preserveLicenseComments: false,

          replaceRequireScript: [{
            files: ['<%= app.build %>/index.html'],
            name: 'main',
            modulePath: 'main'
          }]
        }
      }
    },
    rev: {
      prod: {
        files: {
          src: [
            '<%= app.build %>/<%= app.core %>/**/*.js',
            '<%= app.build %>/<%= app.services %>/**/*.js',
            '<%= app.build %>/<%= app.styles %>/{,*/}*.css',
            '<%= app.build %>/<%= app.images %>/{,*/}*.{png,jpg,jpeg,gif,webp}',
            '!<%= app.build %>/<%= app.images %>/f_*/*.{png,jpg,jpeg,gif,webp,svg}',
            '<%= app.build %>/<%= app.fonts %>/{,*/}*.{ttf,otf,woff,svg,eot}'
          ]
        }
      }
    },

    modernizr: {

      // Path to the build you're using for development.
      "devFile" : "<%= app.dev %>/assets/bower_components/modernizr/modernizr.js",

      // Path to save out the built file.
      "outputFile" : "<%= app.build %>/<%= app.scripts %>/modernizr.js",

      // Based on default settings on http://modernizr.com/download/
      "extra" : {
        "shiv" : true,
        "printshiv" : false,
        "load" : true,
        "mq" : false,
        "cssclasses" : true
      },

      // Based on default settings on http://modernizr.com/download/
      "extensibility" : {
        "addtest" : false,
        "prefixed" : false,
        "teststyles" : false,
        "testprops" : false,
        "testallprops" : false,
        "hasevents" : false,
        "prefixes" : false,
        "domprefixes" : false
      },

      // By default, source is uglified before saving
      "uglify" : true,

      // Define any tests you want to impliticly include.
      "tests" : [],

      // By default, this task will crawl your project for references to Modernizr tests.
      // Set to false to disable.
      "parseFiles" : true,

      // When parseFiles = true, this task will crawl all *.js, *.css, *.scss files, except files that are in node_modules/.
      // You can override this by defining a "files" array below.
      // "files" : [],

      // When parseFiles = true, matchCommunityTests = true will attempt to
      // match user-contributed tests.
      "matchCommunityTests" : false,

      // Have custom Modernizr tests? Add paths to their location here.
      "customTests" : []
    },

    useminPrepare: {
      options: {
        dest: '<%= app.build %>'
      },
      html: '<%= app.dev %>/index.html'
    },
    usemin: {
      html: [
        '<%= app.build %>/index.html',
        '<%= app.build %>/<%= app.core %>/**/*.html',
        '<%= app.build %>/<%= app.services %>/**/*.html'
      ],
      css: '<%= app.build %>/<%= app.styles %>/{,*/}*.css',
      options: {
        dirs: ['<%= app.build %>'],
        assetsDirs: ['<%= app.build %>', '<%= app.build %>/assets/',
          '<%= app.build %>/<%= app.styles %>',
          '<%= app.build %>/<%= app.images %>',
          '<%= app.build %>/<%= app.fonts %>'
        ]
      //   patterns: {
      //     js: [
      //       [/(image\.png)/, 'Replacing reference to image.png'],
      //     ]
      //   }
      }
      // js: '<%= app.build %>/<%= app.core %>/**/*.js'
    },
    imagemin: {
      prod: {
        files: [{
          expand: true,
          cwd: '<%= app.dev %>/<%= app.images %>',
          src: '{,*/}*.{png,jpg,jpeg}',
          dest: '<%= app.build %>/<%= app.images %>'
        }]
      }
    },
    svgmin: {
      prod: {
        files: [{
          expand: true,
          cwd: '<%= app.dev %>/<%= app.images %>',
          src: '{,*/}*.svg',
          dest: '<%= app.build %>/<%= app.images %>'
        }]
      }
    },
    cssmin: {
      prod: {
        files: [{
          expand: true,
          cwd: '.tmp/<%= app.styles %>',
          src: ['{,*/}*.css'],
          dest: '<%= app.build %>/<%= app.styles %>'
        }]
      }
    },
    htmlmin: {
      prod: {
        options: {
          /*removeCommentsFromCDATA: true,
          // https://github.com/yeoman/grunt-usemin/issues/44
          //collapseWhitespace: true,
          collapseBooleanAttributes: true,
          removeAttributeQuotes: true,
          removeRedundantAttributes: true,
          useShortDoctype: true,
          removeEmptyAttributes: true,
          removeOptionalTags: true*/
        },
        files: [{
          expand: true,
          cwd: '<%= app.dev %>',
          src: [
            '*.html',
            '<%= app.core %>/**/*.html',
            '<%= app.services %>/**/*.html'
          ],
          dest: '<%= app.build %>'
        }]
      }
    },
    ngAnnotate: {
      prod: {
        files: [{
          expand: true,
          cwd: '<%= app.build %>',
          src: [
            'main.js'
          ],
          dest: '<%= app.build %>'
        }]
      }
    },
    replace: {
      version: {
        options: {
          patterns: [
            // For config.xml
            {
              match: /version="(\d*)\.(\d+)(\.\d+)?(-(\w+))?"/g,
              replacement: 'version="<%= pkg.version %>"'
            },
            // for app.config.js
            {
              match: /'version':\s'(\d*)\.(\d+)(\.\d+)?(-(\w+))?'/g,
              replacement: '\'version\': \'<%= pkg.version %>\''
            },
            {
              match: /'env':\s'(\w+)'/,
              replacement: '\'env\': \'qa\''
            },
            // For main.scss
            {
              match: /v(\d*)\.(\d+)(\.\d+)?(-(\w+))?/g,
              replacement: 'v<%= pkg.version %>'
            }
          ]
        },
        files: [{
          expand: true,
          cwd: '<%= app.prj %>',
          src: [
            'config.xml',
            '<%= app.dev %>/<%= app.core %>/app.config.js',
            '<%= app.dev %>/<%= app.styles %>/main.scss'
          ],
          dest: '<%= app.prj %>'
        }]
      },
      release: {
        options: {
          patterns: [{
            match: /android-versionCode"\svalue="(\d+)"/g,
            replacement: function (full, val) { return 'android-versionCode" value="' + (+val+1) + '"'; }
          }, {
            match: /'env':\s'(\w+)'/,
            replacement: '\'env\': \'production\''
          }]
        },
        files: [{
          expand: true,
          src: [
            'config.xml',
            '<%= app.dev %>/<%= app.core %>/app.config.js'
          ]
        }]
      }
    },
    // Put files not handled in other tasks here
    copy: {
      prod: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= app.dev %>',
          dest: '<%= app.build %>',
          src: [
            '*.{ico,png,txt,xml}',
            '<%= app.images %>/{,*/}*.{gif,webp}',
            '<%= app.fonts %>/{,*/}*.{ttf,otf,woff,svg,eot}'
          ]
        }, {
          expand: true,
          cwd: '.tmp/<%= app.images %>',
          dest: '<%= app.build %>/<%= app.images %>',
          src: [
            'generated/*'
          ]
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= app.dev %>/<%= app.styles %>',
        dest: '.tmp/<%= app.styles %>/',
        src: '{,*/}*.css'
      },
      templates: {
        files: [{
          expand: true,
          cwd: '<%= app.dev %>',
          src: [
            '<%= app.core %>/**/*.html',
            '<%= app.services %>/**/*.html'
          ],
          dest: '.tmp/templates/'
        }]
      },
      mobile: {
        expand: true,
        flatten: true,
        filer: 'isFile',
        cwd: '',
        dest: '<%= app.dist %>',
        src: [
          '<%= app.ios %>/<%= mobile.ios.devAsset %>-debug.ipa',
          '<%= app.android %>/<%= mobile.android.devAsset %>',
          '<%= app.ios %>/<%= mobile.ios.distAsset %>-release.ipa',
          '<%= app.android %>/<%= mobile.android.distAsset %>'
        ]
      }
    },
    karma: {
      unit: {
        configFile: '<%= app.test %>/karma.conf.js'
      }/*,
      unitAuto: {
        configFile: '<%= app.test %>/karma.conf.js',
        autoWatch: true,
        singleRun: false
      },
      unitCoverage: {
        configFile: '<%= app.test %>/karma.conf.js',
        autoWatch: false,
        singleRun: true,
        reporters: ['progress', 'coverage'],
        preprocessors: {
          'app/scripts/*.js': ['coverage']
        },
        coverageReporter: {
          type : 'html',
          dir : 'coverage/'
        }
      },*/
    },

    bower: {
      options: {
        exclude: ['modernizr']
      },
      all: {
        rjsConfig: '<%= app.dev %>/main.js'
      }
    },
    testflight: {
      options: {
        apiToken: '<%= mobile.ios.testApiToken %>',
        teamToken: '<%= mobile.ios.testTeamToken %>',
        buildributionLists: ['Alpha testers'],
        notify: true
      },
      ios: {
        options: {
          file: '<%= app.ios %>/<%= mobile.ios.devAsset %>-debug.ipa',
          notes: 'iOS debug build: <%= pkg.name %> v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>'
        }
      }
    },

    shell: {
      options: {
        // failOnError: true,
        stdout: false,
        stderr: true,
        execOptions: {
          maxBuffer: 500*1024
        }
      },

      // iossimstart: {
      //   command: 'ios-sim launch <%= app.ios %>/<%= mobile.ios.devAsset %>.app --exit' + (device.family !== 'default' ? ' --family ' + device.family : ''),
      //   options: {
      //     stdout: true
      //   }
      // },
      // iossimend: {
      //   command: 'killall -9 "iPhone Simulator"'
      // },
      // serveend: {
      //   command: 'killall -9 "cordova serve"'
      // },
      // rippleend: {
      //   command: 'killall -9 "cordova ripple"'
      // },

      build: {
        command: 'cordova build --debug --device'
      },
      buildProd: {
        command: 'cordova build --release --device'
      },
      device: {
        command: 'cordova run --device'
      },
      emulate: {
        command: 'cordova run --emulator'
      },
      iosDev: {
        command: [
          // './scripts/xcodeproj.rb ' + __dirname + '/<%= app.ios %>/<%= mainCfg.title %>',
          // 'xcodebuild -xcconfig "$CORDOVA_PATH/build.xcconfig" -project ' + __dirname + '/<%= app.ios %>/<%= mainCfg.title %>.xcodeproj -configuration Debug -sdk iphoneos -arch armv7 -arch armv7s -arch arm64 -arch i386 -alltargets clean build VALID_ARCHS="armv7 armv7s arm64 i386"',
          '/usr/bin/xcrun -sdk iphoneos PackageApplication -v "' + __dirname + '/<%= app.ios %>/<%= mobile.ios.devAsset %>.app" -o "' + __dirname + '/<%= app.ios %>/<%= mobile.ios.devAsset %>-debug.ipa" --sign "<%= mobile.ios.devSignID %>" --embed "' + __dirname + '/<%= mobile.ios.devProfile %>"'
        ].join('&&')
      },
      iosProp: {
        command: [
          '/usr/bin/xcrun -sdk iphoneos PackageApplication -v "' + __dirname + '/<%= app.ios %>/<%= mobile.ios.distAsset %>.app" -o "' + __dirname + '/<%= app.ios %>/<%= mobile.ios.distAsset %>-release.ipa" --sign "<%= mobile.ios.distSignID %>" --embed "' + __dirname + '/<%= mobile.ios.distProfile %>"'
        ].join('&&')
      },
      androidKeystore: {
        command: [
          'echo "' +
          'key.store=../../<%= mobileCfg.android.keystore %>\n' +
          'key.alias=<%= mobileCfg.android.keyalias %>\n' +
          'key.alias.password=<%= mobileCfg.android.aliaspass %>\n' +
          'key.store.password=<%= mobileCfg.android.storepass %>'+
          '" > <%= appCfg.android %>/ant.properties'
        ].join('&&')
      },
      testfairy: {
        command: [
          'curl https://app.testfairy.com/api/upload ',
          '-F api_key=\'<%= mobile.android.testApiKey %>\'',
          '-F apk_file=@<%= app.android %>/<%= mobile.android.devAsset %>',
          //'-F proguard_file=@sample_mapping.txt'
          '-F metrics=\'cpu,network,memory\'',
          '-F testers_groups=\'alpha\''
        ].join(' ')
      }
    },
    concurrent: {
      server: [
        'compass:server',
        'copy:styles'
      ],
      test: [
        'compass'
      ],
      prod: [
        'compass:prod',
        'copy:styles',
        'imagemin',
        'svgmin',
        'htmlmin'
      ]
    }
  });

  grunt.registerTask('server', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  grunt.registerTask('serve', function (target) {
    if (target === 'prod') {
      return grunt.task.run(['build', 'open', 'connect:prod:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'concurrent:server',
      'copy:templates',
      'autoprefixer',
      // 'template',
      'html2js:server',
      'connect:livereload',
      'open:server',
      'watch'
    ]);
  });

  grunt.registerTask('test', [
    'replace:version',
    'clean:server',
    'concurrent:test',
    'autoprefixer',
    'html2js:server',
    'connect:test',
    'karma'
  ]);

  grunt.registerTask('build', [
    'replace:version',
    'jshint',
    'clean:prod',
    'useminPrepare',
    'concurrent:prod',
    'requirejs',
    'ngAnnotate',
    'autoprefixer',
    'copy:prod',
    'cssmin',
    // 'responsive_images:dev',
    'concat',
    // 'uglify',
    'rev',
    'usemin',
    'html2js:prod'
  ]);

  grunt.registerTask('build-app', function ( target, inclDebug ) {
    var tasks = [];
    if ( target === 'dist' ) {
      // tasks.push('replace:release');
      tasks.push('shell:androidKeystore');
    }
    tasks = tasks.concat([
      'build',
      'replace:env' + (target === 'dist' ? 'Prod' : ''),
      'shell:build' + (target === 'dist' ? 'Prod' : ''),
      'shell:ios' + (target === 'dist' ? 'Prod' : 'Dev')
    ]);
    if ( target === 'dist' && inclDebug === 'yes' ) {
      tasks = tasks.concat(['replace:env', 'shell:build', 'shell:iosDev']);
    }
    tasks.push('copy:mobile');
    grunt.task.run( tasks );
  });

  grunt.registerTask('distrib-app', function () {
    grunt.task.run([
      'shell:testfairy',
      'testflight:ios'
    ]);
  });

  grunt.registerTask('device', function () {
    grunt.task.run([
      'build',
      'shell:device'
    ]);
  });

  grunt.registerTask('default', [
    'jshint',
    'test',
    'build'
  ]);

  grunt.registerTask('screenshots', [
    'clean:server',
    'concurrent:server',
    'connect:livereload',
    'autoshot'
  ]);

};
