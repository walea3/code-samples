Author: WaleA3 - Adewale Adeyemi

Project info
================================

Build info can be located in package.json, bower.json & Gruntfile.js

# Build tools

*	for Development - see package.json
	- 	Node ( http://nodejs.org )
	-	Grunt ( http://gruntjs.com/getting-started )
	-	Bower ( http://bower.io )
	- 	Assemble ( http://assemble.io/docs/Quickstart.html )
		-	Handlebars ( http://handlebarsjs.com )
	-	SASS ( http://sass-lang.com )
	-	Compass ( http://compass-style.org/install )
		-	Requires Ruby ( https://www.ruby-lang.org/en/downloads )

* 	for Site - see bower.json


Project Notes
==============

# Getting started ( in Terminal / Cmd Prompt )

1 	Ensure all the development tools are installed.
2	Run 'npm install' - to install dev dependancies in package.json
3	Run 'bower install' - to install site dependancies in bower.json
4	Run 'grunt server' - to start the development server which complies and watches for changes
5	Run 'grunt build' - to complite templates for distribution / production


# Directory structure

All working files are stored in the /app directory

*	/src/views - for all HTML (mushtashe) templates
	- 	/layout
	-	/pages
	-	/partials
*	/src/data - any dta JSON that's used by the templates
*	/src/sass
*	/css - complied SASS
*	/js - working JS files
*	/img
*	/bower_components - plugins, tools & frameworks used by site - managed in bower.json

Distribution templates in /dist with complied HTML, SASS, JS and images - ready server-side integration

# Ignores

The following should be ignored in the repo as they are generated from running 'npm' & 'grunt' commands
-	node_modules
- 	.tmp
-	.sass-cache
