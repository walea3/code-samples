'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {
	// show elapsed time at the end
	require('time-grunt')(grunt);
	// load all grunt tasks
	require('load-grunt-tasks')(grunt);

	grunt.initConfig({
		// configurable paths
		yeoman: {
			app: 'app',
			dist: 'dist'
		},
		watch: {
			assemble: {
				files: ['<%= yeoman.app %>/src/views/layouts/*.hbs',
						'<%= yeoman.app %>/src/views/pages/*.hbs',
						'<%= yeoman.app %>/src/views/partials/{,*/}*.hbs'],
				tasks: ['assemble']
			},
			compass: {
				files: ['<%= yeoman.app %>/src/sass/{,*/}*.{scss,sass}'],
				tasks: ['compass', 'autoprefixer', 'cmq']
			},
			styles: {
				files: ['.tmp/css/{,*/}*.css'],
				tasks: ['copy:styles', 'autoprefixer']
			},
			scripts:{
			    files: ['<%= yeoman.app %>/js/{,*/}*.js'],
			    tasks: ['jshint']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'<%= yeoman.app %>/*.html',
					'{.tmp,<%= yeoman.app %>}/css/{,*/}*.css',
					'{.tmp,<%= yeoman.app %>}/js/{,*/}*.js',
					'<%= yeoman.app %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
				]
			}
		},
		connect: {
			options: {
				port: 9001,
				livereload: 35780,
				// change this to '0.0.0.0' to access the server from outside
				hostname: '0.0.0.0'
			},
			livereload: {
				options: {
					open: true,
					base: [
						'.tmp',
						'<%= yeoman.app %>'
					]
				}
			},
			test: {
				options: {
					base: [
						'.tmp',
						'test',
						'<%= yeoman.app %>'
					]
				}
			},
			dist: {
				options: {
					open: true,
					base: '<%= yeoman.dist %>'
				}
			}
		},
		clean: {
			dist: {
				files: [{
					// dot: true,
					src: [
						'.tmp',
						'<%= yeoman.dist %>/{,*/}*.*',
						'!<%= yeoman.dist %>/{,*/}.svn*',
						'!<%= yeoman.dist %>/.git*'
					]
				}]
			},
			server: '.tmp'
		},
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'<%= yeoman.app %>/js/{,*/}*.js',
				'!<%= yeoman.app %>/js/vendor/*',
				'test/spec/{,*/}*.js'
			]
		},
		assemble: {
			// Task-level options.
			options: {
				flatten: true,
				assets: '<%= yeoman.dist %>/assets',
				layout: 'default.hbs',
				layoutdir: '<%= yeoman.app %>/src/views/layouts/',
				partials: '<%= yeoman.app %>/src/views/partials/{,*/}*.hbs',
				data: '<%= yeoman.app %>/src/data/*.{json,yml}'
			},
			// Templates to build into pages
			pages: {
				files: {
					'<%= yeoman.app %>/': ['<%= yeoman.app %>/src/views/pages/*.hbs']
				}
			}
		},
		// mocha: {
		//     all: {
		//         options: {
		//             run: true,
		//             urls: ['http://<%= connect.test.options.hostname %>:<%= connect.test.options.port %>/index.html']
		//         }
		//     }
		// },
		compass: {
			options: {
				sassDir: '<%= yeoman.app %>/src/sass',
				cssDir: '.tmp/css',
				generatedImagesDir: '.tmp/img/generated',
				imagesDir: '<%= yeoman.app %>/img',
				javascriptsDir: '<%= yeoman.app %>/js',
				fontsDir: '<%= yeoman.app %>/fonts',
				importPath: '<%= yeoman.app %>/bower_components',
				httpImagesPath: '/img',
				httpGeneratedImagesPath: '/img/generated',
				httpFontsPath: '/fonts',
				relativeAssets: false,
                outputStyle: 'nested',
				raw: 'Sass::Script::Number.precision = 15\n' // Use `raw` since it's not directly available.
			},
			dist: {
				options: {
					generatedImagesDir: '<%= yeoman.dist %>/img/generated',
                    outputStyle: 'compressed',
                    environment: 'production'
				}
			},
			server: {
                options: {
                    debugInfo: false
                }
            }
		},
		autoprefixer: {
			options: {
				browsers: ['last 1 version']
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/css/',
					src: '{,*/}*.css',
					dest: '.tmp/css/'
				}]
			}
		},
		uglify: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= yeoman.dist %>/js/',
					src: '{,*/}*.js',
					dest: '<%= yeoman.dist %>/js/'
				}]
			}
		},
		// not used since Uglify task does concat,
		// but still available if needed
		concat: {
			dev: {
                files: {
                    '<%= yeoman.app %>/js/lib/lbg_vendors.js': [
                        '<%= yeoman.app %>/bower_components/modernizr/modernizr.js',
                        '<%= yeoman.app %>/bower_components/jquery/jquery.js',
                        '<%= yeoman.app %>/bower_components/sass-bootstrap/dist/js/bootstrap.min.js',
                        '<%= yeoman.app %>/js/responsive-carousel.js'
                    ],
                    '<%= yeoman.app %>/js/lib/html5shiv.js': [
                        '<%= yeoman.app %>/bower_components/html5shiv/dist/html5shiv.js'
                    ],
                    '<%= yeoman.app %>/js/lib/respond.min.js': [
                        '<%= yeoman.app %>/bower_components/respond/respond.min.js'
                    ],
                    '<%= yeoman.app %>/js/lbg.js': [
                        '<%= yeoman.app %>/js/lbg.js'
                    ]
                }
            },
            dist: {
                files: [
                    {
                        expand: true,                   // Enable dynamic expansion.
                        cwd: '<%= yeoman.dist %>/',     // Src matches are relative to this path.
                        src: [                          // Actual pattern(s) to match.
                            '<%= yeoman.dist %>/css/*.css',                // Process only main css files in CSS root.
                            '<%= yeoman.dist %>/js/*.js',                  // Process only main js files in JS app root.
                            '<%= yeoman.dist %>/js/lib/*.js'                  // Process only main js library files in JS app root.
                        ],
                        dest: '<%= yeoman.dist %>/',    // Destination path prefix.
                        nonull: false                   // Set nonull to true if you want the concat task to warn if a given file is missing or invalid.
                    }
                ]
            }
		},
		rev: {
			dist: {
				files: {
				    src: [
                        // Reinstate lines below if you want revisioned filenames to be used
						'<%= yeoman.dist %>/js/{,*/}*.js',
						'<%= yeoman.dist %>/css/{,*/}*.css',
						'<%= yeoman.dist %>/img/{,*/}*.{png,jpg,jpeg,gif,webp}',
						'<%= yeoman.dist %>/fonts/{,*/}*.*'
					]
				}
			}
		},
        cmq: {
            options: {
              log: false
            },
            dev: {
	        	files: {
	        	'<%= yeoman.app %>/css' : ['.tmp/css/{,*/}*.css']
	        	}
            }
        },
		useminPrepare: {
			options: {
				root: '<%= yeoman.app %>', 
				dest: '<%= yeoman.dist %>'
			},
			html: '<%= yeoman.app %>/index.html'
		},
		usemin: {
			options: {
				dirs: ['<%= yeoman.dist %>']
			},
			html: ['<%= yeoman.dist %>/{,*/}*.html'],
			css: ['<%= yeoman.dist %>/css/{,*/}*.css']
		},
		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= yeoman.app %>/img',
					src: '{,*/}*.{png,jpg,jpeg}',
					dest: '<%= yeoman.dist %>/img'
				}]
			}
		},
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= yeoman.app %>/img',
					src: '{,*/}*.svg',
					dest: '<%= yeoman.dist %>/img'
				}]
			}
		},
		cssmin: {
			// This task is pre-configured if you do not wish to use Usemin
			// blocks for your CSS. By default, the Usemin block from your
			// `index.html` will take care of minification, e.g.
			//
			//     <!-- build:css({.tmp,app}) css/main.css -->
			//
			// dist: {
			//     files: {
			//         '<%= yeoman.dist %>/css/main.css': [
			//             '.tmp/css/{,*/}*.css',
			//             '<%= yeoman.app %>/css/{,*/}*.css'
			//         ]
			//     }
			// }
		},
		htmlmin: {
			dist: {
				options: {
					/*removeCommentsFromCDATA: true,
					// https://github.com/yeoman/grunt-usemin/issues/44
					//collapseWhitespace: true,
					collapseBooleanAttributes: true,
					removeAttributeQuotes: true,
					removeRedundantAttributes: true,
					useShortDoctype: true,
					removeEmptyAttributes: true,
					removeOptionalTags: true*/
				},
				files: [{
					expand: true,
					cwd: '<%= yeoman.app %>',
					src: '*.html',
					dest: '<%= yeoman.dist %>'
				}]
			}
		},
		// Put files not handled in other tasks here
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= yeoman.app %>',
					dest: '<%= yeoman.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'css/{,*/}*.css',
                        'data/{,*/}*.json',
						'js/{,*/}*.js',
						'img/{,*/}*.{webp,gif}',
						'fonts/{,*/}*.*',
						'bower_components/sass-bootstrap/fonts/*.*'
					]
				}]
			},
			styles: {
				expand: true,
				dot: true,
				cwd: '<%= yeoman.app %>/css',
				dest: '.tmp/css/',
				src: '{,*/}*.css'
			}
		},
		concurrent: {
			server: [
				'compass',
				'copy:styles'
			],
			test: [
				'copy:styles'
			],
			dist: [
				'compass:dist',
				'copy:styles',
				'imagemin',
				'svgmin',
				'htmlmin'
			]
		},
		bower: {
			options: {
				exclude: ['modernizr']
			},
			all: {
				rjsConfig: '<%= yeoman.app %>/js/main.js'
			}
		}
	});

	grunt.loadNpmTasks('assemble');
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-combine-media-queries');

	grunt.registerTask('server', function (target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		grunt.task.run([
			'clean:server',
			'concurrent:server',
			'autoprefixer',
			'connect:livereload',
			'watch'
		]);
	});

	grunt.registerTask('test', [
		'clean:server',
		'concurrent:test',
		'autoprefixer',
		'cmq',
		'connect:test'
	]);

	grunt.registerTask('build', [
		'clean:dist',
		'useminPrepare',
		'concurrent:dist',
		'autoprefixer',
		'cmq',
		'concat:dev',
		'copy:dist',
		'usemin',
		'concat:dist',
		'uglify'
	]);

	grunt.registerTask('default', [
		'jshint',
		'test',
		'build'
	]);
};