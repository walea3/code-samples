/* 
 * Resident Evil: No Hope Left - Last Goodbye
 * Author: Adewale Adeyemi - MiyoMint
 */ 
(function() {
	'use strict';
	LG.css = {
		HIDE: 'visuallyhidden'
	};
	LG.model = LG.model || {};
	LG.view = LG.view || {};
	
	//Submissions
	LG.Subs = {
		options: {
			status: false
		},
		items: [],
		init: function() {
			this.items = $('#subs .box');
			$('#subs').css('padding', '10px 0');
			$('#subs .box .share .plugins').hide();
			
			//masonary
			$('#subs').masonry({
				itemSelector: '.box:not(.' + LG.css.HIDE + ')',
				columnWidth: 250,
				isAnimated: Modernizr.csstransitions,
				isFitWidth: true
			});
			
			$('#subs .box .share button').live('click', function(e) {
				$(this).next().stop().animate({height: 'toggle'}, 200);
			});
			
			//Filter event
			$('[data-sub-filter]').live('click', function(e) {
				e.preventDefault();
				LG.Subs.filter(this);
			});
			return this;
		},
		list: function() {
			$('[data-sub-mode=submission]').removeClass('in').addClass(LG.css.HIDE + ' fade out');
			$('[data-sub-mode=browsing]' + (!LG.config.user.loggedin ? ':not([data-sub-login])' : ''))
				.removeClass(LG.css.HIDE + ' out').addClass('fade in');
			return this;
		},
		show: function( stage, id ) {
			var sub = $('#subs .box.' + stage + '[data-sub-id=' + id + '] .wrap').html();
			if (sub) {
				$('.stage-info').addClass(LG.css.HIDE + ' out').removeClass('in');
				$('#subsModal')
					.children('.modal-body').html( sub )
					.parent().modal('show').on('hidden', function () {
						if (LG.config.routes.current) {
							Backbone.history.navigate('!/' + stage, true);
							$('.stage-info.' + LG.css.HIDE).removeClass(LG.css.HIDE + ' out').addClass('in');
						}
					});
			}
			return this;
		},
		filter: function( el ) {
			var filterCon = $(el)[0].dataset.subFilter,
				isActive = $(el).hasClass('active') || $(el).parent().hasClass('active') ? true : false,
				filterLink = '', filter = [];
				
			switch (filterCon) {
				case 'lang':
					var lang = $(el)[0].dataset.subLang;
					LG.config.filter.lang = !isActive ? '[data-sub-lang=' + lang + ']' : null;
					filterLink = '[data-sub-lang=' + lang + ']';
					break;
				case 'type':
					var type = $(el)[0].dataset.subType;
					LG.config.filter.type = !isActive ? '[data-sub-type=' + type + ']' : null;
					filterLink = '[data-sub-type=' + type + ']';
					break;
				case 'friends':
					LG.config.filter.popular = !isActive ? '[data-user-friend=yes]' : null;
					delete(LG.config.filter.sort);
					break;
				case 'clear':
					LG.config.filter = {};
					$('button[data-sub-filter][data-sub-filter!='+ filterCon +']').removeClass('active');
					$('a[data-sub-filter][data-sub-filter!='+ filterCon +']').parent().removeClass('active');
					break;
				default:
					LG.config.sort = filterCon;
					delete(LG.config.filter.popular);
			}
			
			if ( filterCon != 'clear' ) {
				$('a[data-sub-filter=' + filterCon + ']' + filterLink)
					.parent().toggleClass('active').siblings().removeClass('active');
				$('button[data-sub-filter=' + filterCon + ']' + filterLink)
					.toggleClass('active').siblings().removeClass('active');
			}
			
			for (var k in LG.config.filter) {
				if (LG.config.filter.hasOwnProperty(k)) {
					filter.push(LG.config.filter[k]);
				}
			}
			
			filter = filter.join('');
			
			if (filter) {
				$('#subs .box:not(' + filter + ')').stop().animate({opacity:0}, 500, function(){ 
					$(this).addClass(LG.css.HIDE);
				});
				$('#subs .box' + filter).removeClass(LG.css.HIDE).stop().animate({opacity:1}, 500, function(){ 
					$('#subs').masonry('reload');
				});
			} else {
				$('#subs .box').removeClass(LG.css.HIDE).stop().animate({opacity:1}, 500, function(){ 
					$('#subs').masonry('reload');
				});
			}
			$('#subs').masonry('reload');
			return this;
		}
	};
	
	//SoundCloud
	LG.SC = {
		init: function () {
			(function() {
				var e = document.createElement('script');
				e.src = document.location.protocol + '//connect.soundcloud.com/sdk.js';
				e.async = true; document.getElementsByTagName('body')[0].appendChild(e);
			}());
			
			setTimeout(function() {
				SC.initialize(LG.config.SC);
			}, 16);
			return this;
		}
	};
	
	//YouTube
	LG.YT = {};
	
	//Facebook
	LG.FB = {
		init: function() {
			if ( LG.config.user.loggedin ) {
				this.loggedIn();
			} else {
				this.loggedOut();
			}
			window.fbAsyncInit = function() {
				FB.init({
					appId: LG.config.FB.id,
					channelUrl: LG.config.baseUrl + 'public/channel.html',
					status: true,
					cookie: true,
					xfbml: true,
					frictionlessRequests: true
				});
				FB.Event.subscribe('auth.statusChange', function(response) {
					if ( response.authResponse ) {
						LG.FB.connect.toApp();
					}
				});
			};

			if (!$('#fb-root')[0]) {
				(function() {
					$('<div id="fb-root" />').prependTo('body');
					var e = document.createElement('script');
					e.src = document.location.protocol + '//connect.facebook.net/en_GB/all.js';
					e.async = true; document.getElementById('fb-root').appendChild(e);
				}());
			}
			return this;
		},
		connect: {
			toFb: function() {
				FB.login(function(response) {
					if (response.authResponse) {
						LG.FB.connect.toApp();
					} else {
						this.loggedOut();
						alert('common');
						$('.alert.fbPerm').addClass('fade in').removeClass(LG.css.HIDE);
					}
				}, {scope: 'email'});
				return this;
			},
			toApp: function(e) {
				if (e) { 
					e.preventDefault();
				}
				FB.api('/me', function(me) {
					if (me.first_name) {
						LG.config.user = LG.config.user || {};
						LG.config.user.loggedin = true;
						LG.config.user.username = me.username;
						LG.config.user.firstname = me.first_name;
						LG.config.user.lastname = me.last_name;
						LG.config.user.email = me.email || null;
						LG.FB.loggedIn();
					}
				});
				return this;
			}
		},
		share: {
			feed: function(e) {
				if (e) { 
					e.preventDefault();
				}
				FB.ui({
					method: 'feed',
					link: LG.config.absolute,
					picture: LG.share.FB.img,
					name: LG.share.FB.title,
					caption: LG.share.FB.desc
				}, LG.FB.share.fbCallback);
				return this;
			},
			request: function(e) {
				if (e) { 
					e.preventDefault();
				}
				FB.ui({
					method: 'apprequests',
					title: LG.share.FB.friends_title,
					message: LG.share.FB.friends_desc
				}, LG.FB.share.fbCallback);
				return this;
			},
			fbCallback: function(response) {
				if (response && (response.post_id || response.request)) {
					LG.userShare();
					if (!response.request) {
						//LG.FB.share.request();
					}
				}
			}
		},
		logout: function() {
			FB.logout(LG.FB.loggedOut());
			return this;
		},
		loggedIn: function () {
			$('.alert.fbPerm:not(.' + LG.css.HIDE + ')').addClass(LG.css.HIDE + ' out').removeClass('in');		
			$('.userFb .username').text(LG.config.user.firstname);
			$('.userFb .pic img').attr('src', 'https://graph.facebook.com/' + LG.config.user.username + '/picture');
			$('.userFb .pic, .userFb .info p, .stage-info .cta');
			$('.logout').live('click', function (e) {
				e.preventDefault();
				LG.FB.logout();
			});
			$('[data-sub-login=false]').addClass(LG.css.HIDE + ' fade out').removeClass('in');
			$('[data-sub-login=true]').removeClass(LG.css.HIDE + ' out').addClass('fade in');
		},
		loggedOut: function () {
			LG.config.user.loggedin = false;
			$('.userFb .pic, .userFb .info p, .stage-info .cta').addClass(LG.css.HIDE); 
			$('[data-sub-login=true]').removeClass('in').addClass(LG.css.HIDE + ' out');
			$('[data-sub-login=false]').addClass('fade in').removeClass(LG.css.HIDE + ' out');
		}
	};
	
	Backbone.View.prototype.close = function(nextView) {
		var delay = 600,
			me = this;
		
		if ( LG.appView === 'mobile' || (nextView.el.id !== 'result' || nextView.el.id !== this.el.id) ) {
			this.$el.children('article.sidePanel' + (this.el.id !== 'result' ? ', .trunk' : '')).css('opacity', 0).removeClass('in').addClass('out reverse');
		}
		this.$el.children('.rightPanel').removeClass('in').addClass('out reverse');
		$('#result .trunk .fx').removeClass('in').addClass('out reverse');
		if (LG.appView === 'full' && this.$el.hasClass('question')) {
			LG.view.Manager.questions('close');
		}
		if ( LG.appView === 'mobile' || (nextView.el.id !== 'result' || nextView.el.id !== this.el.id) ) {
			this.$el.removeClass(LG.view.Manager.transitions.open() + ' current').addClass(LG.view.Manager.transitions.close());
			setTimeout(function() {
				me.$el.css('opacity', 0).removeClass(LG.view.Manager.transitions.close());
				me.remove();
				me.unbind();
				if (this.onClose) {
					this.onClose();
				}
			}, delay);
		}
	};
	
	LG.view.Manager = {
		transitions: {
			opts: {},
			open: 'fade in',
			close: 'fade out'
		},
		renderView: function() {
			if ( LG.appView === 'mobile' || (this.previousView.el.id !== 'result' || this.previousView.el.id !== this.currentView.el.id) ) {
				this.currentView.$el.prependTo('#main');
			}
			var me = this, scrTop = 0,
				elChildren = (LG.appView === 'mobile' || (me.previousView.el.id !== 'result' || me.previousView.el.id !== me.currentView.el.id) ? '.sidePanel,' : '') + '.rightPanel, .signOff',
				delay = (typeof ($('.main')[0]) === 'undefined' || !$('.main').hasClass('current')) ? 0 : 510;
			clearInterval(LG.initTimer);
			clearTimeout(LG.pageTimer);
			LG.pageTimer = setTimeout(function() {
				$('#container')[0].className = me.currentView.el.id !== 'result' ? '' : $('#container')[0].className;
				me.currentView.render().$el.removeClass(LG.view.Manager.transitions.close()).addClass(LG.view.Manager.transitions.open() + ' current').css('opacity', 1);				
				 
				$('.rightPanel,.sidePanel,#ageGateForm,.trunk').addClass('fade');
				if ( LG.appView === 'mobile' ) {
					scrTop = ( me.currentView.el.id === 'result' ? ( me.currentView.$el.hasClass('goodluck') ? 0 : $('#container').offset().top + 36) : ( me.currentView.$el.hasClass('question') ? 0 : ( !me.currentView.$el.hasClass('age') ? $('#container').offset().top + 20 : 0)) );
					$('html, body').scrollTop(scrTop);
				} else {
					$('.csstransforms3d .rightPanel,.csstransforms3d #ageGateForm').removeClass('fade').addClass('flip');
					$('.csstransforms3d .sidePanel').removeClass('fade').addClass('pop');
					if ( me.previousView.el.id === 'result' && me.previousView.el.id === me.currentView.el.id ) {
						$('.sidePanel').removeClass('fade pop');
					}
				}
				
				if ( $('.lt-ie9')[0] )  {
					me.currentView.$el.children(elChildren).hide();
				} else {
					me.currentView.$el.children(elChildren).css('opacity', 0);
				}
				
				if (me.currentView.$el.hasClass('question')) {
					if ( LG.appView === 'mobile' ) {
						var i = 0, qImgs = me.currentView.$('.answer img').length;
						me.currentView.$('.answer img').load(function(){
							i+=1;
							if (i == qImgs) {
								setTimeout(function() { LG.react(); }, 16);
							}
						});
					} else {
						LG.view.Manager.questions('open');
					}
				}
				
				me.events();
				
				delay = (LG.appView === 'mobile') ? 10 : (me.previousView.el.id === 'result' && me.previousView.el.id === me.currentView.el.id ? 0: 300);
				setTimeout(function() {
					if (LG.config.gate === 'open') {
						me.currentView.$('article.sidePanel').css('opacity', 1).addClass('in');
					}
					me.currentView.$('.rightPanel:not(.' + LG.css.HIDE + ')').show().css('opacity', 1).addClass('in');
				}, delay);
				if ( LG.appView === 'mobile' ) { LG.react(); } else {
					setTimeout(function() { LG.react(); }, 800);
				}
			}, delay);
		},
		showView: function(view) {
			if (this.currentView) {
				this.previousView = this.currentView;
				this.previousView.close(view);
				this.currentView = view;
				var me = this,
					delay = ( LG.appView === 'full' ? (this.previousView.$el.hasClass('question') ? 250 : (view.el.id === 'result' && view.el.id === this.previousView.el.id ? 0 : 90)) : 0 );
				setTimeout(function() {
					me.renderView();
				}, delay);
			} else {
				this.previousView = this.currentView = view;
				this.renderView();
			}
			setTimeout(function() {
				//console.log('uri: /#' + Backbone.history.fragment + ' | title: ' + LG.appTitle.d);
				Webtrends.multiTrack({ args: { "DCS.dcsuri" : "/#" + Backbone.history.fragment, "WT.ti" : LG.appTitle.d } });
			}, 300);
		},
		events: function() {
			if ($.fn.placeholder) {
				$('input, textarea').placeholder();
			}
		}
	};
	
	/*
	 *	React / Adapt
	 */
	LG.react = function() {
		clearTimeout(LG.timer);
		LG.timer = setTimeout(LG.adapt, 16);
		return this;
	};
	
	LG.adapt = function() {
		LG.appWidth = document.documentElement.clientWidth || document.body.clientWidth || (window.innerWidth - 15) || 0;
		LG.appView = (LG.appWidth > 960 || $('.lt-ie9')[0] ) ? 'full' : (LG.appWidth > 640 ? 'full tablet' : 'mobile');
		
		$('html,body').removeClass('mobile full tablet').addClass(LG.appView);
		
		/*$( (LG.appView === 'mobile' ? '.mobileOnly:not(.' + LG.css.HIDE + '), .mobileShow:not(.' + LG.css.HIDE + ')' : '.mobileHide, .tabletHide') ).removeClass(LG.css.HIDE);
		$( ( LG.appView.indexOf('full') >= 0 ? ', .mobileOnly' : ', .mobileHide') + ( LG.appView.indexOf('tablet') >= 0 ? ', .tabletHide' : '') ).addClass(LG.css.HIDE);*/
		
		//adj.
		$('#container').width( ( LG.appView.indexOf('full') >= 0 ? LG.appWidth - (parseInt($('section.left').width()) + parseInt($('section.right').width())) : 'auto' ) );
		
		// no lang filter
		if (LG.appView == 'mobile' ) {
			delete(LG.config.filter.lang);
		} else {
			if (typeof(FB) == "undefined") LG.FB.init();
		}
					
		return this;
	};

	if (window.addEventListener) {
		window.addEventListener('resize', LG.react, false);
	} else if (window.attachEvent) {
		window.attachEvent('onresize', LG.react);
	} else {
		window.onresize = LG.react();
	}
	
	LG.Router = Backbone.Router.extend({
		routes: {
			'': 'indexView',
			'!/:stage': 'listView',
			'!/:stage/submit': 'submissionView',
			'!/:stage/submit/:step': 'submissionView',
			'!/:stage/:id': 'detailView',
			'!*rouge': 'indexView'
		},
		initialize: function() {
			//LG.view.Manager.currentView = LG.view.Manager.currentView || new LG.view.Main();
		},
		indexView: function() {
			/* LG.view.MainView = new LG.view.Main();
			LG.view.Manager.showView(LG.view.MainView); */
			LG.Subs.list();
		},
		listView: function( stage ) {
			/* LG.view.Manager.openOverlay($('#' + Backbone.history.fragment.slice(2))[0]);
			LG.appTitle.changeTitle( $('#' + Backbone.history.fragment.slice(2) + ' .title').text() ); */
			LG.Subs.list();
		},
		submissionView: function( stage, step ) {
			/*
var progress = $('.fields')[0].dataset.subProgress;
			$('.fields')[0].dataset.subStep = step = parseInt(step) || 1;
*/
			
			/* LG.view.Manager.openOverlay($('#' + Backbone.history.fragment.slice(2))[0]);
			LG.appTitle.changeTitle( $('#' + Backbone.history.fragment.slice(2) + ' .title').text() ); */
			$('.stage-info').removeClass('in').addClass(LG.css.HIDE + ' fade out');
			$('[data-sub-mode=submission]').removeClass(LG.css.HIDE + ' out').addClass('fade in');
			$('[data-sub-mode=browsing]').addClass(LG.css.HIDE + ' fade out').removeClass('in');
			
		/*
	$('#submission #steps li[data-sub-step=s' + step + ']')
				.addClass('active').removeClass('complete')
				.prev().addClass('complete').removeClass('active');
			$('#submission #steps li[data-sub-step=s' + step + ']')
				.next().removeClass('active complete');
			
			$('#submission #steps .bar').css('right', progress + '%');
*/
		},
		detailView: function( stage, id ) {
			LG.Subs.list().show(stage, id);
		}
	});
	
	/*
	 * App init
	 */
	LG.init = function() {
		this.FB.init();
		this.appRouter = new LG.Router();
		this.appRouter.bind('all', function(route, router) {
			route = route.replace('route:', '');
			
			if (typeof(router) === 'undefined') {
				Backbone.history.navigate('!/' + LG.config.routes.current || LG.config.routes.default, true);
			}
			
			router = router || LG.config.routes.current || LG.config.routes.default;
			var stage = router.split('/'),
				navItem = $('.stages.navbar .nav:not(.filter) a[href="/' + stage[0] + '"]'),
				actualRouter = navItem[0] ? router : LG.config.routes.default;
				
			stage = actualRouter.split('/');
			
			if ( router !== actualRouter && (route !== "detailView" || route !== "submissionView") || (route === "submissionView" && !LG.config.user.loggedin)) {
				$('.stages.navbar .nav:not(.filter) a[href="/' + stage[0] + '"]').click();
			} else {
				$('.stages.navbar .nav:not(.filter) a[href="/' + stage[0] + '"]')
					.parent().addClass('active').siblings().removeClass('active');
			}
			
			if ( route !== "detailView" ) {
				$('#subsModal').modal('hide');
			}
			
			LG.config.routes.previous = LG.config.routes.current;
			LG.config.routes.current = actualRouter;
		});
		
		//backbone
		setTimeout(function() {
			Backbone.emulateHTTP = true;
			Backbone.emulateJSON = true;
			Backbone.history.start();
			LG.react();
		}, 16);
		
		//facebook connect
		$('.fbConnect .btn').live('click', this.FB.connect.toFb);
		
		//Button States
		$('.stages.navbar .nav:not(.filter) a').live('click', function(){
			if ($(this).attr("href") != null) {
				$(this).parent().addClass('active').siblings().removeClass('active');
			
				if ( $(this)[0].dataset.id && $('.stage-info')[0] ) {
					$('.stage-info')[0].dataset.relId = $(this)[0].dataset.id;
				}
			}
		});
		
		//Toggle event
		$('[data-sub-toggle]').live('click', function(e) {
			e.preventDefault();
			var state = $(this)[0].dataset.subToggle,
				dir = (state == 'off' ? { left: '36px' } : { left: '-9px' }),
				speed = 150;
			LG.config[$(this)[0].dataset.subConfig] = $(this)[0].dataset.subToggle = (state == 'off' ? 'on' : 'off');
			
			$(this).children('.switch').stop().animate(dir, speed);
			$(this).children('.off').stop().animate({width: (state == 'on' ? '100%' : '2%')}, speed);
			$(this).children('.on').stop().animate({width: (state == 'off' ? '100%' : '2%')}, speed);
		});
		
		//Hijack Links
		$(document).delegate("a", "click", function(e) {
			var href = $(this).attr("href"),
				protocol = this.protocol + "//";
				
			if (href != null && href.slice(0, protocol.length) !== protocol && href !== '#') {
				e.preventDefault();
				Backbone.history.navigate('!' + href, true);
			}
		});
		
		setTimeout(function() {
			LG.react();
		}, 16);
		
		return this;
	};

}());

$(function() {
	'use strict';
	LG.adapt().init();
});

MBP.scaleFix();
MBP.hideUrlBarOnLoad();