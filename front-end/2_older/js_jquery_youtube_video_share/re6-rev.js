/* 
 * Resident Evil 6 Revelations
 * Author: Adewale Adeyemi - MiyoMint
 */ 
var limit, twAdd='#re6',twMaxLength=140,twLoaded=false,twScroll,invalid = false,
	emailReg = new RegExp("[a-z0-9!#$%&'*+\/=?\^_`{|}~\-]+(?:\.[a-z0-9!#$%&'*+\/=?\^_`{|}~\-]+)*@(?:[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9\-]*[a-z0-9])?","i"),
	nameReg = new RegExp("\^[a-zA-Z\.\,\'\-\s]*$"),
	phoneReg = new RegExp("\^[0-9\-\s]*$"),
	requiredFields = jQuery('#frmSS9 .req'), i, len,
	// Validate confirm field
	compareFieldCheck = function(el1, el2) {
		'use strict';
		if (el2.val() !== el1.val()) {
			el2.focus().addClass('error').prev().addClass('error');
		} else { 
			el2.removeClass('error').prev().removeClass('error');
		}

		return el2.val() !== el1.val() ? false : true;
	},

	dateCheck = function(el) {
		'use strict';
		var dmy = el.value.split('/'),
			day = parseInt(dmy[0], 10),
			month = parseInt(dmy[1], 10),
			year = parseInt(dmy[2], 10),
			appointDate = new Date(year, month - 1, day),
			today = new Date();
			
		return appointDate > today ? true : false;
	},

	multipleCheck = function(frm, name) {
		'use strict';
		for (i=0; i < frm.length; i++)
		{
			var fldObj = frm.elements[i];
			if (fldObj.name === name) {
				if (fldObj.checked) {
					return true;
				}
			}
		}
		return false;
	},

	trackLink = function(link, category, action, label) {
		'use strict';
		if ( typeof (_gat) !== 'undefined' ) {
			_gat._getTrackerByName()._trackEvent(category, action, label);
		}
	};

!(function($) {
	'use strict';

	window.setupLabel = function() {
		'use strict';
	    if ($('.label_check input').length) {
	        $('.label_check').each(function(){ 
	            $(this).removeClass('c_on');
	        });
	        $('.label_check input:checked').each(function(){ 
	            $(this).parent('label').addClass('c_on');
	        });                
	    }
	    if ($('.label_radio input').length) {
	        $('.label_radio').each(function(){ 
	            $(this).removeClass('r_on');
	        });
	        $('.label_radio input:checked').each(function(){ 
	            $(this).parent('label').addClass('r_on');
	        });
	    }
	};

	// Validate requied fields
	window.requiredCheck = function() {
		'use strict';
		var err = false, el, len, i;
		for (i = 0, len = requiredFields.length; i < len; i++) { 
			el = requiredFields[i];
			
			if (($(el).hasClass('name') && !nameReg.test(el.value)) || 
				($(el).hasClass('tel') && !phoneReg.test(el.value)) || 
				($(el).hasClass('email') && !emailReg.test(el.value)) || 
				($(el).hasClass('date') && !dateCheck(el)) || 
				($(el).hasClass('opts') && !multipleCheck($(el).parents('form')[0], $(el).find('input').attr('name'))) || 
				($(el).attr('type') === 'checkbox' && $(el).is(':checked') !== true) || 
				($(el).attr('type') === 'readio' && $(el).is(':checked') !== true) || 
				($(el).attr('type') !== 'checkbox' && $(el).attr('type') !== 'radio' && !$(el).hasClass('opts') && ($.trim(el.value) === '' || $.trim(el.value) === null))) 
			{
				$(el).focus().parents('.row').addClass('error');
				err = true;
			} else {
				$(el).parents('.row').removeClass('error');
			}
		}
		return !err;
	};
	
	RE.css = { HIDE: 'visuallyhidden' };
	
	/*
	 *	React / Adapt
	 */
	RE.react = function() {
		clearTimeout(RE.timer);
		RE.timer = setTimeout(RE.adapt, 16);
		return this;
	};
	
	RE.adapt = function() {
		RE.appWidth = document.documentElement.clientWidth || document.body.clientWidth || (window.innerWidth - 15) || 0;
		RE.appView = (RE.appWidth >= 960 || $('.lt-ie9')[0] ) ? 'full' : (RE.appWidth > 640 ? 'full tablet' : 'mobile');
		
		if ($('.lt-ie9').length) {
			$('html,body').removeClass('mobile full tablet').addClass(RE.appView);
			
			// $( (RE.appView === 'mobile' ? '.mobileOnly:not(.' + RE.css.HIDE + '), .mobileShow:not(.' + RE.css.HIDE + ')' : '.mobileHide, .tabletHide') ).removeClass(RE.css.HIDE);
			// $( ( RE.appView.indexOf('full') >= 0 ? ', .mobileOnly' : ', .mobileHide') + ( RE.appView.indexOf('tablet') >= 0 ? ', .tabletHide' : '') ).addClass(RE.css.HIDE);
		}

		// no lang filter
		if ( RE.appView !== 'mobile' ) {
			$('.nav a[rel="popover"]').popover('hide').parent().removeClass('active');
			$(".collapse").height('auto');
			
			if(!twLoaded && RE.appView.indexOf('full') >= 0) {
				$('.twlist').tweet({
					filter: function(t){ return !/^@\w+/.test(t.tweet_raw_text); },
		            username: "@RE_Games",
		            join_text: "auto",
		            avatar_size: 60,
		            fetch: 20,
		            count: 20,
		            refresh_interval: 120,
		            loading_text: "loading tweets..."
		        })
		        .bind("empty", function() { $(this).append("<span class='loading'>No tweets found</span>"); })
				.bind("loaded", function() {
				var ul = $(this).find(".tweet_list"),
					ticker = function() {
						setTimeout(function() {
							var top = ul.position().top,
								h = ul.height(),
								incr = (h / ul.children().length),
								newTop = top - incr;
							if (h + newTop <= 0) { newTop = 0; }
							ul.animate( {top: newTop}, 500 );
							ticker();
						}, 10000);
					};
					ticker();
				});
			}
		} else {
			$(".collapse:not(.in)").height('0px');
			$('[data-toggle="collapse"]:not(.active)').children('i').removeClass('icon-minus').addClass('icon-plus');
		}
					
		return this;
	};
	
	RE.scroll = function() {
		if ( !$('body').hasClass('scrolling') ) {
			$('.navbar .nav li.active:not(:has([rel="popover"])), .menu.popover .nav li.active').removeClass('active');
		}
	};
	
	//Facebook
	RE.FB = {
		init: function() {
			window.fbAsyncInit = function() {
				FB.init({
					appId: RE.config.FB.id,
					channelUrl: RE.config.baseUrl + '/channel.html',
					status: true,
					cookie: true,
					xfbml: true,
					frictionlessRequests: true
				});
				FB.Canvas.setAutoGrow();
				RE.FB.ready = true;
			};
			return this;
		},
		toFb: function( callback ) {
			FB.login(function(response) {
				if (response.authResponse) {
					RE.config.user = { loggedin : true };
					if(callback) {
						callback();
					}
				}
			}, {scope: 'publish_stream, publish_actions'});
			return this;
		},
		onReady: function( callback ) {
			if(!RE.FB.ready) {
				setTimeout(function() {
					RE.FB.onReady(callback);
				}, 50);
			} else {
				if(callback) {
					callback();
				}
			}
		},
		share: {
			feed: function(e) {
				if ( e && typeof (e.preventDefault) === "function" ) { 
					e.preventDefault();
				} else if ( e ) {
					$.extend(RE.config.FB, e);
				}

				FB.ui({
					method: 'feed',
					link: RE.config.FB.link ? RE.config.FB.link : RE.config.baseUrl,
					picture: RE.config.FB.link ? null : RE.config.FB.icon,
					name: RE.config.FB.title,
					caption: RE.config.FB.caption
				}, RE.FB.share.fbCallback);
				return this;
			},
			send: function( to, name, link ) {
				FB.ui({
					method: 'send',
					to: to || null,
					name: name || RE.config.FB.title,
					link: link || RE.config.baseUrl
				}, RE.FB.share.fbCallback);
				return this;
			},
			request: function() {
				FB.ui({
					method: 'apprequests',
					title: RE.config.FB.title,
					message: RE.config.FB.caption
				}, RE.FB.share.fbCallback);
				return this;
			},
			fbCallback: function(response) {
				if (response && (response.post_id || response.request)) {
					if (!response.request) {
						RE.config.user.shared = true;
					}
				}
			}
		},
		Timeline: function (opts) {
			var action = opts.action,
				obj = {};

			if ( action == 'remove' ) {
				FB.api('/' + opts.id , 'delete', function() {
					RE.FB.Action.removed();
				});
			} else {
				obj[ opts.object ] = opts.link;
				obj[ 'fb:explicitly_shared' ] = 'true';

				if ( RE.config.socialBrowsing === 1 ) {
					FB.api('/me/video:' + action, 'post', obj, function( response ) {
						if ( response.id ) {
							RE.config.user.actions = RE.config.user.actions || [];
							RE.config.user.actions[ RE.config.videoCurrent ] = response.id;
							RE.FB.Action.published();
						}
					});
				}
			}
		},
		Action: {
			published: function() {
				RE.FB.Action.state('published');
				if ( $('.state.published [href="#removeAction"]', '.socialBrowsing').length ) {
					$('.state.published [href="#removeAction"]', '.socialBrowsing').data('actionId', RE.config.user.actions[ RE.config.videoCurrent ]);
				} else {
					$('.state.published', '.socialBrowsing').html(
						$('.state.published', '.socialBrowsing').html().replace(/\[remove=(.*)\]/g, '<a href="#removeAction" data-action-id="' + RE.config.user.actions[ RE.config.videoCurrent ] + '">$1</a>')
					);
				}
			},
			removed: function() {
				if ( typeof( RE.config.user.actions ) !== 'undefined' )
					RE.config.user.actions[ RE.config.videoCurrent ] = 0;

				RE.FB.Action.state('removed');

				setTimeout(function(){
					RE.FB.Action.state('idle');
				}, 2000);
			},
			state: function( state ) {
				$('.state:not(.' + state + ')', '.socialBrowsing').addClass(RE.css.HIDE);
				$('.state.' + state, '.socialBrowsing').removeClass(RE.css.HIDE);
			}
		}
	};
	
	window.onYouTubePlayerAPIReady = function () {
		RE.YT.player = new YT.Player(RE.YT.id, { width:'100%', height:'100%', videoId: RE.YT.load,
				playerVars: { modestbranding: 1, wmode: "opaque", hd: '1' },
				events: { onReady: function() {
					RE.YT.player.setPlaybackQuality( 'hd1080' );
					RE.YT.player.playVideo();
				}}
		});
		RE.YT.loaded = 1;
	};
	
	RE.YT = {
		id: 'videoPlayer',
		loaded: false,
		init: function() {
			if ( RE.appView.indexOf('full') >= 0 || typeof( initAgeGate ) === 'undefined' || !initAgeGate ) { this.loadVideos(); }

			$(document).on('click','[data-video-id]', function( e ){
				e.preventDefault();
				if ( typeof( initAgeGate ) !== 'undefined' && initAgeGate ) {
					RE.AgeGate.open();
					$('#videoContainer').hide();
				} else {
					var title = $('.title .text', this).text(),
						url = $(this).attr("href"),
						img = $(this).find('.img img').attr('src'),
						social = '<li class="fb"><a href="#fbShare" data-share-caption="'+ title +'" data-share-link="' + url + '">Facebook</a></li> ' +
							'<li class="tw"><a href="https://twitter.com/intent/tweet?url=' + encodeURIComponent(url) + '&amp;via=RE_Games&amp;text='+ encodeURIComponent(title +' - ') +'&amp;hashtags=revelations">Twitter</a></li> ' +
						'<li class="gp"><a href="https://plus.google.com/share?url=' + encodeURIComponent(url) + '" onclick="javascript:window.open(this.href,\'\', \'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600\');return false;">Google+</a></li>';
					
					$('.title', this).addClass('mobileOnly');
					$('#videoContainer').show();
					RE.YT.playVideo($(this).data('videoId'));
					$('#videoContainer .share ul').html(social);
				}
			});
		},
		loadVideos: function() {
			$.getJSON( RE.config.YT.playlistURL, function (data) {
	        	if (data.feed.entry) {
				    var list_data = "", feedTitle, feedURL, fragments, videoID, url, img, isAgeRestricted;
				    $.each(data.feed.entry, function(i, item) {
				        feedTitle = item.title.$t;
				        feedURL = item.link[1].href+'&hd=1';
				        fragments = feedURL.split("/");
				        videoID = fragments[fragments.length - 2];
				        url = RE.config.YT.videoURL + videoID;
				        img = "https://img.youtube.com/vi/"+ videoID +"/maxresdefault.jpg";
				        isAgeRestricted = ( item.media$group.media$rating && item.media$group.media$rating[0].scheme == 'http://gdata.youtube.com/schemas/2007#mediarating' ? true : false );
				        list_data += '<li class="item"><a href="'+ url +'" data-video-id="'+ videoID +'" title="'+ feedTitle +'" data-age-restricted="'+ isAgeRestricted.toString() +'"> <img src="'+ RE.config.assetUrl + 'img/btn_play.png" alt="Play" class="play" /> <strong class="title"> <span class="text">'+ feedTitle+'<span class="watch"><img src="'+ RE.config.assetUrl + 'img/btn_play_yellow.png" alt="" /></span></span></strong> <span class="img"><img src="'+ img +'" alt="" /></span></a></li>';
				    });
				    
				    $("#videos .slides").html(list_data);
				    
					$('#videos').flexslider({
						animation: "slide",
						slideshow: true,
						prevText: '<span class="arr">&lsaquo;</span>',
						nextText: '<span class="arr">&rsaquo;</span>',
						controlsContainer: $('.controlsContainer', '#videos'),
						before: function( slider ) {
							RE.YT.stopVideo();
							if ( RE.config.socialBrowsing === 1 ) {
								RE.FB.Action.state('idle');
							}
						}
					});
				}
			}).error(function(){});
		},
		playVideo: function( vid ) {
			RE.config.videoCurrent = this.load = vid;

			$('#videos').flexslider('pause');
			
			if(!this.loaded) {
				var tag = document.createElement('script'),
					firstScriptTag = document.getElementsByTagName('script')[0];
				tag.src = "https://www.youtube.com/iframe_api";
				firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			} else {
				this.player.setPlaybackQuality('hd1080');
				this.player.loadVideoById({videoId:this.load, suggestedQuality:'hd1080'}).playVideo();
			}

			if ( typeof( RE.config.user.actions ) == 'undefined' || !RE.config.user.actions[ vid ] ) {
				RE.config.socialShare = setTimeout( function(){	
					RE.FB.onReady(function(){
						RE.FB.Timeline({ action: 'watches', object: 'video', link: RE.config.baseUrl + '?video=' + encodeURIComponent(RE.config.YT.videoURL + vid) });
					});
				}, 10000);
			} else {
				RE.FB.Action.published();
			}

			return this;
		},
		stopVideo: function() {
			if(this.loaded) { this.player.stopVideo(); }
			$('#videoContainer').hide();
			$('#videos .title').removeClass('mobileOnly');
			clearTimeout(RE.config.socialShare);
		}
	};
	
	if (window.addEventListener) {
		window.addEventListener('resize', RE.react, false);
		window.addEventListener('scroll', RE.scroll, false);
	} else if (window.attachEvent) {
		window.attachEvent('onresize', RE.react);
		window.attachEvent('onscroll', RE.scroll);
	} else {
		window.onresize = RE.react();
		window.onscroll = RE.scroll();
	}
	
	RE.scrollTo = function (href) {
		if (href !== "#mailing-list") {
			$('#mailing-list').modal('hide');
		}
		if ( RE.appView == "mobile" && !$(href).hasClass('in') ) {
			$('[data-target="' + href + '"]').click();
		}

		href = (href === "#mailing-list" ? '#wrapper' : href);

		if ( RE.config.FB.canvas === '1') {
			FB.Canvas.scrollTo( 0, $(href).offset().top );
		} else {
			$('body').addClass('scrolling');
			var offsetTop = href === "#" ? 0 : $(href).offset().top-(RE.appView == "mobile" ? 55 : 30);

			$('html, body').stop().animate({ 
				scrollTop: offsetTop
			}, 500, function() {
				setTimeout(function(){
					$('body').removeClass('scrolling');
				}, 150);
			});
		}
	};
	/*
	 * App init
	 */
	RE.init = function() {
		$(document).on('click','.navbar a:not([rel="popover"], .dropdown-toggle), .menu.popover a, a[data-scrollTo]', function(e) {
			var me = this,
				href = $(this).attr("href"),
				protocol = this.protocol + "//", cat = RE.config.langCode, action = 'Menu', label = $(this).text();
			
			if ($(this).data('scrollTo')) {
				$('.navbar a:not([rel="popover"]), .menu.popover a').removeClass('active');
			} else {
				$(this).parent().addClass('active')
					.siblings('li:not(:has([rel="popover"]))').removeClass('active');
			}

			trackLink(me, cat, action, label);
			
			if (href.slice(0, 1) == '#' && href !== '#') {
				//$('body').attr({scrollLeft:0, scrollTop:0});
					RE.scrollTo(href);
				return false;
			}
			
		})
		.on('click', '[data-toggle="collapse"]', function(e){
			e.preventDefault();
			if (RE.appView == 'mobile') {
				var target = $(this).data('target');
				//$(target).collapse('toggle');
				$(this).toggleClass('active').children('i').toggleClass('icon-plus icon-minus');
			}
		})
		.on('click', '[data-social-toggle]', function(e) {
			var state = ($(this).data('socialToggle') === 'off' ? 'on' : 'off'), 
				dir = (state === 'on' ? { left: '50px' } : { left: '36px' }),
				speed = 150;

			if ( !RE.config.user ) {
				RE.FB.onReady(function() {
					RE.FB.toFb(function() {
						$('[data-social-toggle]').click();
					});
				});
				return false;
			}

			$(this).removeClass('on off').data('socialToggle', state).addClass(state);

			RE.FB.Action.state( state === 'off' ? 'deactivated' : 'idle' );
			RE.config[$(this).data('socialConfig')] = (state === 'off' ? 0 : 1);

			$(this).children('.knob').stop().animate(dir, speed);
		})
		.on('click', '[href="#removeAction"]', function(e){
			e.preventDefault();
			RE.FB.onReady(function(){
				RE.FB.Timeline({ action: 'remove', id: $(e.currentTarget).data('actionId') });
			});
		})
		.on('click','#otherGames .handle a', function(e) {
			var me = this, cat = RE.config.langCode, action = 'Other Games', 
				label = $(this).attr('title'), 
				isClosed = $('#otherGames').hasClass('closed');

			trackLink(me, cat, action, label);

			e.preventDefault();
			if($('.nav a[rel="popover"]').parent().hasClass('active')) {
				$('.nav a[rel="popover"]').click();
			}
			$('#otherGames').toggleClass('closed')
				.stop().animate({ top: ( isClosed ? ('-' + $('#otherGames .wrapper').height()) : 0) }, 350);
		})
		.on('click', '[href="#fbShare"]', function( e ){
			e.preventDefault();
			RE.FB.onReady(function() {
				RE.FB.share.feed({ 
					caption : $( e.currentTarget ).data( 'shareCaption' ),
					link : $( e.currentTarget ).data( 'shareLink' ) 
				});
			});
		})
		.on('click','.label_check, .label_radio', function(){
			setupLabel();
		}).on('click', '.offerLinks li a', function(e) {
			var me = this, cat = RE.config.langCode, action = 'Pre Order', label = $(this).attr('title');
			trackLink(me, cat, action, label);
		}).on('click', '.social .links a', function(e) {
			var me = this, cat = RE.config.langCode, action = 'Social', label = $(this).text();
			trackLink(me, cat, action, label);
		}).on('click', '#videos .item a', function(e) {
			var me = this, cat = RE.config.langCode, action = 'Video', label = $(this).attr('title');
			trackLink(me, cat, action, label);
		});

		$('#otherGames .wrapper').flexslider({
			animation: "slide",
			animationLoop: false,
		    itemWidth: 188,
		    itemMargin: 25,
		    minItems: 1,
		    maxItems: 4,
		    slideshow: false,
			prevText: '<span class="arr"><</span>',
			nextText: '<span class="arr">></span>',
			controlsContainer: $('.controlsContainer', '#otherGames')
		});
		
		setupLabel();
		
		$('[rel="tooltip"]').tooltip();
		
		$('#mailing-list').modal({ show : false }).on('hidden', function () {
			$('.navbar .nav a[href="#mailing-list"]').parent().removeClass('active');
		});
		
		$("#frmSS9").submit(function (e) {
			return requiredCheck();
		});
		
		var popoverTimeout,
			popoverDelay = function () { 
				popoverTimeout = setTimeout(function(){$('#preorder button[rel="popover"], #buynow button[rel="popover"]').popover('hide')},500); 
			};
		$('#preorder button[rel="popover"], #buynow button[rel="popover"]')
		.popover({
			placement: 'top', 
			trigger: 'manual',
			html: true
		}).click(function(e) {
			e.preventDefault();
		}).mouseenter(function(e){
			if (RE.appView !== 'mobile') {
				e.preventDefault();
				var width = $(this).parent().width(),
					buttonWidth = ($(this).width()/2)+10,
					$popover = $('.preorderLinks');
		        (popoverTimeout) && clearTimeout(popoverTimeout);
		        ($popover.length) && $popover.remove();
		        
				$(this).popover('show');
			}
		}).mouseleave(function(e){
			if (RE.appView !== 'mobile') {
				e.preventDefault();
				popoverDelay();
				$('.preorderLinks').mouseenter(function(){
					clearTimeout(popoverTimeout);
				}).mouseleave(function(e) { popoverDelay(); });
			}
		});
		
		$('.nav a[rel="popover"]').popover({
			placement: 'bottom',
			trigger: 'click',
			html: true,
			template: '<div class="popover menu"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><div></div></div></div></div>',
			content: function() {
				return $('.navbar').html();
			}
		});

		// $( document ).on( 'click', '.nav a[rel="popover"]', function( e ) {
		// 	e.preventDefault();
		// 	$(this).popover('toggle');
		// 	if ($('#otherGames').hasClass('closed')) { $('#otherGames .handle a').click(); }
		// 	$(this).parent().toggleClass('active');
		// });

		if ( typeof( initAgeGate ) !== 'undefined' && initAgeGate && RE.config.FB.canvas !== '1' ) {
			RE.AgeGate = {
				init : function() {
					$(document).on('click', '.dob .dropdown-menu a', function(e){
						e.preventDefault();
						$(this).parent().siblings().children('a').removeClass('active');
						var val = $(this).addClass('active').text(),
							name = $(this).parents('.btn-group')[0].className.match(/(day|month|year)/)[1];
						
						$(this).parents('.dropdown-menu').prev().children('.frame').text(val);

						$('option[value="' + ( $('option[value="' + val + '"]', '.'+ name +' select').length ? val : '' ) + '"]', '.'+ name +' select')[0].selected = true;
						
						RE.AgeGate.changeMonth();
					})
					.on('click', '.country .dropdown-menu a', function(e){
						e.preventDefault();
						$(this).parent().siblings().children('a').removeClass('active');
						var val = $(this).addClass('active').attr('hreflang');
						$(this).parents('.dropdown-menu').prev().children('.frame').removeClass().addClass('frame qtrans_flag qtrans_flag_' + val);
						
						RE.AgeGate.changeMonth();
						if (val in {'us':'','ca':''}) {
							$(".dob .month").addClass('fl');
						} else {
							$(".dob .month").removeClass('fl');
						}
					})
					.on('change', '.dob select', function (e) {
						var name = this.name,
							value = this.value || this.value != '' ? this.value : ' ',
							el = $('a[href="#' + value + '"]', '.' + name + ' .dropdown-menu');
							if ( el.length )
								el.click();
					})
					.on('submit', '#age_form', this.validate);
				},
				open : function() {
					$('#age_div').animate({ right : '0%', opacity : 1}, 500).removeClass('visuallyhidden');
				},
				close : function() {
					RE.YT.loadVideos();
					$('#videos').removeClass('gated');
					$('#age_div').animate({ right : '-100%', opacity : 0}, 300, function() {
						$(this).addClass('visuallyhidden');
					});
				},
				validate : function( e ) {
					if (!$('.country .dropdown-menu a.active').attr('hreflang')) {
						alert('Please, select a location');
						return false;
					}
					if (new Date(parseInt($(".dob .year .dropdown-menu a.active").text(), 10), parseInt($(".dob .month .dropdown-menu a.active").text(), 10)-1, parseInt($(".dob .day .dropdown-menu a.active").text(),10)) == 'Invalid Date') {
						alert('Please, enter your date of birth to continue');
						return false;
					}
					
					var today = new Date();
					today.setDate(today.getDate());
					var birthday = new Date(parseInt($(".dob .year .dropdown-menu a.active").text(), 10), parseInt($(".dob .month .dropdown-menu a.active").text(), 10)-1, parseInt($(".dob .day .dropdown-menu a.active").text(),10)),
						todayYear = today.getFullYear(),
						todayMonth = today.getMonth(),
						todayDay = today.getDate(),
				    	birthdayYear = birthday.getFullYear(),
				    	birthdayMonth = birthday.getMonth(),
				    	birthdayDay = birthday.getDate(),
				    	age = (todayMonth == birthdayMonth && todayDay >= birthdayDay) ? 
				            todayYear - birthdayYear : (todayMonth > birthdayMonth) ? 
				            todayYear - birthdayYear : todayYear - birthdayYear-1,
				        min_age = ($('.country .dropdown-menu a.active').attr('hreflang') == "de" ? 18 : 17);

			        if (age < min_age) {
			        	$("#age_form .error").removeClass('visuallyhidden');
			        	$("#age_form .body").addClass('visuallyhidden');
						RE.AgeGate.setC('false', true);
						return false;
			        }

			        RE.AgeGate.setC('true', $('#remember').is(':checked'));
			        RE.AgeGate.close();
			        initAgeGate = false;
			        if ( RE.config.langCode !== $('.country .dropdown-menu a.active').attr('hreflang') ) {
			        	parent.location.href = RE.config.baseUrl + '?lang=' + $('.country .dropdown-menu a.active').attr('hreflang');
			        }
					return false;
				},
				setC : function( v, l ) {
			    	var exdate=new Date(),
			    		now=new Date(),
			    		time = now.getTime();

			    	exdate.setDate(exdate.getDate() + 30*12*10);
			    	
					time += 1 * 1000;
					now.setTime(time);
					
			    	document.cookie="bIsOldEnough=" + v + "; expires=" + ( l ? exdate.toGMTString() : now.toGMTString()) +"; path=/;";
				},
				changeMonth : function() {
					if ( new Date(parseInt($(".dob .year .dropdown-menu a.active").text(), 10), parseInt($(".dob .month .dropdown-menu a.active").text(), 10)-1, 0) != 'Invalid Date' ) {
						var month = $(".dob .month .dropdown-menu a.active").text(),
							year = $(".dob .year .dropdown-menu a.active").text(),
							day = $(".dob .day .dropdown-menu a.active").text(),
							dd = new Date(year, month, 0), str = '<li><a href="# ">DD</a></li>', i;
						for (i=1; i<=dd.getDate(); i++) {
							str += ' <li><a href="#'+i+'"';
							if (i==day) str += ' class="active"';
							str += '>'+i+'</a></li>';
						}
						$('.dob .day .dropdown-menu').html(str);
					}
				}
			};
			RE.AgeGate.init();
		}

		setTimeout(function(){
			if ($('#screensSlider .item').length) {
				$('#screensSlider').flexslider({
					animation: "slide",
					animationLoop: false,
					smoothHeight: true,
					slideshow: false,
					prevText: '<span class="arr"><</span>',
					nextText: '<span class="arr">></span>',
					controlsContainer: $('.controlsContainer', '#screensSlider')
				});
			}
			if ($('#wallpapersSlider .item').length) {
				$('#wallpapersSlider').flexslider({
					animation: "slide",
					animationLoop: true,
					smoothHeight: true,
					slideshow: false,
					prevText: '<span class="arr"><</span>',
					nextText: '<span class="arr">></span>',
					controlsContainer: $('.controlsContainer', '#wallpapersSlider')
				});
			}
		}, 1000);
		
		setTimeout(function() {
			RE.react();
			RE.FB.init();
			RE.YT.init();
			if(location.hash && $('.navbar a[href="'+ location.hash +'"]:not([rel="popover"])').length) {
				$('.navbar a[href="'+ location.hash +'"]:not([rel="popover"])').click();
			}
		
		}, 100);
		
		return this;
	};

	RE.react().init();
}(window.jQuery));