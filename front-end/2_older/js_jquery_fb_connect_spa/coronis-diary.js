/* 
 * Coronis Diary
 * Author: Adewale Adeyemi
 */ 

!function($) {
	'use strict';
	
	/*
	 *	React / Adapt
	 */
	Coronis.react = function() {
		clearTimeout( Coronis.timer );
		Coronis.timer = setTimeout( Coronis.adapt, 16 );
		return this;
	};
	
	Coronis.adapt = function() {
		Coronis.appWidth = document.documentElement.clientWidth || document.body.clientWidth || ( window.innerWidth - 15 ) || 0;
		Coronis.appHeight = document.documentElement.clientHeight || document.body.clientHeight || ( window.innerHeight - 15 ) || 0;
		Coronis.appView = ( Coronis.appWidth >= 960 || $( '.lt-ie9' )[0] ) ? 'full' : ( Coronis.appWidth > 640 ? 'full tablet' : 'mobile' );
		
		if ( $( '.lt-ie9' ).length ) {
			$( 'html, body' ).removeClass( 'mobile full tablet' ).addClass( Coronis.appView );
		}

		//responsive adj.
		if ( Coronis.appView !== 'mobile' ) {
			clearTimeout( Coronis.scrollTo );
			if ( $('body').attr('data-coronis-active') !== 'start' )
				Coronis.scrollTo = setTimeout(function(){ Coronis.UI.scroll( '['+ Coronis.storage.viewKey + '="' + $('body').attr('data-coronis-active') + '"]' ); }, 500);
		} else {
			
		}

		//adjust video
		if ( Coronis.YT.currentPlaying[0] ) {
			var seekTo = 0, vPlayer;
			$.each( Coronis.YT.currentPlaying, function( i, v ) {
				vPlayer = Coronis.YT.players[ v ];
				if ( vPlayer ) {
					if ( typeof vPlayer.getCurrentTime === 'function' && vPlayer.getCurrentTime() > 0 ) {
						seekTo = vPlayer.getCurrentTime();
					} else if ( typeof vPlayer.seekTo === 'function' ) {
						vPlayer.seekTo( seekTo );
					}
					
					if ( typeof vPlayer.pauseVideo === 'function' && ( ( v.indexOf('vf') >= 0 && Coronis.appView !== 'mobile' ) || ( v.indexOf('vm') >= 0 && Coronis.appView === 'mobile' ) ) ) {	
						vPlayer.pauseVideo();
					}
				}
			});
		}

		$('['+ Coronis.storage.viewKey + ']').height( Coronis.appView !== 'mobile' ? Coronis.appHeight : 'auto' );

		return this;
	};
	
	//Facebook
	Coronis.FB = {
		ogTrigger : 0,
		init : function() {
			window.fbAsyncInit = function() {
				FB.init({
					appId: Coronis.storage.FB.id,
					channelUrl: Coronis.storage.assetUrl + 'channel.html',
					status: true,
					cookie: true,
					xfbml: true,
					frictionlessRequests: true
				});
				Coronis.FB.ready = true;
				FB.Canvas.setAutoGrow();
			};

			return this;
		},
		connect : function( callback ) {
			Coronis.storage.isConnecting = true;
			this.onReady(function() {
				FB.login(function( response ) {
					if ( response.authResponse ) {
						//check permissions
						FB.api( '/me/permissions', function( perms ) {
							if ( perms.error && perms.error.code === 2500 ) {
								Coronis.FB.connect( callback );
							} else {
								perms = perms.data[ 0 ];
								if ( perms.publish_actions === 1 ) {
									FB.api( '/me', function( me ) {
										if ( me.first_name && callback && typeof callback  === 'function' ) {
											callback( me );
										}
									});
								} else {
									alert( 'Please accept permissions to decode transmissions and track your progress.' );
								}
							}
						});
					} else {
						alert( 'Please accept permissions to decode transmissions and track your progress.' );
					}
					Coronis.storage.isConnecting = false;
				}, {
					scope: 'email, publish_actions, publish_stream, user_location'
				});
			});
		},
		onReady : function( callback ) {
			if ( !Coronis.FB.ready ) {
				setTimeout(function() {
					Coronis.FB.onReady( callback );
				}, 50);
			} else if ( callback && typeof callback === 'function' ) {  
		        callback();  
		    }
		},
		share : {
			feed: function(e) {
				var shareData = {
					method: 'feed',
					link: Coronis.storage.baseUrl,
					picture: Coronis.storage.FB.picture,
					name: Coronis.storage.FB.name,
					caption: Coronis.storage.FB.caption
				};

				if ( e && typeof e.preventDefault === 'function' ) { 
					e.preventDefault();
					$(e.currentTarget).attr('data-share-link') && ( shareData.link = $(e.currentTarget).attr('data-share-link') );
					$(e.currentTarget).attr('data-share-caption') && ( shareData.caption = $(e.currentTarget).attr('data-share-caption') );
					$(e.currentTarget).attr('data-share-text') && ( shareData.description = $(e.currentTarget).attr('data-share-text') );
					$(e.currentTarget).attr('data-share-img') && $(e.currentTarget).attr('data-share-img') !== 'false' && ( shareData.picture = $(e.currentTarget).attr('data-share-img') );
				} else if ( e ) {
					$.extend(shareData, e);
				}

				FB.ui( shareData, Coronis.FB.share.fbCallback);
				return this;
			},
			fbCallback : function( response ) {
				if ( response && ( response.post_id || response.request ) ) {
					if ( !response.request ) {
						Coronis.storage.user.shared = true;
					}
				}
			}
		},
		timeline: function (opts) {
			var action = opts.action,
				obj = {};

			if ( action == 'remove' ) {
				FB.api('/' + opts.id , 'delete', function() {
					if ( Coronis.FB.share.fbCallback && typeof Coronis.FB.share.fbCallback === 'function' )
						Coronis.FB.share.fbCallback();
				});
			} else {
				obj[ opts.object ] = opts.link;
				
				obj[ 'fb:explicitly_shared' ] = 'true';

				if ( !opts.explicitly_shared )
					delete obj[ 'fb:explicitly_shared' ];

				FB.api('/me/coronis_diary:' + action, 'post', obj, function( response ) {
					if ( response.id ) {
						Coronis.storage.user.actions = Coronis.storage.user.actions || [];
						Coronis.storage.user.actions[ opts.id ] = response.id;
						if ( Coronis.FB.share.fbCallback && typeof Coronis.FB.share.fbCallback === 'function' )
							Coronis.FB.share.fbCallback();
					}
				});
			}
		}
	};
	
	window.onYouTubePlayerAPIReady = function () {
		Coronis.YT.loaded = 1;
		if ( Coronis.YT.vid !== 0 ) {
			Coronis.YT.players[ 'vf_' + Coronis.YT.vid ] = new YT.Player( 'vf_' + Coronis.YT.vid, $.extend(Coronis.YT.config, { videoId: Coronis.YT.vid }));
			Coronis.YT.players[ 'vm_' + Coronis.YT.vid ] = new YT.Player( 'vm_' + Coronis.YT.vid, $.extend(Coronis.YT.config, { videoId: Coronis.YT.vid }));
			Coronis.YT.currentPlaying = [ 'vf_' + Coronis.YT.vid , 'vm_' + Coronis.YT.vid ];
		} 
	};
	
	Coronis.YT = {
		loaded: false,
		config: { 
			width:'100%', height:'350px',
			playerVars: { modestbranding: 1, wmode: 'opaque', hd: '1' },
			events: { 
				onReady: function( e ) {
					e.target.setPlaybackQuality( 'hd1080' );
					//e.target.playVideo(); 
				},
				onPlayerStateChange: function( e ) {
					if ( e.data == YT.PlayerState.PLAYING && !done ) {
						Coronis.YT.stopVideo();
						Coronis.YT.loaded = true;
					}
				}
			}
		},
		vid: 0,
		players: {},
		currentPlaying: [],
		setVideo: function( vid ) {
			this.vid = typeof vid !== 'undefined' ? vid : 0;

			if ( !this.loaded ) {
				var tag = document.createElement( 'script' ),
					firstScriptTag = document.getElementsByTagName( 'script' )[0];
				tag.src = 'http://www.youtube.com/iframe_api';
				firstScriptTag.parentNode.insertBefore( tag, firstScriptTag );
			} else {
				this.players[ 'vf_' + vid ] = new YT.Player( 'vf_' + vid, $.extend(Coronis.YT.config, { videoId: vid }) );
				this.players[ 'vm_' + vid ] = new YT.Player( 'vm_' + vid, $.extend(Coronis.YT.config, { videoId: vid }) );
				this.currentPlaying = [ 'vf_' + vid , 'vm_' + vid ]; 
			}
			
			return this;
		},
		stopVideo: function( vid ) {
			if ( this.loaded ) {
				if ( vid && typeof this.players[ 'v_' + vid ].stopVideo === 'function' ) {
					this.players[ 'v_' + vid ].stopVideo();
				} else {
					$.each( this.players, function( i, v ) {
						if ( typeof v.stopVideo === 'function' )
							v.stopVideo();
					});
				}
			}
		}
	};

	Coronis.SC = {
		init: function () {
			$.scPlayer.defaults.apiKey = 'a971248f876d2f81dc044f578ec7a9cb';
			if ( $.isFunction( $.scPlayer.defaults.onDomReady ) ) {
				$.scPlayer.defaults.onDomReady();
			}
			return this;
		},
		setTrack: function( trackUrl ) { 
			$('.audio[data-coronis-sound="'+ trackUrl +'"]').scPlayer({
				loadArtworks: 0,
				links: [{ url: trackUrl }]
			});
		},
		stopAll: $.scPlayer.stopAll
	};

	Coronis.Diary = {
		info : {
			booted : false,
			isLoading : false
		},
		init : function() {
			window.CorD = this;
			Coronis.UI.boot();
			Coronis.FB.init();
		},
		boot : function() {
			var scrollStart = function(){
				if ( $('.loader.out').length === 0 )
					$('.loader, .continue').toggleClass('out');

				$(document).one('scroll DOMMouseScroll mousewheel', CorD.start);
			};

			setTimeout( function(){
				if ( CorD.info.isLoading )
					return;

				CorUI.loading();

				if ( !CorD.isBooted() || !Coronis.storage.stages[1] ) {
					$.post(Coronis.storage.nevec, { 
						action : 'boot_coronis', 
						nevecID : Coronis.storage.nevecID, 
						start : !CorD.isBooted() 
					}, function( data ) { 
						scrollStart();
						CorUI.loadingComplete( data ); 
					}, 'json');
				} else {
					scrollStart();
					CorUI.loadingComplete();
				}
			}, 650);
		},
		start: function() {
			if ( !CorD.isBooted() )
				return;

			$(document).off('scroll DOMMouseScroll mousewheel', CorD.start);

			setTimeout( function(){
				CorNav.navigate( '/' + ( !Coronis.storage.startView ? Coronis.storage.stages[1].name : Coronis.storage.startView ) );
				
				if ( Coronis.storage.startView === 0 )
					return;

				CorD.HUD.boot();
				CorUI.loading( true );
				$(document).on('inview', '#container > .stage['+ Coronis.storage.viewKey + ']:not(.inview)', function(event, isInView, visiblePartX, visiblePartY) {
					//console.log( event );

					if ( Coronis.appView !== 'mobile' && !$( 'body' ).hasClass( 'scrolling' ) && 
						$(event.currentTarget).attr( Coronis.storage.viewKey ) !== CorUI.info.currentView &&
						isInView && visiblePartY === 'middle' ) 
					{
						//console.log( visiblePartY , 'of view: ' + $(event.currentTarget).attr( Coronis.storage.viewKey ), 'Current view:', CorUI.info.currentView );
						CorNav.navigate( '!/' + $(event.currentTarget).attr( Coronis.storage.viewKey ) );
					}
				});
				Coronis.storage.startView = 0;
			}, 650);
		},
		isBooted : function() {
			return this.info.booted;
		},
		stage : function( event, params ) {
			var id = parseInt( params.id || 1, 10 ), stage,
				data, key, user;

			if ( !CorD.isBooted() || ( event && !Coronis.storage.stages[ id ] ) ) {
				if ( !CorD.isBooted() ) {
					Coronis.storage.startView = 'stage-' + id;
				} else {
					CorNav.navigate( '/' + CorUI.info.currentView );
				}
				return;
			}

			data = Coronis.storage.stages[ id ];
			user = CorD.profile.user();

			data.status = $.map(data.status, function(k, v) { return [k]; });
			data.user = ( user ? user.name : null );
			data.next = ( Coronis.storage.stages[ ( id + 1 ) ] ? ( id + 1 ) : false );
			data.prev = ( id === 1 ? false : ( id - 1 ) );
			data.newTransmissions = 0;
			data.transmissions = $.map(data.transmissions, function(e, i) {
				data.newTransmissions += ( e.state === 'new' ? 1 : 0 );
				return CorD.transmission.map( e, i ); 
			});

			//console.log( 'Loading Stage ' + id );

			if ( event ) {
				$('#controls li','#hud').removeClass('active');
				$('.stage-' + id ,'#hud').addClass('active');
				CorD.HUD.close( 'display' );
				CorD.HUD.close( 'profile' );
			}

			stage = ich.stageContent( data );
			CorUI.send( stage, 'stage' );
		},
		transmission : {
			load : function( event, params ) {
				var data = Coronis.storage.transmissions[ params.key ], 
					transmission;

				CorD.HUD.close( 'profile' );

				if ( !CorD.isBooted() || ( event && !Coronis.storage.transmissions[ params.key ] ) ) {
					if ( !CorD.isBooted() ) {
						Coronis.storage.startView = 'stage-' + params.stage + '/' + params.key;
					} else {
						CorNav.navigate( '/' + CorUI.info.currentView );
					}
					return;
				}

				//check transmission stage loaded - for full view
				if ( !CorUI.hasSent( data.stage ) ) {
					CorD.stage( true, { id : data.stage } );
				}

				if ( event !== true ) {
					CorUI.go( data.stage, 'stage' );
					CorD.HUD.open( 'display' );
				}

				data.type = ( typeof data.type === 'object' ? data.type.join(' ') : data.type );

				data.message = function() {
					if ( data.messageType === 'audio' ) {
						return '<div class="audio" data-coronis-sound="' + data.audio + '"><iframe width="99.5%" height="112px" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=' + data.audio + '"></iframe></div>';
					} else if ( data.messageType === 'video' ) {
						return '<div class="video" data-coronis-video="' + data.video + '"><div id="vm_' + data.video + '" class="videoPlayer" data-coronis-ui="mobile"></div><div id="vf_' + data.video + '" class="videoPlayer" data-coronis-ui="full"></div></div>';
					} else if ( data.image ) {
						return '<div class="image"><img src="' + data.image + '" alt="' + data.headline + '"></div>';
					}
					return '';
				};

				data.decode = function () {
					return ( data.type.indexOf( 'encoded' ) >= 0 ? true : false );
				};

				transmission = ich.transmissionContent( data );

				CorUI.send( transmission, 'transmission', true );
				// console.log( data.state, ( ( data.state === 'new' || data.state === 'available' ) && data.type.indexOf( 'encoded' ) < 0 && data.type.indexOf( 'expired' ) < 0 ) );
				
				if ( ( data.state === 'new' || data.state === 'available' ) && data.type.indexOf( 'encoded' ) < 0 && data.type.indexOf( 'expired' ) < 0 ) {
					CorD.profile.update( [ data ] );

					if ( data.state !== 'read' ) {
						Coronis.FB.ogTrigger = setTimeout(function(){
							// console.log( 'not read = OG' );
							Coronis.FB.onReady(function() {
								Coronis.FB.timeline({ 
									action : 'unlock', object : 'transmission', link : data.url, explicitly_shared: true
								});
								data.state = 'read';
								clearTimeout( Coronis.FB.ogTrigger );
							});
						}, 1500);
					}
				}

				if ( data.messageType === 'audio' ) {
					Coronis.SC.setTrack( data.audio );
				} else if ( data.messageType === 'video' ) {
					Coronis.YT.setVideo( data.video );
				}
			},
			pull : function ( nevecKey, callback ) {
				$.post( Coronis.storage.nevec, { 
					action : 'get_content', 
					nevecID : Coronis.storage.nevecID, 
					type : 'transmission',
					nevecKey : nevecKey
				}, function( data ) {
					if ( data ) {
						Coronis.storage.transmissions[ nevecKey ] = data[ nevecKey ];
						CorD.transmission.load( true, { key: nevecKey, stage: data[ nevecKey ].stage } );
						if ( callback && typeof callback === 'function' ) {  
					        callback( data );
					    }
					}
				}, 'json' );
			},
			map: function(e, i) { 
				e.type = ( typeof e.type === 'object' ? e.type.join(' ') : e.type ); return [e]; 
			},
			decode : function( event, params ) {
				if ( typeof event.preventDefault === 'function' )
					event.preventDefault();

				if ( !CorD.isBooted() ) {
					CorNav.navigate( '/start' );
					return;
				}

				params = event.currentTarget.hash.replace(CorNav.RegExpCache.trimHash, '$1').split('/');

				var transmission = Coronis.storage.transmissions[ params[2] ];
				if ( transmission.state !== 'read' )
					CorD.profile.isConnected( true, function(){ CorUI.decoder( 'decode', transmission ); });
			}
		},
		profile : {
			user : function() { return Coronis.storage.user },
			load: function( display ) {
				if ( !CorD.isBooted() ) {
					CorNav.navigate( '/start' );
					return;
				}

				if ( Coronis.storage.user.profile ) {
					Coronis.storage.user.profile.special && ( Coronis.storage.user.profile.special = $.map( Coronis.storage.user.profile.special, CorD.transmission.map ) );
					Coronis.storage.user.profile.normal && ( Coronis.storage.user.profile.normal = $.map( Coronis.storage.user.profile.normal, CorD.transmission.map ) );
				}
				
				CorUI.send( ich.profileContent( CorD.profile.user() ), 'profile', ( !display ? 0 : false ) );

				if ( display ) {
					if ( !CorD.HUD.isOpen( 'profile' ) ) {
						CorD.HUD.close( 'display' );
						CorD.HUD.open( 'profile' );
					} else {
						CorD.HUD.close( 'profile' );
					}
					CorNav.navigate( '/' );
				}
			},
			isConnected: function( connect, callback ){
				var isGuest = CorD.profile.user().first_name === 'Guest';

				if ( Coronis.storage.isConnecting )
					return false;

				if ( isGuest && connect ) {
					CorD.profile.connect( callback );
				} else if ( callback && typeof callback === 'function' ) {
			        callback();
			    }
				
				return !isGuest;
			},
			connect : function( callback ) {
				if ( callback && typeof callback.preventDefault === 'function' ) { 
					callback.preventDefault();
				}
				Coronis.FB.connect(function( me ) {
					$.post( Coronis.storage.nevec, { 
						action : 'get_user', 
						nevecID : Coronis.storage.nevecID, 
						user : me 
					}, function( data ) {
						if ( data ) {
							Coronis.storage.user = data;
							$( 'body' ).addClass( 'fb-connected' );
							$( '[href="#/connect"]' ).after(
								$( '[href="#/connect"]' ).last().clone()
									.attr( 'href', '#/disconnect' )
									.removeClass( 'login' )
									.addClass( 'logout' )
									.text( 'Log Out' )
							);
							$( '.user .name' ).text( data.name );
							$( '.user img' ).attr( 'src', 'https://graph.facebook.com/' + data.username + '/picture' );
							$( '.user.hide' ).removeClass( 'hide' );
							CorD.profile.load();
							if ( callback && typeof callback === 'function' ) {  
						        callback();
						    }
						}
					}, 'json');
				});
			},
			disconnect : function ( callback ) {
				if ( callback && typeof callback.preventDefault === 'function' ) { 
					callback.preventDefault();
				}

				FB.logout();
				$.post( Coronis.storage.nevec, { 
					action : 'disconnect'
				}, function(data) {
					$( 'body' ).removeClass( 'fb-connected' )
					location.reload( true );
				}, 'json');
			},
			update : function( transmissions, callback ) {
				$.post(Coronis.storage.nevec, { 
					action : 'user_progress', 
					nevecID : Coronis.storage.nevecID, 
					user : Coronis.storage.user, 
					transmissions : transmissions 
				}, function( data ) {
					if ( data ) {
						Coronis.storage.user = data;
						CorD.profile.load( false );
						if ( callback && typeof callback === 'function' ) {  
					        callback();  
					    }
					}
				}, 'json');
			}
		},
		HUD : { //controls: navigation, profile, display
			openDelay : 650,
			boot : function( callback ) {
				this.booted = true;
				if ( callback && typeof callback === 'function' ) {  
			        callback();  
			    }
			    $('#hud').addClass('active');
				$('#hud li a').tooltip({ placement: 'right', delay: { 'hide' : 250 } });
			},
			isBooted : function() {
				return this.booted;
			},
			open : function( what ) {
				if ( !this.isBooted() ) {
					this.boot(function() {
						CorD.HUD.open( what );
					});
				} else {
					clearTimeout( CorD.HUD.clearDisplay );
					setTimeout(function() {
						$( '#' + what, '#hud' ).addClass( 'open' );
						$('#hud').addClass('active');
					}, ( this.isOpen( what, true ) ? this.openDelay : 1 ) );
				}
			},
			close : function( what ) {
				if ( what ) {
					$( '#' + what, '#hud' ).removeClass( 'open' );
				} else {
					$('#hud').removeClass('active');
				}
			},
			isOpen : function( what, close ) {
				if ( $( '#' + what, '#hud' ).hasClass( 'open' ) ) {
					if ( close ) {
						this.close( what );
						return false;
					}
					return true;
				} else {
					return false;
				}
			}
		}
	};

	Coronis.UI = {
		info: {
			currentView : 'start',
			previousView : null,
			loaded : false,
			scrollQueue: [],
			queueCount: 0
		},
		boot : function() {
			window.CorUI = this;
			ich.refresh();

			// $.stellar({
			// 	horizontalScrolling: false,
			// 	positionProperty: 'transform'
			// });

			$('body').removeClass('pre-load');

			$( document )
			.on('keydown', function( e ) {
				if ( typeof CorNav === 'undefined' )
					return;

				if ( e.keyCode === 37 || e.keyCode === 38 ) {
					if ( $('.inview .prev').length ) {
						$('.inview .prev')[0].click();
					} else {
						CorNav.navigate( '/coronis' );
					}
				} else if ( e.keyCode === 39 || e.keyCode === 40 ) {
					if ( $('.inview .next').length ) {
						$('.inview .next')[0].click();
					}
				}
			})
			.on('scroll mousedown DOMMouseScroll mousewheel', function( e ) {
				if ( e.which > 0 || e.type === "mousedown" || e.type === "mousewheel") {
					$('body').removeClass('scrolling');
					$('html, body').stop();
				}
			})
			.on( 'click touchstart', 'a[href="#/connect"]', CorD.profile.connect)
			.on( 'click touchstart', 'a[href="#/disconnect"]', CorD.profile.disconnect)
			.on( 'click', '.fb a[href="#share"]', Coronis.FB.share.feed )
			.on( 'click', 'a[href^="#/decode"]', CorD.transmission.decode );

			$('.spinner[data-coronis-ui="full"]').sprite({ height: 20, width: 20, fps: 6, no_of_frames: 18 });
			$('.spinner[data-coronis-ui="mobile"]').sprite({ height: 50, width: 50, fps: 6, no_of_frames: 18 });
			$('.jim').sprite({ height: 28, width: 28, fps: 8, no_of_frames: 8 }).addClass('anim');

			$('section.landing').waypoint({
				handler: function(direction) {
					if ( Coronis.appView !== 'mobile' && direction === 'down' ) {
						CorNav.navigate( '/start' );
					}
				},
				offset: '-5%',
				triggerOnce: true
			});

			Coronis.Navi();
		},
		loading: function( finish ) {
			if ( !finish ) {
				CorD.info.isLoading = true;
				$('['+ Coronis.storage.viewKey + '="start"]').addClass('load');
				this.animate( { 'type' : 'loading' } );
				$('.spinner').spStart();
				if ( $('.loader.out').length )
					$('.loader, .continue').toggleClass('out');
			} else {
				$('['+ Coronis.storage.viewKey + '="start"]').removeClass('load');
				$('section.landing').waypoint('destroy');
				if ( finish === 1 ) {
					this.animate( { 'type' : 'start' }, function(){
						CorD.info.isLoading = false;
					} );
				} else {
					CorD.info.isLoading = false;
				}
			}
		},
		loadingComplete: function( data ) {
			if ( data ) {
				Coronis.storage.stages = data.stages;
				Coronis.storage.transmissions = data.transmissions;
				Coronis.storage.user = data.user;
			}

			CorD.info.booted = true;

			CorUI.send( ich.stageList( { 
				stages : $.extend( [], Coronis.storage.stages ) 
			} ), 'controls', true );

			$.each( Coronis.storage.stages, function( i, v ) {
				CorD.stage( null, { id : v.id } );
			});

			CorUI.stage.setup();
		},
		animate : function( info, callback ) {
			switch ( info.type ) {
				case 'stage':
					//console.log( 'animating stage' );
					CorUI.scroll( info.el );
					break;
				case 'transmission':
					//console.log( 'animating transmission' );
					//CorUI.scroll( info.el );
					break;
				case 'loading':
					// $('.landing', '.load[' + Coronis.storage.viewKey + '="start"]').animate( { 'opacity' : 0, 'margin-left' : '-200%' }, 350 );
					// $('.loading', '.load[' + Coronis.storage.viewKey + '="start"]').animate( { 'opacity' : 1, 'top' : '-10px' }, 350, function(){
						setTimeout( function(){
							if ( callback && typeof callback  === 'function' ) {
								callback();
							}
						}, 500);
					// });
					CorUI.scroll( '#loadingContainer' );
					//console.log( 'animating loading screen' );
					break;
				case 'start':
					// $('.landing', '[' + Coronis.storage.viewKey + '="start"]:not(.load)').animate( { 'opacity' : 1, 'margin-left' : '0%' }, 350 );
					// $('.loading', '[' + Coronis.storage.viewKey + '="start"]:not(.load)').animate( { 'opacity' : 0, 'top' : '-90px' }, 350, function(){
						if ( callback && typeof callback  === 'function' ) {
							callback();
						}
					// });
					CorUI.scroll( '[' + Coronis.storage.viewKey + '="start"]' );
					//console.log( 'animating start screen' );
					break;
			}
		},
		scroll : function( to, callback ) {
			clearTimeout( CorUI.info.scrollQueue[2] );

			if ( !CorD.info.isLoading )
				$( '.jim' ).spStart().addClass( 'walking' );

			clearTimeout( CorUI.info.scrollQueue[1] );

			if ( !$( 'body' ).hasClass( 'scrolling' ) && CorUI && typeof to === 'string' && $(to).length ) {
				to = ( $(to).attr( Coronis.storage.stageKey ) ? '['+ Coronis.storage.viewKey + '="' + $(to).attr( Coronis.storage.stageKey ) + '"]' : to );

				clearInterval( CorUI.info.scrollQueue[0] );
				CorUI.info.scrollQueue = [];
				CorUI.info.queueCount = 0;

				if ( Coronis.storage.FB.canvas === '1') {
					FB.Canvas.scrollTo( 0, $(to).offset().top );
					if ( callback && typeof callback  === 'function' ) {
						callback();
					}
				} else if ( $('body').scrollTop() !== ( CorUI.offsetTop = ( to === '#' ? 0 : $(to).offset().top + ( to === '#loadingContainer' ? 150 : 0 ) ) ) ) {

					$('body').addClass('scrolling');
					$('html, body').stop().animate({ 
						scrollTop: CorUI.offsetTop
					}, ( CorUI.offsetTop * 2 ), 'easeOutQuint', function() {
						if ( callback && typeof callback  === 'function' ) {
							callback();
						}
						setTimeout(function(){
							$('body').removeClass('scrolling');
						}, 150);
					});
				}
			} /* else if ( typeof to === 'string' && $(to).length ) {
				console.log( to, 'waiting in scroll queue ...' );
				CorUI.info.scrollQueue[0] = setInterval(function(){
					CorUI.info.queueCount += 1;
					clearInterval( CorUI.info.scrollQueue[0] );
					CorUI.info.scrollQueue[0] = false;
					if ( CorUI.info.queueCount < 5 )
						CorUI.scroll( to );
				}, 500);
			} */

			//stop scroll anims after 250ms
			CorUI.info.scrollQueue[2] = setTimeout( function() { 
				if ( !CorD.info.isLoading )
					$( '.jim' ).spStop( true ).removeClass( 'walking' ); 
			}, 250);
		},
		go : function( where, animate ) {
			if ( ( CorD.isBooted() && !CorD.info.isLoading ) || ( where === CorUI.info.currentView && CorUI.info.previousView === null ) ) {
				var el = $( '[' + Coronis.storage.viewKey + '="' + where + '"]' ),
					data = el.data();
				//console.log( 'UI going to ' + where );

				CorUI.info.previousView = CorUI.info.currentView;
				CorUI.info.currentView = where;

				if ( el.length ) {
					$( '[' + Coronis.storage.viewKey + '="' + CorUI.info.previousView + '"]' ).removeClass( 'inview' );
					el.addClass( 'inview' );
					$( 'body' ).attr( 'data-coronis-active', where );
					if ( animate ) {
						CorUI.animate( { 'type': animate, 'el' : '.inview[' + Coronis.storage.viewKey + '="' + where + '"]' } );
					}
				}
				$( '.decoder' ).removeClass( 'open' );
			} else {
				if ( !CorD.isBooted() ) {
					CorNav.navigate( '/start' );
					$( '[' + Coronis.storage.viewKey + ']' ).removeClass( 'inview' );
					$( '[' + Coronis.storage.viewKey + '="start"], [' + Coronis.storage.viewKey + '="loading"]' ).addClass( 'inview' );
					$( 'body' ).attr( 'data-coronis-active', 'start' );
				}
			}
			Coronis.SC.stopAll();
			Coronis.YT.stopVideo();
			if ( animate )
				Coronis.react();
		},
		send : function( el, type, removeExisting ) {
			if ( removeExisting === 0 || !CorUI.hasSent( el.attr( Coronis.storage.viewKey ), removeExisting ) ) {
				switch ( type ) {
					case 'controls':
						if ( removeExisting )
							$( '.stage', '#hud' ).remove();
						$( '#controls .social', '#hud' ).after( el );
						break;
					case 'profile':
						$( '#profile', '#hud' ).replaceWith( el );
						break;
					case 'transmission':
						clearTimeout( Coronis.FB.ogTrigger );
						var displayEl = el.clone().attr( 'data-coronis-ui', 'full' );	
						
						if ( displayEl.attr( 'data-coronis-entry-state' ) !== 'locked' && displayEl.attr( 'data-coronis-entry-type' ).indexOf( 'encoded' ) < 0 && displayEl.attr( 'data-coronis-entry-type' ).indexOf( 'expired' ) < 0 )
							displayEl.children( '.entry' ).children( '.text' ).jScrollPane({
								animateEase: 'swing',
								autoReinitialise: true
							});

						$( '#display .decoder', '#hud' ).after( displayEl );
						el.attr( 'data-coronis-ui', 'mobile' );
					default:
						$( '#hud' ).before( el );
				}

				$('[data-coronis-ui="mobile"] [data-coronis-ui="full"], [data-coronis-ui="full"] [data-coronis-ui="mobile"]').remove();
			}
		},
		hasSent : function( what, removeExisting ) {
			var el = $( '[' + Coronis.storage.viewKey + '="' + what + '"]' );
			if ( el.length && removeExisting ) {
				el.removeClass('inview open');
				setTimeout( function() { el.remove(); }, 650);
				return false;
			}
			return ( el.length ? true : false );
		},
		// getViewEl : function( key ) {

		// },
		decoder : function( action, transmission ) {
			switch ( action ) {
				case 'decode' :
					$( '.decoder' ).addClass( 'open' );
					Coronis.FB.onReady(function() {
						Coronis.FB.timeline({ 
							action : 'decode', object : 'transmission', link : transmission.url, explicitly_shared: true
						});
						Coronis.FB.share.fbCallback = function() {
							CorD.transmission.pull( transmission.nevecKey, function ( updatedTransmission ) {
								transmission = updatedTransmission[ transmission.nevecKey ];
								setTimeout( function(){ CorUI.decoder( 'complete', transmission ); }, 1650);
							});
							Coronis.FB.share.fbCallback = function(){};
						};
					});
					break;
				case 'complete' :
					$( '.decoder' ).removeClass( 'open' );
					CorNav.navigate( '/' + transmission.stage + '/' + transmission.nevecKey );
					break;
			}
		},
		stage: {
			setupComplete : false,
			layers: {
				preload: [
					{ img: 'hud.png' }, { img: 'sprite_box.png' }, { img: 'sprites/jim.png' }, { img: 'layers/ground.png' }
				],
				clouds: [
					{ img: 'cloud_1.png', position: { top: '30%', right: '0px' }, attr: ' data-stellar-ratio="1.2"' },
					{ img: 'cloud_2.png', position: { top: '60%', right: '0px' }, attr: ' data-stellar-ratio="1.5"' },
					{ img: 'cloud_3.png', position: { top: '50%', left: '0px' }, attr: ' data-stellar-ratio="1.8"' },
					{ img: 'cloud_4.png', position: { top: '75%', left: '0px' }, attr: ' data-stellar-ratio="1.4"' },
					{ img: 'cloud_5.png', position: { top: '80%', left: '0px' }, attr: ' data-stellar-ratio="1.2"' },
					{ img: 'cloud_6.png', position: { bottom: '20%', right: '0px' }, attr: ' data-stellar-ratio="1.5"' }
				],
				ice: [
					{ img: 'ice_top.png', position: { top: '0px', right: '0px' } }
				],
				// snow: {
				// 	opts: {
				// 		NUM_FLAKES: 100,
				// 		canvas: $('[data-coronis-bg-layer="snow"]')[0],
				// 		context: 0,
				// 		intervalID: 0,
				// 		flakes: []
				// 	},
				// 	init: function() {
				// 		this.opts.canvas.width = window.innerWidth;
				// 		this.opts.canvas.height = window.innerHeight;
    // 					this.opts.context = this.opts.canvas.getContext('2d');
    // 					this.createFlakes();  
    // 					this.opts.intervalID = setInterval(this.update, 16);
				// 	},
				// 	flake: function() {
				// 		this.x = 0;  
				// 	    this.y = 0;  
				// 	    this.radius = 4;  
				// 	    this.alpha = 0.7;  
				// 	    this.velocityX = -7;  
				// 	    this.velocityY = 8;  
				// 	    this.fillStyle = "rgba(255, 255, 255, 0.7)";  
					      
				// 	    this.initialize = function()      
				// 	    {  
				// 	        this.x = Math.random() * window.innerWidth;  
				// 	        this.y = Math.random() * -100;  
				// 	        this.radius = Math.random() * 4 + 2;  
				// 	        this.alpha = Math.random();  
				// 	        this.velocityX = Math.random() * 4;  
				// 	        this.velocityY = Math.random() * 2 + 2;  
					          
				// 	        // make the minimum alpha 0.25  
				// 	        // if less the flake is hard to see  
				// 	        if(this.alpha < 0.25)  
				// 	            this.alpha = 0.25;  
					          
				// 	        this.fillStyle = "rgba(255, 255, 255, " + this.alpha.toString() + ")";  
					          
				// 	        // we will randomly flip the velocity x  
				// 	        // because all flakes going the same direction is boring!  
				// 	        if(Math.random() > 0.5)  
				// 	            this.velocityX *= -1;  
				// 	    }; 
				// 	},
				// 	createFlakes: function() {
				// 	    var f;  
				// 	    for(var i = 0; i < this.opts.NUM_FLAKES; i++)  
				// 	    {  
				// 	        f = new this.flake();  
				// 	        f.initialize();       
				// 	        this.opts.flakes.push(f);  
				// 	    }
				// 	},
				// 	update: function() {
				// 		var self = CorUI.stage.layers.snow;

				// 		self.opts.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
						  
				// 		var curFlake;  
				// 		for(var i = 0; i < self.opts.NUM_FLAKES; i++)  
				// 		{  
				// 		    curFlake = self.opts.flakes[i];  
				// 		    curFlake.x += curFlake.velocityX;  
				// 		    curFlake.y += curFlake.velocityY;  
						      
				// 		    // draw a circular flake  
				// 		    self.opts.context.beginPath();  
				// 		    self.opts.context.arc(curFlake.x, curFlake.y, curFlake.radius, Math.PI * 2, false);  
				// 		    self.opts.context.fillStyle = curFlake.fillStyle;  
				// 		    self.opts.context.fill();  
						      
				// 		    // if a flake goes out of bounds, recycle it and use it again  
				// 		    if(curFlake.y > window.innerHeight || curFlake.x > window.innerWidth || curFlake.x < 0)  
				// 		    {  
				// 		        curFlake.initialize();  
				// 		    }  
				// 		}
				// 	}
				// },
				connections: {
					'layout_1' : [{  
						styling: { 'background-image': 'c1_a.png', height: '275px', width: '366px', bottom: '50%', right: '50%', marginBottom: '-116px', marginRight: '-6px' },
						pod : { top: '-23px', left: '-18px' },
						points: [ 
							{ top: '158px', right: '0px' }, 
							{ top: '257px' },
							{ right: '114px' },
							{ top: '0px' },
							{ right: '367px' }
						],
						straightEnd : true
					},
					{ 
						styling: { 'background-image': 'c1_b.png', height: '121px', width: '413px', top: '50%', right: '50%', marginTop: '20px', marginRight: '-6px' },
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_d.png', height: '42px', width: '219px', top: '50%', right: '50%', marginTop: '53px', marginRight: '-6px' }, 
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_h.png', height: '189px', width: '320px', top: '50%', left: '50%', marginLeft: '-5px', marginTop: '-182px' }, 
						pod : { top: '-23px', right: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_i.png', height: '81px', width: '370px', top: '50%', left: '50%', marginTop: '35px', marginLeft: '-5px' },
						pod : { top: '-23px', right: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_f.png', height: '249px', width: '166px', top: '15%', right: '45%' }, 
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_g.png', height: '256px', width: '189px', top: '10%', right: '32%' }, 
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_c.png', height: '64px', width: '115px', top: '50%', right: '50%' }, 
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_e.png', height: '52px', width: '100px', top: '50%', right: '42%' }, 
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					},
					{ 
						styling: { 'background-image': 'c1_j.png', height: '148px', width: '254px', top: '60%', right: '50%' }, 
						pod : { bottom: '-23px', left: '-18px' },
						points: [],
						straightEnd : false
					}]
				}
			},
			setup: function() {
				if ( this.setupComplete )
					return;

				var self = this,
					layout, transmissions, i,
					connectionImg = Coronis.storage.assetUrl + 'img/layers/connections/';

				$.each(this.layers.clouds, function( index, v ) {
					$( '<img class="cloud_' + index + '" src="' + Coronis.storage.assetUrl + 'img/layers/' + v.img + '" alt="" '+ v.attr +' />' )
						.css( v.position )
						.appendTo('[data-coronis-bg-layer="clouds"]');
				});

				$('article.stage').each(function( index, v ) {
					layout = $.extend( true, {}, self.layers.connections['layout_1'].slice(0) );
					transmissions = $( '.transmissions li', v );

					for ( i = 0; i < transmissions.length; i++ ) {
						layout[i].styling['background-image'] = layout[i].styling['background-image'].indexOf( connectionImg ) >= 0 ? layout[i].styling['background-image'] : 'url(' + ( connectionImg + layout[i].styling['background-image'] ) + ')';
						$( transmissions[i] ).css( layout[i].styling );
						
						if ( $( transmissions[i] ).attr('data-coronis-entry-state') === 'available' ||  ( $( transmissions[i] ).attr('data-coronis-entry-state') === 'locked' && $( transmissions[i] ).hasClass( 'special' ) ) ) {
							var topBottom = '-' + ( layout[i].straightEnd ? ( $( '.pod', transmissions[i] ).height() / 2 ) - 5 : $( '.pod', transmissions[i] ).height() ) + 'px',
								leftRight = '-' + ( ( $( '.pod', transmissions[i] ).width() / 2 ) - 5 ) + 'px';
							
							layout[i].pod.bottom && ( layout[i].pod.bottom = topBottom );
							layout[i].pod.top && ( layout[i].pod.top = topBottom );
							layout[i].pod.left && ( layout[i].pod.left = leftRight );
							layout[i].pod.right && ( layout[i].pod.right = leftRight );
						}

						$( '.pod', transmissions[i] ).css( layout[i].pod );
					}
				});
				self.setupComplete = true;
			}
		}
	};

	Coronis.Navi = function() {
		 window.CorNav = new Simrou({
			'/stage-:stage/:key' : [CorD.transmission.load, function( event, params ) {
				setTimeout(function(){ CorUI.go( 'transmission:' + params.key, 'transmission' ); }, 50);
			}],
			'!/stage-:id' : function( event, params ) {
				$('#controls li','#hud').removeClass('active');
				$('.stage-' + params.id ,'#hud').addClass('active');
				$('body').removeClass('scrolling');
				$('html, body').stop();
				// CorD.HUD.close( 'display' );
				// CorD.HUD.close( 'profile' );
				Coronis.storage.startView = 'stage-' + params.id;
				CorUI.go( 'stage-' + params.id );
			},
			'/stage-:id' : [CorD.stage, function( event, params ) {
				CorUI.go( 'stage-' + params.id, 'stage' );
			}],
			'/profile' : CorD.profile.load,
			'/back' : function() {
				var hasStage = $( '[' + Coronis.storage.viewKey + '="'+ CorUI.info.previousView +'"]' ).attr( Coronis.storage.stageKey );
				CorNav.navigate( '/' + ( hasStage ? hasStage + '/' : '' ) + CorUI.info.previousView );
			},
			'/continue' : CorD.start,
			'/start' : CorD.boot,
			'/coronis' : function() {
				CorUI.go( 'start', 'start' );
				CorD.HUD.close();
			},
			'/' : function() {}
		});
		CorNav.start( '/' + ( !Coronis.storage.startView ? 'coronis' : Coronis.storage.startView ) );
		return CorNav;
	};
	
	if ( window.addEventListener ) {
		window.addEventListener( 'resize', Coronis.react, false );
		window.addEventListener( 'scroll', Coronis.UI.scroll, false );
	} else if ( window.attachEvent ) {
		window.attachEvent( 'onresize', Coronis.react );
		window.attachEvent( 'onscroll', Coronis.UI.scroll );
	} else {
		window.onresize = Coronis.react();
		window.onscroll = Coronis.UI.scroll();
	}

	Coronis.react().Diary.init();

} ( window.jQuery );