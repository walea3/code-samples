/* 
 * Street Fighter X Tekken - Facebook Fighter
 * Author: Adewale Adeyemi - MiyoMint
 */ 

(function() {
	'use strict';
	
	window.onYouTubePlayerAPIReady = function() {
		SF.YT.player = new YT.Player(SF.YT.id, { width:'100%', height:300, videoId: SF.YT.load, playerVars: {
	            modestbranding: 1,
	            wmode: "opaque"
	        } });
		SF.YT.loaded = 1;
	};
	
	SF.css = { HIDE: 'visuallyhidden' };
	
	SF.loading = function ( on ) {
		if ( on ) {
			if ( !$('.loading')[0] ) {
				$('<div class="loading modal" />').modal({ backdrop: false });
			}
			$('.loading').modal('show');
		} else {
			setTimeout(function(){ $('.loading').modal('hide'); },550);
		}
	};
	
	/*
	 *	React / Adapt
	 */
	SF.react = function() {
		clearTimeout(SF.timer);
		SF.timer = setTimeout(SF.adapt, 16);
		return this;
	};
	
	SF.adapt = function() {
		/*
SF.appWidth = document.documentElement.clientWidth || document.body.clientWidth || (window.innerWidth - 15) || 0;
		SF.appView = (SF.appWidth > 960 || $('.lt-ie9')[0] ) ? 'full' : (SF.appWidth > 640 ? 'full tablet' : 'mobile');
*/
		
		/*$('html,body').removeClass('mobile full tablet').addClass(SF.appView);
		
		$( (SF.appView === 'mobile' ? '.mobileOnly:not(.' + SF.css.HIDE + '), .mobileShow:not(.' + SF.css.HIDE + ')' : '.mobileHide, .tabletHide') ).removeClass(SF.css.HIDE);
		$( ( SF.appView.indexOf('full') >= 0 ? ', .mobileOnly' : ', .mobileHide') + ( SF.appView.indexOf('tablet') >= 0 ? ', .tabletHide' : '') ).addClass(SF.css.HIDE);*/
					
		return this;
	};
	
	//Facebook
	SF.FB = {
		init: function() {
			var me = this;
			//me.loggedOut();
			window.fbAsyncInit = function() {
				FB.init({
					appId: SF.config.FB.id,
					channelUrl: SF.config.baseUrl + 'public/channel.html',
					status: true,
					cookie: true,
					xfbml: true,
					frictionlessRequests: true
				});
				
				//social share info / timeline
				if ($('#leaderboards').length) {
					SF.config.FB.Share.caption = 'View the leaderboard for all ' + SF.config.LANG.app_title + ' matches';
					SF.config.FB.Share.link = SF.config.FB.Share.link + '/leaderboards';
					SF.config.FB.Share.picture = SF.config.FB.Share.picture.replace('http://','');
				} else if ($('#scores').length) {
					var bestTitle = ($('.bestTitle .match').text() != 'No Wins' ? ' and my best title is "' + $('.bestTitle .match').text() + '"' : ''),
						worldRank = $('#worldRank .heading').text().replace(':','No.'),
						pts = $('.userInfo .pts').text();
					SF.config.FB.Share.caption = 'I\'m ' + worldRank + ', with ' + pts + bestTitle + '. Think you can beat me?';
					SF.config.FB.Share.link = SF.config.FB.Share.link + '/scores';
					SF.config.FB.Share.picture = SF.config.FB.Share.picture.replace('http://','');
				} else if ($('#result').length) {
					
					var friend = ($('#lifeBar .tk .name .fb_link').text() || 'a friend');
					SF.config.FB.Share.caption = 'I just ' + ( matchResult==='draw' ? 'drew against' : (matchResult==='win' ? 'beat' : 'lost to') ) + ' ' + friend + ' in a ' + matchName + ' battle!';
					SF.config.FB.Share.link = SF.config.FB.Share.link + '/results';
					SF.config.FB.Share.picture = SF.config.assetUrl.replace('http://','') + 'img/match_' + matchTitle + '.jpg';
					
					/*
setTimeout(function(){ SF.FB.Timeline({ 
						action: matchResult, 
						object: 'fight',
						image: SF.config.FB.Share.picture,
						link: SF.config.baseUrl + 'og/fight/' + matchTitle
					}); }, 1000);
*/
				}
			
				$(".social .fb").click(function(e) {
					if ($('#result').length) {
						SF.config.FB.Share.caption = 'I just ' + ( matchResult==='draw' ? 'drew against' : (matchResult==='win' ? 'beat' : 'lost to') ) + ' ' + ($('#lifeBar .tk .name .fb_link').text() || 'a friend') + ' in a ' + matchName + ' battle!';
					}
					e.preventDefault();
					me.share.feed();
		  		});
		  		
				$(".social .tw").popupWindow({ 
					windowURL:'https://twitter.com/intent/tweet?wrap_links=true&text=' + escape(SF.config.FB.Share.caption) + '&url=' + encodeURIComponent(SF.config.FB.Share.link) + '&via=StreetFighter&related=Capcom_Unity&hashtags=SFxT,FriendFighter', 
					centerScreen:1,
					windowName:'twttr',
					height:252, 
					width:550
				});
		  		
				$(".social .gplus").popupWindow({ 
					windowURL:'https://plus.google.com/share?url=' + encodeURIComponent(SF.config.FB.Share.link), 
					centerScreen:1,
					windowName:'gplus',
					height:600, 
					width:600
				});
				
				FB.Event.subscribe('edge.create', function(response) {
					window.top.location = SF.config.FB.postUrl;
				});
				
				FB.Event.subscribe('auth.statusChange', function(response) {
					if ( response.authResponse ) {
						SF.FB.connect.toApp();
					}
				});
				
				if ($('#match').length) {
					SF.Battle.init();
				}
				
				FB.Canvas.setAutoGrow();
				me.ready = true;
			};
			return this;
		},
		onReady: function( callback ) {
			if(!SF.FB.ready) {
				setTimeout(function() {
					SF.FB.onReady(callback);
				}, 50);
			} else {
				if(callback) {
					callback();
				}
			}
		},
		connect: {
			toFb: function() {
				FB.login(function(response) {
					if (response.authResponse) {
						SF.FB.connect.toApp();
					} else {
						SF.FB.loggedOut();
					}
				}, {scope: 'email, publish_stream, publish_actions, user_likes, friends_likes, user_photos, friends_photos, user_status, friends_status, user_photo_video_tags, friends_photo_video_tags, user_events, friends_events'});
				return this;
			},
			toApp: function(e) {
				if (e) { 
					e.preventDefault();
				}
				FB.api('/me/permissions', function (perms) {
					perms = perms.data[0];
					if ( perms.email === 1 && perms.publish_stream === 1 && perms.publish_actions === 1 && perms.user_likes === 1 && perms.friends_likes === 1 && perms.user_photos === 1 && perms.friends_photos === 1 && perms.user_status === 1 && perms.friends_status === 1 && perms.user_photo_video_tags === 1 && perms.friends_photo_video_tags === 1 && perms.user_events === 1 && perms.friends_events === 1 ) {
						FB.api('/me', function(me) {
							if (me.first_name) {
								SF.config.user = SF.config.user || {};
								SF.config.user.loggedin = true;
								SF.config.user.fbid = me.id;
								SF.config.user.username = me.username;
								SF.config.user.firstname = me.first_name;
								SF.config.user.lastname = me.last_name;
								SF.config.user.gender = me.gender;
								
								$.post(SF.config.apiUrl + 'user/' + me.id, 
									function(data) {
										SF.config.user.id = parseInt(data.id, 10);
										SF.FB.loggedIn();
								}, 'json');
							}
						});
					} else {
						SF.FB.loggedOut(($('#match').length ? true : null));
					}
				});
				
				return this;
			}
		},
		share: {
			feed: function(e) {
				if (e) { 
					e.preventDefault();
				}
				FB.ui(SF.config.FB.Share, SF.FB.share.fbCallback);
				return this;
			},
			send: function( to, name, link ) {
				FB.ui({
					method: 'send',
					to: to || null,
					name: name || SF.config.FB.title,
					link: link || SF.config.baseUrl
				}, SF.FB.share.fbCallback);
				return this;
			},
			request: function() {
				FB.ui({
					method: 'apprequests',
					title: SF.config.FB.title,
					message: SF.config.FB.caption
				}, SF.FB.share.fbCallback);
				return this;
			},
			fbCallback: function(response) {
				if (response && (response.post_id || response.request)) {
					if (!response.request) {
						SF.config.user.shared = true;
					}
				}
			}
		},
		friendUsers: function( callback ) {
			FB.api({ method: 'friends.getAppUsers' }, callback);
		},
		Timeline: function (opts) {
			var action = opts.action,
				obj = {};
				obj[opts.object] = opts.link;
			setTimeout(function(){
				FB.api('/me/' + SF.config.FB.namespace + ':' + action, 'post', obj, function( response ) {
					if ( response.id ) {
						SF.config.user.actions = SF.config.user.actions || [];
						SF.config.user.actions.push(response.id);
					}
				});
			}, 1500);
		},
		logout: function() {
			FB.logout(SF.FB.loggedOut());
			return this;
		},
		loggedIn: function () {
			SF.config.user.loggedin = false;
			$('body').addClass('loggedin');
			$('[data-sub-login=false]').addClass(SF.css.HIDE + ' out').removeClass('in');
			$('[data-sub-login=true]').removeClass(SF.css.HIDE + ' out').addClass('in');
			$('.logout').live('click', function (e) {
				e.preventDefault();
				SF.FB.logout();
			});
		},
		loggedOut: function (redir) {
			if (redir && window.location.href !== SF.config.baseUrl + SF.config.chosen) {
				window.location.href = SF.config.baseUrl + SF.config.chosen;
			}
			SF.config.user.loggedin = false;
			$('body').removeClass('loggedin');
			$('[data-sub-login=true]').removeClass('in').addClass(SF.css.HIDE + ' out');
			$('[data-sub-login=false]').addClass('in').removeClass(SF.css.HIDE + ' out');
		},
		friendSelector: function() {
			var me = this;
	        $('#TDFriendSelector_buttonOK').unbind('click').bind('click', function(e){
	        	e.preventDefault();
	        	if (typeof TDFriendSelector.callbackSubmit === "function") { TDFriendSelector.callbackSubmit(selectedFriendIds); }
	        });
	        TDFriendSelector.init();
	        this.selectFriend = TDFriendSelector.newInstance({
	            callbackSubmit           : function(selectedFriendIds) {
	            	if (selectedFriendIds.length) {
	            		SF.Battle.fightData.friend = selectedFriendIds.join("/");
						SF.Battle.step(2);
					} else { 
						me.selectFriend.showFriendSelector();
					}
	            },
				maxSelection             : 1,
				friendsPerPage           : 10,
				autoDeselection          : true
	        });
	        this.selectFriend.showFriendSelector();
		}
	};
	
	SF.Battle = {
		init: function () {
			$('a[href=#step]').live('click', function(e){
				e.preventDefault();
				var dir = $(this).data('stepDir'),
					step = (dir == 'next' ? SF.Battle.currentStep + 1 : SF.Battle.currentStep - 1);
				if (step > 0) {
					SF.Battle.step(step);
				} else {
					window.location.href = SF.config.baseUrl + SF.config.chosen;
				}
			});
			$('a[href=#fight]:not(.fought)').live('click', function(e){
				e.preventDefault();
				SF.Battle.fightData.match = $(this).addClass('active').data('matchId');
				SF.Battle.fight();
			});
			if (typeof currentStep !== 'undefined' && currentStep == 2 && selectedFreind) {
				SF.Battle.fightData.friend = selectedFreind;
				SF.Battle.step(2);
			} else {
				SF.Battle.step(1);
			}
			return this;
		},
		currentStep: 0,
		fightData: { friend: '', match: '' },
		fight: function () {
			var me = this;
			if (me.fightData.friend !== '' && me.fightData.match !== '') {
	        	me.step( 3 );
				$.post(SF.config.baseUrl + 'api/fight/' + me.fightData.match, me.fightData, function(data) {
					if (data == 1) {
						setTimeout(function() {
							location.href = SF.config.baseUrl + SF.config.chosen + '/result';
						}, 2000);
					} else {
						SF.Battle.step(1);
					}
				});
			}
		},
		matchesFought: function() {
			var me = this;
			$('[data-match-id]').removeClass('fought');
			$.post(SF.config.baseUrl + 'api/fought/' + SF.config.user.fbid + '/' + me.fightData.friend, function(data) {
				if (data) {
					var i, f;
					for (i = 0, f = data.length; i < f; i++) {
						$('[data-match-id=' + data[i].match_id + ']').addClass('fought');
					}
				}
			});			
		},
		step: function ( step ) {
			if ($('[data-step=s' + step +']').length && step !== this.currentStep) {
				$('.ring.s' + (step !== 3 ? step : '')).addClass('active');
				$('.ring.s' + (step+1)).removeClass('active');
				$('.help').attr('href', '#help');
		        $('body').removeClass('stepOne');
				
				switch (step) {
					case 1:
		            	SF.FB.friendSelector();
		            	$('.back, .help').removeClass(SF.css.HIDE);
		            	$('header.top .heading').text(SF.config.LANG.copy_heading_1);
		            	$('#progressBar .bar').stop().animate({width:'23%'}, 2000);
		            	$('body').addClass('stepOne');
						break;
					case 2:
						this.matchesFought();
		            	$('.back, .help').removeClass(SF.css.HIDE);
		            	$('header.top .heading').text(SF.config.LANG.copy_heading_2);
		            	$('#progressBar .bar').stop().animate({width:'53%'}, 2000);
						break;
					case 3:
		            	$('.back, .help').addClass(SF.css.HIDE);
		            	$('header.top .heading').text(SF.config.LANG.text_fighting);
		            	$('#progressBar .bar').stop().animate({width:'78%'}, 5000);
						break;
				}
				
				this.currentStep = step;
				setTimeout(function() {
					$('[data-step]:not([data-step=s' + step +'])').stop().animate({opacity:0}, 200, function(){
						$(this).addClass(SF.css.HIDE);
						$('[data-step=s' + step +']').css('opacity',0).removeClass(SF.css.HIDE).show().stop().animate({opacity:1}, 350);
					}); 
				}, 100);
			}
			return this;
		}
	};
	
	if (window.addEventListener) {
		window.addEventListener('resize', SF.react, false);
	} else if (window.attachEvent) {
		window.attachEvent('onresize', SF.react);
	} else {
		window.onresize = SF.react();
	}
	
	/*
	 * App init
	 */
	SF.init = function() {
		this.FB.init();
		
		//facebook connect
		$('.fbConnect .btn').live('click', this.FB.connect.toFb);
		
		$('[rel=tooltip]').tooltip({ placement: 'bottom' });
		$('#mainModal').modal({ keyboard: false, show: false });
		
		if ( $('a.modalContent').length ) {
			$('a.modalContent').live('click', function(e) {
				e.preventDefault();
				$('#mainModal .modal-body').html( $($(this).attr('href')).html() )
					.prepend('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>')
					.parent().modal('show').on('hidden', function (e) {
						$(e.currentTarget).children('.modal-body').empty();
					});
					$('#mainModal .modal-body').find('h3').attr('id', 'mainModalLabel');
			});
		}
		
		if ($('.scroller').length) {
			$('.scroller').flexslider({
				animation: "slide",
				animationLoop: false,
				itemWidth: 108,
				itemMargin: 0,
				minItems: 1,
				prevText: '<span class="arr"><</span>',
				nextText: '<span class="arr">></span>',
				controlsContainer: $('.scroller .paginationContainer')
			}).flexslider('pause');
		}
		
		//Hijack Links
		$(document).delegate("a:not(.handled,[target=_blank],.lang)", "click", function(e) {
			var href = $(this).attr("href"),
				protocol = this.protocol + "//";
			if ($(this).hasClass('disabled')) { 
				return false; 
			} else if (href != null && href.slice(0, protocol.length) !== protocol && href !== '#') {
				return true;
			}
		});
		
		//animate life bar
		if ($('#result').length) {
			$("#lifeBar .sf .bar").delay(1000).stop().animate({'width':$("#lifeBar .sf .bar").data('progress')+'%'},1500);
			$("#lifeBar .tk .bar").delay(1000).stop().animate({'width':$("#lifeBar .tk .bar").data('progress')+'%'},1500);
		}
		
		if ( $('#leaderboards').length ) {
			$('#boards .tab-pane').jScrollPane({
				animateEase: 'swing',
				contentWidth: 637,
				autoReinitialise: true
			});
		}
		
		setTimeout(function() {
			SF.react();
		}, 16);
		
		return this;
	};

}());

$(function() {
	'use strict';
	SF.adapt().init();
});