/* 
 * Looper: TK Challenge
 * Author: Adewale Adeyemi
 */
(function() {
	'use strict';
	
	TKc.Tw = {
		options: { 
			limit: 0, 
			twAdd: TKc.config.baseUrl + ' #' + TKc.config.TW.tag + ' via @LooperUK', 
			twMaxLength: 160, 
			twLoaded: false, 
			twScroll: {},
			status: false
		},
		init: function() {
			var me = this,
				twScroll, opts;
			if( !me.options.twLoaded && $('.twList')[0]) {

				twScroll = $('.twList').jScrollPane({
					animateEase: 'swing',
					autoReinitialise: true
				}).data('jsp');
				opts = {
					query: '#' + TKc.config.TW.tag,
					join_text: "auto",
					avatar_size: 48,
					count: 20,
					loading_text: "loading tweets ...",
					template: "{avatar} <p>@{user} said {time}</p> {text}"
				};
				twScroll.getContentPane().tweet(opts);
				me.options.twLoaded = true;	

				TKc.Tw.updateTweetLimit();
				$('a.tweet')
					.live('mouseenter', function(){
						var twUrl = 'https://twitter.com/intent/tweet?url=' + TKc.config.baseUrl + '&text=' + encodeURIComponent($("#twitter-fld").val()) + '&via=LooperUK&hashtags=' + TKc.config.TW.tag;
						//$("a.tweet").attr('href', twUrl);
					})
					.live('click', TKc.Tw.postTweet);

				this.initTw();
			}
		},
		initTw: function() {
			twttr.anywhere.config({ callbackURL: TKc.config.baseUrl });
			twttr.anywhere(function (T)	{
				TKc.Tw.T = T;
			});
		},
		toApp: function(T){
			if ( !TKc.config.user.active ) {
				TKc.goto('select');
			} else {
				TKc.goto('tweet');
			}
		},
		selectFriends: {
			selected: {},
			init: function(){
				//get friends list
				if (TKc.config.user.friends.length < 1) {
					$.get(TKc.config.apiUrl + '/friendsList',
						function(data){
							TKc.config.user.friends = data;
							TKc.Tw.selectFriends.initList();
					}, "json");
				} else {
					this.initList();
				}
			},
			initList: function(){
				if (!$('.token-input-list')[0]) {
					$("#friendHandle").tokenInput(TKc.config.user.friends, {
						tokenLimit: 3,
						tokenValue: 'id',
						hintText: '',
						preventDuplicates: true,
						propertyToSearch: 'tw_handle',
						tokenFormatter: function(item) {
							return "<li class=\"badge badge-inverse\" data-id=\"" + item.id + "\" data-handle=\"" + item.tw_handle + "\" data-country=\""+ item.country + "\">@" + item.tw_handle + "</li>";
						},
						resultsFormatter: function(item) {
							return "<li>@" + item.tw_handle + "</li>";
						},
						onAdd: function(item){
							TKc.Tw.selectFriends.selected[item.id] = item;
						},
						onDelete: function(item){
							delete TKc.Tw.selectFriends.selected[item.id];
						}
					});
					$('a[href="#submit"], a[href="#skip"]').live('click', this.submit);
				}
			},
			submit: function(e){
				if (e) e.preventDefault();

				var i, count = 0, mentions = '';
				for (i in TKc.Tw.selectFriends.selected) {
					mentions += '@' + TKc.Tw.selectFriends.selected[i].tw_handle + ' ';
					count++;
				}

				TKc.Tw.options.twAdd += ' ' + mentions;
				$("#twitter-fld").val(mentions);
				TKc.goto('tweet');

				if (!TKc.config.user.active) {
					$.post(TKc.config.apiUrl + '/addTeam', {
						"friends": (count > 0 ? TKc.Tw.selectFriends.selected : { '0' : TKc.config.user })
					},
					function(data){
						TKc.config.user.active = true;
						TKc.coin.update();
					}, "json");
				}
			}
		},
		registerUser: function(e) {
			if (e) e.preventDefault();

			$('a[href="#reg"]').text('...');

			if (TKc.validate.requiredCheck($('.view[data-view=register] .box [required]'))) {
				$.post(TKc.config.apiUrl + '/registerUser', {
						"name": $('.view[data-view=register] #fullname').val(),
						"email": $('.view[data-view=register] #email').val(),
						"age": $('.view[data-view=register] #age').val(),
						"country": $('.view[data-view=register] #country').val(),
						"terms": $('.view[data-view=register] #terms').is(':checked')
					},
					function(data){
						$('a[href="#reg"]').text('Next');
						TKc.config.user.reg = true;
						TKc.goto('select');
				}, "json");
			} else {
				$('a[href="#reg"]').text('Next');
			}
		},
		postTweet: function(e) {
			if (e) e.preventDefault();

			if( $("#twitter-fld").val() == 'Type tweet ...' || $("#twitter-fld").val() == '' ) {
				$("#twitter-fld").val('').focus();
			} else {
				$("a.tweet").text('...');
				$.post(TKc.config.apiUrl + '/update', { 'msg' : $("#twitter-fld").val() },
					function(data){
						$("#twitter-fld").val('').focus();
						$("a.tweet").text('Sent');
						setTimeout(function(){
							$("a.tweet").text('Post');
							TKc.coin.update();
						}, 800);
				}, "json");
			}
		},
		updateTweetLimit: function() {
			this.options.limit = this.options.twMaxLength - this.options.twAdd.length;
			$("#twitter-fld")
				.prop("maxlength", this.options.limit)
				.limitMaxlength({
					onEdit: function() {
						$(".twCount").html( TKc.Tw.options.limit - $("#twitter-fld").val().length );
					}
			});
			return this;
		}
	};
	
	TKc.validate = {
		requiredCheck: function(requiredFields) {
			var emailReg = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i,
				nameReg = /^[a-zA-Z\.\,\'\-\s]*$/,
				phoneReg = /^[0-9\-\s]*$/,
				el, i, len;
			for (i = 0, len = requiredFields.length; i < len; i++) { 
				el = requiredFields[i];
				
				if (($(el).hasClass('name') && !nameReg.test(el.value)) || 
					($(el).hasClass('tel') && !phoneReg.test(el.value)) || 
					($(el).hasClass('email') && !emailReg.test(el.value)) || 
					($(el).hasClass('date') && !dateCheck(el)) ||
					($(el).hasClass('opts') && !TKc.multipleCheck($(el).parents('form')[0], $(el).find('input').attr('name'))) || 
					($(el).attr('type') === 'checkbox' && $(el).is(':checked') !== true) || 
					($(el).attr('type') !== 'checkbox' && !$(el).hasClass('opts') && $.trim(el.value) === '')) 
				{
					$(el).focus().parents('label').addClass('error');
					return false;
				} else {
					$(el).parents('label').removeClass('error');
				}
			}
			return true;
		},
		compareFieldCheck: function(el1, el2) {
			if (el2.val() !== el1.val()) {
				el2.focus().addClass('error').prev().addClass('error');
				return false;
			} else { 
				el2.removeClass('error').prev().removeClass('error');
				return true;
			}
		},
		dateCheck: function(el) {
			var dmy = el.value.split('/'),
				day = parseInt(dmy[0]),
				month = parseInt(dmy[1]),
				year = parseInt(dmy[2]),
				appointDate = new Date(year, month - 1, day),
				today = new Date();

			return appointDate > today ? true : false;
		},
		multipleCheck: function(frm, name) {
			for (var i=0; i < frm.length; i++)
			{
				fldObj = frm.elements[i];
				if (fldObj.name === name) {
					if (fldObj.checked) {
						return true;
					}
				}
			}
			return false;
		}
	};

	TKc.react = function() {
		clearTimeout(TKc.timer);
		TKc.timer = setTimeout(TKc.adapt, 16);
		return this;
	};
	
	TKc.adapt = function() {
		if ($('.lt-ie9')[0]) {
			REME.appWidth = document.documentElement.clientWidth || document.body.clientWidth || (window.innerWidth - 15) || 0;
			REME.appView = (REME.appWidth > 960 || $('.lt-ie9')[0]) ? 'full' : (REME.appWidth > 640 ? 'full tablet' : 'mobile');

			$('html,body').removeClass('mobile full tablet').addClass(REME.appView);
	/*$( (REME.appView === 'mobile' ? '.mobileOnly:not(.' + REME.css.HIDE + '), .mobileShow:not(.' + REME.css.HIDE + ')' : '.mobileHide, .tabletHide') ).removeClass(REME.css.HIDE);
			$( ( REME.appView.indexOf('full') >= 0 ? ', .mobileOnly' : ', .mobileHide') + ( REME.appView.indexOf('tablet') >= 0 ? ', .tabletHide' : '') ).addClass(REME.css.HIDE);*/
		}
		return this;
	};

	if (window.addEventListener) {
		window.addEventListener('resize', TKc.react, false);
	} else if (window.attachEvent) {
		window.attachEvent('onresize', TKc.react);
	} else {
		window.onresize = TKc.react();
	}
	
	TKc.init = function() {
		setTimeout(function() {
			TKc.react();
		}, 16);
		if (TKc.config.compState !== 'closed') {
			TKc.goto((TKc.config.user.active && TKc.config.user.reg ? 'tweet' : (TKc.config.user.id ? (!TKc.config.user.reg ? 'register' : 'select') : 'start')));
			TKc.clock.init();
			$('a[href="#reg"]').live('click', this.Tw.registerUser);
		} else {
			TKc.goto('start');
		}
		TKc.coin.init();

		return this;
	};
	
	TKc.goto = function( view ) {
		var el = $('.view[data-view='+ view +']');
		if ( el[0] ) {
			$('.view[data-view]').removeClass('active').hide();
			$('.view[data-view='+ view +']').addClass('active').show();

			if (!TKc.config.user.reg && $.inArray(view, ['select', 'tweet']) >= 0) {
				TKc.goto('register');
				return this;
			}

			switch (view){
				case 'select':
					if (TKc.config.user.active) {
						this.goto('tweet');
					} else {
						this.Tw.selectFriends.init();
					}
					break;
				case 'tweet':
					this.Tw.init();
					break;
			}

			if ( el.data('info') ) {
				$('header[role="banner"] p.info').html(el.data('info'));
			} else {
				$('header[role="banner"] p.info').html($('header[role="banner"] p.info').data('info'));
			}
		}
		return this;
	};

	TKc.coin = {
		aniInterval: null,
		init: function() {
			$('#coin').stop()
				.animate({'bottom':'0%'}, 1200)
				.animate({'bottom':'-10%'}, 800)
				.animate({'bottom':'10%'}, 1100, function(){
					setTimeout(function(){
						TKc.coin.update();
					}, 1000);
				})
				.delay(100)
				.animate({'bottom':'5%'}, 500)
				.animate({'bottom':'-2%'}, 1000, function(){
					TKc.coin.loop();
				});
				return this;
		},
		update: function() {
			$.get(TKc.config.apiUrl + '/coinStatus',
				function(data){
					TKc.config.user.coinStatus = data;
					if (TKc.config.user.coinStatus > 0) {
						$('#progress').animate({opacity: 1, left: '10%'}, 1200, function(){
							TKc.coin.push(TKc.config.user.coinStatus);
						});
					}
			}, "json");
			return this;
		},
		push: function( to ) {
			var position = (to / (($('#progress').height() - 58) / 100));
			$('#coin').animate({'margin-bottom':  position }, 350);
			return this;
		},
		loop: function(){
			this.aniInterval = setInterval(function(){
				$('#coin')
					.animate({'bottom':'-6%'}, 900)
					.animate({'bottom':'-2%'}, 700)
					.animate({'bottom':'-6%'}, 900);
			}, 3500);
			return this;
		},
		stop: function(){
			clearInterval(this.aniInterval);
			$('#coin').stop();
			return this;
		}
	};

	TKc.clock = {
		init: function(){
			$('#countdown').countdown({
				until: TKc.config.sessionEnd,
				compact: true, 
				format: 'HMS',
				alwaysExpire: true,
				layout: '<div id="timer">'+
							'<div id="labels">'+
								'<div id="hl" class="lab">hours</div>'+
								'<div id="ml" class="lab">mins</div>'+
								'<div id="sl" class="lab">secs</div>'+
							'</div>'+
							'<div id="values">'+
								'<div id="hv" class="num">{hnn}</div>'+
								'<div id="mv" class="num">{mnn}</div>'+
								'<div id="sv" class="num">{snn}</div>'+
							'</div>'+
							'<div class="overlay"></div>'+
						'</div>'
			});
			return this;
		}
	};
}());

$(function() {
	'use strict';
	TKc.react().init();
});