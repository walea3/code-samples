/* 
 * Resident Evil: No Hope Left - C-Virus
 * Author: Adewale Adeyemi - MiyoMint
 */ 
(function() {
	'use strict';
	Cvr.zombifyMe = {
		options: Cvr.config.zombifyMe,
		test: function(pre) {
			return this.zombify( this.options.pre[pre] );
		},
		zombify: function(normal) {
			var zombified, 
				zombifiedArr = normal.split(' '),
				iZ = zombifiedArr.length, 
				x3Count = 3, x4Count = 1, tagPosition = 0;
			
			for (; iZ > 0; iZ--) {
				var zf = zombifiedArr[iZ-1], changeLetter, regX,
					ix3 = this.options.x3.length,
					ix4 = this.options.x4.length,
					iCh = this.options.change.length;
				
				if (zf.charAt(0) !== "@" && zf.charAt(0) !== "#") {
					//exceptions
					
					//x3
					for (; ix3 > 0; ix3--) {
						regX = new RegExp( this.options.x3[ ix3-1 ], "i" );
						if ( zf.length < (this.options.maxCount - 3) && x3Count > 0 && zf.indexOf(this.options.x3[ ix3-1 ]) >= 0 ) {
							zf = zf.replace( regX, Array( 4 ).join( this.options.x3[ ix3-1 ] ) );
							x3Count--;
						}
					};
					
					//x4
					for (; ix4 > 0; ix4--) {
						regX = new RegExp( this.options.x4[ ix4-1 ], "i" );
						if ( zf.length < (this.options.maxCount - 4) && x4Count > 0 && zf.indexOf(this.options.x4[ ix4-1 ]) >= 0 ) {
							zf = zf.replace( regX, Array( 5 ).join( this.options.x4[ ix4-1 ] ) );
							x4Count--;
						}
					};
					
					//change
					for (; iCh > 0; iCh--) {
						changeLetter = this.options.change[ iCh-1 ];
						regX = new RegExp( changeLetter[0], "gi" );
						zf = zf.replace( regX, changeLetter[2] );
					};
					
					zombifiedArr[iZ-1] = zf;
				}
			};
			
			//zombieTags
			tagPosition = Math.floor( Math.random() * zombifiedArr.length );
			zombifiedArr.splice( tagPosition, 0, this.options.zombieTags[ Math.floor( Math.random() * this.options.zombieTags.length ) ] );
			
			//finalTag
			zombifiedArr.push(this.options.finalTag);
			
			zombified = zombifiedArr.join(' ');
			return zombified;
		}
	};
	
	Cvr.Twirus = {
		options: { 
			limit: 0, 
			twAdd: 're6', 
			twMaxLength: 110, 
			twLoaded: false, 
			twScroll: {},
			status: false,
			twOpts: {
	    		searchParams: ['q=' + encodeURIComponent('http://bit.ly/nhl-cv')],
	    		count: 50,
	    		showTweetFeed: {
	        		showProfileImages: true,
	        		showUserScreenNames: true,
	            	showActionReply: true,
		            showActionRetweet: true
	    		},
	    		onDataRequestHandler: function(stats) {
	        		if (stats.dataRequestCount < 11) { return true; }
	    		}
			}
		},
		init: function() {
			var me = this;
			if( !me.options.twLoaded ) {
				$('#twtDrawer .twList').jTweetsAnywhere(me.options.twOpts);
				me.options.twLoaded = true;
				
				me.options.scrInterval = setInterval(function(){
					var sc = Cvr.drawer.hScroller;
		        	if ( $('#twtDrawer .twList .jta-tweet-list li').length > 3 ) {
		        		setTimeout(function() {
		        			sc.init(); 
		        		}, 5000);
		        		clearInterval(me.options.scrInterval);
		        	}
		        },300);
			}
		},
		initTw: function() {
			var userOpts = this.options.twOpts,
				twScroll = this.options.twScroll;
				delete userOpts.searchParams;
			twttr.anywhere.config({ callbackURL: Cvr.config.baseUrl });
			twttr.anywhere(function (T)	{
				Cvr.Twirus.T = T;
				if ( !T.isConnected() ){
					T.signIn();
					$('.full #connect .ctrl a').click();
				}
				
				T.bind("authComplete", function (e, user) {
					if (Cvr.appStatus !== 'readonly') {
						Cvr.drawer
							.close( $('#connect'), 'top', true )
							.open( $('#tweet'), 'top', true );
						$('header a.warn').parent().next().removeClass('mobileHide').show().prev().remove();
						$('#twtDrawer .content .ctrl a.cl, a.infect').click();
					}
					Cvr.config.user.id = T.currentUser.data('id');
					Cvr.config.user.name = T.currentUser.data('name');
					Cvr.config.user.screenName = T.currentUser.data('screen_name');
					Cvr.config.user.image = T.currentUser.data('profile_image_url');
					if ( T.currentUser.data('geo_enabled') ) {
						Cvr.config.user.location = T.currentUser.data('location');
					}
					
					userOpts.username = T.currentUser.data('screen_name');
					twScroll = $('#tweet .extra .twList').jScrollPane({animateEase:'swing', contentWidth:280, autoReinitialise:true}).data('jsp');
					twScroll.getContentPane().jTweetsAnywhere(userOpts);
					$('#tweet .mobileOnly .twList').jTweetsAnywhere(userOpts);
	
					Cvr.Twirus.reloadTweet();
					
					$('.reload').live('click', function(e){
						e.preventDefault();
						Cvr.Twirus.reloadTweet();
					});
					
					$("#zombifyMe").live('click', function(e) {
						e.preventDefault();
						if ( !Cvr.Twirus.options.status ) {
							Cvr.Twirus.options.status = true;
							Cvr.Twirus.postTweet();
						}
					});
					Cvr.react();
				});
			});
		},
		reloadTweet: function() {
			$(".zombify").val( Cvr.zombifyMe.options.pre[ Math.floor( Math.random() * Cvr.zombifyMe.options.pre.length ) ] );
			this.updateTweetLimit();
		},
		postTweet: function() {
			if( $(".zombify").val() == 'Type tweet ...' || $(".zombify").val() == '' ) {
				$(".zombify").val('').focus();
			} else {
				var cloneBtn = $("#zombifyMe").clone(),
					zombified = Cvr.zombifyMe.zombify( $(".zombify").val() );
				$("#zombifyMe").text('Sending...');
				
				this.T.Status.update( zombified + " http://bit.ly/nhl-cv", {
					success: function(){
						$.post(Cvr.config.apiUrl + 'store', { 
								"id": Cvr.config.user.id,
								"username": Cvr.config.user.screenName,
								"msg": zombified,
								"pic": Cvr.config.user.image,
								"lat": Cvr.config.user.lat, 
								"long": Cvr.config.user.long,
								"country": Cvr.config.user.country 
							},
							function(data){
								Cvr.Twirus.reloadTweet();
								setTimeout(function() {
									$("#zombifyMe").replaceWith(cloneBtn);
									Cvr.Twirus.options.status = false;
									setTimeout(function() { $('#tweet .ctrl a').click(); }, 200);
								}, 500);
								if (Cvr.appView == "mobile") {
									$('html, body').scrollTop($('#infectedMap').offset().top);
								}
								Cvr.Map.createMarker( 
									new google.maps.LatLng( parseFloat(Cvr.config.user.lat), parseFloat(Cvr.config.user.long) ), 
									Cvr.config.user.image, Cvr.config.user.screenName,
									zombified, Cvr.config.user.id, true
								);
						}, "json");
					},
					error: function(){
						$("#zombifyMe").replaceWith(cloneBtn);
						Cvr.Twirus.options.status = false;
					}
				});
			}
		},
		updateTweetLimit: function() {
			this.options.limit = this.options.twMaxLength - this.options.twAdd.length;
			$(".zombify")
				.prop("maxlength", this.options.limit)
				.limitMaxlength({
					onEdit: function() {
						$(".twCount").html( Cvr.Twirus.options.limit - $(".zombify").val().length );
					}
			});
			return this;
		}
	};
	
	Cvr.Map = {
		options: {
			zoom: 2,
			markIcon: function() {
				return new google.maps.MarkerImage(Cvr.config.assetUrl + 'img/cvr_mrk.png',
					new google.maps.Size(57, 69),
					new google.maps.Point(0,0),
					new google.maps.Point(29, 69));
			},
			markShdw: function() {
				return new google.maps.MarkerImage(Cvr.config.assetUrl + 'img/cvr_mrk_shdw.png',
					new google.maps.Size(95, 69),
					new google.maps.Point(0,0),
					new google.maps.Point(29, 69));
			},
			markShape: { 
				coord: [51,0,54,1,55,2,55,3,56,4,56,5,56,6,56,7,56,8,56,9,56,10,56,11,56,12,56,13,56,14,56,15,56,16,56,17,56,18,56,19,56,20,56,21,56,22,56,23,56,24,56,25,56,26,56,27,56,28,56,29,56,30,56,31,56,32,56,33,56,34,56,35,56,36,56,37,56,38,56,39,56,40,56,41,56,42,56,43,56,44,56,45,56,46,56,47,56,48,56,49,56,50,56,51,56,52,56,53,55,54,55,55,54,56,35,57,34,58,33,59,32,60,32,61,31,62,30,63,30,64,29,65,29,66,28,67,28,68,27,68,27,67,26,66,26,65,25,64,25,63,24,62,23,61,23,60,22,59,21,58,20,57,2,56,1,55,0,54,0,53,0,52,0,51,0,50,0,49,0,48,0,47,0,46,0,45,0,44,0,43,0,42,0,41,0,40,0,39,0,38,0,37,0,36,0,35,0,34,0,33,0,32,0,31,0,30,0,29,0,28,0,27,0,26,0,25,0,24,0,23,0,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,0,13,0,12,0,11,0,10,0,9,0,8,0,7,0,6,0,5,0,4,0,3,1,2,2,1,4,0,51,0],
				type: 'poly' 
			},
			clusterStyle: [{
				url: Cvr.config.assetUrl + 'img/1.png', height: 38, width: 38, 
				anchor: [12, 0], textColor: '#ffffff', textSize: 13
				}, {
				url: Cvr.config.assetUrl + 'img/2.png', height: 67, width: 58,
				anchor: [24, 0], textColor: '#ffffff', textSize: 13
				}, {
				url: Cvr.config.assetUrl + 'img/3.png', height: 80, width: 81,
				anchor: [31, 0], textColor: '#ffffff', textSize: 15
			}],
			infectedMapStyles: [{
				featureType: 'water',
				elementType: 'geometry',
				stylers: [
					{ hue: '#8b0003' },
					{ saturation: 100 },
					{ lightness: -64 },
					{ visibility: 'on' }
				]
			},
			{
				featureType: 'water',
				elementType: 'labels',
				stylers: [
					{ hue: '#ffffff' },
					{ saturation: -100 },
					{ lightness: 100 },
					{ visibility: 'on' }
				]
			}]
		},
		markerClusterer: null,
		markers: [],
		infoWindow: {
			draw: function(data) {
				var me = this,
					_infW = $('<div class="infoWindow" />').css({opacity: 0, left: '65px'}),
					infW = $('<div class="infoWrap" />').appendTo(_infW),
					infWClose = $('<a href="#" class="closeWindow" />').appendTo(infW);
					
				$('<strong class="title" />').html(data.name).appendTo(infW);
				$('<div class="msg" />').html(data.msg).appendTo(infW);
				
				$('.closeWindow').live('click', function(e) {
					e.preventDefault();
					me.remove();
				});
				
				return _infW;
			},
			remove: function() {
				$('.infoWindow').animate({opacity:0, left: '65px'}, 500, function(){
					$(this).remove();
					$('.userImg .active').removeClass('active');
				});
			}
		},
		init: function() {
		/* Cvr Marker */
		var parent = Cvr.Map;
			parent.mark = function(opt_options) {
				this.setValues(opt_options);
				
				var span = this.span_ = document.createElement('span'),
					div = this.div_ = document.createElement('div'),
					imgWrap = document.createElement('div'),
					img = document.createElement("img"),
					wrapLink = document.createElement("div");
                          
				div.className = 'userImg';
				wrapLink.className = 'wrap link';
				imgWrap.className = 'imgWrap';
				span.className = 'markOverlay';
				
				img.src = this.get('image').toString();
				wrapLink.id = 'infected_' + this.get('cvr_id');
				wrapLink.href = "#";
				
				wrapLink.appendChild(span);
				imgWrap.appendChild(img);
				wrapLink.appendChild(imgWrap);
				div.appendChild(wrapLink);
								
				div.style.cssText = 'position: absolute; display: none';
			};
			 
			parent.mark.prototype = new google.maps.OverlayView;
			 
			parent.mark.prototype.onAdd = function() {
				var pane = this.getPanes().overlayImage,
					me = this;
				pane.appendChild(this.div_);
				
				this.listeners_ = [
					google.maps.event.addListener(this, 'position_changed',
						function() { me.draw(); }),
					google.maps.event.addListener(this, 'text_changed',
						function() { me.draw(); }),
					google.maps.event.addListener(this, 'zindex_changed',
						function() { me.draw(); })
				];
			};
			
			parent.mark.prototype.onRemove = function() {
			     this.div_.parentNode.removeChild(this.div_);
			 
			     for (var i = 0, I = this.listeners_.length; i < I; ++i) {
			          google.maps.event.removeListener(this.listeners_[i]);
			     }
			};
			
			parent.mark.prototype.draw = function() {
				var projection = this.getProjection(),
					position = projection.fromLatLngToDivPixel(this.get('position')),
					div = this.div_;
				
				div.style.left = (position.x - 29) + 'px';
				div.style.top = position.y + 'px';
				div.style.display = 'block';
				div.style.zIndex = this.get('zIndex') + 10;
				
			};
		/* END Cvr Marker */
		
			/*
(function() {
		    	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
		    	po.src = Cvr.config.baseUrl + 'js/libs/gv3.infobox.js';
		    	//po.src = Cvr.config.baseUrl + 'public/frontend/global/js/libs/gv3.infobox.js';
		    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
		  	})();
*/
	  	
			if( navigator.geolocation ) {
				navigator.geolocation.getCurrentPosition(function( position ) {
					Cvr.config.user.lat = position.coords.latitude;
					Cvr.config.user.long = position.coords.longitude;
				});
			}
			
			Cvr.map = new google.maps.Map(document.getElementById("map"), {
		        zoom: 3, minZoom: 3, streetViewControl: false,
		        center: new google.maps.LatLng(Cvr.config.user.lat, Cvr.config.user.long), mapTypeId: 'Infected',
		        mapTypeControl: false, scrollwheel: false,
		        zoomControl: true, zoomControlOptions: { 
		        	style: google.maps.ZoomControlStyle.LARGE, 
		        	position: google.maps.ControlPosition.TOP_RIGHT
		        },
				mapTypeControlOptions: {
					mapTypeIds: [ 'Infected', google.maps.MapTypeId.ROADMAP ],
					style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
				}
		    });
		    
			parent.options.styledMapType = new google.maps.StyledMapType(parent.options.infectedMapStyles, { name: 'Infected' });
			Cvr.map.mapTypes.set('Infected', parent.options.styledMapType);
			
			parent.options.mapcenter = new google.maps.LatLng(Cvr.config.user.lat, Cvr.config.user.long);
			
			setTimeout(function() {
				parent.loadMarkers(Cvr.config.infected, true);
				parent.options.loadInterval = setInterval(function(){
		        	parent.updateMarkers(true);
		        }, 6000);
			}, 4000);
			
		    return this;
		},
		refreshClusterer: function() {
			var me = this;
			if (me.markerClusterer) {
				me.markerClusterer.clearMarkers();
			}
			me.markerClusterer = new MarkerClusterer(Cvr.map, me.markers, {
				maxZoom: 15,
				gridSize: 60,
				styles: me.options.clusterStyle
			});
		
		},
		loadMarkers: function(data, clear) {
			var me = this;
			if (clear) me.clearMarkers();
			for (var i = 0, I = data.length; i < I; ++i) {
				me.createMarker( 
					new google.maps.LatLng( parseFloat(data[i].lat), parseFloat(data[i].long) ), 
					data[i].pic, data[i].username, data[i].msg, data[i].id, false
				);
			}
			setTimeout(function() {
				me.refreshClusterer();
			}, 1000);
		},
		updateMarkers: function( int ) {
			var me = this;
			$.post(Cvr.config.apiUrl + 'tweets', { from: Cvr.config.infected.length, next: 600 },
				function(data) {
					if (!data) {
						if (int) { 
							clearInterval(me.options.loadInterval);
						}
					} else {
						Cvr.config.infected = Cvr.config.infected.concat( data );
						Cvr.Map.loadMarkers(data, false);
					}
			}, "json");
		},
		clearMarkers: function() {
			if (this.markers) {
				for (var i in this.markers) {
					this.markers[i].setMap(null);
					this.markers[i].get('cvr').setMap(null);
				}
				this.markers = [];
			}
		},
		createMarker: function( point, image, title, contentString, loc_id, pan ) {
			var userImage = new this.mark({
					position: point, cvr_id: loc_id, image: image, zIndex: 999
				}),
				marker = new google.maps.Marker({
					position: point, icon: Cvr.Map.options.markIcon(), shadow: Cvr.Map.options.markShdw(), shape: Cvr.Map.options.markShape,
					title: title, cvr_id: loc_id, cvr_content: contentString, cvr: userImage
				});
			userImage.bindTo('position', marker, 'position');
			/*
var boxText = document.createElement("div");
			boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: yellow; padding: 5px;";
			boxText.innerHTML = "City Hall, Sechelt<br>British Columbia<br>Canada";
			
			var myOptions = {
			content: boxText
			,disableAutoPan: false
			,maxWidth: 0
			,pixelOffset: new google.maps.Size(-140, 0)
			,zIndex: null
			,boxStyle: { 
			background: "url('tipbox.gif') no-repeat"
			,opacity: 0.75
			,width: "280px"
			}
			,closeBoxMargin: "10px 2px 2px 2px"
			,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
			,infoBoxClearance: new google.maps.Size(1, 1)
			,isHidden: false
			,pane: "floatPane"
			,enableEventPropagation: false
			};
			
			google.maps.event.addListener(marker, "click", function (e) {
			ib.open(theMap, this);
			});
			
			var ib = new InfoBox(myOptions);
			ib.open(theMap, marker);
*/
		
			google.maps.event.addListener(marker, 'click', function() {
				Cvr.Map
					.selectMarker( marker, contentString )
					.options.mapcenter = point;
				Cvr.map.panTo( point );
			});
			
			google.maps.event.addListener(marker, 'visible_changed', function() {
				userImage.visible = this.visible;
			});
			
			if (pan) { 
				Cvr.map.panTo( point ); 
			}
				
			this.markers.push(marker);
			return this;
		},
		selectMarker: function( marker, content ) {
			this.infoWindow.remove();
			$('#infected_' + marker.get('cvr_id') + ':not(.active)')
				.append(this.infoWindow.draw({
					id: marker.get('cvr_id'), 
					name: marker.get('title'), 
					msg: marker.get('cvr_content')
				})).addClass('active').find('.infoWindow').animate({opacity: 1, left: '58px'}, 500);
			return this;
		},
		showNearest: function( locs, limit ) {
			var LatLngList = [], LtLgLen, l = 1, 
				bounds = new google.maps.LatLngBounds();
				
			$.each(locs, function( key, value ) {
				if ( l > limit ) return false;
				LatLngList.push(new google.maps.LatLng( value.latitude, value.longitude ));
				l++;
			});
			
			for (var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++) {
				bounds.extend( LatLngList[i] );
			}
			
			Cvr.map.fitBounds( bounds );
			Cvr.Map.options.mapcenter = bounds.getCenter();
			Cvr.map.setCenter( Cvr.Map.options.mapcenter );
			//Cvr.map.setZoom(12);
		}
	};
	
	Cvr.react = function() {
		clearTimeout(Cvr.timer);
		Cvr.timer = setTimeout(Cvr.adapt, 16);
		return this;
	};
	
	Cvr.adapt = function() {
		Cvr.appWidth = document.documentElement.clientWidth || document.body.clientWidth || (window.innerWidth - 15) || 0;
		Cvr.appView = (Cvr.appWidth > 640 || $('.lt-ie9')[0] ) ? 'full' : 'mobile';
		
		$('html,body').removeClass('mobile full').addClass(Cvr.appView);
		
		$( (Cvr.appView === 'mobile' ? '.mobileOnly:not(.hide), .mobileShow:not(.hide)' : '.extra:not(.hide), .mobileHide') ).show();
		$('.hide' + ( Cvr.appView === 'full' ? ', .mobileOnly' : ', .extra:not(.mobileShow), .mobileHide') ).hide();
		
		if (Cvr.map) {
			if ( Cvr.appView === 'mobile' ) {
				Cvr.map.setCenter( Cvr.Map.options.mapcenter );
			}
			google.maps.event.trigger(Cvr.map, 'resize');
		}
		
		
		if ($('#tweet .ctrl a').hasClass('op') ) {
			if ( Cvr.appView === "full" &&  $('#tweet').css('top') == "auto" ) {
				Cvr.drawer.close( $('#tweet'), 'top' );
			}
		}
					
		return this;
	};

	if (window.addEventListener) {
		window.addEventListener('resize', Cvr.react, false);
	} else if (window.attachEvent) {
		window.attachEvent('onresize', Cvr.react);
	} else {
		window.onresize = Cvr.react();
	}
	
	Cvr.init = function() {
		setTimeout(function() {
			Cvr.react();
		}, 16);
		
		this.Twirus.init();
		
		Cvr.drawer
			.init()
			.open($('#connect'), 'top');
		$('#connect').show();
		
		$('a.warn').live('click', function(e) {
			e.preventDefault();
			if (Cvr.appStatus !== 'readonly') {
				$(this).addClass('active');
				$('header .infect, header .map').removeClass('active');
				Cvr.Twirus.initTw();
			}
		});
		
		$('#twtDrawer .leftArr, #twtDrawer .rightArr')
			.css({ opacity: 0.2, cursor: 'default' })
			.click(function (e) {
				e.preventDefault();
			});
		
		$('a.map').live('click', function(e) {
			e.preventDefault();
			$(this).addClass('active');
			$('header .infect, header .warn').removeClass('active');
			var dr = $('#connect');
			dr.find('.ctrl a').click();
			$('.drawer').removeClass('mobileShow').addClass('mobileHide'); 			
			$('#infectionMap').show(100).removeClass('mobileHide').addClass('mobileShow');
			setTimeout(function() {
				google.maps.event.trigger(Cvr.map, 'resize');
				Cvr.map.setCenter( Cvr.Map.options.mapcenter );
			}, 150);
			Cvr.react();
		});
		
		$('a.infect').live('click', function(e) {
			e.preventDefault();
			$(this).addClass('active');
			$('header .map, header .warn').removeClass('active');
			
			$('#tweet').addClass('mobileShow').removeClass('mobileHide');
			$('#infectionMap').removeClass('mobileShow').addClass('mobileHide');
			Cvr.react();
		});
		return this;
	};
	
	Cvr.drawer = {
		init: function() {
			$('.full .topDrawer').each(function(){
				Cvr.drawer.close( $(this), 'top' );
			});
			$('.full .topDrawer .ctrl a').live('click', function(e) {
				e.preventDefault();
				Cvr.drawer.ctrl( $(this), 'top', 'top' );
			});
			$('.full #tweet .ctrl a').live('click', function(e) {
				e.preventDefault();
				$('#twtDrawer .content .ctrl a').click();
			});
			$('.full #twtDrawer .ctrl a').live('click', function(e) {
				e.preventDefault();
				Cvr.drawer.ctrl( $(this), 'bottom', 'bottom' );
			});
			
			$('html:not(.fb) #twtDrawer .ctrl a').click();
			Cvr.react();
			return this;
		},
		open: function(e, pos, show) {
			if ( show ) {
				e.show();
			}
			if (pos === 'top') {
				e.stop()
					.css({bottom: 'auto'})
					.animate({top: '0px'}, 600)
					.parent().removeClass('closed').addClass('open');
			} else {
				e.stop()
					.css({top: 'auto'})
					.animate({bottom: '0px'})
					.parent().removeClass('closed').addClass('open');
			}
			Cvr.react();
			return this;
		},
		close: function(e, pos, hide) {
			if (pos === 'top') {
				e.stop()
					.css({bottom: 'auto'})
					.animate({top: '-' + (e.height() + $('header[role="banner"]').height() - 130) + 'px'}, 500, function() {
						$(this).parent().removeClass('open').addClass('closed');
					});
			} else {
				e.stop()
					.css({top: 'auto'})
					.animate({bottom: '-' + (e.height() - 28) + 'px'}, function() {
						$(this).parent().removeClass('open').addClass('closed');
					});
			}
			if ( hide ) {
				setTimeout(function() {e.hide();}, 850);
			}
			Cvr.react();
			return this;
		},
		ctrl: function(e, openPos, closePos) {
			if (e.hasClass('op')) {
				Cvr.drawer.open( e.removeClass('op').addClass('cl').parents('.drawer'), openPos );
			} else {
				Cvr.drawer.close( e.addClass('op').removeClass('cl').parents('.drawer'), closePos );
			}
		},
		hScroller: {
			options: {
				scroller: $('#twtDrawer .twList'),
				scr: false
			},
			init: function() {
			var me = this, list = this.options.list = $('#twtDrawer .twList .jta-tweet-list'),
				scroller = me.options.scroller,
				elements = this.options.elements = list.children('li').length,
				last_item = this.options.last_item = list.children('li:last'),
				list_width = this.options.list_width = (list.children("li:first").outerWidth() * elements),
				scrDir = true;
				
				me.nav('left', false)
					.nav('right', (elements < 6 ? false : true) );
				
				$('.full #twtDrawer .ctrl a').removeClass('cl').addClass('op').click();
				
				$('#twtDrawer .leftArr, #twtDrawer .rightArr').click(function (e) {
		            e.preventDefault();
		            if ($(this).hasClass('active')) {
		            	me.scroll(this.hash.slice(1));
		            }
		        });
		        
		        this.options.slideInterval = setInterval(function(){
		        	if ( !me.scroll(scrDir ? 'right' : 'left') ) {
		        		scrDir = !scrDir;
		        	}
		        }, 6000);
		        
                list.width((list_width < scroller.width() ? scroller.width() : list_width));
                
                return this;
			},
			nav: function (dir, active) {
				var el = $('a.' + dir + 'Arr');
		        if (active) {
		        	el.css({ opacity: 1, cursor: 'pointer' }).addClass('active');
		        } else { 
		        	el.css({ opacity: 0.2, cursor: 'default' }).removeClass('active');
		        }
		        return this;
		    },
			scroll: function(dir) {
	            var me = this,
	            	posX = me.options.list.position().left,
	            	diffX = me.options.list_width + (posX - me.options.scroller.width());

		        if (me.options.list_width > me.options.scroller.width() && !me.options.scr) {	
		        	me.options.scr = true;	            	
		            if (dir == "right") {
		                me.nav('left', true);
		                if (diffX >= this.options.scroller.width()) {
		                    me.options.list.stop().animate({ 
		                    	left: "-=" + me.options.scroller.width() }, 800, 
		                    	function () {
		                        	if (diffX == me.options.scroller.width()) {
		                        		me.nav('right', false);
		                        	}
		                        	me.options.scr = false;
		                    });
		                } else {
		                    me.nav('right', false); 
		                    me.options.list.stop().animate({ 
		                    	left: me.options.scroller.width() - me.options.list_width 
		                    }, 800, function(){
		                    	me.options.scr = false;
		                    });
		                    return false;
		                }
		            } else {
		                me.nav('right', true);
		                if (posX + me.options.scroller.width() <= 0) {
		                    me.options.list.stop().animate({ left: "+=" + me.options.scroller.width() }, 800, function () {
		                        if (posX + me.options.scroller.width() == 0) {
		                        	me.nav('left', false);
		                        }
		                        me.options.scr = false;
		                    });
		                } else {
		                    me.nav('left', false); 
		                    me.options.list.stop().animate({ left: 0 }, 800, function(){
		                    	me.options.scr = false;
		                    });
		                    return false;
		                }
		            }
		        }
		        return this;
			}
		}
	};

}());

$(function() {
	'use strict';
	Cvr.adapt().init();
});

MBP.scaleFix();
MBP.hideUrlBarOnLoad();

//==
String.prototype.reverse = function(){
	var splitext = this.split(""),
		revertext = splitext.reverse(),
		reversed = revertext.join("");
	return reversed;
};