# Adewale Adeyemi
Creatively minded and motivated individual that enjoys working with people to create successful solutions to problems.

I thrive on the challenge of crafting creative and intuitive applications and interfaces with a willingness to continually improve.

## Recent Projects
- **MixRadio (React SPA)**
    - [http://www.mixrad.io](http://www.mixrad.io)
- **PassivLiving MyFiT (Hybrid mobile app)**
- **PassivLiving Service Activation Wizard (Responsive)**
    - [https://www.passivliving.com/start](https://www.passivliving.com/start)
- **DigitasLBi site (Responsive)**
    - [http://www.digitaslbi.com/global/](http://www.digitaslbi.com/global/)
    - Worked on all Front-end development
- **Lloyds Banking Group (Partially responsive)**
    - [http://www.lloydsbankinggroup.com/](http://www.lloydsbankinggroup.com/)
    - Worked on general Front-end workflow & dev
    - JS modules:
        - [Meet our colleagues](http://www.lloydsbankinggroup.com/careers/life-with-us/meet-our-colleagues/)
        - [Regional House Prices](http://www.lloydsbankinggroup.com/media/economic-insight/regional-house-prices/)
        - [House Price tools](http://www.lloydsbankinggroup.com/media/economic-insight/house-price-tools/)
- **To Work Or Play (Responsive)**
    - [http://toworkorplay.com/](http://toworkorplay.com/)
    - Worked on all Front-end & WordPress dev
- **MTV: Student of the Year (Responsive)**
    - [http://studentoftheyear.mtv.co.uk/](http://studentoftheyear.mtv.co.uk/)
    - Worked on all Front-end and PHP/Laravel development
- **Comedy Central: Hand Picked (Responsive)**
    - [http://hand-picked.comedycentral.co.uk/](http://hand-picked.comedycentral.co.uk/)
    - Worked on all Front-end and PHP/Laravel development
- **Virgin Media: Presents (Adaptive)**
    - [http://virginmediapresents.com](http://virginmediapresents.com/)
    - Worked on general Front-end and developed Laravel middleware
- **MTV: Brand New Unsigned (Responsive)**
    - [http://brandnewunsigned.mtv.co.uk/](http://brandnewunsigned.mtv.co.uk/)
    - Worked on all PHP/Laravel development
    - Worked on designs and all Front-end dev for **2013 campaign site**
- **Capcom: Resident Evil 6 (Responsive)**
    - [http://www.residentevil.com/6/](http://www.residentevil.com/6/)
    - Worked on all Front-end & WordPress dev

## Development skills
- Architecting complex, modular and scalable JavaScript applications incl. single page and hybrid mobile/desktop w/ Electron, Cordova/PhoneGap & React Native
- Writing structured JS/ES6 (w/ Babel) using design patterns, OO, MV*, CommonJS/AMD and frameworks/libraries incl. React, Redux/Flux, Angular, Backbone, NodeJS, lodash, jQuery
- Front-end workflow / build tools – Git, NPM, Gulp/Grunt, Bower, Yeoman, Emmet, CI, T/BDD – unit & functional automated testing with Jasmine, Mocha, Sinon, Cucumber, Karma
- Hand-coding semantic and cross browser/platform HTML5 & CSS3, Responsive web
- CSS frameworks & preprocessors incl. SASS, LESS; Bourbon, Foundation, Bootstrap
- Designing and consuming RESTful APIs and popular web service APIs incl. Google & Facebook with AJAX
- Good understanding of security concerns – XSS, CSFR, same-origin policy, CORS, JSONP, SSL/HTTPS, OAUTH/Token-based authentication
- LAMP stack with frameworks incl. Laravel, Symfony 2, CodeIgniter
- Proficient in popular CMS and Open-Source web applications; mainly WordPress, and Drupal, some knowledge of Expression Engine, Magento and more
- Strong familiarity with IIS, SQL Server, C# .NET, Java, Scala/Play, Flash/AS3
- Developing further knowledge in other languages and frameworks, incl. NodeJS, Ruby/Rails, Django

## Design skills
- Strong understanding of principles for effective UI and UX responsive design
- Passion for creating engaging user experiences and strong visual design sensibility
- Developing prototypes and wireframes with intuitive workflows

## Recent experience
- JavaScript Developer at **Project Otto**, Remote | November 2015 - May 2016

- Senior Associate Developer at **Microsoft, MixRadio** – **Mosaic Island**, Bristol | October 2014 - November 2015

- Mobile Front-End / JavaScript Engineer - **PassivSystems**, Newbury | January 2014 - October 2014

- Senior Interface Developer - **DigitasLBi**, London | July 2013 - January 2014

- Web Developer - **Redwood Publishing**, London | June 2013

- Head of Web Development & Design - **MiyoMint**, London | Dec 2010 - June 2013

- Senior Front-End Web Developer - **RAPP UK**, London | May 2011 - Oct 2012
